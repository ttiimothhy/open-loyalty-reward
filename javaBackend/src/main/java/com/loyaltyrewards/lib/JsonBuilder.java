package com.loyaltyrewards.lib;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import javax.persistence.Tuple;
import javax.persistence.TupleElement;
import java.util.ArrayList;
import java.util.List;

@Component
public class JsonBuilder {
    public static List<ObjectNode> toJson(List<Tuple> results) {
        List<ObjectNode> json = new ArrayList<ObjectNode>();
        ObjectMapper mapper = new ObjectMapper();
        for (Tuple t : results)
        {
            List<TupleElement<?>> cols = t.getElements();
            ObjectNode one = mapper.createObjectNode();
            for (TupleElement col : cols)
            {
                Object value = t.get(col.getAlias());
                String value_toString = null;
                if (value != null) {
                    value_toString = value.toString();
                }
//                if(value instanceof Integer){
//
//                }else if
                one.put(col.getAlias(), value_toString);

            }
            json.add(one);
        }
        return json;
    }
    public static List<JSONObject> toJson2(List<Tuple> results) {
        List<JSONObject> json = new ArrayList<JSONObject>();
//        ObjectMapper mapper = new ObjectMapper();
        for (Tuple t : results)
        {
            List<TupleElement<?>> cols = t.getElements();
//            ObjectNode one = mapper.createObjectNode();
            JSONObject one = new JSONObject();
            for (TupleElement col : cols)
            {
                Object value = t.get(col.getAlias());
                String value_toString = null;
                if (value != null) {
                    value_toString = value.toString();
                }
                one.put(col.getAlias(), value_toString);
            }
            json.add(one);
        }
        return json;
    }
}
