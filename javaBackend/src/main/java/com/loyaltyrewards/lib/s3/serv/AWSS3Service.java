package com.loyaltyrewards.lib.s3.serv;

import org.springframework.web.multipart.MultipartFile;

public interface AWSS3Service {

	String uploadFile(String folder,MultipartFile multipartFile);
}
