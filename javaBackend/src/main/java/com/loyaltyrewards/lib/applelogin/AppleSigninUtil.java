package com.loyaltyrewards.lib.applelogin;

import com.loyaltyrewards.controllers.JPA.UserController;
import com.loyaltyrewards.lib.applelogin.vo.IdTokenPayload;
import com.loyaltyrewards.lib.applelogin.vo.TokenResponse;
import com.mashape.unirest.http.Unirest;
import io.jsonwebtoken.*;
import org.bouncycastle.asn1.pkcs.PrivateKeyInfo;
import org.bouncycastle.openssl.PEMParser;
import org.bouncycastle.openssl.jcajce.JcaPEMKeyConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;
import com.mashape.unirest.http.HttpResponse;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.security.PrivateKey;
import java.util.*;

/**
 * Main utility class for validating Apple ID Signins.
 *
 * @author mikereem
 *
 */
@Component
public class AppleSigninUtil {
	private static Logger log = LoggerFactory.getLogger(UserController.class);

	private static String APPLE_AUTH_URL = "https://appleid.apple.com/auth/token";

	private static String KEY_ID = "CC64HS8DTZ";
	private static String TEAM_ID = "79GACPXQYP";
	private static String CLIENT_ID = "org.reactjs.native.example.LoyaltyReward.reward.kenson";

	private static PrivateKey pKey;

	private static PrivateKey getPrivateKey() throws Exception {
		//read your key
		InputStream inputStream = new ClassPathResource("key/AuthKey_CC64HS8DTZ.p8").getInputStream();
		Reader reader = new InputStreamReader(inputStream);
		final PEMParser pemParser = new PEMParser(reader);
		final JcaPEMKeyConverter converter = new JcaPEMKeyConverter();
		final PrivateKeyInfo object = (PrivateKeyInfo) pemParser.readObject();
		final PrivateKey pKey = converter.getPrivateKey(object);
		return pKey;
	}

	private static String generateJWT() throws Exception {
		if (pKey == null) {
			pKey = getPrivateKey();
		}

		String token = Jwts.builder()
				.setHeaderParam(JwsHeader.KEY_ID, KEY_ID)
				.setIssuer(TEAM_ID)
				.setAudience("https://appleid.apple.com")
				.setSubject(CLIENT_ID)
				.setExpiration(new Date(System.currentTimeMillis() + (1000 * 60 * 5)))
				.setIssuedAt(new Date(System.currentTimeMillis()))
				.signWith(SignatureAlgorithm.ES256, pKey)
				.compact();

		return token;
	}

	/**
	 * Returns unique user id from apple
	 *
	 * @param authorizationCode
	 * @return
	 * @throws Exception
	 */
	public static String appleAuth(String authorizationCode) throws Exception {

		String token = generateJWT();
		log.info("token {}", token);

		HttpResponse<String> response = Unirest.post(APPLE_AUTH_URL)
				.header("Content-Type", "application/x-www-form-urlencoded")
				.field("client_id", CLIENT_ID)
				.field("client_secret", token)
				.field("grant_type", "authorization_code")
				.field("code", authorizationCode)
				.asString();
		log.info("raw {}", response.getBody());



		return response.getBody();
	}

}
