package com.loyaltyrewards.lib;

import org.springframework.stereotype.Component;

@Component
public class ConvertFormat {
    public static Integer ParseInt(String s) {
        int number = 0;
        try {
            return number = Integer.parseInt(s);
        } catch (NumberFormatException e) {
            return number = 0;
        }
    }
}
