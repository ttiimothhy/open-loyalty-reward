package com.loyaltyrewards.lib;


import com.loyaltyrewards.services.SQLService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class CSVReader {

    @Autowired
    SQLService sqlService;

    public void main(String[] args, String fileLocation, String tableName, String fileType) {

        BufferedReader br = null;
        try{
//            br = new BufferedReader(new FileReader(fileLocation+tableName+"."+fileType));
            br = new BufferedReader(
                    new InputStreamReader(new FileInputStream(fileLocation+tableName+"."+fileType), "UTF-8"));
            String contentLine = br.readLine();

            // get first line info
            String columnName = contentLine.replaceAll("[\uFEFF-\uFFFF]", "").replace("\"", "");

            //Read Next Line
            contentLine = br.readLine();

            // insert to SQL
            while (contentLine != null) {
                // String to List
                List<String> columnDataRaw = new ArrayList<String>(Arrays.asList(contentLine.split(",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)")));

                // List to String and Add quotation
                String columnData = columnDataRaw.stream()
                        .map(n -> String.valueOf(n))
                        .collect(Collectors.joining("','", "'", "'"));


                sqlService.insertQuery(tableName, columnName, columnData);
                //Read Next Line
                contentLine = br.readLine();
            }
        }
        catch (IOException ioe)
        {
            ioe.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally
        {
            try {
                if (br != null)
                    br.close();
            }
            catch (IOException ioe)
            {
                System.out.println("Error in closing the BufferedReader");
            }
        }
    }
}
