package com.loyaltyrewards.models.agent;

import com.loyaltyrewards.models.user.Customer;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;

@Entity(name="referral_commission")
public class ReferralCommission {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne
    @JoinColumn(name = "joined_customer_id")
    private Customer customer;

    @ManyToOne
    @JoinColumn(name="agent_id")
    private Agent agent;

    @Column(nullable = false)
    private Integer points;

    private String remark;

    @Column(name="create_time",columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP",updatable = false)
    @Generated(GenerationTime.INSERT)
    private Timestamp createTime;

    @Column(name="update_time",columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    @Generated(GenerationTime.INSERT)
    private Timestamp updateTime;

    @Column(columnDefinition = "boolean default false")
    private Boolean is_deleted;
}
