package com.loyaltyrewards.models.agent;

import com.loyaltyrewards.models.user.Customer;
import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity(name="agent_contact")
public class AgentContact {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name="agent_id")
    private Agent agent;

    @ManyToOne()
    @JoinColumn(name="customer_id")
    private Customer customer;

    @Column(name="create_time",columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP",updatable = false)
    @Generated(GenerationTime.INSERT)
    private Timestamp createTime;

    @Column(name="update_time",columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    @Generated(GenerationTime.INSERT)
    private Timestamp updateTime;

}
