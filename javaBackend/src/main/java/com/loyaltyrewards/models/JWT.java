package com.loyaltyrewards.models;

public class JWT {
    private Long id;
    private String username;
    private Long customer_id;
    private Long agent_id;
    private String admin;
    private String lastLogin;



    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Long getCustomer_id() {
        return customer_id;
    }

    public Long getAgent_id() {
        return agent_id;
    }

    public void setCustomer_id(Long customer_id) {
        this.customer_id = customer_id;
    }



    public void setAgent_id(Long agent_id) {
        this.agent_id = agent_id;
    }

    public String getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(String lastLogin) {
        this.lastLogin = lastLogin;
    }
}
