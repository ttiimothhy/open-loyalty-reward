package com.loyaltyrewards.models.form;

public class PickupLocationForm {
    private Long pickup_location_id;
    private String total_redeem_points;

    public Long getPickup_location_id() {
        return pickup_location_id;
    }

    public void setPickup_location_id(Long pickup_location_id) {
        this.pickup_location_id = pickup_location_id;
    }

    public String getTotal_redeem_points() {
        return total_redeem_points;
    }

    public void setTotal_redeem_points(String total_redeem_points) {
        this.total_redeem_points = total_redeem_points;
    }
}
