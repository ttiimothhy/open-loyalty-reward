package com.loyaltyrewards.models.form;

import java.util.Date;
import java.util.UUID;

public class SocialLoginForm {

        private String accessToken;
        private String email;

        public String getEmail() {
                return email;
        }

        public void setEmail(String email) {
                this.email = email;
        }

        public String getAccessToken() {
                return accessToken;
        }

        public void setAccessToken(String accessToken) {
                this.accessToken = accessToken;
        }
}
