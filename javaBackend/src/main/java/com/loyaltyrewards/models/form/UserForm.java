package com.loyaltyrewards.models.form;

import java.util.Date;
import java.util.UUID;

public class UserForm {
        private Long id;
        private String username;
        private String password;
        private String token;
        private String email;
        private Long mobile;
        private Long role_id;
        private Long register_status_id;
        private UUID uuid;
        private String otp;
        private String display_name;
        private Long gender_id;
        private String date_of_birth;
        private Date createdOn;
        private Date updatedOn;
        private Integer referral_mobile;
        private Integer referral_agent_id;
        private Boolean is_deleted;

        public Boolean getIs_deleted() {
                return is_deleted;
        }

        public void setIs_deleted(Boolean is_deleted) {
                this.is_deleted = is_deleted;
        }

        public Integer getReferral_agent_id() {
                return referral_agent_id;
        }

        public void setReferral_agent_id(Integer referral_agent_id) {
                this.referral_agent_id = referral_agent_id;
        }

        public Long getId() {
                return id;
        }

        public void setId(Long id) {
                this.id = id;
        }

        public String getUsername() {
                return username;
        }

        public void setUsername(String username) {
                this.username = username;
        }

        public String getPassword() {
                return password;
        }

        public void setPassword(String password) {
                this.password = password;
        }

        public String getToken() {
                return token;
        }

        public void setToken(String token) {
                this.token = token;
        }

        public String getEmail() {
                return email;
        }

        public void setEmail(String email) {
                this.email = email;
        }

        public Long getMobile() {
                return mobile;
        }

        public void setMobile(Long mobile) {
                this.mobile = mobile;
        }

        public Long getRole_id() {
                return role_id;
        }

        public void setRole_id(Long role_id) {
                this.role_id = role_id;
        }

        public Long getRegister_status_id() {
                return register_status_id;
        }

        public void setRegister_status_id(Long register_status_id) {
                this.register_status_id = register_status_id;
        }

        public UUID getUuid() {
                return uuid;
        }

        public void setUuid(UUID uuid) {
                this.uuid = uuid;
        }

        public String getOtp() {
                return otp;
        }

        public void setOtp(String otp) {
                this.otp = otp;
        }

        public String getDisplay_name() {
                return display_name;
        }

        public void setDisplay_name(String display_name) {
                this.display_name = display_name;
        }

        public Long getGender_id() {
                return gender_id;
        }

        public void setGender_id(Long gender_id) {
                this.gender_id = gender_id;
        }

        public String getDate_of_birth() {
                return date_of_birth;
        }

        public void setDate_of_birth(String date_of_birth) {
                this.date_of_birth = date_of_birth;
        }

        public Date getCreatedOn() {
                return createdOn;
        }

        public void setCreatedOn(Date createdOn) {
                this.createdOn = createdOn;
        }

        public Date getUpdatedOn() {
                return updatedOn;
        }

        public void setUpdatedOn(Date updatedOn) {
                this.updatedOn = updatedOn;
        }

        public Integer getReferral_mobile() {
                return referral_mobile;
        }

        public void setReferral_mobile(Integer referral_mobile) {
                this.referral_mobile = referral_mobile;
        }
}
