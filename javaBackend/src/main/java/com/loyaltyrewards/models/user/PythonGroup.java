package com.loyaltyrewards.models.user;

import com.loyaltyrewards.models.treatment.Appointment;

import javax.persistence.*;

@Entity
@Table(
        name="python_group",
        uniqueConstraints=
        @UniqueConstraint(columnNames={"customer_id"})
)
public class PythonGroup {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne
    @JoinColumn(name = "customer_id")
    private Customer customer;

    @Column(columnDefinition = "integer default 0")
    private Integer grouping;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Integer getGrouping() {
        return grouping;
    }

    public void setGrouping(Integer grouping) {
        this.grouping = grouping;
    }
}
