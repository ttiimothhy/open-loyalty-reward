package com.loyaltyrewards.models.treatment;

import com.loyaltyrewards.models.agent.AppointmentCommission;
import com.loyaltyrewards.models.point.EarnedPoint;
import com.loyaltyrewards.models.user.Customer;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Entity(name="appointments")
public class Appointment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name="customer_id")
    private Customer customer;

    @ManyToOne
    @JoinColumn(name="treatment_id")
    private Treatment treatment;

    @ManyToOne
    @JoinColumn(name="branch_id")
    private Branch branch;

    private String timeslot;

    @Column(columnDefinition = "boolean default false")
    private Boolean is_completed;

    @Column(columnDefinition = "boolean default false")
    private Boolean is_cancelled;

    @Column(columnDefinition = "boolean default false")
    private Boolean is_scored;

    @Column(name="create_time",columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP",updatable = false)
    @Generated(GenerationTime.INSERT)
    private Timestamp createTime;

    @Column(name="update_time",columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    @Generated(GenerationTime.INSERT)
    private Timestamp updateTime;

    @OneToOne(mappedBy = "appointment")
    private EarnedPoint earnedPoint;

    @OneToOne(mappedBy = "appointment")
    private AppointmentCommission appointmentCommission;

    @OneToOne(mappedBy = "appointment")
    private Score score;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Customer getCustomers() {
        return customer;
    }

    public void setCustomers(Customer customers) {
        this.customer = customers;
    }


    public String getTimeslot() {
        return timeslot;
    }

    public void setTimeslot(String timeslot) {
        this.timeslot = timeslot;
    }

    public Boolean getIs_completed() {
        return is_completed;
    }

    public void setIs_completed(Boolean is_completed) {
        this.is_completed = is_completed;
    }

    public Boolean getIs_cancelled() {
        return is_cancelled;
    }

    public void setIs_cancelled(Boolean is_cancelled) {
        this.is_cancelled = is_cancelled;
    }

   }
