package com.loyaltyrewards.models.treatment;

import com.loyaltyrewards.models.user.Customer;
import com.loyaltyrewards.models.user.User;
import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;

@Entity(name="scores")
public class Score {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Integer score;

    @Column(name="create_time",columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP",updatable = false)
    @Generated(GenerationTime.INSERT)
    private Timestamp createTime;

    @Column(name="update_time",columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    @Generated(GenerationTime.INSERT)
    private Timestamp updateTime;


    @OneToOne
    @JoinColumn(name="appointment_id", unique=true)
    private Appointment appointment;

    @ManyToOne
    @JoinColumn(name="treatments_id")
    private Treatment treatment;

}
