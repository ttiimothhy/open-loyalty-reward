package com.loyaltyrewards.models.treatment;

import com.loyaltyrewards.models.purchase.PurchasedItem;
import com.loyaltyrewards.models.user.Gender;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity(name="treatments")
public class Treatment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToMany(mappedBy = "treatment")
    private List<Appointment> appointment;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "treatment_branch_relation",
            joinColumns = @JoinColumn(name = "treatment_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "branch_id",
                    referencedColumnName = "id"))
    private List<Branch> branches;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "treatment_service_relation",
            joinColumns = @JoinColumn(name = "treatment_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "service_id",
                    referencedColumnName = "id"))
    private List<Service> services;

    private String imagePath;
    private String image;
    private String title;
    private String detail;

    @Column(columnDefinition = "integer default 0")
    private Integer default_score;

    @Column(columnDefinition = "integer default 0")
    private Integer purchase_benefit_condition;

    @Column(columnDefinition = "integer default 0")
    private Integer purchase_benefit_amount;



    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "gender_treatments_relation",
            joinColumns = @JoinColumn(name = "treatment_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "gender_id",
                    referencedColumnName = "id")
    )
    private List<Gender> gender;

    @Column(columnDefinition = "boolean default false")
    private Boolean is_hidden;

    @OneToMany(mappedBy = "treatment")
    private List<Advertisement> advertisement;

    @OneToMany(mappedBy = "treatment")
    private List<PurchasedItem> purchasedItem;

    @OneToMany(mappedBy = "treatment")
    private List<Score> score;

    @Column(name="create_time",columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP",updatable = false)
    @Generated(GenerationTime.INSERT)
    private Timestamp createTime;

    @Column(name="update_time",columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    @Generated(GenerationTime.INSERT)
    private Timestamp updateTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<Appointment> getAppointment() {
        return appointment;
    }

    public void setAppointment(List<Appointment> appointment) {
        this.appointment = appointment;
    }

    public List<Branch> getBranches() {
        return branches;
    }

    public void setBranches(List<Branch> branches) {
        this.branches = branches;
    }

    public List<Service> getServices() {
        return services;
    }

    public void setServices(List<Service> services) {
        this.services = services;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public Integer getDefault_score() {
        return default_score;
    }

    public void setDefault_score(Integer default_score) {
        this.default_score = default_score;
    }

    public List<Gender> getGender() {
        return gender;
    }

    public void setGender(List<Gender> gender) {
        this.gender = gender;
    }

    public Boolean getIs_hidden() {
        return is_hidden;
    }

    public void setIs_hidden(Boolean is_hidden) {
        this.is_hidden = is_hidden;
    }

    public List<Advertisement> getAdvertisement() {
        return advertisement;
    }

    public void setAdvertisement(List<Advertisement> advertisement) {
        this.advertisement = advertisement;
    }

    public List<Score> getScore() {
        return score;
    }

    public void setScore(List<Score> score) {
        this.score = score;
    }
}
