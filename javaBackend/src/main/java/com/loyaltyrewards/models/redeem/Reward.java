package com.loyaltyrewards.models.redeem;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;

@Entity(name="rewards")
public class Reward {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private String detail;

    @Column(nullable = false)
    private String image;

    @Column(nullable = false)
    private Integer redeem_points;

    @Column(nullable = false)
    private Integer remain_amount;

    @Column(columnDefinition = "boolean default false")
    private Boolean is_hidden;

    @OneToMany(mappedBy = "reward")
    private List<CartItem> cartItem;

    @OneToMany(mappedBy = "reward")
    private List<RedeemItem> redeemItem;

    @ManyToOne
    @JoinColumn(name="reward_type_id")
    private RewardType reward_type;

    @ManyToMany(mappedBy = "reward")
    private List<PickupLocation> pickup_location;

    @Column(name="create_time",columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP",updatable = false)
    @Generated(GenerationTime.INSERT)
    private Timestamp createTime;

    @Column(name="update_time",columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    @Generated(GenerationTime.INSERT)
    private Timestamp updateTime;
}
