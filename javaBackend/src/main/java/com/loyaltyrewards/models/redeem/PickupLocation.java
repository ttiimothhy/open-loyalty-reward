package com.loyaltyrewards.models.redeem;


import com.loyaltyrewards.models.treatment.Treatment;
import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;

@Entity(name="pickup_location")
public class PickupLocation {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private String address;

    private Integer telephone;

    private String office_hour;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "pickup_location_reward_relation",
            joinColumns = @JoinColumn(name = "pickup_location_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "rewards_id",
                    referencedColumnName = "id"))
    private List<Reward> reward;

    @OneToMany(mappedBy = "pickup_location")
    private List<RedeemOrder> redeem_order;

    @Column(name="create_time",columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP",updatable = false)
    @Generated(GenerationTime.INSERT)
    private Timestamp createTime;

    @Column(name="update_time",columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    @Generated(GenerationTime.INSERT)
    private Timestamp updateTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBranch_name() {
        return name;
    }

    public void setBranch_name(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getTelephone() {
        return telephone;
    }

    public void setTelephone(Integer telephone) {
        this.telephone = telephone;
    }

    public String getOffice_hour() {
        return office_hour;
    }

    public void setOffice_hour(String office_hour) {
        this.office_hour = office_hour;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Reward> getReward() {
        return reward;
    }

    public void setReward(List<Reward> reward) {
        this.reward = reward;
    }
}
