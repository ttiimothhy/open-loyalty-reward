package com.loyaltyrewards.controllers;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.loyaltyrewards.models.JWT;
import com.loyaltyrewards.repositories.user.GenderRepository;
import com.loyaltyrewards.repositories.user.UserRepository;
import com.loyaltyrewards.security.DecodeJWT;
import com.loyaltyrewards.services.SQLService;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@Component
@RequestMapping("/agents/contact")
public class AgentContactController {
    @Autowired
    SQLService sqlService;

    @Autowired
    DecodeJWT decodeJWT;

    @Autowired
    UserRepository userRepository;

    @Autowired
    GenderRepository genderRepository;

    @GetMapping("/coldCall/{phoneNumber}/{result_id}")
    public ResponseEntity<String> coldCallResult(@PathVariable Long phoneNumber,@PathVariable Long result_id,HttpServletRequest request) {
        JWT jwt = decodeJWT.decode(request);
        Long agent_id = jwt.getAgent_id();
        if (agent_id == 0) {
            JSONObject json=new JSONObject();
            json.put("success",false);
            json.put("message","Not an agent");
            return ResponseEntity.status(400).body(json.toString());
        }
        try{
            String tableName = "cold_call_history";
            String columnName = "contact_number,agent_id,result_id";
            String columnData = String.format("%1$s,%2$s,%3$s",
                    phoneNumber,agent_id,result_id);
            sqlService.insertQuery(tableName, columnName, columnData);

            JSONObject json=new JSONObject();
            json.put("success",true);
            return ResponseEntity.status(200).body(json.toString());
        }  catch (Exception e) {
            JSONObject json=new JSONObject();
            json.put("success",false);
            json.put("error",e);
            return ResponseEntity.status(400).body(json.toString());
        }
    }

    @GetMapping("/create/{phoneNumber}")
    public ResponseEntity<String> addContact(@PathVariable Long phoneNumber,HttpServletRequest request) {
        JWT jwt = decodeJWT.decode(request);
        Long agent_id = jwt.getAgent_id();
        if (agent_id == 0) {
            JSONObject json=new JSONObject();
            json.put("success",false);
            json.put("message","Not an agent");
            return ResponseEntity.status(400).body(json.toString());
        }

        try{
            // case 1
            String sql1 = String.format("SELECT users.id, mobile " +
                            "from agent_contact " +
                            "left join customers ON agent_contact.customer_id = customers.id " +
                            "left join users ON customers.user_id = users.id " +
                            "where ( mobile = %1$s and agent_id = %2$s)",
                    phoneNumber,agent_id);
            List<ObjectNode> result1 = this.sqlService.selectQuery(sql1);
            if (result1.size()>0) {
                JSONObject json=new JSONObject();
                json.put("success",false);
                json.put("message","duplicated");
                json.put("case",1);
                return ResponseEntity.status(400).body(json.toString());
            }

            // case 2
            String sql2 = String.format("SELECT users.id, mobile " +
                            "from agents " +
                            "left join users ON agents.user_id = users.id " +
                            "where ( mobile = %1$s)",
                    phoneNumber);
            List<ObjectNode> result2 = this.sqlService.selectQuery(sql2);
            if (result2.size()>0) {
                JSONObject json=new JSONObject();
                json.put("success",false);
                json.put("message","agent mobile");
                json.put("case",2);
                return ResponseEntity.status(400).body(json.toString());
            }
            // case 3
            String sql = String.format("SELECT customers.id " +
                            "from customers " +
                            "left join users ON user_id = users.id " +
                            "where ( mobile = %1$s )",
                    phoneNumber);
            List<ObjectNode> result = this.sqlService.selectQuery(sql);
            if (result.size()>0) {
                String jsonStr = result.get(0).toString();
                JSONObject resultJson = new JSONObject(jsonStr);
                String customers_id = resultJson.getString("id");

                String tableName = "agent_contact";
                String columnName = "agent_id,customer_id";
                String columnData = String.format("%1$s,%2$s",
                        agent_id,customers_id);
                sqlService.insertQuery(tableName, columnName, columnData);

                JSONObject json=new JSONObject();
                json.put("success",true);
                json.put("message","added");
                json.put("case",3);
                return ResponseEntity.status(200).body(json.toString());
            } else {
                JSONObject json=new JSONObject();
                json.put("success",true);
                json.put("message","new invite");
                json.put("case",4);
                return ResponseEntity.status(200).body(json.toString());
            }

        }  catch (Exception e) {
            JSONObject json=new JSONObject();
            json.put("success",false);
            json.put("error",e);
            return ResponseEntity.status(400).body(json.toString());
        }
    }

}
