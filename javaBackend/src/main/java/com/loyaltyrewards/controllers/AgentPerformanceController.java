package com.loyaltyrewards.controllers;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.loyaltyrewards.models.JWT;
import com.loyaltyrewards.repositories.user.GenderRepository;
import com.loyaltyrewards.repositories.user.UserRepository;
import com.loyaltyrewards.security.DecodeJWT;
import com.loyaltyrewards.services.SQLService;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@Component
@RequestMapping("/agents/performance")
public class AgentPerformanceController {
    @Autowired
    SQLService sqlService;

    @Autowired
    DecodeJWT decodeJWT;

    @Autowired
    UserRepository userRepository;

    @Autowired
    GenderRepository genderRepository;

    @GetMapping("/referral/{period}")
    public ResponseEntity<String> getReferralRank(@PathVariable String period,HttpServletRequest request) {
        JWT jwt = decodeJWT.decode(request);
        Long agent_id = jwt.getAgent_id();
        if (agent_id == 0) {
            JSONObject json=new JSONObject();
            json.put("success",false);
            json.put("message","Not an agent");
            return ResponseEntity.status(400).body(json.toString());
        }
        if (!(period.equals("day") || period.equals("month") || period.equals("year"))){
            JSONObject json=new JSONObject();
            json.put("success",false);
            json.put("message","Incorrect period");
            return ResponseEntity.status(400).body(json.toString());
        }
        try {
            String referral_sql = String.format("SELECT agents.id, users.display_name, " +
                    "count(referral_commission.id) as referral_count  " +
                    "from agents " +
                    "LEFT JOIN referral_commission on referral_commission.agent_id = agents.id " +
                    "LEFT JOIN users on agents.user_id = users.id " +
                    "where (referral_commission.create_time >= date_trunc('%1$s', CURRENT_DATE)) " +
                    "group by agents.id,display_name " +
                    "ORDER by count(referral_commission.id) desc " +
                    "LIMIT 5 OFFSET 0", period);
            List<JSONObject> referral_result = this.sqlService.selectQuery2(referral_sql);

            return ResponseEntity.status(200).body(referral_result.toString());

        } catch (Exception e) {
            JSONObject json = new JSONObject();
            json.put("success", false);
            json.put("error", e);
            return ResponseEntity.status(400).body(json.toString());
        }
    }

    @GetMapping("/appointment/{period}")
    public ResponseEntity<String> getAppointmentRank(@PathVariable String period,HttpServletRequest request) {
        JWT jwt = decodeJWT.decode(request);
        Long agent_id = jwt.getAgent_id();
        if (agent_id == 0) {
            JSONObject json=new JSONObject();
            json.put("success",false);
            json.put("message","Not an agent");
            return ResponseEntity.status(400).body(json.toString());
        }
        if (!(period.equals("day") || period.equals("month") || period.equals("year"))){
            JSONObject json=new JSONObject();
            json.put("success",false);
            json.put("message","Incorrect period");
            return ResponseEntity.status(400).body(json.toString());
        }
        try {

            String appointment_sql = String.format("SELECT agents.id, users.display_name, " +
                    "count(appointment_commission.id) as appointment_count  " +
                    "from agents " +
                    "LEFT JOIN appointment_commission on appointment_commission.agent_id = agents.id " +
                    "LEFT JOIN users on agents.user_id = users.id " +
                    "where (appointment_commission.create_time >= date_trunc('%1$s', CURRENT_DATE)) " +
                    "group by agents.id,display_name " +
                    "ORDER by count(appointment_commission.id) desc " +
                    "LIMIT 5 OFFSET 0", period);;
            List<ObjectNode> appointment_result = this.sqlService.selectQuery(appointment_sql);

            return ResponseEntity.status(200).body(appointment_result.toString());

        } catch (Exception e) {
            JSONObject json = new JSONObject();
            json.put("success", false);
            json.put("error", e);
            return ResponseEntity.status(400).body(json.toString());
        }
    }
}
