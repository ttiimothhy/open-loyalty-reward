package com.loyaltyrewards.controllers.JPA;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.loyaltyrewards.lib.applelogin.AppleSigninUtil;
import com.loyaltyrewards.lib.applelogin.vo.IdTokenPayload;
import com.loyaltyrewards.models.JWT;
import com.loyaltyrewards.models.JWTAppleLogin;
import com.loyaltyrewards.models.form.AppleLoginForm;
import com.loyaltyrewards.models.form.SocialLoginForm;
import com.loyaltyrewards.models.form.UserForm;
import com.loyaltyrewards.models.user.Customer;
import com.loyaltyrewards.repositories.user.CustomerRepository;
import com.loyaltyrewards.security.DecodeJWT;
import com.loyaltyrewards.security.EncodeJWT;
import com.loyaltyrewards.models.Email;
import com.loyaltyrewards.models.user.RegisterStatus;
import com.loyaltyrewards.models.user.Role;
import com.loyaltyrewards.models.user.User;
import com.loyaltyrewards.repositories.user.RegisterStatusRepository;
import com.loyaltyrewards.repositories.user.RoleRepository;
import com.loyaltyrewards.repositories.user.UserRepository;
import com.loyaltyrewards.services.Mail.MailService;
import com.loyaltyrewards.services.SQLService;
import io.jsonwebtoken.Claims;
import okhttp3.*;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;
import java.util.concurrent.TimeUnit;


@RestController
@RequestMapping("/users")
public class UserController {
    final private UserRepository userRepository;
    final private PasswordEncoder encoder;
    public final OkHttpClient client = new OkHttpClient.Builder()
            .readTimeout(1000, TimeUnit.MILLISECONDS)
            .writeTimeout(1000, TimeUnit.MILLISECONDS)
            .build();

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    CustomerRepository customerRepository;

    @Autowired
    RegisterStatusRepository registerStatusRepository;

    private static Logger logger = LoggerFactory.getLogger(UserController.class);

    @Autowired
    AppleSigninUtil appleSignin;

    @Autowired
    private MailService mailService;

    @Autowired
    private EncodeJWT getEncodeJWT;

    @Autowired
    private DecodeJWT decodeJWT;

    @Autowired
    SQLService sqlService;


    public UserController(UserRepository userRepository, PasswordEncoder encoder) {
        this.userRepository = userRepository;
        this.encoder = encoder;
    }

    @GetMapping("/currentUsersCheck")
    public ResponseEntity<String> currentUserCheck(HttpServletRequest request){
        try{
            JWT jwt = decodeJWT.decode(request);
            Long customer_id = jwt.getCustomer_id();
            String lastLogin = jwt.getLastLogin();
            System.out.println("lastLogin "+lastLogin);
            Long agent_id = jwt.getAgent_id();
            JSONObject json=new JSONObject();
            json.put("success",true);
            json.put("customer_id",customer_id);
            json.put("agent_id",agent_id);
            json.put("lastLogin",lastLogin);
            return ResponseEntity.status(200).body(json.toString());
        }catch(Exception e){
            JSONObject json = new JSONObject();
            json.put("success",false);
            json.put("message","Please login");
            return ResponseEntity.status(400).body(json.toString());
        }
    }

    @PostMapping("/register")
    public ResponseEntity<String> createUser(@RequestBody UserForm form, HttpServletRequest request) {
        Optional<User> checkUsername = this.userRepository.findByUsername(form.getUsername());
        Optional<User> checkEmail = this.userRepository.findByEmail(form.getEmail());
        Optional<User> checkMobile = this.userRepository.findByMobile(form.getMobile());

        System.out.println(form.getReferral_mobile());

        if (checkUsername.isPresent()){
            return ResponseEntity.status(400).body("username used");
        } else
        if (checkEmail.isPresent()){
            return ResponseEntity.status(400).body("email used");
        }
        if (checkMobile.isPresent()){
            return ResponseEntity.status(400).body("mobile used");
        }
        try {
            User user = new User();
            user.setUsername(form.getUsername());
            user.setPassword(encoder.encode(form.getPassword()));
            user.setDisplay_name(form.getDisplay_name());
            user.setEmail(form.getEmail());
            user.setMobile(form.getMobile());
            user.setReferral_mobile(form.getReferral_mobile());


            Optional<RegisterStatus> registerStatus = registerStatusRepository.findByStatus("OTPSent");
            user.setStatus(registerStatus.get());
            UUID uuid = UUID.randomUUID();
            user.setUuid(uuid);
            Random rnd = new Random();
            Integer number = rnd.nextInt(999999);
            String OTP = String.format("%06d", number);
            user.setOTP(OTP);
            userRepository.save(user);

            Email emailBody = new Email();
            emailBody.setTo(form.getEmail());
            emailBody.setFrom("loyaltyprogram2021@gmail.com");
            emailBody.setSubject("Loyalty Program Registration");
            StringBuffer requestURL = request.getRequestURL();
            Long user_id = userRepository.findByUsername(form.getUsername()).get().getId();
            String link = String.format("%1$sOTP/%2$s/%3$s ",
                    "https://loyaltyloyalty.charizard.site/", user_id , OTP);
            String emailContent = String.format("Your one time password is: %1$s\n" +
					"You may also click the link to complete the registration: %2$s\n",
			OTP, link);
            emailBody.setBody(emailContent);
            mailService.sendEmail(emailBody);

            JSONObject json=new JSONObject();
            json.put("success",true);
            json.put("user_id",user_id);
            return ResponseEntity.status(200).body(json.toString());
        } catch (Exception e) {
            JSONObject json=new JSONObject();
            System.out.println(e);
            json.put("success",false);
//            json.put("error",e);
            return ResponseEntity.status(500).body(json.toString());
        }
    }

    @GetMapping("/register/OPTResend/{id}")
    public ResponseEntity<String> resendOTP(@PathVariable Long id, HttpServletRequest request) {
        try {
            User user = userRepository.findById(id).get();

            Email emailBody = new Email();
            emailBody.setTo(user.getEmail());
            emailBody.setFrom("loyaltyprogram2021@gmail.com");
            emailBody.setSubject("Loyalty Program Registration");
            StringBuffer requestURL = request.getRequestURL();
            String link = String.format("%1$sOTP/%2$s/%3$s ",
                    requestURL, id , user.getOTP());
            String emailContent = String.format("Your one time password is: %1$s\n" +
                            "You may also click the link to complete the registration: %2$s\n",
                    user.getOTP(), link);
            emailBody.setBody(emailContent);
            mailService.sendEmail(emailBody);

            JSONObject json=new JSONObject();
            json.put("success",true);
            json.put("user_id",id);
            return ResponseEntity.status(200).body(json.toString());
        } catch (Exception e) {
            JSONObject json=new JSONObject();
            System.out.println(e);
            json.put("success",false);
//            json.put("error",e);
            return ResponseEntity.status(500).body(json.toString());
        }
    }

    @GetMapping("/registerOTP/{id}/{OTP}")
    public ResponseEntity<String> completeRegister(@PathVariable Long id, @PathVariable String OTP) {
        try {
            Optional<User> user = this.userRepository.findById(id);
            if (user.isPresent()) {
                if (user.get().getOTP().equals(OTP)){

                    String sql = String.format("SELECT agents.id,each_referral_commission " +
                                    "from agents " +
                                    "left join users ON users.id = user_id " +
                                    "where (mobile = %1$s and users.id = user_id )",
                            user.get().getReferral_mobile());
                    List<ObjectNode> agent_id_list = this.sqlService.selectQuery(sql);

                    Integer agent_id=0;
                    Integer agent_each_referral_commission=0;
                    if (agent_id_list.size() > 0) {
                        agent_id = Integer.parseInt(agent_id_list.get(0).get("id").toString().replaceAll("\"",""));
                        agent_each_referral_commission = Integer.parseInt(agent_id_list.get(0).get("each_referral_commission").toString().replaceAll("\"",""));
                    }
                    System.out.println("agent_id "+agent_id);


                    Optional<Role> role = roleRepository.findByRole("Customer");
                    user.get().setRole(role.get());
                    Optional<RegisterStatus> status = registerStatusRepository.findByStatus("completed");
                    user.get().setStatus(status.get());
                    user.get().setOTP("");
                    user.get().setReferral_agent_id(agent_id);
                    userRepository.save(user.get());

                    Customer customer = new Customer();
                    customer.setUsers(user.get());
                    customer.setNotification(true);
                    customerRepository.save(customer);

                    String sql2 = "SELECT customers.id " +
                            "from customers " +
                            "left join users ON users.id = user_id " +
                            "where users.id = " + id;
                    String jsonStr = this.sqlService.selectQuery(sql2).get(0).toString();
                    JSONObject resultJson = new JSONObject(jsonStr);
                    String customers_id = resultJson.getString("id");

                    if (agent_id_list.size() > 0) {
                        String tableName = "agent_contact";
                        String columnName = "agent_id,customer_id";
                        String columnData = String.format("%1$s,%2$s",
                                agent_id, customers_id);
                        sqlService.insertQuery(tableName, columnName, columnData);

                        String tableName2 = "referral_commission";
                        String columnName2 = "points,agent_id,joined_customer_id";
                        String columnData2 = String.format("%1$s,%2$s,%3$s",
                                agent_each_referral_commission, agent_id,customers_id);
                        sqlService.insertQuery(tableName2, columnName2, columnData2);
                    }


                    JSONObject json=new JSONObject();
                    json.put("success",true);
                    return ResponseEntity.status(200).body(json.toString());
                } else {
                    JSONObject json=new JSONObject();
                    json.put("success",false);
                    json.put("message","OTP incorrect");
                    return ResponseEntity.status(400).body(json.toString());
                }
            } else {
                JSONObject json=new JSONObject();
                json.put("success",false);
                json.put("message","User ID not found");
                return ResponseEntity.status(400).body(json.toString());
            }
        } catch (Exception e) {
            JSONObject json=new JSONObject();
            json.put("success",false);
            json.put("error",e);
            return ResponseEntity.status(500).body(json.toString());
        }
    }

    @PostMapping("/googleLogin/{role}")
    public ResponseEntity<String> googleLogin(@PathVariable String role, @RequestBody SocialLoginForm form) throws IOException {
        JSONObject json=new JSONObject();
        if (!role.equals("customers") && !role.equals("agents")){
            json.put("success",false);
            json.put("message","Role Error");
            return ResponseEntity.status(400).body(json.toString());
        }
        try {
            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder().url("https://www.googleapis.com/oauth2/v2/userinfo").addHeader("Authorization","Bearer "+form.getAccessToken()).build();
            Response httpResponse = client.newCall(request).execute();
            if (httpResponse.code() >=200 && httpResponse.code() <300 ) {
                String responseContent = httpResponse.body().string();
                System.out.println(responseContent);
                JSONObject responseContentJson = new JSONObject(responseContent);
                String email = responseContentJson.getString("email");
                Optional<User> checkEmail = this.userRepository.findByEmail(email);

                Long user_id = 0L;
                Long customer_id = 0L;
                Long agent_id = 0L;
                String username;
                if (!(checkEmail.isPresent())) {
                    Random rnd = new Random();
                    Integer number = rnd.nextInt(999999);
                    String usernameRnd = String.format("%06d", number);
                    username = System.currentTimeMillis()+usernameRnd;
                    String name = responseContentJson.getString("name");

                    String tableName = "users";
                    String columnName = "username,password,display_name,email";
                    String columnData = String.format("%1$s,'%2$s','%3$s','%4$s'",
                            username,"Password",name,email);
                    user_id = sqlService.insertQuery(tableName, columnName, columnData);

                    String tableName2 = "customers";
                    String columnName2 = "user_id,notification";
                    String columnData2 = String.format("%1$s,%2$s",
                            user_id,true);
                    customer_id = sqlService.insertQuery(tableName2, columnName2, columnData2);
                } else {
                    if (checkEmail.get().getCustomer() != null) {
                        customer_id = checkEmail.get().getCustomer().getId();
                    }
                    if (checkEmail.get().getAgent() != null) {
                        agent_id = checkEmail.get().getAgent().getId();
                    }
                    user_id=checkEmail.get().getId();
                    username=checkEmail.get().getUsername();
                }
                    String token = EncodeJWT.getJWTToken(user_id,username,customer_id,agent_id,role);
                    System.out.println(token);
                    json.put("success",true);
                    json.put("id",user_id);
                    json.put("data",token);
                    return ResponseEntity.status(200).body(json.toString());

            } else {
                System.err.println("Login Fail");
                json.put("success",false);
                json.put("message","Login fail");
                return ResponseEntity.status(400).body(json.toString());
            }
//        client.newCall(request).enqueue(new Callback() {
//            @Override
//            public void onFailure(Call call, IOException e) {
//            }
//            @Override
//            public void onResponse(Call call, Response response) throws IOException {
//                if(response.isSuccessful()){
//                }
//            }
//        });
        } catch (Exception e) {
            System.err.println("URL Fail");
            json.put("success",false);
            json.put("message","URL fail");
            return ResponseEntity.status(500).body(json.toString());
        }
    }

    @PostMapping("/facebookLogin/{role}")
    public ResponseEntity<String> facebookLogin(@PathVariable String role, @RequestBody SocialLoginForm form) throws IOException {
        JSONObject json=new JSONObject();
        System.out.println("facebook login");
        if (!role.equals("customers") && !role.equals("agents")){
            json.put("success",false);
            json.put("message","Role Error");
            return ResponseEntity.status(400).body(json.toString());
        }
        try {
            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder().url("https://graph.facebook.com/me?access_token="+form.getAccessToken()).addHeader("Authorization","Bearer "+form.getAccessToken()).build();
            Response httpResponse = client.newCall(request).execute();
            if (httpResponse.code() >=200 && httpResponse.code() <300 ) {
                String responseContent = httpResponse.body().string();
                System.out.println(responseContent);
                JSONObject responseContentJson = new JSONObject(responseContent);

                String email = form.getEmail();
                Optional<User> checkEmail = this.userRepository.findByEmail(email);

                Long user_id = 0L;
                Long customer_id = 0L;
                Long agent_id = 0L;
                String username;
                if (!(checkEmail.isPresent())) {
                    Random rnd = new Random();
                    Integer number = rnd.nextInt(999999);
                    String usernameRnd = String.format("%06d", number);
                    username = System.currentTimeMillis()+usernameRnd;
                    String name = responseContentJson.getString("name");

                    String tableName = "users";
                    String columnName = "username,password,display_name,email";
                    String columnData = String.format("%1$s,'%2$s','%3$s','%4$s'",
                            username,"Password",name,email);
                    user_id = sqlService.insertQuery(tableName, columnName, columnData);

                    String tableName2 = "customers";
                    String columnName2 = "user_id,notification";
                    String columnData2 = String.format("%1$s,%2$s",
                            user_id,true);
                    customer_id = sqlService.insertQuery(tableName2, columnName2, columnData2);
                } else {
                    if (checkEmail.get().getCustomer() != null) {
                        customer_id = checkEmail.get().getCustomer().getId();
                    }
                    if (checkEmail.get().getAgent() != null) {
                        agent_id = checkEmail.get().getAgent().getId();
                    }
                    user_id=checkEmail.get().getId();
                    username=checkEmail.get().getUsername();
                }
                String token = EncodeJWT.getJWTToken(user_id,username,customer_id,agent_id,role);
                System.out.println(token);
                json.put("success",true);
                json.put("id",user_id);
                json.put("data",token);
                return ResponseEntity.status(200).body(json.toString());

            } else {
                System.err.println("Login Fail");
                json.put("success",false);
                json.put("message","Login fail");
                return ResponseEntity.status(400).body(json.toString());
            }
//        client.newCall(request).enqueue(new Callback() {
//            @Override
//            public void onFailure(Call call, IOException e) {
//            }
//            @Override
//            public void onResponse(Call call, Response response) throws IOException {
//                if(response.isSuccessful()){
//                }
//            }
//        });
        } catch (Exception e) {
            System.err.println("URL Fail");
            json.put("success",false);
            json.put("message","URL fail");
            return ResponseEntity.status(500).body(json.toString());
        }
    }

    @PostMapping("/appleLogin/{role}")
    public ResponseEntity<String> appleLogin(@PathVariable String role, @RequestBody AppleLoginForm form) throws IOException {
        System.out.println("apple login");
            String clientId = form.getClientId();
            String identityToken = form.getIdentityToken();
            String authorizationCode = form.getAuthorizationCode();
        System.out.println("clientId "+clientId);
        System.out.println("identityToken "+identityToken);
        System.out.println("authorizationCode "+authorizationCode);

        JWTAppleLogin jwtClient = decodeJWT.decodeAppleLogin(identityToken);
        String iss = jwtClient.getIss();
        String aud = jwtClient.getAud();
        String sub = jwtClient.getSub();
        String nonce = jwtClient.getNonce();
        String email = jwtClient.getEmail();
        Boolean email_verified = jwtClient.getEmail_verified();

        String authorizationRes = null;
        try {
            try {
                authorizationRes = appleSignin.appleAuth(authorizationCode);
            } catch (Exception e) {
                JSONObject json=new JSONObject();
                json.put("success",false);
                json.put("message","auth error");
                return ResponseEntity.status(400).body(json.toString());
            }
            System.out.println(authorizationRes);

            JSONObject authorizationJSON = new JSONObject(authorizationRes);
            String jwtServerString = authorizationJSON.getString("id_token");
            JWTAppleLogin jwtServer = decodeJWT.decodeAppleLogin(jwtServerString);
            String issServer = jwtServer.getIss();
            String audServer = jwtServer.getAud();
            String subServer = jwtServer.getSub();
            String nonceServer = jwtServer.getNonce();
            String emailServer = jwtServer.getEmail();
            Boolean email_verifiedServer = jwtServer.getEmail_verified();

            if (!(iss.equals(issServer) &&
                    aud.equals(audServer)&&
                    subServer.equals(subServer) &&
                    nonce.equals(nonceServer)&&
                    email.equals(emailServer)&&
                    email_verified.equals(email_verifiedServer))) {
                JSONObject json=new JSONObject();
                json.put("success",false);
                json.put("message","token error");
                return ResponseEntity.status(400).body(json.toString());
            }

            Optional<User> checkEmail = this.userRepository.findByEmail(email);

            Long user_id = 0L;
            Long customer_id = 0L;
            Long agent_id = 0L;
            String username;
            if (!(checkEmail.isPresent())) {
                Random rnd = new Random();
                Integer number = rnd.nextInt(999999);
                String usernameRnd = String.format("%06d", number);
                username = System.currentTimeMillis()+usernameRnd;

                String tableName = "users";
                String columnName = "username,password,email";
                String columnData = String.format("%1$s,'%2$s','%3$s'",
                        username,"Password",email);
                user_id = sqlService.insertQuery(tableName, columnName, columnData);

                String tableName2 = "customers";
                String columnName2 = "user_id,notification";
                String columnData2 = String.format("%1$s,%2$s",
                        user_id,true);
                customer_id = sqlService.insertQuery(tableName2, columnName2, columnData2);
            } else {
                if (checkEmail.get().getCustomer() != null) {
                    customer_id = checkEmail.get().getCustomer().getId();
                }
                if (checkEmail.get().getAgent() != null) {
                    agent_id = checkEmail.get().getAgent().getId();
                }
                user_id=checkEmail.get().getId();
                username=checkEmail.get().getUsername();
            }
            String token = EncodeJWT.getJWTToken(user_id,username,customer_id,agent_id,role);
            System.out.println(token);
            JSONObject json=new JSONObject();
            json.put("success",true);
            json.put("id",user_id);
            json.put("data",token);
            return ResponseEntity.status(200).body(json.toString());

        } catch (Exception e){
            System.out.println("login Fail");
            JSONObject json=new JSONObject();
            json.put("success",false);
            json.put("message","login fail");
            return ResponseEntity.status(500).body(json.toString());
        }
    }


    @PostMapping("/login/{role}")
    public ResponseEntity<String> login(@PathVariable String role,@RequestBody User form) {
//        public ResponseEntity<String> login(@RequestParam("username") String username, @RequestParam("password") String password) {
        if (!role.equals("customers") && !role.equals("agents")){
            JSONObject json=new JSONObject();
            json.put("success",false);
            json.put("message","Role Error");
            return ResponseEntity.status(400).body(json.toString());
        }

        Optional<User> checkUser = this.userRepository.findByUsername(form.getUsername());
        if (checkUser.isPresent()){
            String encodedPassword = checkUser.get().getPassword();
            if (this.encoder.matches(form.getPassword(),encodedPassword)){
                Long customer_id = 0L;
                Long agent_id = 0L;
                if (checkUser.get().getCustomer() != null) {
                    customer_id = checkUser.get().getCustomer().getId();
                }
                if (checkUser.get().getAgent() != null) {
                    agent_id = checkUser.get().getAgent().getId();
                }

                String token = EncodeJWT.getJWTToken(checkUser.get().getId(),form.getUsername(),customer_id,agent_id,role);
                System.out.println(token);
                System.out.println(role);

                User user = checkUser.get();
//                userRepository.save(user);
                System.out.println("success");
                JSONObject json=new JSONObject();
                json.put("success",true);
                json.put("role",role);
                json.put("customer_id",customer_id);
                json.put("agent_id",agent_id);
                json.put("id",user.getId());
                json.put("data",token);
                System.out.println(json.toString());
                return ResponseEntity.status(200).body(json.toString());
            } else {
                JSONObject json=new JSONObject();
                json.put("success",false);
                json.put("message","Password not match");
                return ResponseEntity.status(400).body(json.toString());
            }
        } else {
            JSONObject json=new JSONObject();
            json.put("success",false);
            json.put("message","User not found");
            return ResponseEntity.status(400).body(json.toString());
        }
    }

    @PostMapping("/resetPassword")
    public ResponseEntity<String> resetPassword(@RequestBody User email, HttpServletRequest request) {
        Optional<User> checkEmail = this.userRepository.findByEmail(email.getEmail());

        if (!checkEmail.isPresent()){
            JSONObject json=new JSONObject();
            json.put("success",false);
            json.put("message","Email not found");
            return ResponseEntity.status(400).body(json.toString());
        }
        try {
            User user = checkEmail.get();
            Email emailBody = new Email();
            emailBody.setTo(user.getEmail());
            emailBody.setFrom("loyaltyprogram2021@gmail.com");
            emailBody.setSubject("Loyalty Program Reset Password");
            String serverDomain = request.getServerName();
            String link = String.format("https://%1$s/users/resetPassword/%2$s ",
                    serverDomain, user.getUuid());
            emailBody.setBody("Please click on this link to reset the password: " + link);

            mailService.sendEmail(emailBody);

            JSONObject json=new JSONObject();
            json.put("success",true);
            json.put("message","Email sent");
            return ResponseEntity.status(200).body(json.toString());

        } catch (Exception e) {
            JSONObject json=new JSONObject();
            json.put("success",false);
            json.put("message","Email error");
            return ResponseEntity.status(400).body(json.toString());
        } finally {
            System.out.println("resetPassword end");
        }
    }

    @GetMapping ("/resetPassword/{uuid}")
    public ResponseEntity<String> resetPasswordPage(@PathVariable UUID uuid) {
        Optional<User> requestUuid = this.userRepository.findByUuid(uuid);
        JSONObject json=new JSONObject();
        if (requestUuid.isPresent()){
            json.put("success",true);
            return ResponseEntity.status(200).body(json.toString());
        } else {
            json.put("success",false);
            json.put("message","UUID not found");
            return ResponseEntity.status(400).body(json.toString());
        }
    }

    @PostMapping ("/resetPassword/{uuid}")
    @ResponseBody
    public ResponseEntity<String> resetPassword(@PathVariable UUID uuid,@RequestBody String Password) {
        Optional<User> requestUuid = this.userRepository.findByUuid(uuid);
        if (!requestUuid.isPresent()){
            JSONObject json=new JSONObject();
            json.put("success",false);
            json.put("message","UUID not found");
            return ResponseEntity.status(400).body(json.toString());
        }
        try {
            requestUuid.get().setPassword(encoder.encode(Password));
            UUID newUuid = UUID.randomUUID();
            requestUuid.get().setUuid(newUuid);
            userRepository.save(requestUuid.get());

            JSONObject json=new JSONObject();
            json.put("success",true);
            json.put("message","Password reset");
            return ResponseEntity.status(200).body(json.toString());

        } catch (Exception e) {
            JSONObject json=new JSONObject();
            json.put("success",false);
            json.put("message","Password reset error");
            return ResponseEntity.status(500).body(json.toString());
        } finally {
            System.out.println("resetPassword end");
        }
    }
}
