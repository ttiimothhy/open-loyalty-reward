package com.loyaltyrewards.controllers.admin;

import com.loyaltyrewards.lib.s3.serv.AWSS3Service;
import com.loyaltyrewards.models.JWT;
import com.loyaltyrewards.models.form.RewardForm;
import com.loyaltyrewards.models.form.TreatmentForm;
import com.loyaltyrewards.models.redeem.PickupLocation;
import com.loyaltyrewards.models.redeem.RewardType;
import com.loyaltyrewards.repositories.redeem.CartItemRepository;
import com.loyaltyrewards.repositories.user.GenderRepository;
import com.loyaltyrewards.repositories.user.RoleRepository;
import com.loyaltyrewards.repositories.user.UserRepository;
import com.loyaltyrewards.security.DecodeJWT;
import com.loyaltyrewards.services.SQLService;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@CrossOrigin
@RestController
@Component
@RequestMapping("/admin/edit")
public class EditRewardsController {
    @Autowired
    SQLService sqlService;

    @Autowired
    DecodeJWT decodeJWT;

    @Autowired
    CartItemRepository cartItemRepository;

    @Autowired
    GenderRepository genderRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    PasswordEncoder encoder;

    @Autowired
    private AWSS3Service awsService;


    @GetMapping("/reward")
    public ResponseEntity<String> selectAllRewards(@RequestParam Integer display, @RequestParam Integer page, HttpServletRequest request) {
        JWT jwt = decodeJWT.decode(request);
        String checkAdmin = jwt.getLastLogin();

        if (!(checkAdmin.equals("admin"))) {
            JSONObject json=new JSONObject();
            json.put("success",false);
            json.put("message","Not a Admin");
            return ResponseEntity.status(400).body(json.toString());
        }
        try{
            String sql = String.format("SELECT id,detail,image,is_hidden,name,redeem_points,remain_amount " +
                    "from rewards " +
                    "order by id asc " +
                    "LIMIT %1$s OFFSET %2$s", display, display*page-display);
            List<JSONObject> result = this.sqlService.selectQuery2(sql);

            if (result.size() == 0) {
                JSONObject json = new JSONObject();
                json.put("success",false);
                json.put("message","empty page");
                return ResponseEntity.status(400).body(json.toString());
            }
            return ResponseEntity.status(200).body(result.toString());

        }  catch (Exception e) {
            JSONObject json=new JSONObject();
            json.put("success",false);
            json.put("error",e);
            return ResponseEntity.status(400).body(json.toString());
        }
    }

    @GetMapping("/reward/{id}")
    public ResponseEntity<String> selectReward(@PathVariable Integer id, HttpServletRequest request) {
        JWT jwt = decodeJWT.decode(request);
        String checkAdmin = jwt.getLastLogin();

        if (!(checkAdmin.equals("admin"))) {
            JSONObject json=new JSONObject();
            json.put("success",false);
            json.put("message","Not a Admin");
            return ResponseEntity.status(400).body(json.toString());
        }
        try{
            String sql1 = String.format("SELECT rewards.id,rewards.detail,rewards.image,rewards.is_hidden,rewards.name,redeem_points,remain_amount, reward_type.type " +
                    "from rewards " +
                    "left join reward_type on reward_type.id = reward_type_id " +
                    "where  rewards.id = %1$s", id);
            List<JSONObject> result1 = this.sqlService.selectQuery2(sql1);
            String sql2 = String.format("SELECT pickup_location_id,pickup_location.name " +
                    "from pickup_location_reward_relation " +
                    "left join pickup_location on pickup_location_reward_relation.pickup_location_id = pickup_location.id " +
                    "where  rewards_id = %1$s", id);
            List<JSONObject> result2 = this.sqlService.selectQuery2(sql2);

            if (result1.size() == 0 ||result2.size() == 0 ) {
                JSONObject json = new JSONObject();
                json.put("success",false);
                json.put("message","information error");
                return ResponseEntity.status(400).body(json.toString());
            }
            JSONObject json=new JSONObject();
            json.put("reward",result1.get(0));
            json.put("pickup_location",result2);
            return ResponseEntity.status(200).body(json.toString());

        }  catch (Exception e) {
            JSONObject json=new JSONObject();
            json.put("success",false);
            json.put("error",e);
            return ResponseEntity.status(400).body(json.toString());
        }
    }

    @PostMapping("/reward/create")
    public ResponseEntity<String> createTreatment(
            @ModelAttribute RewardForm form,
            HttpServletRequest request) {
        System.out.println("Hello Timothy");
        JWT jwt = decodeJWT.decode(request);
        String checkAdmin = jwt.getLastLogin();
        if (!(checkAdmin.equals("admin"))) {
            JSONObject json=new JSONObject();
            json.put("success",false);
            json.put("message","Not a Admin");
            return ResponseEntity.status(400).body(json.toString());
        }
        try{

            String name= form.getName();
            String detail= form.getDetail();
            Integer redeem_point= form.getRedeem_points();
            Integer remain_amount= form.getRemain_amount();
            Boolean is_hidden= form.getIs_hidden();
            Integer reward_type= form.getReward_type();
            List<Integer> pickup_location= form.getPickup_location();
            MultipartFile multipartFile = form.getFile();

            if (multipartFile == null) {
                JSONObject json=new JSONObject();
                json.put("success",false);
                json.put("message","image not found");
                return ResponseEntity.status(400).body(json.toString());
            }
            if (pickup_location == null) {
                JSONObject json=new JSONObject();
                json.put("success",false);
                json.put("message","pickup_location is empty");
                return ResponseEntity.status(400).body(json.toString());
            }

            String folder = "/rewards";
            String uniqueFileName = awsService.uploadFile(folder,multipartFile);
            System.out.println(uniqueFileName);
            String tableName = "rewards";
            String columnName = "detail,image,is_hidden,name,redeem_points,remain_amount,reward_type_id";
            String columnData = String.format("'%1$s','%2$s',%3$s,'%4$s',%5$s,%6$s,%7$s",
                    detail,uniqueFileName,is_hidden,name,redeem_point,remain_amount,reward_type);
            Long reward_id = sqlService.insertQuery(tableName, columnName, columnData);




            System.out.println(pickup_location.get(0));
            for ( int i = 0 ; i < pickup_location.size() ; i++) {
                String genderTableName = "pickup_location_reward_relation";
                String genderColumnName = "rewards_id,pickup_location_id";
                String genderColumnData = String.format("%1$s,%2$s", reward_id, pickup_location.get(i));
                sqlService.insertQuery(genderTableName, genderColumnName, genderColumnData);
            }


            JSONObject json=new JSONObject();
            json.put("success",true);
            return ResponseEntity.status(200).body(json.toString());
        }catch (Exception e) {
            JSONObject json=new JSONObject();
            json.put("success",false);
            json.put("message","server error");
            return ResponseEntity.status(500).body(json.toString());
        }
    }

    @PostMapping("/reward/update/{id}")
    public ResponseEntity<String> updateTreatment(
            @PathVariable Integer id,
            @ModelAttribute RewardForm form,
            HttpServletRequest request) {
        JWT jwt = decodeJWT.decode(request);
        String checkAdmin = jwt.getLastLogin();
        if (!(checkAdmin.equals("admin"))) {
            JSONObject json=new JSONObject();
            json.put("success",false);
            json.put("message","Not a Admin");
            return ResponseEntity.status(400).body(json.toString());
        }
        try{
            Boolean is_hidden = form.getIs_hidden();
            if (is_hidden == null) {
            } else {
                String sql= String.format("UPDATE rewards " +
                        "set is_hidden=%1$s " +
                        "where id=%2$s RETURNING id",is_hidden,id);
                sqlService.selectQuery(sql);
                JSONObject json=new JSONObject();
                json.put("success",true);
                return ResponseEntity.status(200).body(json.toString());
            }

            String name= form.getName();
            String detail= form.getDetail();
            Integer redeem_point= form.getRedeem_points();
            Integer remain_amount= form.getRemain_amount();
            Integer reward_type= form.getReward_type();
            List<Integer> pickup_location= form.getPickup_location();
            MultipartFile multipartFile = form.getFile();

            if (pickup_location == null) {
                JSONObject json=new JSONObject();
                json.put("success",false);
                json.put("message","pickup_location is empty");
                return ResponseEntity.status(400).body(json.toString());
            }


            if (multipartFile != null) {
                String folder = "/rewards";
                String uniqueFileName = awsService.uploadFile(folder,multipartFile);
                String tableName = "rewards";
                String columnName = "detail,image,name,redeem_points,remain_amount,reward_type_id";
                String columnData = String.format("'%1$s','%2$s','%3$s',%4$s,%5$s,%6$s",
                        detail,uniqueFileName,name,redeem_point,remain_amount,reward_type);
                sqlService.updateOneQueryKnowID(tableName, columnName, columnData,id);
            } else {
                String tableName = "rewards";
                String columnName = "detail,name,redeem_points,remain_amount,reward_type_id";
                String columnData = String.format("'%1$s','%2$s',%3$s,%4$s,%5$s",
                        detail,name,redeem_point,remain_amount,reward_type);
                sqlService.updateOneQueryKnowID(tableName, columnName, columnData,id);
            }



            System.out.println(pickup_location.get(0));
            for ( int i = 0 ; i < pickup_location.size() ; i++) {
                String genderTableName = "pickup_location_reward_relation";
                String genderColumnName = "rewards_id,pickup_location_id";
                String genderColumnData = String.format("%1$s,%2$s", id, pickup_location.get(i));
                sqlService.insertQuery(genderTableName, genderColumnName, genderColumnData);
            }

            JSONObject json=new JSONObject();
            json.put("success",true);
            return ResponseEntity.status(200).body(json.toString());
        }catch (Exception e) {
            JSONObject json=new JSONObject();
            json.put("success",false);
            json.put("message","server error");
            return ResponseEntity.status(500).body(json.toString());
        }
    }
}
