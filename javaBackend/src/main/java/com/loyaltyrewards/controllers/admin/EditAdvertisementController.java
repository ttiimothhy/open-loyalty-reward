package com.loyaltyrewards.controllers.admin;

import com.loyaltyrewards.lib.s3.serv.AWSS3Service;
import com.loyaltyrewards.models.JWT;
import com.loyaltyrewards.models.form.AdvertisementForm;
import com.loyaltyrewards.models.form.TreatmentForm;
import com.loyaltyrewards.repositories.redeem.CartItemRepository;
import com.loyaltyrewards.repositories.user.GenderRepository;
import com.loyaltyrewards.repositories.user.RoleRepository;
import com.loyaltyrewards.repositories.user.UserRepository;
import com.loyaltyrewards.security.DecodeJWT;
import com.loyaltyrewards.services.SQLService;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;


@RestController
@Component
@RequestMapping("/admin/edit")
public class EditAdvertisementController {
    @Autowired
    SQLService sqlService;

    @Autowired
    DecodeJWT decodeJWT;

    @Autowired
    CartItemRepository cartItemRepository;

    @Autowired
    GenderRepository genderRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    PasswordEncoder encoder;

    @Autowired
    private AWSS3Service awsService;


    @GetMapping("/advertisement")
    public ResponseEntity<String> selectAllAdvertisement(@RequestParam Integer display, @RequestParam Integer page, HttpServletRequest request) {
        JWT jwt = decodeJWT.decode(request);
        String checkAdmin = jwt.getLastLogin();

        if (!(checkAdmin.equals("admin"))) {
            JSONObject json=new JSONObject();
            json.put("success",false);
            json.put("message","Not a Admin");
            return ResponseEntity.status(400).body(json.toString());
        }
        try{
            String sql = String.format("SELECT image,is_hidden,title " +
                    "from advertisements " +
                    "order by id asc " +
                    "LIMIT %1$s OFFSET %2$s", display, display*page-display);
            List<JSONObject> result = this.sqlService.selectQuery2(sql);

            if (result.size() == 0) {
                JSONObject json = new JSONObject();
                json.put("success",false);
                json.put("message","empty page");
                return ResponseEntity.status(400).body(json.toString());
            }
            return ResponseEntity.status(200).body(result.toString());

        }  catch (Exception e) {
            JSONObject json=new JSONObject();
            json.put("success",false);
            json.put("error",e);
            return ResponseEntity.status(400).body(json.toString());
        }
    }

    @GetMapping("/advertisement/{id}")
    public ResponseEntity<String> selectAdvertisement(@PathVariable Integer id, HttpServletRequest request) {
        JWT jwt = decodeJWT.decode(request);
        String checkAdmin = jwt.getLastLogin();

        if (!(checkAdmin.equals("admin"))) {
            JSONObject json=new JSONObject();
            json.put("success",false);
            json.put("message","Not a Admin");
            return ResponseEntity.status(400).body(json.toString());
        }
        try{
            String sql = String.format("SELECT advertisements.image,advertisements.is_hidden,advertisements.title as advertisements_title,treatments.id,treatments.title as treatments_title,treatments.detail " +
                    "from advertisements " +
                    "left join treatments on treatments.id = treatment_id " +
                    "where  advertisements.id = %1$s", id);
            List<JSONObject> result = this.sqlService.selectQuery2(sql);
            System.out.println(result.toString());

            return ResponseEntity.status(200).body(result.get(0).toString());

        }  catch (Exception e) {
            JSONObject json=new JSONObject();
            json.put("success",false);
            json.put("error","server error");
            return ResponseEntity.status(400).body(json.toString());
        }
    }

    @PostMapping("/advertisement/create")
    public ResponseEntity<String> createTreatment(
            @ModelAttribute AdvertisementForm form,
            HttpServletRequest request) {
        JWT jwt = decodeJWT.decode(request);
        String checkAdmin = jwt.getLastLogin();
      System.out.println(checkAdmin);
        if (!(checkAdmin.equals("admin"))) {
            JSONObject json=new JSONObject();
            json.put("success",false);
            json.put("message","Not a Admin");
            return ResponseEntity.status(400).body(json.toString());
        }
        try{

            Boolean is_hidden = form.getIs_hidden();
            String title = form.getTitle();
            Integer treatment_id = form.getTreatment_id();
            MultipartFile multipartFile = form.getFile();

            if (multipartFile == null) {
                JSONObject json=new JSONObject();
                json.put("success",false);
                json.put("message","image not found");
                return ResponseEntity.status(400).body(json.toString());
            }

            String folder = "/advertisements";
            String uniqueFileName = awsService.uploadFile(folder,multipartFile);
            String tableName = "advertisements";
            String columnName = "is_hidden,title,treatment_id,image";
            String columnData = String.format("%1$s,'%2$s',%3$s,'%4$s'",
                    is_hidden,title,treatment_id,uniqueFileName);
            sqlService.insertQuery(tableName, columnName, columnData);

            JSONObject json=new JSONObject();
            json.put("success",true);
            return ResponseEntity.status(200).body(json.toString());
        }catch (Exception e) {
            JSONObject json=new JSONObject();
            json.put("success",false);
            json.put("message","server error");
            return ResponseEntity.status(500).body(json.toString());
        }
    }

    @PostMapping("/advertisement/update/{id}")
    public ResponseEntity<String> updateTreatment(
            @PathVariable Integer id,
            @ModelAttribute AdvertisementForm form,
            HttpServletRequest request) {
        JWT jwt = decodeJWT.decode(request);
        String checkAdmin = jwt.getLastLogin();
        if (!(checkAdmin.equals("admin"))) {
            JSONObject json=new JSONObject();
            json.put("success",false);
            json.put("message","Not a Admin");
            return ResponseEntity.status(400).body(json.toString());
        }
        try{
            Boolean is_hidden = form.getIs_hidden();
            if (is_hidden == null) {
            } else {
                String sql= String.format("UPDATE advertisements " +
                        "set is_hidden=%1$s " +
                        "where id=%2$s RETURNING id",is_hidden,id);
                sqlService.selectQuery(sql);
                JSONObject json=new JSONObject();
                json.put("success",true);
                return ResponseEntity.status(200).body(json.toString());
            }

            String title = form.getTitle();
            Integer treatment_id = form.getTreatment_id();
            MultipartFile multipartFile = form.getFile();

            if (multipartFile != null) {
                String folder = "/advertisements";
                String uniqueFileName = awsService.uploadFile(folder,multipartFile);
                String tableName = "advertisements";
                String columnName = "title,treatment_id,image";
                String columnData = String.format("'%1$s',%2$s,'%3$s'",
                       title,treatment_id,uniqueFileName);
                sqlService.updateOneQueryKnowID(tableName, columnName, columnData,id);
            } else {
                String tableName = "advertisements";
                String columnName = "title,treatment_id";
                String columnData = String.format("'%1$s',%2$s",
                        title,treatment_id);
                sqlService.updateOneQueryKnowID(tableName, columnName, columnData,id);
            }


            JSONObject json=new JSONObject();
            json.put("success",true);
            return ResponseEntity.status(200).body(json.toString());
        }catch (Exception e) {
            JSONObject json=new JSONObject();
            json.put("success",false);
            json.put("message","server error");
            return ResponseEntity.status(500).body(json.toString());
        }
    }
}
