package com.loyaltyrewards.controllers.admin;

import com.loyaltyrewards.lib.s3.serv.AWSS3Service;
import com.loyaltyrewards.models.JWT;
import com.loyaltyrewards.models.form.TreatmentForm;
import com.loyaltyrewards.repositories.redeem.CartItemRepository;
import com.loyaltyrewards.repositories.user.GenderRepository;
import com.loyaltyrewards.repositories.user.RoleRepository;
import com.loyaltyrewards.repositories.user.UserRepository;
import com.loyaltyrewards.security.DecodeJWT;
import com.loyaltyrewards.services.SQLService;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;


@RestController
@Component
@RequestMapping("/admin/edit")
public class EditTreatmentController {
    @Autowired
    SQLService sqlService;

    @Autowired
    DecodeJWT decodeJWT;

    @Autowired
    CartItemRepository cartItemRepository;

    @Autowired
    GenderRepository genderRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    PasswordEncoder encoder;

    @Autowired
    private AWSS3Service awsService;


    @GetMapping("/treatment")
    public ResponseEntity<String> selectAllTreatment(@RequestParam Integer display, @RequestParam Integer page, HttpServletRequest request) {
        JWT jwt = decodeJWT.decode(request);
        String checkAdmin = jwt.getLastLogin();

        if (!(checkAdmin.equals("admin"))) {
            JSONObject json=new JSONObject();
            json.put("success",false);
            json.put("message","Not a Admin");
            return ResponseEntity.status(400).body(json.toString());
        }
        try{
            String sql = String.format("SELECT treatments.id,treatments.title,purchase_benefit_condition,purchase_benefit_amount,treatments.is_hidden,treatments.image,treatments.detail,default_score " +
                    "from treatments " +
                    "order by id asc " +
                    "LIMIT %1$s OFFSET %2$s ", display, display*page-display);
            List<JSONObject> result = this.sqlService.selectQuery2(sql);

            if (result.size() == 0) {
                JSONObject json = new JSONObject();
                json.put("success",false);
                json.put("message","empty page");
                return ResponseEntity.status(400).body(json.toString());
            }
            return ResponseEntity.status(200).body(result.toString());

        }  catch (Exception e) {
            JSONObject json=new JSONObject();
            json.put("success",false);
            json.put("error",e);
            return ResponseEntity.status(400).body(json.toString());
        }
    }

    @GetMapping("/treatment/{id}")
    public ResponseEntity<String> selectTreatment(@PathVariable Integer id, HttpServletRequest request) {
        JWT jwt = decodeJWT.decode(request);
        String checkAdmin = jwt.getLastLogin();

        if (!(checkAdmin.equals("admin"))) {
            JSONObject json=new JSONObject();
            json.put("success",false);
            json.put("message","Not a Admin");
            return ResponseEntity.status(400).body(json.toString());
        }
        try{
            String sql1 = String.format("SELECT treatments.id,treatments.title,purchase_benefit_condition,purchase_benefit_amount,treatments.is_hidden,treatments.image,treatments.detail,default_score " +
                    "from treatments " +
                    "where  id = %1$s", id);
            List<JSONObject> result1 = this.sqlService.selectQuery2(sql1);
            String sql2 = String.format("SELECT gender_id,gender.gender " +
                    "from gender_treatments_relation " +
                    "left join gender on gender_treatments_relation.gender_id = gender.id " +
                    "where  treatment_id = %1$s", id);
            List<JSONObject> result2 = this.sqlService.selectQuery2(sql2);
            String sql3 = String.format("SELECT branch_id,branches.name " +
                    "from treatment_branch_relation " +
                    "left join branches on treatment_branch_relation.branch_id = branches.id " +
                    "where  treatment_id = %1$s", id);
            List<JSONObject> result3 = this.sqlService.selectQuery2(sql3);
            String sql4 = String.format("SELECT service_id,services.service " +
                    "from treatment_service_relation " +
                    "left join services on treatment_service_relation.service_id = services.id " +
                    "where  treatment_id = %1$s", id);
            List<JSONObject> result4 = this.sqlService.selectQuery2(sql4);

            if (result1.size() == 0 ||result2.size() == 0 ||result3.size() == 0 ||result4.size() == 0  ) {
                JSONObject json = new JSONObject();
                json.put("success",false);
                json.put("message","information error");
                return ResponseEntity.status(400).body(json.toString());
            }
            JSONObject json=new JSONObject();
            json.put("treatment",result1.get(0));
            json.put("gender",result2);
            json.put("branch",result3);
            json.put("service",result4);
            return ResponseEntity.status(200).body(json.toString());

        }  catch (Exception e) {
            JSONObject json=new JSONObject();
            json.put("success",false);
            json.put("error",e);
            return ResponseEntity.status(400).body(json.toString());
        }
    }

    @PostMapping("/treatment/create")
    public ResponseEntity<String> createTreatment(
            @ModelAttribute TreatmentForm form,
            HttpServletRequest request) {
        JWT jwt = decodeJWT.decode(request);
        String checkAdmin = jwt.getLastLogin();
      System.out.println(checkAdmin);
        if (!(checkAdmin.equals("admin"))) {
            JSONObject json=new JSONObject();
            json.put("success",false);
            json.put("message","Not a Admin");
            return ResponseEntity.status(400).body(json.toString());
        }
        try{

            String default_score = form.getDefault_score();
            String detail = form.getDetail();
            Boolean is_hidden = form.getIs_hidden();
            Integer purchase_benefit_condition = form.getPurchase_benefit_condition();
            Integer purchase_benefit_amount = form.getPurchase_benefit_amount();
            String title = form.getTitle();
            List<Integer> gender = form.getGender();
            List<Integer> branch = form.getBranch();
            List<Integer> service = form.getService();
            MultipartFile multipartFile = form.getFile();

            if (gender == null ||branch == null||service == null) {
                JSONObject json=new JSONObject();
                json.put("success",false);
                json.put("message","gender/branch/service is empty");
                return ResponseEntity.status(400).body(json.toString());
            }
            System.out.println("gender "+gender);
            System.out.println("branch "+branch);
            System.out.println("service "+service);
            if (multipartFile == null) {
                JSONObject json=new JSONObject();
                json.put("success",false);
                json.put("message","image not found");
                return ResponseEntity.status(400).body(json.toString());
            }
            String folder = "/treatments";
            String uniqueFileName = awsService.uploadFile(folder,multipartFile);
            String tableName = "treatments";
            String columnName = "default_score,detail,is_hidden,purchase_benefit_condition,purchase_benefit_amount,title,image";
            String columnData = String.format("%1$s,'%2$s',%3$s,%4$s,%5$s,'%6$s','%7$s'",
                    default_score,detail,is_hidden,purchase_benefit_condition,purchase_benefit_amount,title,uniqueFileName);
            Long treatment_id = sqlService.insertQuery(tableName, columnName, columnData);


            System.out.println(gender.get(0));
            for ( int i = 0 ; i < gender.size() ; i++) {
                String genderTableName = "gender_treatments_relation";
                String genderColumnName = "treatment_id,gender_id";
                String genderColumnData = String.format("%1$s,%2$s", treatment_id, gender.get(i));
                sqlService.insertQuery(genderTableName, genderColumnName, genderColumnData);
            }

            System.out.println(branch.get(0));
            for ( int i = 0 ; i < branch.size() ; i++) {
                String branchTableName = "treatment_branch_relation";
                String branchColumnName = "treatment_id,branch_id";
                String branchColumnData = String.format("%1$s,%2$s", treatment_id, branch.get(i));
                sqlService.insertQuery(branchTableName, branchColumnName, branchColumnData);
            }

            System.out.println(service.get(0));
            for ( int i = 0 ; i < branch.size() ; i++) {
                String serviceTableName = "treatment_service_relation";
                String serviceColumnName = "treatment_id,service_id";
                String serviceColumnData = String.format("%1$s,%2$s", treatment_id, service.get(i));
                sqlService.insertQuery(serviceTableName, serviceColumnName, serviceColumnData);
            }

            JSONObject json=new JSONObject();
            json.put("success",true);
            return ResponseEntity.status(200).body(json.toString());
        }catch (Exception e) {
            JSONObject json=new JSONObject();
            json.put("success",false);
            json.put("message","server error");
            return ResponseEntity.status(500).body(json.toString());
        }
    }

    @PostMapping("/treatment/update/{id}")
    public ResponseEntity<String> updateTreatment(
            @PathVariable Integer id,
            @ModelAttribute TreatmentForm form,
            HttpServletRequest request) {
        JWT jwt = decodeJWT.decode(request);
        String checkAdmin = jwt.getLastLogin();
        if (!(checkAdmin.equals("admin"))) {
            JSONObject json=new JSONObject();
            json.put("success",false);
            json.put("message","Not a Admin");
            return ResponseEntity.status(400).body(json.toString());
        }
        try{
            Boolean is_hidden = form.getIs_hidden();
            if (is_hidden == null) {
            } else {
                String sql= String.format("UPDATE treatments " +
                        "set is_hidden=%1$s " +
                        "where id=%2$s RETURNING id",is_hidden,id);
                sqlService.selectQuery(sql);
                JSONObject json=new JSONObject();
                json.put("success",true);
                return ResponseEntity.status(200).body(json.toString());
            }


            String default_score = form.getDefault_score();
            String detail = form.getDetail();
            Integer purchase_benefit_condition = form.getPurchase_benefit_condition();
            Integer purchase_benefit_amount = form.getPurchase_benefit_amount();
            String title = form.getTitle();
            List<Integer> gender = form.getGender();
            List<Integer> branch = form.getBranch();
            List<Integer> service = form.getService();

            if (gender == null ||branch == null||service == null) {
                JSONObject json=new JSONObject();
                json.put("success",false);
                json.put("message","gender/branch/service is empty");
                return ResponseEntity.status(400).body(json.toString());
            }

            MultipartFile multipartFile = form.getFile();
            if (multipartFile != null) {

                String folder = "/treatments";
                String uniqueFileName = awsService.uploadFile(folder,multipartFile);
                String tableName = "treatments";
                String columnName = "default_score,detail,purchase_benefit_condition,purchase_benefit_amount,title,image";
                String columnData = String.format("%1$s,'%2$s',%3$s,%4$s,'%5$s','%6$s'",
                        default_score,detail,is_hidden,purchase_benefit_condition,purchase_benefit_amount,title,uniqueFileName);
                sqlService.updateOneQueryKnowID(tableName, columnName, columnData,id);
            } else {
                String tableName = "treatments";
                String columnName = "default_score,detail,purchase_benefit_condition,purchase_benefit_amount,title";
                String columnData = String.format("%1$s,'%2$s',%3$s,%4$s,'%5$s'",
                        default_score,detail,purchase_benefit_condition,purchase_benefit_amount,title);
                sqlService.updateOneQueryKnowID(tableName, columnName, columnData,id);
            }

            String deleteTableName1 = "gender_treatments_relation";
            String condition1 = "treatment_id = " + id;
            sqlService.deleteQuery(deleteTableName1,condition1);

            System.out.println(gender.get(0));
            for ( int i = 0 ; i < gender.size() ; i++) {
                String genderTableName = "gender_treatments_relation";
                String genderColumnName = "treatment_id,gender_id";
                String genderColumnData = String.format("%1$s,%2$s", id, gender.get(i));
                sqlService.insertQuery(genderTableName, genderColumnName, genderColumnData);
            }

            String deleteTableName2 = "treatment_branch_relation";
            String condition2 = "treatment_id = " + id;
            sqlService.deleteQuery(deleteTableName2,condition2);

            System.out.println(branch.get(0));
            for ( int i = 0 ; i < branch.size() ; i++) {
                String branchTableName = "treatment_branch_relation";
                String branchColumnName = "treatment_id,branch_id";
                String branchColumnData = String.format("%1$s,%2$s", id, branch.get(i));
                sqlService.insertQuery(branchTableName, branchColumnName, branchColumnData);
            }

            String deleteTableName3 = "treatment_service_relation";
            String condition3 = "treatment_id = " + id;
            sqlService.deleteQuery(deleteTableName3,condition3);

            System.out.println(service.get(0));
            for ( int i = 0 ; i < branch.size() ; i++) {
                String serviceTableName = "treatment_service_relation";
                String serviceColumnName = "treatment_id,service_id";
                String serviceColumnData = String.format("%1$s,%2$s", id, service.get(i));
                sqlService.insertQuery(serviceTableName, serviceColumnName, serviceColumnData);
            }

            JSONObject json=new JSONObject();
            json.put("success",true);
            return ResponseEntity.status(200).body(json.toString());
        }catch (Exception e) {
            JSONObject json=new JSONObject();
            json.put("success",false);
            json.put("message","server error");
            return ResponseEntity.status(500).body(json.toString());
        }
    }
}
