package com.loyaltyrewards.controllers.admin;

import com.loyaltyrewards.lib.s3.serv.AWSS3Service;
import com.loyaltyrewards.models.JWT;
import com.loyaltyrewards.models.form.CommissionForm;
import com.loyaltyrewards.models.form.EarnedPointForm;
import com.loyaltyrewards.models.form.TreatmentForm;
import com.loyaltyrewards.repositories.redeem.CartItemRepository;
import com.loyaltyrewards.repositories.user.GenderRepository;
import com.loyaltyrewards.repositories.user.RoleRepository;
import com.loyaltyrewards.repositories.user.UserRepository;
import com.loyaltyrewards.security.DecodeJWT;
import com.loyaltyrewards.services.SQLService;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;


@RestController
@Component
@RequestMapping("/admin/edit")
public class EditPointController {
    @Autowired
    SQLService sqlService;

    @Autowired
    DecodeJWT decodeJWT;

    @Autowired
    CartItemRepository cartItemRepository;

    @Autowired
    GenderRepository genderRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    PasswordEncoder encoder;

    @Autowired
    private AWSS3Service awsService;


    @GetMapping("/point/{id}")
    public ResponseEntity<String> getAllPointHistory(@PathVariable Integer id, HttpServletRequest request) {
        JWT jwt = decodeJWT.decode(request);
        String checkAdmin = jwt.getLastLogin();

        if (!(checkAdmin.equals("admin"))) {
            JSONObject json=new JSONObject();
            json.put("success",false);
            json.put("message","Not a Admin");
            return ResponseEntity.status(400).body(json.toString());
        }

        try{
            String sql1 = String.format("SELECT earned_point.id,expired_date,points,remark,treatments.title as treatments_title ,earned_point.is_deleted " +
                    "from earned_point " +
                    "left join appointments on earned_point.appointment_id = appointments.id " +
                    "left join treatments on appointments.treatment_id = treatments.id " +
                    "left join customers on appointments.customer_id = customers.id " +
                    "left join users on customers.user_id = users.id " +
                    "where  users.id = %1$s", id);
            List<JSONObject> result1 = this.sqlService.selectQuery2(sql1);
            String sql2 = String.format("SELECT appointment_commission.id,points,remark,treatments.title as treatments_title,appointment_commission.is_deleted  " +
                    "from appointment_commission " +
                    "left join appointments on appointment_commission.appointment_id = appointments.id " +
                    "left join treatments on appointments.treatment_id = treatments.id " +
                    "left join agents on appointment_commission.agent_id = agents.id " +
                    "left join users on agents.user_id = users.id " +
                    "where  users.id  = %1$s", id);
            List<JSONObject> result2 = this.sqlService.selectQuery2(sql2);
            String sql3 = String.format("SELECT referral_commission.id,points,remark,referral_commission.is_deleted  " +
                    "from referral_commission " +
                    "left join customers on referral_commission.joined_customer_id = customers.id " +
                    "left join agents on referral_commission.agent_id = agents.id " +
                    "left join users on agents.user_id = agents.id " +
                    "where users.id  = %1$s", id);
            List<JSONObject> result3 = this.sqlService.selectQuery2(sql3);

            if (result1.size() == 0 ||result2.size() == 0 ) {
                JSONObject json = new JSONObject();
                json.put("success",false);
                json.put("message","information error");
                return ResponseEntity.status(400).body(json.toString());
            }
            JSONObject json=new JSONObject();
            json.put("earned_point_user",result1);
            json.put("appointment_commission_agent",result2);
            json.put("referral_commission_agent",result3);
            return ResponseEntity.status(200).body(json.toString());

        }  catch (Exception e) {
            e.printStackTrace();
            JSONObject json=new JSONObject();
            json.put("success",false);
            json.put("message","server error");
            return ResponseEntity.status(400).body(json.toString());
        }
    }

    @PutMapping("/point/update/earned_point/{id}")
    public ResponseEntity<String> updateEarnedPoint(@PathVariable Integer id, @RequestBody EarnedPointForm form, HttpServletRequest request) {
        JWT jwt = decodeJWT.decode(request);
        String checkAdmin = jwt.getLastLogin();
        if (!(checkAdmin.equals("admin"))) {
            JSONObject json=new JSONObject();
            json.put("success",false);
            json.put("message","Not a Admin");
            return ResponseEntity.status(400).body(json.toString());
        }
        try{

            String expired_date = form.getExpired_date();
            String remark = form.getRemark();
            Integer points = form.getPoints();
            Boolean is_deleted = form.getIs_deleted();

            String tableName = "earned_point";
            String columnName = "expired_date,remark,points,is_deleted";
            String columnData = String.format("'%1$s','%2$s',%3$s,%4$s",
                    expired_date,remark,points,is_deleted);
            sqlService.updateOneQueryKnowID(tableName, columnName, columnData,id);

            JSONObject json=new JSONObject();
            json.put("success",true);
            return ResponseEntity.status(200).body(json.toString());
        }catch (Exception e) {
            e.printStackTrace();
            JSONObject json=new JSONObject();
            json.put("success",false);
            json.put("message","server error");
            return ResponseEntity.status(500).body(json.toString());
        }
    }
    @PutMapping("/point/update/appointment_commission/{id}")
    public ResponseEntity<String> updateAppointmentCommission(@PathVariable Integer id, @RequestBody CommissionForm form, HttpServletRequest request) {
        JWT jwt = decodeJWT.decode(request);
        String checkAdmin = jwt.getLastLogin();
        if (!(checkAdmin.equals("admin"))) {
            JSONObject json=new JSONObject();
            json.put("success",false);
            json.put("message","Not a Admin");
            return ResponseEntity.status(400).body(json.toString());
        }
        try{
            String remark = form.getRemark();
            Integer points = form.getPoints();
            Boolean is_deleted = form.getIs_deleted();

            String tableName = "appointment_commission";
            String columnName = "remark,points,is_deleted";
            String columnData = String.format("'%1$s',%2$s,%3$s",
                    remark,points,is_deleted);
            sqlService.updateOneQueryKnowID(tableName, columnName, columnData,id);

            JSONObject json=new JSONObject();
            json.put("success",true);
            return ResponseEntity.status(200).body(json.toString());
        }catch (Exception e) {
            e.printStackTrace();
            JSONObject json=new JSONObject();
            json.put("success",false);
            json.put("message","server error");
            return ResponseEntity.status(500).body(json.toString());
        }
    }

    @PutMapping("/point/update/referral_commission/{id}")
    public ResponseEntity<String> updateReferralCommission(@PathVariable Integer id, @RequestBody CommissionForm form, HttpServletRequest request) {
        JWT jwt = decodeJWT.decode(request);
        String checkAdmin = jwt.getLastLogin();
        if (!(checkAdmin.equals("admin"))) {
            JSONObject json=new JSONObject();
            json.put("success",false);
            json.put("message","Not a Admin");
            return ResponseEntity.status(400).body(json.toString());
        }
        try{
            String remark = form.getRemark();
            Integer points = form.getPoints();
            Boolean is_deleted = form.getIs_deleted();

            String tableName = "referral_commission";
            String columnName = "remark,points,is_deleted";
            String columnData = String.format("'%1$s',%2$s,%3$s",
                    remark,points,is_deleted);
            sqlService.updateOneQueryKnowID(tableName, columnName, columnData,id);

            JSONObject json=new JSONObject();
            json.put("success",true);
            return ResponseEntity.status(200).body(json.toString());
        }catch (Exception e) {
            e.printStackTrace();
            JSONObject json=new JSONObject();
            json.put("success",false);
            json.put("message","server error");
            return ResponseEntity.status(500).body(json.toString());
        }
    }


    @PostMapping("/point/create/earned_point/{id}")
    public ResponseEntity<String> createEarnedPoint(@PathVariable Integer id, @RequestBody EarnedPointForm form, HttpServletRequest request) {
        JWT jwt = decodeJWT.decode(request);
        String checkAdmin = jwt.getLastLogin();
        if (!(checkAdmin.equals("admin"))) {
            JSONObject json=new JSONObject();
            json.put("success",false);
            json.put("message","Not a Admin");
            return ResponseEntity.status(400).body(json.toString());
        }
        try{

            String expired_date = form.getExpired_date();
            String remark = form.getRemark();
            Integer points = form.getPoints();
            Boolean is_deleted = form.getIs_deleted();

            String tableName = "earned_point";
            String columnName = "expired_date,remark,points,is_deleted,customer_id";
            String columnData = String.format("'%1$s','%2$s',%3$s,%4$s,%5$s",
                    expired_date,remark,points,is_deleted,id);
            sqlService.insertQuery(tableName, columnName, columnData);

            JSONObject json=new JSONObject();
            json.put("success",true);
            return ResponseEntity.status(200).body(json.toString());
        }catch (Exception e) {
            e.printStackTrace();
            JSONObject json=new JSONObject();
            json.put("success",false);
            json.put("message","server error");
            return ResponseEntity.status(500).body(json.toString());
        }
    }

    @PostMapping("/point/create/appointment_commission/{id}")
    public ResponseEntity<String> createAppointmentCommission(@PathVariable Integer id, @RequestBody CommissionForm form, HttpServletRequest request) {
        JWT jwt = decodeJWT.decode(request);
        String checkAdmin = jwt.getLastLogin();
        if (!(checkAdmin.equals("admin"))) {
            JSONObject json=new JSONObject();
            json.put("success",false);
            json.put("message","Not a Admin");
            return ResponseEntity.status(400).body(json.toString());
        }
        try{
            String remark = form.getRemark();
            Integer points = form.getPoints();
            Boolean is_deleted = form.getIs_deleted();

            String tableName = "appointment_commission";
            String columnName = "remark,points,is_deleted,agent_id";
            String columnData = String.format("'%1$s',%2$s,%3$s,%4$s",
                    remark,points,is_deleted,id);
            sqlService.insertQuery(tableName, columnName, columnData);

            JSONObject json=new JSONObject();
            json.put("success",true);
            return ResponseEntity.status(200).body(json.toString());
        }catch (Exception e) {
            e.printStackTrace();
            JSONObject json=new JSONObject();
            json.put("success",false);
            json.put("message","server error");
            return ResponseEntity.status(500).body(json.toString());
        }
    }

    @PostMapping("/point/create/referral_commission/{id}")
    public ResponseEntity<String> createReferralCommission(@PathVariable Integer id, @RequestBody CommissionForm form, HttpServletRequest request) {
        JWT jwt = decodeJWT.decode(request);
        String checkAdmin = jwt.getLastLogin();
        if (!(checkAdmin.equals("admin"))) {
            JSONObject json=new JSONObject();
            json.put("success",false);
            json.put("message","Not a Admin");
            return ResponseEntity.status(400).body(json.toString());
        }
        try{
            String remark = form.getRemark();
            Integer points = form.getPoints();
            Boolean is_deleted = form.getIs_deleted();

            String tableName = "referral_commission";
            String columnName = "remark,points,is_deleted,agent_id";
            String columnData = String.format("'%1$s',%2$s,%3$s,%4$s",
                    remark,points,is_deleted,id);
            sqlService.insertQuery(tableName, columnName, columnData);

            JSONObject json=new JSONObject();
            json.put("success",true);
            return ResponseEntity.status(200).body(json.toString());
        }catch (Exception e) {
            e.printStackTrace();
            JSONObject json=new JSONObject();
            json.put("success",false);
            json.put("message","server error");
            return ResponseEntity.status(500).body(json.toString());
        }
    }
}
