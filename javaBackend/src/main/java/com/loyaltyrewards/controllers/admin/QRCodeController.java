package com.loyaltyrewards.controllers.admin;

import com.loyaltyrewards.models.JWT;
import com.loyaltyrewards.repositories.redeem.CartItemRepository;
import com.loyaltyrewards.repositories.user.GenderRepository;
import com.loyaltyrewards.security.DecodeJWT;
import com.loyaltyrewards.services.SQLService;
import org.joda.time.DateTime;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.sql.Timestamp;
import java.util.Date;

@RestController
@Component
@RequestMapping("/QR")
public class QRCodeController {
    @Autowired
    SQLService sqlService;

    @Autowired
    DecodeJWT decodeJWT;

    @Autowired
    CartItemRepository cartItemRepository;

    @Autowired
    GenderRepository genderRepository;

    @GetMapping("/orderRef/{customer_id}/{order_ref}")
    public ResponseEntity<String> setPickedReward(@PathVariable Long customer_id, @PathVariable Long order_ref,HttpServletRequest request) {
        JWT jwt = decodeJWT.decode(request);
        String checkAdmin = jwt.getLastLogin();

        if (!(checkAdmin.equals("admin"))) {
            JSONObject json=new JSONObject();
            json.put("success",false);
            json.put("message","Not a Admin");
            return ResponseEntity.status(400).body(json.toString());
        }
        System.out.println("Scan QR code");
        try{
            String tableName = "redeem_order";
            String columnName = "is_pickup,is_deleted";
            String columnData = "true,false";
            String updateCondition = String.format("customer_id=%1$s and order_ref=%2$s ",customer_id,order_ref);
            String firstRowCondition = String.format("customer_id=%1$s and order_ref=%2$s and is_pickup=false ", customer_id,order_ref);
            sqlService.updateOneQuery(tableName, columnName, columnData,updateCondition,firstRowCondition);
            JSONObject json=new JSONObject();
            json.put("success",true);
            return ResponseEntity.status(200).body(json.toString());
        }  catch (Exception e) {
            JSONObject json=new JSONObject();
            json.put("success",false);
            json.put("error",e);
            return ResponseEntity.status(400).body(json.toString());
        }
    }

    @GetMapping("/appointment/{customer_id}/{appointment_id}")
    public ResponseEntity<String> setCompletedAppoint(@PathVariable Long customer_id, @PathVariable Long appointment_id,HttpServletRequest request) {
        JWT jwt = decodeJWT.decode(request);
        String checkAdmin = jwt.getLastLogin();

        if (!(checkAdmin.equals("admin"))) {
            JSONObject json=new JSONObject();
            json.put("success",false);
            json.put("message","Not a Admin");
            return ResponseEntity.status(400).body(json.toString());
        }
        System.out.println("Scan QR code");
        try{
            String sql = String.format("SELECT default_score " +
                            "from treatments " +
                            "left join appointments ON treatments.id = treatment_id " +
                            "where appointments.id = %1$s", appointment_id);
            Object default_score = this.sqlService.selectQuery2(sql).get(0).get("default_score");
            System.out.println(default_score);

        Date dt = new Date();
        DateTime dtOrg = new DateTime(dt);
        DateTime dtPlusOneYear = dtOrg.plusDays(90);
        Timestamp expired_data = new Timestamp(dtPlusOneYear.getMillis());

        System.out.println(expired_data);

            String tableName = "earned_point";
            String columnName = "points,appointment_id,customer_id,expired_date";
            String columnData = String.format("%1$s,%2$s,%3$s,'%4$s'",
                    default_score,
                    appointment_id,
                    customer_id,
                    expired_data);
            sqlService.insertQuery(tableName, columnName, columnData);

            JSONObject json=new JSONObject();
            json.put("success",true);
            return ResponseEntity.status(200).body(json.toString());
        }  catch (Exception e) {
            JSONObject json=new JSONObject();
            json.put("success",false);
            json.put("error","Server error");
            return ResponseEntity.status(400).body(json.toString());
        }
    }

}
