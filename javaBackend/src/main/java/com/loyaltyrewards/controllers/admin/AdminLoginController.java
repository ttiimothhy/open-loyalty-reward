package com.loyaltyrewards.controllers.admin;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.loyaltyrewards.models.Email;
import com.loyaltyrewards.models.JWT;
import com.loyaltyrewards.models.form.SocialLoginForm;
import com.loyaltyrewards.models.form.UserForm;
import com.loyaltyrewards.models.user.Customer;
import com.loyaltyrewards.models.user.RegisterStatus;
import com.loyaltyrewards.models.user.Role;
import com.loyaltyrewards.models.user.User;
import com.loyaltyrewards.repositories.user.CustomerRepository;
import com.loyaltyrewards.repositories.user.RegisterStatusRepository;
import com.loyaltyrewards.repositories.user.RoleRepository;
import com.loyaltyrewards.repositories.user.UserRepository;
import com.loyaltyrewards.security.DecodeJWT;
import com.loyaltyrewards.security.EncodeJWT;
import com.loyaltyrewards.services.Mail.MailService;
import com.loyaltyrewards.services.SQLService;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

@RestController
@RequestMapping("/admin")
public class AdminLoginController {
    private final UserRepository userRepository;
    private final  PasswordEncoder encoder;
    public final OkHttpClient client = new OkHttpClient.Builder()
            .readTimeout(1000, TimeUnit.MILLISECONDS)
            .writeTimeout(1000, TimeUnit.MILLISECONDS)
            .build();

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    CustomerRepository customerRepository;

    @Autowired
    RegisterStatusRepository registerStatusRepository;



    @Autowired
    private MailService mailService;

    @Autowired
    private EncodeJWT getEncodeJWT;

    @Autowired
    private DecodeJWT decodeJWT;

    @Autowired
    SQLService sqlService;


    public AdminLoginController(UserRepository userRepository, PasswordEncoder encoder) {
        this.userRepository = userRepository;
        this.encoder = encoder;
    }

    @GetMapping("/currentUsersCheck")
    public ResponseEntity<String> currentUserCheck(HttpServletRequest request){
        try{
            JWT jwt = decodeJWT.decode(request);
            String checkAdmin = jwt.getLastLogin();

            if (!(checkAdmin.equals("admin"))) {
                JSONObject json=new JSONObject();
                json.put("success",false);
                json.put("message","Not a Admin");
                return ResponseEntity.status(400).body(json.toString());
            }
            JSONObject json=new JSONObject();
            json.put("success",true);
            return ResponseEntity.status(200).body(json.toString());
        }catch(Exception e){
            JSONObject json = new JSONObject();
            json.put("success",false);
            json.put("message","Please login");
            return ResponseEntity.status(400).body(json.toString());
        }
    }

    @PostMapping("/login")
    public ResponseEntity<String> login (@RequestBody User form) {
        // String Injection spot
        String sql = String.format("SELECT id,username, password from admin where username='%1$s'",form.getUsername());
        List<ObjectNode> admin_user = this.sqlService.selectQuery(sql);

        if (admin_user.size() > 0) {
            String sql_password = admin_user.get(0).get("password").toString().replace("\"", "");
            if (this.encoder.matches(form.getPassword(),sql_password)){
                Long sql_id = Long.parseLong(admin_user.get(0).get("id").toString().replace("\"", ""));
                String sql_username = admin_user.get(0).get("username").toString().replace("\"", "");
                String token = EncodeJWT.getJWTToken(sql_id,sql_username,0L,0L,"admin");
                System.out.println(token);

                System.out.println("success");
                JSONObject json=new JSONObject();
                json.put("success",true);
                json.put("role","admin");
                json.put("id",sql_id);
                json.put("data",token);
                System.out.println(json.toString());
                return ResponseEntity.status(200).body(json.toString());
            } else {
                JSONObject json=new JSONObject();
                json.put("success",false);
                json.put("message","Password not match");
                return ResponseEntity.status(400).body(json.toString());
            }
        } else {
            JSONObject json=new JSONObject();
            json.put("success",false);
            json.put("message","User not found");
            return ResponseEntity.status(400).body(json.toString());
        }
    }
}
