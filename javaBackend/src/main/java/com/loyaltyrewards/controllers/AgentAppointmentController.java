package com.loyaltyrewards.controllers;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.loyaltyrewards.models.JWT;
import com.loyaltyrewards.models.form.AppointmentForm;
import com.loyaltyrewards.security.DecodeJWT;
import com.loyaltyrewards.services.SQLService;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@Component
@RequestMapping("/agents/appointment")
public class AgentAppointmentController {
    @Autowired
    SQLService sqlService;

    @Autowired
    DecodeJWT decodeJWT;


    @PostMapping("/{id}")
    public ResponseEntity<String> makeAppointment(@PathVariable Long id, @RequestBody AppointmentForm form, HttpServletRequest request) {
        JWT jwt = decodeJWT.decode(request);
        Long agent_id = jwt.getAgent_id();
        if (agent_id == 0) {
            JSONObject json=new JSONObject();
            json.put("success",false);
            json.put("message","Not an agent");
            return ResponseEntity.status(400).body(json.toString());
        }
        try{
            String tableName = "appointments";
            String columnName = "customer_id,treatment_id,branch_id,timeslot";
            String columnData = String.format("%1$s,%2$s,%3$s,'%4$s'",
                    form.getCustomer_id(),id,form.getBranch_id(),form.getTimeslot());
            Long appointments_returningID = sqlService.insertQuery(tableName, columnName, columnData);

            String sql = String.format("SELECT agents.id,each_appointment_commission " +
                            "from agents " +
                            "where (id = %1$s)",
                    agent_id);
            List<ObjectNode> agent_id_list = this.sqlService.selectQuery(sql);
            Integer agent_each_appointment_commission = Integer.parseInt(agent_id_list.get(0).get("each_appointment_commission").toString().replaceAll("\"",""));

            String tableName2 = "appointment_commission";
            String columnName2 = "points,agent_id,appointment_id";
            String columnData2 = String.format("%1$s,%2$s,%3$s",
                    agent_each_appointment_commission, agent_id,appointments_returningID);
            sqlService.insertQuery(tableName2, columnName2, columnData2);

            JSONObject json=new JSONObject();
            json.put("success",true);
            return ResponseEntity.status(200).body(json.toString());
        }  catch (Exception e) {
            JSONObject json=new JSONObject();
            json.put("success",false);
            json.put("error",e);
            return ResponseEntity.status(400).body(json.toString());
        }
    }
}
