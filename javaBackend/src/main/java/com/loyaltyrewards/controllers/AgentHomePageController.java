package com.loyaltyrewards.controllers;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.loyaltyrewards.models.JWT;
import com.loyaltyrewards.security.DecodeJWT;
import com.loyaltyrewards.services.SQLService;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Calendar;
import java.util.List;

@RestController
@Component
@RequestMapping("/agents/index")
public class AgentHomePageController {

    @Autowired
    SQLService sqlService;

    @Autowired
    DecodeJWT decodeJWT;

    @GetMapping("/advertisements")
    public ResponseEntity<String> getAdvertisement(HttpServletRequest request) {
        JWT jwt = decodeJWT.decode(request);
        Long agent_id = jwt.getAgent_id();
        if (agent_id == 0) {
            JSONObject json=new JSONObject();
            json.put("success",false);
            json.put("message","Not an agent");
            return ResponseEntity.status(400).body(json.toString());
        }
        try{
            String sql = "SELECT id,title,image,treatment_id from advertisements " +
                    "where is_hidden = false";
            List<ObjectNode> result = this.sqlService.selectQuery(sql);
            return ResponseEntity.status(200).body(result.toString());
        }  catch (Exception e) {
            JSONObject json=new JSONObject();
            json.put("success",false);
            json.put("error",e);
            return ResponseEntity.status(400).body(json.toString());
        }
    }

    @GetMapping("/contact")
    public ResponseEntity<String> getPopularTreatments(HttpServletRequest request) {
        JWT jwt = decodeJWT.decode(request);
        Long agent_id = jwt.getAgent_id();
        if (agent_id == 0) {
            JSONObject json=new JSONObject();
            json.put("success",false);
            json.put("message","Not an agent");
            return ResponseEntity.status(400).body(json.toString());
        }
        try{
            String sql = String.format("SELECT users.id as id, customers.id as customer_id, gender, display_name, date_of_birth, mobile  " +
                    "from users " +
                    "LEFT JOIN gender on users.gender_id = gender.id " +
                    "LEFT JOIN customers on customers.user_id = users.id " +
                    "LEFT JOIN agent_contact on agent_contact.customer_id = customers.id " +
                    "where agent_contact.agent_id = %1$s ",
                    agent_id);
            List<ObjectNode> result = this.sqlService.selectQuery(sql);
            return ResponseEntity.status(200).body(result.toString());
        }  catch (Exception e) {
            JSONObject json=new JSONObject();
            json.put("success",false);
            json.put("error",e);
            return ResponseEntity.status(400).body(json.toString());
        }
    }

    @GetMapping("/performance")
    public ResponseEntity<String> getPerformance(HttpServletRequest request) {
        JWT jwt = decodeJWT.decode(request);
        Long agent_id = jwt.getAgent_id();
        if (agent_id == 0) {
            JSONObject json=new JSONObject();
            json.put("success",false);
            json.put("message","Not an agent");
            return ResponseEntity.status(400).body(json.toString());
        }
        try {

            String appointment_sql = String.format("SELECT agents.id, " +
                            "count(appointment_commission.id) as appointment_count, " +
                            "sum( appointment_commission.points) as points " +
                            "from agents " +
                            "LEFT JOIN appointment_commission on appointment_commission.agent_id = agents.id " +
                            "where (appointment_commission.agent_id = %1$s and " +
                            "appointment_commission.create_time >= date_trunc('month', CURRENT_DATE))  " +
                            "group by agents.id",
                    agent_id);
            List<ObjectNode> appointment_result = this.sqlService.selectQuery(appointment_sql);

            String appointment_count ="0";
            String appointment_points ="0";
            if (appointment_result.size() > 0) {
                String jsonStr = appointment_result.get(0).toString();
                JSONObject resultJson = new JSONObject(jsonStr);
                appointment_count = resultJson.getString("appointment_count");
                appointment_points = resultJson.getString("points");
            }

            String referral_sql = String.format("SELECT agents.id, " +
                            "count(referral_commission.id) as referral_count , " +
                            "sum( referral_commission.points) as points " +
                            "from agents " +
                            "LEFT JOIN referral_commission on referral_commission.agent_id = agents.id " +
                            "where (referral_commission.agent_id = %1$s and " +
                            "referral_commission.create_time >= date_trunc('month', CURRENT_DATE)) " +
                            "group by agents.id",
                    agent_id);
            List<ObjectNode> referral_result = this.sqlService.selectQuery(referral_sql);

            String referral_count = "0";
            String referral_points = "0";
            if (referral_result.size() > 0) {
                String jsonStr = referral_result.get(0).toString();
                JSONObject resultJson = new JSONObject(jsonStr);
                referral_count = resultJson.getString("referral_count");
                referral_points = resultJson.getString("referral_count");
            }


            JSONObject json=new JSONObject();
            json.put("success",true);
            json.put("referral_count",referral_count);
            json.put("appointment_count",appointment_count);
            json.put("total_point",Integer.parseInt(appointment_points)+Integer.parseInt(referral_points));
            return ResponseEntity.status(200).body(json.toString());

        } catch (Exception e) {
            JSONObject json = new JSONObject();
            json.put("success", false);
            json.put("error", "error");
            return ResponseEntity.status(400).body(json.toString());
        }
    }
}


