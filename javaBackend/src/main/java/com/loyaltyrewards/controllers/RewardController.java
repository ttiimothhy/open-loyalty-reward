package com.loyaltyrewards.controllers;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.loyaltyrewards.lib.MongoDB;
import com.loyaltyrewards.models.JWT;
import com.loyaltyrewards.models.user.Gender;
import com.loyaltyrewards.models.user.User;
import com.loyaltyrewards.repositories.user.GenderRepository;
import com.loyaltyrewards.repositories.user.UserRepository;
import com.loyaltyrewards.security.DecodeJWT;
import com.loyaltyrewards.services.SQLService;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@Component
@RequestMapping("/reward")
public class RewardController {
    @Autowired
    SQLService sqlService;

    @Autowired
    DecodeJWT decodeJWT;

    @Autowired
    UserRepository userRepository;

    @Autowired
    GenderRepository genderRepository;

    // Dependency injection
    // Inversion of control -> Spring IOC Container
    @Autowired
    MongoDB mongoDB;

    @GetMapping("/all")
    public ResponseEntity<String> getAvailableReward(HttpServletRequest request) {
        JWT jwt = decodeJWT.decode(request);
        Long customer_id = jwt.getCustomer_id();

        try{
            String sql = String.format("SELECT rewards.id,detail,name,redeem_points,type,image " +
                    "from rewards " +
                    "left join reward_type ON reward_type.id = reward_type_id " +
                    "where (is_hidden=false and remain_amount>0)");
            List<ObjectNode> result = this.sqlService.selectQuery(sql);

            try {
                mongoDB.insertToMongoDB("getLog",customer_id,"rewards", "all");
            } catch (Exception e) {
                System.err.println("mongoDB not found");
                System.err.println(e);
            }

            return ResponseEntity.status(200).body(result.toString());

        }  catch (Exception e) {
            JSONObject json=new JSONObject();
            json.put("success",false);
            json.put("error",e);
            return ResponseEntity.status(400).body(json.toString());
        }
    }

    @GetMapping
    public ResponseEntity<String> getAvailableRewardFilter(@RequestParam Integer type,@RequestParam Integer location,HttpServletRequest request) {
        JWT jwt = decodeJWT.decode(request);
        Long customer_id = jwt.getCustomer_id();

        System.out.println(type);
        System.out.println(location);

        String typeCondition="";
        String typeSelect="";
        String typeJoin="";
        String locationCondition="";
        String locationSelect="";
        String locationJoin="";
        if (type > 0 ) {
            typeCondition = " and reward_type.id=" + type;
            typeSelect=",type";
            typeJoin="left join reward_type ON reward_type.id = reward_type_id ";
        }
        if (location > 0  ) {
            locationCondition = " and pickup_location.id=" + location;
            locationSelect="";
            locationJoin="left join pickup_location_reward_relation ON pickup_location_reward_relation.rewards_id = rewards.id left join pickup_location ON pickup_location_reward_relation.pickup_location_id = pickup_location.id ";
        }
        String condition = typeCondition+locationCondition;
        try{
            String sql = String.format("SELECT rewards.id,detail,rewards.name,image,redeem_points %2$s " +
                            "from rewards " +
                            "%4$s " +
                            "%3$s " +
                            "where (is_hidden=false %1$s )",
                     condition,typeSelect,typeJoin,locationSelect,locationJoin);
            List<ObjectNode> result = this.sqlService.selectQuery(sql);

            try {
                mongoDB.insertToMongoDB("getLog",customer_id,"rewards", "filter");
            } catch (Exception e) {
                System.err.println("mongoDB not found");
                System.err.println(e);
            }

            return ResponseEntity.status(200).body(result.toString());
        }  catch (Exception e) {
            JSONObject json=new JSONObject();
            json.put("success",false);
            json.put("error",e);
            return ResponseEntity.status(400).body(json.toString());
        }
    }

    @GetMapping("/location")
    public ResponseEntity<String> getAllPickupLocation(HttpServletRequest request) {
        try{
            String sql = String.format("SELECT name,id " +
                    "from pickup_location");
            System.out.println(sql);
        List<ObjectNode> result = this.sqlService.selectQuery(sql);
        System.out.println(result);
        return ResponseEntity.status(200).body(result.toString());
        }  catch (Exception e) {
            JSONObject json=new JSONObject();
            json.put("success",false);
            json.put("error",e);
            return ResponseEntity.status(400).body(json.toString());
        }
    }
    @GetMapping("/type")
    public ResponseEntity<String> getAllServiceType(HttpServletRequest request) {
        try{
            String sql = String.format("SELECT type,id " +
                    "from reward_type");
            System.out.println(sql);
            List<ObjectNode> result = this.sqlService.selectQuery(sql);
            System.out.println(result);
            return ResponseEntity.status(200).body(result.toString());
        }  catch (Exception e) {
            JSONObject json=new JSONObject();
            json.put("success",false);
            json.put("error",e);
            return ResponseEntity.status(400).body(json.toString());
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<String> getRewardWithID(@PathVariable Long id,HttpServletRequest request) {
        JWT jwt = decodeJWT.decode(request);
        Long user_id = jwt.getId();
        Long customer_id = jwt.getCustomer_id();

        try{
            String sql = String.format("SELECT detail,image,name,redeem_points,remain_amount " +
                    "from rewards " +
                    "where id = %1$s ",
                    id);
            System.out.println(sql);
            List<ObjectNode> result = this.sqlService.selectQuery(sql);
            System.out.println(result);

            try {
                mongoDB.insertToMongoDB("getLog",customer_id,"rewards", id.toString());
            } catch (Exception e) {
                System.err.println("mongoDB not found");
                System.err.println(e);
            }

            return ResponseEntity.status(200).body(result.toString());
        }  catch (Exception e) {
            JSONObject json=new JSONObject();
            json.put("success",false);
            json.put("error",e);
            return ResponseEntity.status(400).body(json.toString());
        }
    }
}
