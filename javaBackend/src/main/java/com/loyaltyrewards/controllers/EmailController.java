package com.loyaltyrewards.controllers;

import com.loyaltyrewards.models.Email;
import com.loyaltyrewards.services.Mail.MailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.mail.MessagingException;

@RestController
public class EmailController {

	@Autowired
	private MailService mailService;

//	@PostMapping("/sendMail")
	public String send(@RequestBody Email email) {
		try {
			mailService.sendEmail(email);
			return "Congratulations! Your mail has been sent to the user.";
		} catch (MailException mailException) {
			System.out.println(mailException);
			return mailException.toString();
		}
	}

//	@PostMapping("/sendMailAttachment")
	public String sendWithAttachment(@RequestBody Email email) throws MessagingException {
		try {
			mailService.sendEmailWithAttachment(email);
			return "Congratulations! Your mail has been sent to the user.";
		} catch (MailException mailException) {
			System.out.println(mailException);
			return mailException.toString();
		}

	}
}