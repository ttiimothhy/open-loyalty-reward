package com.loyaltyrewards.controllers;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.loyaltyrewards.models.JWT;
import com.loyaltyrewards.models.form.UserForm;
import com.loyaltyrewards.models.user.Gender;
import com.loyaltyrewards.models.user.User;
import com.loyaltyrewards.repositories.user.GenderRepository;
import com.loyaltyrewards.repositories.user.UserRepository;
import com.loyaltyrewards.security.DecodeJWT;
import com.loyaltyrewards.services.SQLService;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

@RestController
@Component
@RequestMapping("/profile")
public class ProfileController {

    @Autowired
    PasswordEncoder encoder;

    @Autowired
    SQLService sqlService;

    @Autowired
    DecodeJWT decodeJWT;

    @Autowired
    UserRepository userRepository;

    @Autowired
    GenderRepository genderRepository;

    @GetMapping("/user")
    public ResponseEntity<String> getUserDetail(HttpServletRequest request) {
//        System.out.println(request);
            JWT jwt = decodeJWT.decode(request);
            Long id = jwt.getId();
        try {
        String sql = "SELECT users.id,date_of_birth ,display_name,email,mobile,username,gender from users " +
                "left join gender ON gender.id = gender_id " +
                "where users.id = " + id;
            System.out.println(sql);
        List<ObjectNode> checkUsername = this.sqlService.selectQuery(sql);
            System.out.println(checkUsername);
        Object toJson = checkUsername.get(0);
        System.out.println(toJson.toString());
        return ResponseEntity.status(200).body(toJson.toString());
        } catch (Exception e) {
            e.printStackTrace();
            JSONObject json=new JSONObject();
            json.put("success",false);
            return ResponseEntity.status(400).body(json.toString());
        }
    }

    @PostMapping("/update")
    public ResponseEntity<String> updateUserDetail(HttpServletRequest request, @RequestBody UserForm form) {
            JWT jwt = decodeJWT.decode(request);
            Long id = jwt.getId();
        try {
            User user = userRepository.findById(id).get();
            user.setId(id);
            if (form.getUsername().length() > 0) {
                user.setUsername(form.getUsername());
            }
            if (form.getDisplay_name().length() > 0) {
                user.setDisplay_name(form.getDisplay_name());
            }
            if (form.getEmail().length() > 0) {
                user.setEmail(form.getEmail());
            }
            if (form.getMobile() < 100000000) {
                user.setMobile(form.getMobile());
            } else {
                JSONObject json=new JSONObject();
                json.put("success",false);
                json.put("message","mobile error");
                return ResponseEntity.status(400).body(json.toString());
            }

            // Optional
            if (form.getDate_of_birth().length() > 0){
                DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
                Date data_of_birth = format.parse(form.getDate_of_birth());
                user.setDate_of_birth(data_of_birth);
            }
            if (form.getPassword().length() > 0) {
                user.setPassword(encoder.encode(form.getPassword()));
            }
            if (form.getGender_id() != null) {
                Gender gender = genderRepository.findById(form.getGender_id()).get();
                user.setGender(gender);
            }

            userRepository.save(user);


            JSONObject json=new JSONObject();
            json.put("success",true);
            return ResponseEntity.status(200).body(json.toString());

        } catch (Exception e) {
            JSONObject json=new JSONObject();
            json.put("success",false);
            json.put("error",e);
            return ResponseEntity.status(400).body(json.toString());
        }
    }
}
