package com.loyaltyrewards.controllers;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.gson.JsonObject;
import com.loyaltyrewards.lib.MongoDB;
import com.loyaltyrewards.models.JWT;
import com.loyaltyrewards.models.user.User;
import com.loyaltyrewards.repositories.user.GenderRepository;
import com.loyaltyrewards.repositories.user.UserRepository;
import com.loyaltyrewards.security.DecodeJWT;
import com.loyaltyrewards.services.SQLService;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@Component
@RequestMapping("/")
public class SearchController {
    @Autowired
    SQLService sqlService;

    @Autowired
    DecodeJWT decodeJWT;

    @Autowired
    UserRepository userRepository;

    @Autowired
    MongoDB mongoDB;


    @GetMapping("/search/{keyword}")
    public ResponseEntity<String> searchAllByCustomer(@PathVariable String keyword, HttpServletRequest request) {
        JWT jwt = decodeJWT.decode(request);
        Long user_id = jwt.getId();
        Long customer_id = jwt.getCustomer_id();

        String keyword_wild = "%" + keyword + "%";
        System.out.println(keyword_wild);
        try{
            String sql1 = String.format("SELECT id,title,image,detail " +
                            "from treatments " +
                            "where (is_hidden=false and (detail ilike '%1$s' or title ilike '%1$s'))",keyword_wild);
            List<JSONObject> result1 = this.sqlService.selectQuery2(sql1);

            String sql2 = String.format("SELECT id,name,image,detail " +
                    "from rewards " +
                    "where (is_hidden=false and (detail ilike '%1$s'))",keyword_wild);
            List<JSONObject> result2 = this.sqlService.selectQuery2(sql2);

            String sql3 = String.format("SELECT id,content " +
                    "from enquiry " +
                    "where (customer_id=%2$s and (content ilike '%1$s'))",keyword_wild,customer_id);
            List<JSONObject> result3 = this.sqlService.selectQuery2(sql3);

            JSONObject json=new JSONObject();
            json.put("treatments",result1);
            json.put("rewards",result2);
            json.put("enquiry",result3);

            try {
                mongoDB.insertToMongoDB("getLog",customer_id,"search", keyword);
            } catch (Exception e) {
                System.err.println("mongoDB not found");
                System.err.println(e);
            }

            return ResponseEntity.status(200).body(json.toString());
        }  catch (Exception e) {
            System.out.println(e);
            JSONObject json=new JSONObject();
            json.put("success",false);
            return ResponseEntity.status(500).body(json.toString());
        }
    }


    @GetMapping("/agents/search/{keyword}")
    public ResponseEntity<String> searchAllByAgent(@PathVariable String keyword, HttpServletRequest request) {
        JWT jwt = decodeJWT.decode(request);
        Long user_id = jwt.getId();
        Long agent_id = jwt.getAgent_id();

        String keyword_wild = "%" + keyword + "%";
        System.out.println(keyword_wild);
        try{
            String sql1 = String.format("SELECT id,title,image,detail " +
                    "from treatments " +
                    "where (is_hidden=false and (detail ilike '%1$s' or title ilike '%1$s'))",keyword_wild);
            List<JSONObject> result1 = this.sqlService.selectQuery2(sql1);


            String sql3 = String.format("SELECT users.id as user_id, username, customers.id as id, gender, display_name, date_of_birth, mobile  " +
                            "from users " +
                            "LEFT JOIN gender on users.gender_id = gender.id " +
                            "LEFT JOIN customers on customers.user_id = users.id " +
                            "LEFT JOIN agent_contact on agent_contact.customer_id = customers.id " +
                            "where (agent_contact.agent_id = %1$s and (display_name ilike '%2$s' or username ilike '%2$s'))",
                    agent_id,keyword_wild);
            List<JSONObject> result3 = this.sqlService.selectQuery2(sql3);

            JSONObject json=new JSONObject();
            json.put("treatments",result1);
            json.put("contact",result3);
            return ResponseEntity.status(200).body(json.toString());
        }  catch (Exception e) {
            System.out.println(e);
            JSONObject json=new JSONObject();
            json.put("success",false);
            return ResponseEntity.status(500).body(json.toString());
        }
    }
}
