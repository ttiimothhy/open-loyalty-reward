package com.loyaltyrewards.controllers;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.loyaltyrewards.models.JWT;
import com.loyaltyrewards.security.DecodeJWT;
import com.loyaltyrewards.services.SQLService;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@Component
@RequestMapping("/bar")
public class TopAndBottomController {
    @Autowired
    SQLService sqlService;

    @Autowired
    DecodeJWT decodeJWT;

    @GetMapping("/bottom")
    public ResponseEntity<String> getTopBar(HttpServletRequest request) {
        JWT jwt = decodeJWT.decode(request);
        Long customer_id = jwt.getCustomer_id();

        try{
            String sql = String.format("SELECT count(appointments.id)" +
                    "from appointments " +
                    "where (is_completed = false and is_cancelled = false and customer_id = %1$s)",
                    customer_id);
            List<ObjectNode> result = this.sqlService.selectQuery(sql);
            return ResponseEntity.status(200).body(result.toString());
        }  catch (Exception e) {
            JSONObject json=new JSONObject();
            json.put("success",false);
            json.put("error",e);
            return ResponseEntity.status(400).body(json.toString());
        }
    }
}
