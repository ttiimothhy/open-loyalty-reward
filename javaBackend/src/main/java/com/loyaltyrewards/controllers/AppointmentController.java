package com.loyaltyrewards.controllers;

import com.loyaltyrewards.models.JWT;
import com.loyaltyrewards.models.form.AppointmentForm;
import com.loyaltyrewards.security.DecodeJWT;
import com.loyaltyrewards.services.SQLService;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@Component
@RequestMapping("/appointment")
public class AppointmentController {
    @Autowired
    SQLService sqlService;

    @Autowired
    DecodeJWT decodeJWT;


    @PostMapping("/{id}")
    public ResponseEntity<String> makeAppointment(@PathVariable Long id, @RequestBody AppointmentForm form, HttpServletRequest request) {
        JWT jwt = decodeJWT.decode(request);
        Long customer_id = jwt.getCustomer_id();
        try{
            String tableName = "appointments";
            String columnName = "customer_id,treatment_id,branch_id,timeslot";
            String columnData = String.format("%1$s,%2$s,%3$s,'%4$s'",
                    customer_id,id,form.getBranch_id(),form.getTimeslot());
            sqlService.insertQuery(tableName, columnName, columnData);
            JSONObject json=new JSONObject();
            json.put("success",true);

            return ResponseEntity.status(200).body(json.toString());
        }  catch (Exception e) {
            JSONObject json=new JSONObject();
            json.put("success",false);
            json.put("error",e);
            return ResponseEntity.status(400).body(json.toString());
        }
    }
}
