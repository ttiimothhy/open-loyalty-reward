package com.loyaltyrewards.controllers;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.loyaltyrewards.lib.MongoDB;
import com.loyaltyrewards.models.JWT;
import com.loyaltyrewards.models.form.PurchaseForm;
import com.loyaltyrewards.models.form.SocialLoginForm;
import com.loyaltyrewards.security.DecodeJWT;
import com.loyaltyrewards.services.SQLService;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@Component
@RequestMapping("/purchase")
public class PurchaseController {
    @Autowired
    SQLService sqlService;

    @Autowired
    DecodeJWT decodeJWT;

    @Autowired
    MongoDB mongoDB;

    @PostMapping("/confirm")
    public ResponseEntity<String> confirmPurchase(@RequestBody PurchaseForm form, HttpServletRequest request) {
        JWT jwt = decodeJWT.decode(request);
        Long customer_id = jwt.getCustomer_id();

        try{
            String sql = String.format("SELECT purchase_benefit_condition,purchase_benefit_amount " +
                            "from treatments " +
                            "where (id = %1$s)",
                    form.getTreatment_id());
            List<ObjectNode> result = this.sqlService.selectQuery(sql);

        Integer purchase_benefit_condition = Integer.parseInt(result.get(0).get("purchase_benefit_condition").toString().replace("\"", ""));

            Integer benefit_amount = 0;
            if (purchase_benefit_condition > 0) {
                 benefit_amount = form.getPurchased_amount()/purchase_benefit_condition;
                 System.out.println(purchase_benefit_condition);
                 System.out.println(form.getPurchased_amount());
                 System.out.println(benefit_amount);
            }
            Integer remaining_amount = form.getPurchased_amount()+benefit_amount;

//            System.out.println(form.getPurchased_amount());
//            System.out.println(purchase_benefit_condition);
//            System.out.println(benefit_amount);
//            System.out.println(remaining_amount);

            String tableName = "purchased_item";
            String columnName = "benefit_amount,purchase_amount,remaining_amount,customer_id,treatment_id";
            String columnData = String.format("%1$s,%2$s,%3$s,%4$s,%5$s",
                    benefit_amount,form.getPurchased_amount(),remaining_amount,customer_id,form.getTreatment_id());
            sqlService.insertQuery(tableName, columnName, columnData);

            JSONObject json=new JSONObject();
            json.put("success",true);
            return ResponseEntity.status(200).body(json.toString());
        }  catch (Exception e) {
            JSONObject json=new JSONObject();
            json.put("success",false);
            json.put("error","server error");
            return ResponseEntity.status(500).body(json.toString());
        }
    }

    @GetMapping("/item")
    public ResponseEntity<String> getPurchasedItem( HttpServletRequest request) {
        JWT jwt = decodeJWT.decode(request);
        Long customer_id = jwt.getCustomer_id();

        try{
            String sql = String.format("SELECT treatments.id ,sum(remaining_amount) as remaining_amount,treatments.detail,treatments.image,treatments.title " +
                    "from purchased_item " +
                    "left join treatments ON treatments.id = purchased_item.treatment_id " +
                    "where (purchased_item.customer_id = %1$s) " +
                    "group by treatments.id ,treatments.detail,treatments.image,treatments.title ", customer_id);
            List<ObjectNode> result = this.sqlService.selectQuery(sql);




            return ResponseEntity.status(200).body(result.toString());
        }  catch (Exception e) {
            JSONObject json=new JSONObject();
            json.put("success",false);
            json.put("error","server error");
            return ResponseEntity.status(500).body(json.toString());
        }
    }



}
