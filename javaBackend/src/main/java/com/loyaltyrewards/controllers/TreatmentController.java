package com.loyaltyrewards.controllers;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.loyaltyrewards.lib.MongoDB;
import com.loyaltyrewards.models.JWT;
import com.loyaltyrewards.models.user.Gender;
import com.loyaltyrewards.models.user.User;
import com.loyaltyrewards.repositories.user.GenderRepository;
import com.loyaltyrewards.repositories.user.UserRepository;
import com.loyaltyrewards.security.DecodeJWT;
import com.loyaltyrewards.services.SQLService;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController
@Component
@RequestMapping("/treatment")
public class TreatmentController {
    @Autowired
    SQLService sqlService;

    @Autowired
    DecodeJWT decodeJWT;

    @Autowired
    UserRepository userRepository;

    @Autowired
    GenderRepository genderRepository;

    @Autowired
    MongoDB mongoDB;

    @GetMapping("/all")
    public ResponseEntity<String> getAvailableTreatment(HttpServletRequest request) {
        JWT jwt = decodeJWT.decode(request);
        Long user_id = jwt.getId();
        Long customer_id = jwt.getCustomer_id();

        User user = userRepository.findById(user_id).get();

        try{
            String sql = String.format("SELECT treatments.id,treatments.title,treatments.image,purchase_benefit_amount,purchase_benefit_condition " +
                            "from treatments " +
                            "where (is_hidden=false)");
            List<ObjectNode> result = this.sqlService.selectQuery(sql);

            try {
                mongoDB.insertToMongoDB("getLog",customer_id,"treatment", "all");
            } catch (Exception e) {
                System.err.println("mongoDB not found");
                System.err.println(e);
            }


            return ResponseEntity.status(200).body(result.toString());
        }  catch (Exception e) {
            JSONObject json=new JSONObject();
            json.put("success",false);
            json.put("error",e);
            return ResponseEntity.status(400).body(json.toString());
        }
    }

    @GetMapping
    public ResponseEntity<String> getAvailableTreatmentFilter(@RequestParam Integer service,@RequestParam Integer branch,HttpServletRequest request) {
        JWT jwt = decodeJWT.decode(request);
        Long user_id = jwt.getId();
        Long customer_id = jwt.getCustomer_id();

        String serviceCondition="";
        String serviceSelect="";
        String serviceJoin="";
        String branchCondition="";
        String branchSelect="";
        String branchJoin="";
        if (service > 0) {
            serviceCondition = " and services.id=" + service;
            serviceSelect = "";
            serviceJoin = "left join treatment_service_relation ON treatment_service_relation.treatment_id = treatments.id left join services ON treatment_service_relation.service_id = services.id ";
        }
        if (branch >0 ) {
            branchCondition = " and branches.id=" + branch;
            branchSelect = "";
            branchJoin = "left join treatment_branch_relation ON treatment_branch_relation.treatment_id = treatments.id left join branches ON treatment_branch_relation.branch_id = branches.id ";
        }


        String condition = serviceCondition+branchCondition;
        System.out.println(condition);
        try{
            String sql = String.format("SELECT treatments.id,treatments.title,treatments.image,purchase_benefit_amount,purchase_benefit_condition " +
                            "from treatments " +
                            "%3$s " +
                            "%5$s " +
                            "where ( true %1$s and is_hidden=false)",
                    condition,serviceSelect,serviceJoin,branchSelect,branchJoin);
            List<ObjectNode> result = this.sqlService.selectQuery(sql);

            try {
                mongoDB.insertToMongoDB("getLog",customer_id,"treatment", "filter");
            } catch (Exception e) {
                System.err.println("mongoDB not found");
                System.err.println(e);
            }

            return ResponseEntity.status(200).body(result.toString());
        }  catch (Exception e) {
            JSONObject json=new JSONObject();
            json.put("success",false);
            json.put("error",e);
            return ResponseEntity.status(400).body(json.toString());
        }
    }

    @GetMapping("/branch")
    public ResponseEntity<String> getAllBranch(HttpServletRequest request) {
        try{
            String sql = String.format("SELECT name,id " +
                    "from branches");
            System.out.println(sql);
        List<ObjectNode> result = this.sqlService.selectQuery(sql);
        System.out.println(result);
        return ResponseEntity.status(200).body(result.toString());
        }  catch (Exception e) {
            JSONObject json=new JSONObject();
            json.put("success",false);
            json.put("error",e);
            return ResponseEntity.status(400).body(json.toString());
        }
    }
    @GetMapping("/service")
    public ResponseEntity<String> getAllServiceType(HttpServletRequest request) {
        try{
            String sql = String.format("SELECT service,id " +
                    "from services");
            System.out.println(sql);
            List<ObjectNode> result = this.sqlService.selectQuery(sql);
            System.out.println(result);
            return ResponseEntity.status(200).body(result.toString());
        }  catch (Exception e) {
            JSONObject json=new JSONObject();
            json.put("success",false);
            json.put("error",e);
            return ResponseEntity.status(400).body(json.toString());
        }
    }


    @GetMapping("/{id}")
    public ResponseEntity<String> getServiceWithID(@PathVariable Long id,HttpServletRequest request) {
        JWT jwt = decodeJWT.decode(request);
        Long customer_id = jwt.getCustomer_id();
        try{
            String sql = String.format("SELECT id,detail,image,title,default_score,purchase_benefit_amount,purchase_benefit_condition " +
                    "from treatments " +
                    "where id = %1$s ",
                    id);
            System.out.println(sql);
            List<ObjectNode> result = this.sqlService.selectQuery(sql);
            System.out.println(result);

            try {
                mongoDB.insertToMongoDB("getLog",customer_id,"treatment", id.toString());
            } catch (Exception e) {
                System.err.println("mongoDB not found");
                System.err.println(e);
            }

            return ResponseEntity.status(200).body(result.get(0).toString());
        }  catch (Exception e) {
            JSONObject json=new JSONObject();
            json.put("success",false);
            json.put("message","ID not found");
            return ResponseEntity.status(400).body(json.toString());
        }
    }
}
