package com.loyaltyrewards.seed;

import com.loyaltyrewards.lib.CSVReader;
import com.loyaltyrewards.services.SQLService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import javax.persistence.Query;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.List;


@Component
public class CreateRelationTableID implements CommandLineRunner {

    @Autowired
    CSVReader CSVReader;

    @Autowired
    SQLService sqlService;

    @Override
    public void run(String... args) throws Exception {
        String[] array = {
                "gender_treatments_relation",
                "treatment_branch_relation",
                "treatment_service_relation",
                "pickup_location_reward_relation"
        };
        List<String> list = Arrays.asList(array);

        for (int i = 0; i < list.size(); i = i + 1) {
            String tableName= list.get(i);
            sqlService.addIdColumn(tableName);
        }
    }
}
