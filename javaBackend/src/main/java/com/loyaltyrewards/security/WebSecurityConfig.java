package com.loyaltyrewards.security;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import java.util.Arrays;

@EnableWebSecurity
@Configuration
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {


    public void configure(HttpSecurity http) throws Exception {
        http.cors().and()
            .csrf().disable()
            .addFilterAfter(new JWTAuthorizationFilter(), UsernamePasswordAuthenticationFilter.class)
            .authorizeRequests()
            .antMatchers(HttpMethod.POST,"/**").permitAll() // Testing only
            .antMatchers(HttpMethod.PUT,"/**").permitAll() // Testing only
            .antMatchers(HttpMethod.GET,"/**").permitAll() // Testing only
            .antMatchers(HttpMethod.GET,"/appointment/").permitAll()
            .antMatchers(HttpMethod.POST,"/users/login").permitAll()
            .antMatchers(HttpMethod.POST,"/users/register").permitAll()
            .antMatchers(HttpMethod.POST,"/users/OTPByEmail").permitAll()
            .antMatchers(HttpMethod.GET,"/users/resetPassword").permitAll()
            .antMatchers(HttpMethod.POST,"/users/resetPassword").permitAll()
            .anyRequest().authenticated();
    }


    @Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurer() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/**").allowedMethods("*");
            }
        };
    }

    @Bean
    public PasswordEncoder getEncoder() {
        return new BCryptPasswordEncoder();
    }
}
