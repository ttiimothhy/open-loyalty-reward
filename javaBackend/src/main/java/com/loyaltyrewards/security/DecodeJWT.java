package com.loyaltyrewards.security;

import com.google.gson.Gson;
import com.loyaltyrewards.models.JWT;
import com.loyaltyrewards.models.JWTAppleLogin;
import com.loyaltyrewards.models.user.Customer;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Component
public class DecodeJWT{

        public static JWT decode(HttpServletRequest request){
                try {
                        //                String jwtToken = "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ0ZXN0Iiwicm9sZXMiOiJST0xFX0FETUlOIiwiaXNzIjoibXlzZWxmIiwiZXhwIjoxNDcxMDg2MzgxfQ.1EI2haSz9aMsHjFUXNVz2Z4mtC0nMdZo6bo3-x-aRpw";

                        String authorization = WebUtils.getUserAuthorization(request);
                        String jwtToken = (new ArrayList<String>(Arrays.asList(authorization.split(" ")))).get(1);

//                System.out.println("------------ Decode JWT ------------");
                        String[] split_string = jwtToken.split("\\.");
                        String base64EncodedHeader = split_string[0];
                        String base64EncodedBody = split_string[1];
                        String base64EncodedSignature = split_string[2];

//                System.out.println("~~~~~~~~~ JWT Header ~~~~~~~");
                        Base64 base64Url = new Base64(true);
                        String header = new String(base64Url.decode(base64EncodedHeader));
//                System.out.println("JWT Header : " + header);


//                System.out.println("~~~~~~~~~ JWT Body ~~~~~~~");
                        String body = new String(base64Url.decode(base64EncodedBody));
//                System.out.println("JWT Body : "+body);
//                        List<String> strings = new ArrayList<String>();
//                        strings.add("a123");
//                        strings.add("12313");

                        Gson gson = new Gson();
                        JWT jwt = gson.fromJson(body, JWT.class); // JSON.parse
                        return jwt;
                } catch (Exception e) {
                        JWT jwt = new JWT();
                        jwt.setId(0L);
                        jwt.setUsername("");
                        jwt.setCustomer_id(0L);
                        jwt.setAgent_id(0L);
                        return jwt;
                }

        }
        public static JWTAppleLogin decodeAppleLogin(String request){
                try {
                        //                String jwtToken = "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ0ZXN0Iiwicm9sZXMiOiJST0xFX0FETUlOIiwiaXNzIjoibXlzZWxmIiwiZXhwIjoxNDcxMDg2MzgxfQ.1EI2haSz9aMsHjFUXNVz2Z4mtC0nMdZo6bo3-x-aRpw";

                        String jwtToken = request;

//                System.out.println("------------ Decode JWT ------------");
                        String[] split_string = jwtToken.split("\\.");
                        String base64EncodedHeader = split_string[0];
                        String base64EncodedBody = split_string[1];
                        String base64EncodedSignature = split_string[2];

//                System.out.println("~~~~~~~~~ JWT Header ~~~~~~~");
                        Base64 base64Url = new Base64(true);
                        String header = new String(base64Url.decode(base64EncodedHeader));
//                System.out.println("JWT Header : " + header);


//                System.out.println("~~~~~~~~~ JWT Body ~~~~~~~");
                        String body = new String(base64Url.decode(base64EncodedBody));
//                System.out.println("JWT Body : "+body);

                        Gson gson = new Gson();
                        JWTAppleLogin jwt = gson.fromJson(body, JWTAppleLogin.class);
                        return jwt;
                } catch (Exception e) {
                        JWTAppleLogin jwt = new JWTAppleLogin();
                        return jwt;
                }

        }

}