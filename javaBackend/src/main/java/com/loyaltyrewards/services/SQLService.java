package com.loyaltyrewards.services;


import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.loyaltyrewards.lib.JsonBuilder;
import com.vladmihalcea.hibernate.type.json.JsonNodeBinaryType;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


import javax.persistence.*;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

@Repository
@Transactional
public class SQLService {

    @PersistenceContext
    EntityManager entityManager;

    @Autowired
    JsonBuilder jsonBuilder;

    public Long insertQuery(String table, String columnName, String columnData) {
        String sql = String.format("INSERT INTO %1$s (%2$s) VALUES (%3$s) RETURNING id",
                table, columnName,columnData);
//        System.out.println(sql);
//        Query query = entityManager.createNativeQuery(sql);
//        query.executeUpdate();
        Query query = entityManager.createNativeQuery(sql);
        BigInteger returningID = (BigInteger) query.getSingleResult();
        return returningID.longValue();
    }

    public void updateQuery(String table, String columnName, String columnData, String condition) {
        String sql = String.format("UPDATE %1$s set (%2$s)=(%3$s) where %4$s",
                table, columnName,columnData,condition);
        System.out.println(sql);
        Query query = entityManager.createNativeQuery(sql);
        query.executeUpdate();
    }

    public Long updateOneQuery(String table, String columnName, String columnData, String updateCondition, String firstRowCondition) {
        String firstID = String.format("SELECT id from %1$s " +
                        "where %2$s " +
                        "LIMIT 1",
                table,firstRowCondition);
        String sql = String.format("UPDATE %1$s set (%2$s)=(%3$s) " +
                        "where (%4$s and id=(%5$s)) RETURNING id",
                table, columnName,columnData,updateCondition,firstID);
        System.out.println(sql);
        Query query = entityManager.createNativeQuery(sql);
        BigInteger returningID = (BigInteger) query.getSingleResult();
        return returningID.longValue();
    }

    public Long updateOneQueryKnowID(String table, String columnName, String columnData, Integer id) {
        String sql = String.format("UPDATE %1$s set (%2$s)=(%3$s) " +
                        "where (id=(%4$s)) RETURNING id",
                table, columnName,columnData,id);
        System.out.println(sql);
        Query query = entityManager.createNativeQuery(sql);
        BigInteger returningID = (BigInteger) query.getSingleResult();
        return returningID.longValue();
    }

    public void deleteQuery(String table, String condition) {
        String sql = String.format("DELETE FROM %1$s WHERE %2$s",
                table, condition);
        System.out.println(sql);
        Query query = entityManager.createNativeQuery(sql);
        query.executeUpdate();
    }

    public List<ObjectNode> selectQuery(String sql) {
        System.out.println(sql);
//        String name = "Abc";
        Query query = entityManager.createNativeQuery(sql, Tuple.class);
        // Use setParameter to avoid SQL Injection
//        query.setParameter(0,name);
        List<Tuple> results = query.getResultList();
        List<ObjectNode> json = jsonBuilder.toJson(results);
        return json;
    }

    public List<JSONObject> selectQuery2(String sql) {
        System.out.println(sql);
        Query query = entityManager.createNativeQuery(sql, Tuple.class);
        List<Tuple> results = query.getResultList();
        List<JSONObject> json = jsonBuilder.toJson2(results);
        return json;
    }

    public void addIdColumn(String table) {
        String sql = String.format("ALTER TABLE %1$s " +
                        "ADD COLUMN id BIGSERIAL PRIMARY KEY ;",
                table);
        System.out.println(sql);
        Query query = entityManager.createNativeQuery(sql);
        query.executeUpdate();
    }
    public void deleteData(String table,String condition) {
        // Never use binding for table name
        String sql = String.format("DELETE FROM %1$s WHERE %2$s;",
                table,condition);
        System.out.println(sql);
        Query query = entityManager.createNativeQuery(sql);
        query.executeUpdate();
    }
}
