package com.loyaltyrewards.services.Mail;


import com.loyaltyrewards.models.Email;
import org.springframework.mail.MailException;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;

@Service
public interface MailService {
	public void sendEmail(Email email) throws MailException;
	public void sendEmailWithAttachment(Email email) throws MailException, MessagingException;
}