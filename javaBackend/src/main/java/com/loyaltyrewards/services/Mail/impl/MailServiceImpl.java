package com.loyaltyrewards.services.Mail.impl;

import com.loyaltyrewards.models.Email;
import com.loyaltyrewards.services.Mail.MailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

@Service(value = "mailServiceImpl")
public class MailServiceImpl implements MailService {
	private JavaMailSender javaMailSender;

	@Autowired
	public MailServiceImpl(JavaMailSender javaMailSender) {
		this.javaMailSender = javaMailSender;
	}

	public void sendEmail(Email email) throws MailException {

		SimpleMailMessage mail = new SimpleMailMessage();
		mail.setTo(email.getTo());
		mail.setFrom(email.getFrom());
		mail.setSubject(email.getSubject());
		mail.setText(email.getBody());

		javaMailSender.send(mail);
	}

	public void sendEmailWithAttachment(Email email) throws MailException, MessagingException {

		MimeMessage mimeMessage = javaMailSender.createMimeMessage();

		MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);

		helper.setTo(email.getTo());
		helper.setFrom(email.getFrom());
		helper.setSubject(email.getSubject());
		helper.setText(email.getBody());

		ClassPathResource classPathResource = new ClassPathResource(email.getAttachment());
		helper.addAttachment(classPathResource.getFilename(), classPathResource);

		javaMailSender.send(mimeMessage);
	}

	public void validateEmail(Email email) {

	}
}