package com.loyaltyrewards.repositories.treatment;


import com.loyaltyrewards.models.treatment.Treatment;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface TreatmentRepository extends PagingAndSortingRepository<Treatment, Long> {

    @Query(value = "SELECT * FROM course WHERE course_name = ?1",
            nativeQuery = true)
    Iterable<Treatment> insertTreatmentSeed();
    Treatment findByTitle(String title);
}
