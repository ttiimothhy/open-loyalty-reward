package com.loyaltyrewards.repositories.treatment;


import com.loyaltyrewards.models.treatment.Appointment;
import org.springframework.data.repository.CrudRepository;


public interface AppointmentRepository extends CrudRepository<Appointment, Long> {

}
