package com.loyaltyrewards.repositories.enquiry;

import com.loyaltyrewards.models.enquiry.Enquiry;
import org.springframework.data.repository.CrudRepository;


public interface EnquiryRepository extends CrudRepository<Enquiry, Long> {
}
