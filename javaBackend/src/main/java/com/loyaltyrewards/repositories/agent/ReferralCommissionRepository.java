package com.loyaltyrewards.repositories.agent;

import com.loyaltyrewards.models.agent.ReferralCommission;
import org.springframework.data.repository.CrudRepository;


public interface ReferralCommissionRepository extends CrudRepository<ReferralCommission, Long> {
}
