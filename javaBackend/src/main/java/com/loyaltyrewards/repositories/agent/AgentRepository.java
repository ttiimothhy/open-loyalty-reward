package com.loyaltyrewards.repositories.agent;

import com.loyaltyrewards.models.agent.Agent;
import com.loyaltyrewards.models.user.Role;
import com.loyaltyrewards.models.user.User;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;


public interface AgentRepository extends CrudRepository<Agent, Long> {
    Optional<Agent> findById(Long id); // SELECT * from agents where id = id
    Optional<Agent> findByUsers(User user);
}
