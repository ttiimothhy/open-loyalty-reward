package com.loyaltyrewards.repositories.agent;

import com.loyaltyrewards.models.agent.ColdCallHistory;
import org.springframework.data.repository.CrudRepository;


public interface ColdCallHistoryRepository extends CrudRepository<ColdCallHistory, Long> {

}
