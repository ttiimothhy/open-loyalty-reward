package com.loyaltyrewards.repositories.agent;

import com.loyaltyrewards.models.agent.AgentContact;
import org.springframework.data.repository.CrudRepository;


public interface AgentContactRepository extends CrudRepository<AgentContact, Long> {
}
