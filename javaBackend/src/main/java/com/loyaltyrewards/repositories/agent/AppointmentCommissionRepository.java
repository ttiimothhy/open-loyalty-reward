package com.loyaltyrewards.repositories.agent;

import com.loyaltyrewards.models.agent.AppointmentCommission;
import org.springframework.data.repository.CrudRepository;


public interface AppointmentCommissionRepository extends CrudRepository<AppointmentCommission, Long> {
}
