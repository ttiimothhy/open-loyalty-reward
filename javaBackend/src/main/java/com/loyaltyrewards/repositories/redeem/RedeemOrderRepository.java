package com.loyaltyrewards.repositories.redeem;


import com.loyaltyrewards.models.redeem.RedeemOrder;
import org.springframework.data.repository.CrudRepository;

public interface RedeemOrderRepository extends CrudRepository<RedeemOrder, Long> {

}
