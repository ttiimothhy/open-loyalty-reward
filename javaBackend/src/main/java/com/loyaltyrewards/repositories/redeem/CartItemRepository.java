package com.loyaltyrewards.repositories.redeem;


import com.loyaltyrewards.models.redeem.CartItem;
import com.loyaltyrewards.models.user.User;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface CartItemRepository extends CrudRepository<CartItem, Long> {

}
