package com.loyaltyrewards.repositories.redeem;


import com.loyaltyrewards.models.redeem.RedeemItem;
import org.springframework.data.repository.CrudRepository;

public interface RedeemItemRepository extends CrudRepository<RedeemItem, Long> {

}
