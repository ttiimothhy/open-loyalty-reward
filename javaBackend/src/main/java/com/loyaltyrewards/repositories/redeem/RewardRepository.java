package com.loyaltyrewards.repositories.redeem;


import com.loyaltyrewards.models.redeem.Reward;
import org.springframework.data.repository.CrudRepository;

public interface RewardRepository extends CrudRepository<Reward, Long> {

}
