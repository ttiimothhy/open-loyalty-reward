package com.loyaltyrewards.repositories.user;

import com.loyaltyrewards.models.user.Role;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface RoleRepository extends CrudRepository<Role, Long> {
    Optional<Role> findByRole(String role);

    Optional<Role> findById(Long id);
}
