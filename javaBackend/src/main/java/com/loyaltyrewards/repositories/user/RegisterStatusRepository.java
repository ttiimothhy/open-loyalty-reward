package com.loyaltyrewards.repositories.user;

import com.loyaltyrewards.models.user.RegisterStatus;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface RegisterStatusRepository extends CrudRepository<RegisterStatus, Long> {
    Optional<RegisterStatus> findByStatus(String Status);
}
