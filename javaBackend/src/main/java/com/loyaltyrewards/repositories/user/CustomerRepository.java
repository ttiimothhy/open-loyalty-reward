package com.loyaltyrewards.repositories.user;


import com.loyaltyrewards.models.user.Customer;
import com.loyaltyrewards.models.user.User;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface CustomerRepository extends CrudRepository<Customer, Long> {
    Optional<Customer> findById(Long id);
}
