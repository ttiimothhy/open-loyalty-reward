package com.loyaltyrewards.repositories.point;

import com.loyaltyrewards.models.point.ExpiredPoint;
import org.springframework.data.repository.CrudRepository;


public interface ExpiredPointRepository extends CrudRepository<ExpiredPoint, Long> {
}
