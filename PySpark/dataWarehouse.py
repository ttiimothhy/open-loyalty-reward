
#%%[markdown]
# # install necessary package
import pip
def import_or_install(package):
        try:
            __import__(package)
        except ImportError:
            pip.main(['install','--user', package])
import_or_install('findspark')
import_or_install('python-dotenv')
import_or_install('psycopg2-binary')
#%% [markdown]
# # load dotenv
from dotenv import load_dotenv
import os
load_dotenv()
DB_NAME=os.getenv('DB_NAME')
DB_USERNAME=os.getenv('DB_USERNAME')
DB_PASSWORD=os.getenv('DB_PASSWORD')
DB_HOST = os.getenv('DB_HOST') or 'localhost'
SPARK_HOME = os.getenv('SPARK_HOME')
#%% [markdown]
# # Find the spark home
import findspark
findspark.init(SPARK_HOME)
#%% [markdown]
# # Initialize Spark Session
from pyspark.sql import SparkSession
spark = SparkSession.builder\
    .appName("Loyalty Reward event stream").getOrCreate()
#%%
import os
import sys
from pyspark import SparkContext
from pyspark import SparkConf
from pyspark.sql import SQLContext
from pyspark.sql import SparkSession
from pyspark.sql import DataFrameReader
# %%
import psycopg2 as psycopg
conn = psycopg.connect(dbname=DB_NAME,user=DB_USERNAME,password=DB_PASSWORD,host=DB_HOST)
conn.autocommit = True
# Open a cursor to perform database operations
cur = conn.cursor()
#%%
branchDF = spark.read \
    .format("jdbc") \
    .option("driver", "org.postgresql.Driver") \
    .option("url", "jdbc:postgresql:loyaltyprogram") \
    .option("dbtable", "branches") \
    .option("user", "kc") \
    .option("password", "1234") \
    .load()
branchDF.show()
# %% [markdown]
# # Insert users data
def insert_data(row):
    conn = psycopg.connect(dbname=DB_NAME,user=DB_USERNAME,password=DB_PASSWORD,host=DB_HOST)
    conn.autocommit = True
    # Open a cursor to perform database operations
    cur = conn.cursor()
    # check conflict on only one column is fine
    insert_sql = "insert into branch (id,name,address,capacity,office_hour)\
        values (%(id)s,%(name)s,%(address)s,%(capacity)s,%(office_hour)s)\
        on conflict(id,name,address,capacity,office_hour)  do nothing returning id"
    cur.execute(insert_sql,row)
    return (True)
branchDF.rdd.foreach(insert_data)
#%%
genderDF = spark.read \
    .format("jdbc") \
    .option("driver", "org.postgresql.Driver") \
    .option("url", "jdbc:postgresql:loyaltyprogram") \
    .option("dbtable", "gender") \
    .option("user", "kc") \
    .option("password", "1234") \
    .load()
genderDF.show()
# %% [markdown]
# # Insert users data
def insert_data(row):
    conn = psycopg.connect(dbname=DB_NAME,user=DB_USERNAME,password=DB_PASSWORD,host=DB_HOST)
    conn.autocommit = True
    # Open a cursor to perform database operations
    cur = conn.cursor()
    insert_sql = "insert into gender (id,gender)\
        values (%(id)s,%(gender)s)\
        on conflict(id,gender)  do nothing returning id"
    cur.execute(insert_sql,row)
    return (True)
genderDF.rdd.foreach(insert_data)
#%%
scoreDF = spark.read \
    .format("jdbc") \
    .option("driver", "org.postgresql.Driver") \
    .option("url", "jdbc:postgresql:loyaltyprogram") \
    .option("dbtable", "scores") \
    .option("user", "kc") \
    .option("password", "1234") \
    .load()
scoreDF.show()
# %% [markdown]
# # Insert users data
def insert_data(row):
    conn = psycopg.connect(dbname=DB_NAME,user=DB_USERNAME,password=DB_PASSWORD,host=DB_HOST)
    conn.autocommit = True
    # Open a cursor to perform database operations
    cur = conn.cursor()
    insert_sql = "insert into score (id,score,appointment_id,treatments_id)\
        values (%(id)s,%(score)s,%(appointment_id)s,%(treatments_id)s)\
        on conflict(id)  do nothing returning id"
    cur.execute(insert_sql,row)
    return (True)
scoreDF.rdd.foreach(insert_data)
#%%
usersDF = spark.read \
    .format("jdbc") \
    .option("driver", "org.postgresql.Driver") \
    .option("url", "jdbc:postgresql:loyaltyprogram") \
    .option("dbtable", "users") \
    .option("user", "kc") \
    .option("password", "1234") \
    .load()
usersDF.show()
# %% [markdown]
# # Insert users data
def insert_data(row):
    conn = psycopg.connect(dbname=DB_NAME,user=DB_USERNAME,password=DB_PASSWORD,host=DB_HOST)
    conn.autocommit = True
    # Open a cursor to perform database operations
    cur = conn.cursor()
    insert_sql = "insert into users (id,date_of_birth,gender)\
        values (%(id)s,%(date_of_birth)s,\
        (select gender from gender where id = %(gender_id)s limit 1))\
        on conflict(id,date_of_birth,gender)  do nothing returning id"
    cur.execute(insert_sql,row)
    return (True)
usersDF.rdd.foreach(insert_data)
#%%
customersDF = spark.read \
    .format("jdbc") \
    .option("driver", "org.postgresql.Driver") \
    .option("url", "jdbc:postgresql:loyaltyprogram") \
    .option("dbtable", "customers") \
    .option("user", "kc") \
    .option("password", "1234") \
    .load()
customersDF.show()
# %% [markdown]
# # Insert users data
def insert_data(row):
    conn = psycopg.connect(dbname=DB_NAME,user=DB_USERNAME,password=DB_PASSWORD,host=DB_HOST)
    conn.autocommit = True
    # Open a cursor to perform database operations
    cur = conn.cursor()
    insert_sql = "insert into customer (id,date_of_birth,gender) \
        values (%(id)s,(select date_of_birth from users where id = %(user_id)s limit 1),(select gender from users where id = %(user_id)s limit 1)) \
        on conflict(id,date_of_birth,gender)  do nothing returning id"
    cur.execute(insert_sql,row)
    return (True)
customersDF.rdd.foreach(insert_data)
#%%
treatmentsDF = spark.read \
    .format("jdbc") \
    .option("driver", "org.postgresql.Driver") \
    .option("url", "jdbc:postgresql:loyaltyprogram") \
    .option("dbtable", "treatments") \
    .option("user", "kc") \
    .option("password", "1234") \
    .load()
treatmentsDF.show()
# %% [markdown]
# # Insert users data
def insert_data(row):
    conn = psycopg.connect(dbname=DB_NAME,user=DB_USERNAME,password=DB_PASSWORD,host=DB_HOST)
    conn.autocommit = True
    # Open a cursor to perform database operations
    cur = conn.cursor()
    insert_user_sql = "insert into treatment (id,detail,title,default_score)\
        values (%(id)s,%(detail)s,%(title)s,%(default_score)s)\
        on conflict(id)  do update set detail=%(detail)s,title=%(title)s,default_score=%(default_score)s returning id"
    cur.execute(insert_user_sql,row)
    return (True)
treatmentsDF.rdd.foreach(insert_data)
#%%
appointed_earned_pointDF = spark.read \
    .format("jdbc") \
    .option("driver", "org.postgresql.Driver") \
    .option("url", "jdbc:postgresql:loyaltyprogram") \
    .option("dbtable", "earned_point") \
    .option("user", "kc") \
    .option("password", "1234") \
    .load()
appointed_earned_pointDF.show()
# %% [markdown]
# # Insert users data
def insert_data(row):
    conn = psycopg.connect(dbname=DB_NAME,user=DB_USERNAME,password=DB_PASSWORD,host=DB_HOST)
    conn.autocommit = True
    # Open a cursor to perform database operations
    cur = conn.cursor()
    insert_user_sql = "insert into appointed_earned_point (id,point,customer_id,appointment_id)\
        values (%(id)s,%(points)s,%(customer_id)s,%(appointment_id)s)\
        on conflict(id)  do update set point=%(points)s,customer_id=%(customer_id)s,appointment_id=%(appointment_id)s returning id"
    cur.execute(insert_user_sql,row)
    return (True)
appointed_earned_pointDF.rdd.foreach(insert_data)
# %%
appointment_commissionDF = spark.read \
    .format("jdbc") \
    .option("driver", "org.postgresql.Driver") \
    .option("url", "jdbc:postgresql:loyaltyprogram") \
    .option("dbtable", "appointment_commission") \
    .option("user", "kc") \
    .option("password", "1234") \
    .load()
appointment_commissionDF.show()
# %% [markdown]
# # Insert users data
def insert_data(row):
    conn = psycopg.connect(dbname=DB_NAME,user=DB_USERNAME,password=DB_PASSWORD,host=DB_HOST)
    conn.autocommit = True
    # Open a cursor to perform database operations
    cur = conn.cursor()
    insert_user_sql = "insert into appointed_commission (id,points,agent_id,appointment_id)\
        values (%(id)s,%(points)s,%(agent_id)s,%(appointment_id)s)\
        on conflict(id) do update set points=%(points)s,agent_id=%(agent_id)s,appointment_id=%(appointment_id)s returning id"
    cur.execute(insert_user_sql,row)
    return (True)
appointment_commissionDF.rdd.foreach(insert_data)
#%%
fact_appointmentsDF = spark.read \
    .format("jdbc") \
    .option("driver", "org.postgresql.Driver") \
    .option("url", "jdbc:postgresql:loyaltyprogram") \
    .option("dbtable", "appointments") \
    .option("user", "kc") \
    .option("password", "1234") \
    .load()
fact_appointmentsDF.show()
# %% [markdown]
# # Insert users data
def insert_data_appointment_commission(row):
    conn = psycopg.connect(dbname=DB_NAME,user=DB_USERNAME,password=DB_PASSWORD,host=DB_HOST)
    conn.autocommit = True
    # Open a cursor to perform database operations
    cur = conn.cursor()
    insert_user_sql = "insert into fact_appointments (id,treatment_id,branch_id,customer_id,score_id,appointed_earned_point_id,appointed_commission_id) \
        values (%(id)s,%(treatment_id)s,%(branch_id)s,%(customer_id)s,\
            (select id from score where appointment_id = %(id)s),\
            (select id from appointed_earned_point where appointment_id = %(id)s),\
            (select id from appointed_commission where appointment_id = %(id)s)) \
        on conflict(id)  do update set \
            treatment_id=%(treatment_id)s,\
            branch_id=%(branch_id)s,\
            customer_id=%(customer_id)s,\
            score_id=(select id from score where appointment_id = %(id)s),\
            appointed_earned_point_id=(select id from appointed_earned_point where appointment_id = %(id)s),\
            appointed_commission_id=(select id from appointed_commission where appointment_id = %(id)s) \
                returning id"
    cur.execute(insert_user_sql,row)
    return (True)
fact_appointmentsDF.rdd.foreach(insert_data_appointment_commission)
# %%
purchased_itemDF = spark.read \
    .format("jdbc") \
    .option("driver", "org.postgresql.Driver") \
    .option("url", "jdbc:postgresql:loyaltyprogram") \
    .option("dbtable", "purchased_item") \
    .option("user", "kc") \
    .option("password", "1234") \
    .load()
purchased_itemDF.show()
# %% [markdown]
# # Insert users data
def insert_data(row):
    conn = psycopg.connect(dbname=DB_NAME,user=DB_USERNAME,password=DB_PASSWORD,host=DB_HOST)
    conn.autocommit = True
    # Open a cursor to perform database operations
    cur = conn.cursor()
    insert_user_sql = "insert into purchase_info (id,purchase_amount,benefit_amount,treatment_id,customer_id) \
        values (%(id)s,%(purchase_amount)s,%(benefit_amount)s,%(treatment_id)s,%(customer_id)s) \
        on conflict(id) do update set purchase_amount=%(purchase_amount)s,benefit_amount=%(benefit_amount)s,treatment_id=%(treatment_id)s,customer_id=%(customer_id)s returning id"
    cur.execute(insert_user_sql,row)
    return (True)
purchased_itemDF.rdd.foreach(insert_data)
#%%
fact_purchaseDF = spark.read \
    .format("jdbc") \
    .option("driver", "org.postgresql.Driver") \
    .option("url", "jdbc:postgresql:loyaltyprogram") \
    .option("dbtable", "purchased_item") \
    .option("user", "kc") \
    .option("password", "1234") \
    .load()
fact_purchaseDF.show()
# %% [markdown]
# # Insert users data
def insert_data(row):
    conn = psycopg.connect(dbname=DB_NAME,user=DB_USERNAME,password=DB_PASSWORD,host=DB_HOST)
    conn.autocommit = True
    # Open a cursor to perform database operations
    cur = conn.cursor()
    insert_user_sql = "insert into fact_purchase (id,purchase_info_id,customer_id,treatment_id) \
        values (%(id)s,%(id)s,%(customer_id)s,%(treatment_id)s) \
        on conflict(id)  do update set \
            customer_id=%(customer_id)s,\
            treatment_id=%(treatment_id)s \
            returning id"
    cur.execute(insert_user_sql,row)
    return (True)
fact_purchaseDF.rdd.foreach(insert_data)
# %%
appointmentDF = spark.read \
    .format("jdbc") \
    .option("driver", "org.postgresql.Driver") \
    .option("url", "jdbc:postgresql:loyaltyprogram") \
    .option("dbtable", "appointments") \
    .option("user", "kc") \
    .option("password", "1234") \
    .load()
appointmentDF.show()
#%% [markdown]
customer_treatment_DF = appointmentDF.groupBy("customer_id").pivot("treatment_id").count()
customer_treatment_DF.show()
#%% [markdown]
No_of_treatment = len(customer_treatment_DF.columns)-1
create_sql ='''CREATE TABLE python_user (
    id SERIAL PRIMARY KEY,
    date_of_birth DATE,
    gender VARCHAR(255),
'''
for x in range(0, No_of_treatment-1):
    create_sql=create_sql + '''treatment'''+str(x+1)+''' INT,'''
create_sql=create_sql + '''treatment'''+str(x+2)+''' INT'''
create_sql =create_sql+''')'''
print (create_sql)
#%%
update_sql='''update python_user set '''
for x in range(0, No_of_treatment-1):
    update_sql=update_sql +  "treatment"+str(x+1)+ "=%("+ str(x+1) +")s" + ","
update_sql=update_sql + "treatment"+str(x+2)+ "=%("+ str(x+2) +")s "
update_sql =update_sql+  "WHERE id =%" + "(customer_id)s"
print (update_sql)
#%%
def drop_create():
    conn = psycopg.connect(dbname=DB_NAME,user=DB_USERNAME,password=DB_PASSWORD,host=DB_HOST)
    conn.autocommit = True
    # Open a cursor to perform database operations
    cur = conn.cursor()
    cur.execute("DROP TABLE IF EXISTS python_user")
    cur.execute(create_sql)
drop_create()
#%%
def insert_data(row):
    conn = psycopg.connect(dbname=DB_NAME,user=DB_USERNAME,password=DB_PASSWORD,host=DB_HOST)
    conn.autocommit = True
    # Open a cursor to perform database operations
    cur = conn.cursor()
    insert_sql = "insert into python_user (id,date_of_birth,gender)\
        values (%(id)s,%(date_of_birth)s,\
        (select gender from gender where id = %(gender_id)s limit 1))\
        on conflict(id)  do nothing returning id"
    cur.execute(insert_sql,row)
    return (True)
usersDF.rdd.foreach(insert_data)
#%% [markdown]
def insert_data(row):
    conn = psycopg.connect(dbname=DB_NAME,user=DB_USERNAME,password=DB_PASSWORD,host=DB_HOST)
    conn.autocommit = True
    # Open a cursor to perform database operations
    cur = conn.cursor()
    cur.execute(update_sql,row)
    return (True)
customer_treatment_DF.rdd.foreach(insert_data)
#%%

# Analysis should be in different file
python_userDF = spark.read \
    .format("jdbc") \
    .option("driver", "org.postgresql.Driver") \
    .option("url", "jdbc:postgresql://loyaltyprogramdw.ch6vljvitgnc.ap-southeast-1.rds.amazonaws.com:5432/loyaltyprogramdw") \
    .option("dbtable", "python_user") \
    .option("user", "postgres") \
    .option("password", "postgres96483571") \
    .load()
python_userDF.show()
#%%
No_of_python_userDF = python_userDF.select("id")
No_of_python_userDF.show()
#%%
import random
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans
from sklearn.datasets.samples_generator import make_blobs
#%%
# np.random.seed(0)
# X, y = make_blobs(n_samples=5000, centers=[[4,4], [-2, -1], [2, -3], [1, 1]], cluster_std=0.9)
# plt.scatter(X[:, 0], X[:, 1], marker='.')
#%%
df = python_userDF.na.fill(0)
print(python_userDF.show())
print(df.show())
#%%
df = df.drop('date_of_birth')
df = df.drop('gender')
df = df.drop('id')
print(df.show())
df=np.array(df.collect())
print(df)
#%%
k_means = KMeans(init = "k-means++", n_clusters = 4, n_init = 12)
k_means.fit(df)
k_means_labels = k_means.labels_
k_means_labels
k_means_cluster_centers = k_means.cluster_centers_
k_means_cluster_centers
print(k_means_cluster_centers)
# k_means.predict()
#%% [markdown]
# print(df[0])
#%%
current_sample = df[100].reshape(1,-1)
predicted_label = k_means.predict(current_sample)
print(predicted_label)
#%%
from dotenv import load_dotenv
import os
load_dotenv()
DB_NAME_LOCAL=os.getenv('DB_NAME_LOCAL')
DB_USERNAME_LOCAL=os.getenv('DB_USERNAME_LOCAL')
DB_PASSWORD_LOCAL=os.getenv('DB_PASSWORD_LOCAL')
DB_HOST_LOCAL = os.getenv('DB_HOST_LOCAL') or 'localhost'
#%%
No_of_user = customersDF.count()
for x in range(0, No_of_user):
    customer_id = No_of_python_userDF.take(No_of_user)[x].asDict()['id']
    current_sample = df[x].reshape(1,-1)
    predicted_label = k_means.predict(current_sample)
    conn = psycopg.connect(dbname=DB_NAME_LOCAL,user=DB_USERNAME_LOCAL,password=DB_PASSWORD_LOCAL,host="localhost")
    conn.autocommit = True
    # Open a cursor to perform database operations
    cur = conn.cursor()
    insert_user_sql = "insert into python_group (customer_id,grouping)\
        values ("+str(customer_id)+","+str(predicted_label[0])+")\
        on conflict(customer_id)  do update set customer_id="+str(customer_id)+",grouping="+str(predicted_label[0])+" returning id"
    # print(insert_user_sql)
    cur.execute(insert_user_sql)















# %%


# TBC
#  # %%
# appointmentDF = spark.read \
#     .format("jdbc") \
#     .option("driver", "org.postgresql.Driver") \
#     .option("url", "jdbc:postgresql:loyaltyprogram") \
#     .option("dbtable", "appointments") \
#     .option("user", "kc") \
#     .option("password", "1234") \
#     .load()
# appointmentDF.show()

# # %% [markdown]
# # # Insert users data
# def insert_data(row):
#     conn = psycopg.connect(dbname=DB_NAME,user=DB_USERNAME,password=DB_PASSWORD,host=DB_HOST)
#     conn.autocommit = True
#     # Open a cursor to perform database operations
#     cur = conn.cursor()
#     insert_user_sql = "insert into appointment (id,timeslot,branch_name) \
#         values (%(id)s,%(timeslot)s,(select name from branch where id = %(branch_id)s)) \
#         on conflict(id) do update set timeslot=%(timeslot)s,branch_name=(select name from branch where id = %(branch_id)s) returning id"
#     cur.execute(insert_user_sql,row)
#     return (True)
# appointmentDF.rdd.foreach(insert_data)