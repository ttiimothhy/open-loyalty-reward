from datawarehouse import schedule_runner 
import schedule
import time

def job():
    schedule_runner()

# schedule.every(10).minutes.do(job)
# schedule.every().hour.do(job)
schedule.every().day.at("02:00").do(job)

while 1:
    schedule.run_pending()
    time.sleep(1)