import {createBottomTabNavigator} from "@react-navigation/bottom-tabs";
import {NavigationContainer, useNavigation} from "@react-navigation/native";
import {createStackNavigator} from "@react-navigation/stack";
import React,{useState} from "react";
import {Provider,useDispatch,useSelector} from "react-redux";
import HomeScreen from "./src/HomeScreen";
import LoadingScreen from "./src/LoadingScreen";
import LoginScreen from "./src/LoginScreen";
import RegisterScreen from "./src/RegisterScreen";
import ResetPasswordScreen from "./src/ResetPasswordScreen";
import {IRootState,store} from "./store";
import Ionicon from "react-native-vector-icons/Ionicons";
import MaterialCommunityIcon from "react-native-vector-icons/MaterialCommunityIcons";
import {TextInput,TouchableOpacity,Text,View} from "react-native";
import MaterialIcon from "react-native-vector-icons/MaterialIcons";
import {styles} from "./src/styles/AppStyles";
import FontAwesome5 from "react-native-vector-icons/FontAwesome5";
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import MyAppointmentsScreen from "./src/MyAppointmentsScreen";
import AvailableAppointmentsScreen from "./src/AvailableAppointmentsScreen";
import AppointmentDetailsScreen from "./src/AppointmentDetailsScreen";
import CustomerProfileScreen from "./src/CustomerProfileScreen";
import FinishAppointmentScreen from "./src/FinishAppointmentScreen";
import GiftScreen from "./src/GiftScreen";
import GiftDetailsScreen from "./src/GiftDetailsScreen";
import MyPointScreen from "./src/MyPointScreen";
import RedeemHistoryScreen from "./src/RedeemHistoryScreen";
import RedeemHistoryDetailsScreen from "./src/RedeemHistoryDetailsScreen";
import RedeemCompletedScreen from "./src/RedeemCompletedScreen";
import GiftCartScreen from "./src/GiftCartScreen";
import ConfirmRedemptionScreen from "./src/ConfirmRedemptionScreen";
import SearchScreen from "./src/SearchScreen";
import AgentLoginScreen from "./src/AgentLoginScreen";
import AgentResetPasswordScreen from "./src/AgentResetPasswordScreen";
import searchHeader from "./src/components/SearchHeader";
import {AgentMainPackage} from "./Agent";
import AppointmentScoringScreen from "./src/AppointmentScoringScreen";
import AlreadyAppointmentDetailsScreen from "./src/AlreadyAppointmentDetailsScreen";
import FinishScoringScreen from "./src/FinishScoringScreen";
import {library} from "@fortawesome/fontawesome-svg-core";
import {faApple,fab} from "@fortawesome/free-brands-svg-icons";
import {far} from "@fortawesome/free-regular-svg-icons"
import {fas,faTimes,faSortNumericUp,faSortNumericDownAlt,faFilter,faTrashRestore,faCheckDouble,faPlus,faStar} from
"@fortawesome/free-solid-svg-icons"
import AdminChangePictureScreen from "./src/AdminChangePictureScreen";
import {searchKeyword} from "./src/redux/searchHeader/thunks";
import {updateSearchWord} from "./src/redux/searchHeader/actions";
import TreatmentPurchasesScreen from "./src/TreatmentPurchaseScreen";
import AvailablePurchasesScreen from "./src/AvailablePurchasesScreen";
import FinishTreatmentPurchaseScreen from "./src/FinishTreatmentPurchaseScreen";
import PurchasedTreatmentDetailsScreen from "./src/PurchasedTreatmentDetailsScreen";
import SecurityCodeScreen from "./src/SecurityCodeScreen";

library.add(fab,fas,far,faTimes,faSortNumericUp,faSortNumericDownAlt,faFilter,faCheckDouble,faTrashRestore,faPlus,faStar,
faApple);

const AppStack = createStackNavigator();
const LoginStack = createStackNavigator();
const MainStack = createStackNavigator();
const MainTab = createBottomTabNavigator();
const HomeStack = createStackNavigator();
const MyAppointmentsStack = createStackNavigator();
const TreatmentPurchaseStack = createStackNavigator();
const AvailableAppointmentsStack = createStackNavigator();
const GiftStack = createStackNavigator();
const ResetPasswordStack = createStackNavigator();
const RoleLoginStack = createStackNavigator();
const CustomerProfileStack = createStackNavigator();
const RegisterStack = createStackNavigator();


export function CustomerProfilePackage(){
	return(
		<CustomerProfileStack.Navigator initialRouteName="CustomerProfileScreen" screenOptions={({
			headerBackTitleVisible:false,
			headerStyle:{
				height:100, // Specify the height of your custom header
			},
			headerTitleAlign:"left",
			headerTitleStyle:{
				fontSize:24
			},
			headerRight:() => searchHeader(),
			title:"用戶設定"
		})}>
			<CustomerProfileStack.Screen name="CustomerProfileScreen" component={CustomerProfileScreen}/>
		</CustomerProfileStack.Navigator>
	)
}

function GiftPackage(){
	const [search,setSearch] = useState(false);
	const navigation = useNavigation();
	const dispatch = useDispatch();
	const searchWord = useSelector((state:IRootState)=>state.searchHeader.searchWord);
	const isAuthenticated = useSelector((state:IRootState)=>state.auth.isAuthenticated);
	const isAuthenticatedAgent = useSelector((state:IRootState)=>state.agentAuth.isAuthenticated);

	return(
		<GiftStack.Navigator initialRouteName="GiftScreen" screenOptions={({
			gestureEnabled:false
		})}>
			<GiftStack.Screen name="GiftScreen" component={GiftScreen}
				options={({
					headerLeft:() => null,
					headerStyle:{
						height:100, // Specify the height of your custom header
					},
					headerTitleAlign:"left",
					headerTitleStyle:{
						fontSize:24
					},
					headerRight:() => (
						<View style={styles.iconContainer}>
							{search && <View style={styles.smallSearch}>
								<TouchableOpacity onPress={() => {
									setSearch(false)
									dispatch(updateSearchWord(""))
								}}>
									<FontAwesomeIcon size={28} color={"#cccccc"} icon="times"/>
								</TouchableOpacity>
								<View style={styles.searchSmallInputWidth}>
									<TextInput style={styles.searchInput} onChangeText={(event)=>{
										dispatch(updateSearchWord(event))
									}}/>
								</View>
								<TouchableOpacity onPress={() => {
									setSearch(false)
									navigation.navigate("SearchScreen")
									dispatch(searchKeyword(searchWord,isAuthenticated,isAuthenticatedAgent))
								}}>
									<FontAwesome5 size={26} color={"#11bb00"} name="searchengin"/>
								</TouchableOpacity>
							</View>}
							{!search && <TouchableOpacity onPress={() => {
								setSearch(true)
							}}>
								<Ionicon style={styles.headerIcon} size={26} name="search-sharp"/>
							</TouchableOpacity>}
							<TouchableOpacity style={styles.headerContainer} onPress={() => {
								navigation.navigate("GiftCartScreen")
							}}>
								<Ionicon name="cart-outline" style={styles.headerCartIcon}/>
							</TouchableOpacity>
							<MaterialIcon size={28} name="notifications-none"/>
						</View>
					),
					title:"兌換禮物"
				})}
			/>
			<GiftStack.Screen name="MyPointScreen" component={MyPointScreen}
				options={({
					headerBackTitleVisible:false,
					headerStyle:{
						height:100, // Specify the height of your custom header
					},
					headerTitleStyle:{
						fontSize:20
					},
					headerRight:() => (
						<View style={styles.iconContainer}>
							<MaterialIcon size={28} name="notifications-none"/>
						</View>
					),
					title:"我的積分",
				})}
			/>
			<GiftStack.Screen name="RedeemHistoryScreen" component={RedeemHistoryScreen}
				options={({
					headerBackTitleVisible:false,
					headerStyle:{
						height:100, // Specify the height of your custom header
					},
					headerTitleStyle:{
						fontSize:20
					},
					headerRight:() => (
						<View style={styles.iconContainer}>
							<MaterialIcon size={28} name="notifications-none"/>
						</View>
					),
					title:"兌換歷史",
				})}
			/>
			<GiftStack.Screen name="RedeemHistoryDetailsScreen" component={RedeemHistoryDetailsScreen}
				options={({
					headerBackTitleVisible:false,
					headerStyle:{
						height:100, // Specify the height of your custom header
					},
					headerTitleStyle:{
						fontSize:20
					},
					headerRight:() => (
						<View style={styles.iconContainer}>
							<MaterialIcon size={28} name="notifications-none"/>
						</View>
					),
					title:"兌換詳情",
				})}
			/>
			<GiftStack.Screen name="GiftCartScreen" component={GiftCartScreen}
				options={({
					headerLeft:() => null,
					headerStyle:{
						height:100, // Specify the height of your custom header
					},
					headerTitleStyle:{
						fontSize:24
					},
					headerTitleAlign:"left",
					headerRight:() => searchHeader(),
					title:"禮品車",
				})}
			/>
			<GiftStack.Screen name="GiftDetailsScreen" component={GiftDetailsScreen}
				options={({
					headerLeft:() => null,
					headerStyle:{
						height:100, // Specify the height of your custom header
					},
					headerTitleStyle:{
						fontSize:20
					},
					headerRight:() => (
						<View style={styles.iconContainer}>
							<MaterialIcon size={28} name="notifications-none"/>
						</View>
					),
					title:"禮品詳情",
				})}
			/>
			<GiftStack.Screen name="ConfirmRedemptionScreen" component={ConfirmRedemptionScreen}
				options={({
					headerLeft:() => null,
					headerStyle:{
						height:100, // Specify the height of your custom header
					},
					headerTitleStyle:{
						fontSize:24
					},
					headerTitleAlign:"left",
					headerRight:() => (
						<View style={styles.iconContainer}>
							<MaterialIcon size={28} name="notifications-none"/>
						</View>
					),
					title:"確認換領",
				})}
			/>
		</GiftStack.Navigator>
	)
}

function TreatmentPurchasePackage(){
	return(
		<TreatmentPurchaseStack.Navigator initialRouteName="AvailablePurchasesScreen" screenOptions={({
			headerLeft:() => null,
			gestureEnabled:false
		})}>
			<CustomerProfileStack.Screen name="AvailablePurchasesScreen" component={AvailablePurchasesScreen}
				options={({
					headerStyle:{
						height:100, // Specify the height of your custom header
					},
					headerTitleAlign:"left",
					headerTitleStyle:{
						fontSize:24
					},
					headerRight:() => searchHeader(),
					title:"可購買療程"
				})}
			/>
			<CustomerProfileStack.Screen name="TreatmentPurchasesScreen" component={TreatmentPurchasesScreen}
				options={({
					headerStyle:{
						height:100, // Specify the height of your custom header
					},
					headerTitleStyle:{
						fontSize:20
					},
					headerRight:() => (
						<View style={styles.iconContainer}>
							<MaterialIcon size={28} name="notifications-none"/>
						</View>
					),
					title:"購買詳情",
				})}
			/>
		</TreatmentPurchaseStack.Navigator>
	)
}

function AvailableAppointmentsPackage(){
	return(
		<AvailableAppointmentsStack.Navigator initialRouteName="AvailableAppointmentsScreen" screenOptions={({
			headerLeft:() => null,
			gestureEnabled:false
		})}>
			<AvailableAppointmentsStack.Screen name="AvailableAppointmentsScreen" component={AvailableAppointmentsScreen}
				options={({
					headerStyle:{
						height:100, // Specify the height of your custom header
					},
					headerTitleAlign:"left",
					headerTitleStyle:{
						fontSize:24
					},
					headerRight:() => searchHeader(),
					title:"可預約療程"
				})}
			/>
			<AvailableAppointmentsStack.Screen name="AppointmentDetailsScreen" component={AppointmentDetailsScreen}
				options={({
					headerStyle:{
						height:100, // Specify the height of your custom header
					},
					headerTitleStyle:{
						fontSize:20
					},
					headerRight:() => (
						<View style={styles.iconContainer}>
							<MaterialIcon size={28} name="notifications-none"/>
						</View>
					),
					title:"預約詳情",
				})}
			/>
		</AvailableAppointmentsStack.Navigator>
	)
}

function MyAppointmentsPackage(){
	// const search = false;
	return(
		<MyAppointmentsStack.Navigator initialRouteName="MyAppointmentsScreen" screenOptions={({
			headerLeft:() => null,
			gestureEnabled:false
		})}>
			<MyAppointmentsStack.Screen name="MyAppointmentsScreen" component={MyAppointmentsScreen} options={{
				headerStyle:{
					height:100, // Specify the height of your custom header
				},
				headerTitleAlign:"left",
				headerTitleStyle:{
					fontSize:24
				},
				headerRight:() => searchHeader(),
				title:"我的療程"
			}}/>
			<MyAppointmentsStack.Screen name="AppointmentScoringScreen" component={AppointmentScoringScreen} options={{
				headerStyle:{
					height:100, // Specify the height of your custom header
				},
				headerTitleStyle:{
					fontSize:20
				},
				headerRight:() => searchHeader(),
				title:"療程評分"
			}}/>
			<MyAppointmentsStack.Screen name="AlreadyAppointmentDetailsScreen" component={AlreadyAppointmentDetailsScreen}
				options={({
					headerStyle:{
						height:100, // Specify the height of your custom header
					},
					headerTitleStyle:{
						fontSize:20
					},
					headerRight:() => (
						<View style={styles.iconContainer}>
							<MaterialIcon size={28} name="notifications-none"/>
						</View>
					),
					title:"已預約詳情",
				})}
			/>
			<MyAppointmentsStack.Screen name="PurchasedTreatmentDetailsScreen" component={PurchasedTreatmentDetailsScreen}
				options={({
					headerStyle:{
						height:100, // Specify the height of your custom header
					},
					headerTitleStyle:{
						fontSize:20
					},
					headerRight:() => (
						<View style={styles.iconContainer}>
							<MaterialIcon size={28} name="notifications-none"/>
						</View>
					),
					title:"已賺買的療程詳情",
				})}
			/>
		</MyAppointmentsStack.Navigator>
	)
}

function HomePackage(){
	const [search,setSearch] = useState(false);
	const navigation = useNavigation();
	const dispatch = useDispatch();
	const searchWord = useSelector((state:IRootState)=>state.searchHeader.searchWord);
	const isAuthenticated = useSelector((state:IRootState)=>state.auth.isAuthenticated);
	const isAuthenticatedAgent = useSelector((state:IRootState)=>state.agentAuth.isAuthenticated);
	return(
		<HomeStack.Navigator initialRouteName="HomeScreen" screenOptions={({
			headerLeft:() => null,
			gestureEnabled:false
		})}>
			<HomeStack.Screen name="HomeScreen" component={HomeScreen} options={{
				headerStyle:{
					height:100, // Specify the height of your custom header
				},
				headerTitleAlign:"left",
				headerTitleStyle:{
					fontSize:24
				},
				headerRight:() =>
				// searchHeader(),
				(
					<View style={styles.iconContainer}>
						{search && <View style={styles.smallSearch}>
							<TouchableOpacity onPress={() => {
								setSearch(false)
								dispatch(updateSearchWord(""))
							}}>
								<FontAwesomeIcon size={28} color={"#cccccc"} icon="times"/>
							</TouchableOpacity>
							<View style={styles.searchSmallInputWidth}>
								<TextInput style={styles.searchInput} onChangeText={(event)=>{
									dispatch(updateSearchWord(event))
								}}/>
							</View>
							<TouchableOpacity onPress={() => {
								setSearch(false)
								navigation.navigate("SearchScreen")
								dispatch(searchKeyword(searchWord,isAuthenticated,isAuthenticatedAgent))
							}}>
								<FontAwesome5 size={26} color={"#11bb00"} name="searchengin"/>
							</TouchableOpacity>
						</View>}
						{!search && <TouchableOpacity onPress={() => {
							setSearch(true)
						}}>
							<Ionicon style={styles.headerIcon} size={26} name="search-sharp"/>
						</TouchableOpacity>}
						<TouchableOpacity style={styles.headerContainer} onPress={() => {
							navigation.navigate("AdminChangePictureScreen")
						}}>
							<Ionicon name="camera" size={30}/>
						</TouchableOpacity>
						<MaterialIcon size={28} name="notifications-none"/>
					</View>
				),
				title:"首頁"
			}}/>
			<HomeStack.Screen name="AdminChangePictureScreen" component={AdminChangePictureScreen} options={{
				headerStyle:{
					height:100, // Specify the height of your custom header
				},
				headerTitleStyle:{
					fontSize:20
				},
				headerRight:() => searchHeader(),
				title:"上傳相片"
			}}/>
		</HomeStack.Navigator>
	)
}

function MainMinorPackage(){
	return(
		<MainTab.Navigator tabBarOptions={{
			activeTintColor:"#ff0000",
			inactiveTintColor:"#a6a6a6",
		}}
		screenOptions={({route}) => ({
			tabBarIcon:({focused,color,size}) => {
				let iconName;
				let materialCommunityIconName;
				let materialIconName;
				if(route.name === "Home"){
					iconName = focused ? "home" : "home-outline";
				}
				else if(route.name === "MyAppointments"){
					materialCommunityIconName = focused ? "clock" : "clock-outline";
				}
				else if(route.name === "AvailableAppointments"){
					materialCommunityIconName = focused ? "emoticon-happy" : "emoticon-happy-outline";
				}
				else if(route.name === "TreatmentPurchase"){
					materialIconName = focused ? "payment" : "payment";
				}
				else if(route.name === "Gift"){
					iconName = focused ? "gift" : "gift-outline";
				}
				else if(route.name === "CustomerProfile"){
					iconName = focused ? "settings" : "settings-outline";
				}
				// You can return any component that you like here
				return iconName ? <Ionicon name={iconName} size={28} color={color}/> :
				materialCommunityIconName ?
				<MaterialCommunityIcon name={materialCommunityIconName} size={28} color={color}/> :
				materialIconName && <MaterialIcon name={materialIconName} size={28} color={color}/>
			}
		})}>
			<MainTab.Screen name="Home" component={HomePackage} options={{title:"首頁"}}/>
			<MainTab.Screen name="MyAppointments" component={MyAppointmentsPackage} options={{title:"我的預約"}}/>
			<MainTab.Screen name="AvailableAppointments" component={AvailableAppointmentsPackage} options={{
				title:"預約療程",
				// tabBarBadge:3,
			}}/>
			<MainTab.Screen name="TreatmentPurchase" component={TreatmentPurchasePackage} options={{title:"購買療程"}}/>
			<MainTab.Screen name="Gift" component={GiftPackage} options={{title:"禮物"}}/>
			<MainTab.Screen name="CustomerProfile" component={CustomerProfilePackage} options={{title:"設定"}}/>
		</MainTab.Navigator>
	)
}

function MainPackage(){
	return(
		<MainStack.Navigator screenOptions={{gestureEnabled:false}}>
			<MainStack.Screen name="MainMinor" component={MainMinorPackage} options={{
				headerShown: false
			}}/>
			<MainStack.Screen name="FinishAppointmentScreen" component={FinishAppointmentScreen} options={{
				headerShown: false
			}}/>
			<MainStack.Screen name="RedeemCompletedScreen" component={RedeemCompletedScreen} options={{
				headerShown: false
			}}/>
			<MainStack.Screen name="FinishScoringScreen" component={FinishScoringScreen} options={{
				headerShown: false
			}}/>
			<MainStack.Screen name="FinishTreatmentPurchaseScreen" component={FinishTreatmentPurchaseScreen} options={{
				headerShown: false
			}}/>
			<MainStack.Screen name="SearchScreen" component={SearchScreen} options={({
				headerBackTitleVisible:false,
				headerStyle:{
					height:100, // Specify the height of your custom header
				},
				headerTitleStyle:{
					fontSize:20
				},
				headerRight:() => (
					<View style={styles.iconContainer}>
						<MaterialIcon size={28} name="notifications-none"/>
					</View>
				),
				title:"搜索結果",
			})}/>
		</MainStack.Navigator>
	)
}

function ResetPasswordPackage(){
	const role = useSelector((state:IRootState)=>state.role.role);
	return(
		<ResetPasswordStack.Navigator headerMode="none" screenOptions={{
			gestureEnabled:false
		}}>
			{role === "customer" ?
			<ResetPasswordStack.Screen name="ResetPasswordScreen" component={ResetPasswordScreen}/> :
			role === "agent" &&
			<ResetPasswordStack.Screen name="AgentResetPasswordScreen" component={AgentResetPasswordScreen}/>
			}
		</ResetPasswordStack.Navigator>
	)
}

export function RegisterPackage(){
	return(
		<RegisterStack.Navigator headerMode="none" screenOptions={{
			gestureEnabled:false
		}}>
			<RegisterStack.Screen name="RegisterScreen" component={RegisterScreen}/>
			<RegisterStack.Screen name="SecurityCodeScreen" component={SecurityCodeScreen}/>
		</RegisterStack.Navigator>
	)
}

function RoleLoginPackage(){
	const role = useSelector((state:IRootState)=>state.role.role);
	return(
		<RoleLoginStack.Navigator headerMode="none" screenOptions={{
			gestureEnabled:false
		}}>
			{role === "customer" ?
			<RoleLoginStack.Screen name="LoginScreen" component={LoginScreen}/> :
			role === "agent" &&
			<RoleLoginStack.Screen name="AgentLoginScreen" component={AgentLoginScreen}/>
			}
		</RoleLoginStack.Navigator>
	)
}
function LoginPackage(){
	return(
		<LoginStack.Navigator headerMode="none" screenOptions={{
			gestureEnabled:false
		}}>
			<LoginStack.Screen name="RoleLogin" component={RoleLoginPackage}/>
			<LoginStack.Screen name="Register" component={RegisterPackage}/>
			<LoginStack.Screen name="ResetPassword" component={ResetPasswordPackage}/>
		</LoginStack.Navigator>
	)
}

function App(){
	return(
		<Provider store={store}>
			<NavigationContainer>
				<AppStack.Navigator headerMode="none" initialRouteName="LoadingScreen" screenOptions={{
					gestureEnabled:false
				}}>
					<AppStack.Screen name="LoadingScreen" component={LoadingScreen}/>
					<AppStack.Screen name="Main" component={MainPackage}/>
					<AppStack.Screen name="AgentMain" component={AgentMainPackage}/>
					<AppStack.Screen name="Login" component={LoginPackage}/>
				</AppStack.Navigator>
			</NavigationContainer>
		</Provider>
	)
}

export default App;