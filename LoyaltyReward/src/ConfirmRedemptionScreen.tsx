import {useNavigation} from "@react-navigation/native";
import React,{useState,useEffect} from "react";
import {View,ScrollView,Text,Image,useColorScheme,TouchableOpacity} from "react-native";
import {Colors} from "react-native/Libraries/NewAppScreen";
import {useDispatch,useSelector} from "react-redux";
import envfile from "../envfile";
import {IRootState} from "../store";
import {MiddlePickUpLocationCriteriaPicker} from "./components/Picker";
import {changeNoLocationToFalse,changeNoLocationToNull,noEnoughPointToFalse} from "./redux/redeem/actions";
import {checkoutCart,getAllPickupLocations,getInStockItem} from "./redux/redeem/thunks";
import {styles} from "./styles/ConfirmRedemptionScreenStyles";

export const InStockItemToConfirm:React.FC<{name:string,imageSource:string,redeem_points:string,type:string,amount_in_cart:string}> =
({name,imageSource,redeem_points,type,amount_in_cart}) => {
	return(
		<View style={styles.giftOtherContainer}>
			<View style={styles.description}>
				<View>
					<Text style={styles.name}>{name}</Text>
					<Text style={styles.giftType}>{type}</Text>
					<View style={styles.pointRowContainer}>
						<Text>{parseInt(redeem_points)*parseInt(amount_in_cart)} pt</Text>
					</View>
				</View>
			</View>
			<View style={styles.imageContainer}>
				<Image style={styles.image} source={{uri:`${envfile.REACT_APP_IMAGE_PATH}/rewards/${imageSource}`}}/>
			</View>
		</View>
	)
}

function ConfirmRedemptionScreen(){
	const isDarkMode = useColorScheme() === "dark";
	const navigation = useNavigation();
	const dispatch = useDispatch();
	const inStockItemList = useSelector((state:IRootState)=>state.redeemPage.inStockItemDetail);
	const [pickup_location_id, setPickup_location_id] =  useState<number|null>(null);
	const placeCriteria = useSelector((state:IRootState)=>state.redeemPage.rewardPickupLocation);
	// console.log(placeCriteria)
	const place = placeCriteria.map((criteria) => ({label:criteria.name,value:criteria.id + ""}));
	const noPickUpLocation = useSelector((state:IRootState)=>state.redeemPage.noPickLocation)
	const noEnoughPoint = useSelector((state:IRootState)=>state.redeemPage.noEnoughPoint);
	const myPoint = useSelector((state:IRootState)=>state.redeemPage.currentPoint);

	useEffect(() => {
		dispatch(getInStockItem())
		dispatch(getAllPickupLocations())
	},[dispatch])

	return(
		<View style={[styles.flexContainer,{backgroundColor:Colors.white}]}>
			<View style={styles.scrollView}>
				<ScrollView>
					{inStockItemList.length > 0 && inStockItemList.map((inStockItem,index)=>(
						<InStockItemToConfirm key={index} imageSource={inStockItem.image}
						name={inStockItem.name} redeem_points={inStockItem.redeem_points} type={inStockItem.type}
						amount_in_cart={inStockItem.amount_in_cart}/>
					))}
				</ScrollView>
			</View>
			<View style={styles.payContainer}>
				<Text style={styles.payTitle}>支付積分</Text>
				<View style={styles.payRowContainer}>
					<Text>總共需付積分</Text>
					<Text>{inStockItemList.map(inStockItem => parseInt(inStockItem.amount_in_cart)*parseInt(inStockItem.redeem_points))
					.reduce((exSum, eachValue) => exSum + eachValue, 0)} pt</Text>
				</View>
			</View>
			<View style={styles.placeContainer}>
				<Text style={styles.payTitle}>兌換資料</Text>
				<View style={styles.payRowContainer}>
					<Text>地點</Text>
					<MiddlePickUpLocationCriteriaPicker defaultValue="" label={"選擇分店"} items={place} criteria={setPickup_location_id}
					reactDispatch={null} nearCriteria={null}/>
					{/* <MiddlePicker label="選擇分店" items={place} /> */}
				</View>
			</View>
			{noPickUpLocation === false && <Text style={styles.warning}>沒有選擇領取分店</Text>}
			{noEnoughPoint && <Text style={styles.warning}>沒有足夠積分</Text>}
			<TouchableOpacity style={styles.confirmButton} onPress={() => {
				{!pickup_location_id && dispatch(changeNoLocationToFalse())}
				{pickup_location_id && !!myPoint && myPoint >= inStockItemList.map(inStockItem => parseInt(inStockItem.amount_in_cart)*parseInt(inStockItem.redeem_points))
				.reduce((exSum, eachValue) => exSum + eachValue, 0) && navigation.navigate("RedeemCompletedScreen")};
				{pickup_location_id && dispatch(checkoutCart(pickup_location_id,inStockItemList.map(inStockItem =>
				(parseInt(inStockItem.amount_in_cart)*parseInt(inStockItem.redeem_points)))
				.reduce((exSum, eachValue) => exSum + eachValue, 0)))}
			}}>
				<Text style={styles.confirmText}>確認</Text>
			</TouchableOpacity>
			<TouchableOpacity style={styles.backButton} onPress={() => {
				dispatch(changeNoLocationToNull())
				dispatch(noEnoughPointToFalse())
				navigation.navigate("GiftCartScreen")
			}}>
				<Text style={styles.backText}>返回</Text>
			</TouchableOpacity>
		</View>
	)
}

export default ConfirmRedemptionScreen;