import React,{useState,useEffect} from "react"
import {View,Text,TouchableOpacity,TextInput,KeyboardAvoidingView,Platform,TouchableWithoutFeedback,Keyboard,
useColorScheme,
ScrollView} from "react-native"
import {styles} from "./styles/CustomerProfileScreenStyles";
import {useNavigation} from "@react-navigation/native";
import {useDispatch,useSelector} from "react-redux";
import {logout} from "./redux/auth/thunks";
import {agentLogout} from "./redux/agentAuth/thunks";
import {getUserProfile,updateUserProfile} from "./redux/customerProfile/thunks";
import {IRootState} from "../store";
import {updateDisplayName,updateUsername,updateEmail,updateBirthday,updateMobile,updateGender}
from "./redux/customerProfile/actions";
import {Colors} from "react-native/Libraries/NewAppScreen";
import {flexStyles} from "./styles/LoadingScreenStyles";
import {MiddleCriteriaPickerForGender} from "./components/Picker";

export const genderList = [
	{label:"Male",value:"1"},
	{label:"Female",value:"2"}
]

function checkMobile(phone:string|null){
	if(phone){
		for(let i = 0; i < phone.length; i++){
			if(isNaN(parseInt(phone[i]))){
				return false
			}
		}
		return true
	}
	return false
}

function CustomerProfileScreen(){
    const [isEditing,setIsEditing] = useState(false);
    const navigation = useNavigation();
	const dispatch = useDispatch();
	const username = useSelector((state:IRootState)=>state.customerProfilePage.username);
	const display_name = useSelector((state:IRootState)=>state.customerProfilePage.display_name);
	const email = useSelector((state:IRootState)=>state.customerProfilePage.email);
	const telephone = useSelector((state:IRootState)=>state.customerProfilePage.mobile);
	const birthday = useSelector((state:IRootState)=>state.customerProfilePage.date_of_birth);
	const gender = useSelector((state:IRootState)=>state.customerProfilePage.gender);
	// const isDarkMode = useColorScheme() === "dark";
	const [genderId,setGenderId] = useState("");
	const [password,setPassword] = useState("");
	const [passwordAgain,setPasswordAgain] = useState("");
	const [wrongPassword,setWrongPassword] = useState(false);
	const [wrongPhone,setWrongPhone] = useState(false);
	const [wrongEmail,setWrongEmail] = useState(false);
	const isAuthenticated = useSelector((state:IRootState)=>state.auth.isAuthenticated);
	const isAuthenticatedAgent = useSelector((state:IRootState)=>state.agentAuth.isAuthenticated);
	useEffect(() => {
		dispatch(getUserProfile());
	},[dispatch])

	useEffect(()=>{
		if(gender){
			// console.log(gender)
			setGenderId((genderList.filter(item=>item.label === gender).map(item=>item.value))[0]);
			// console.log(genderId)
		}
	},[gender])
	// console.log(genderId)
	// console.log(gender)
    return(
		<TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
			<KeyboardAvoidingView behavior={Platform.OS === "ios" ? "padding" : "height"} style={flexStyles.flex}>
				<View style={[styles.flexContainer,{backgroundColor:Colors.white}]}>
				<View style={styles.content}>
					<ScrollView style={styles.profileContainer}>
						<View style={isEditing ? styles.profileRow : styles.noProfileRow}>
							<Text style={styles.title}>用戶名稱</Text>
							{isEditing ? <TextInput style={styles.cell} value={username} onChangeText={(event)=>{
								dispatch(updateUsername(event))
							}}></TextInput> :
							<TextInput editable={false} value={username}></TextInput>
							}
						</View>
						<View style={isEditing ? styles.profileRow : styles.noProfileRow}>
							<Text style={styles.title}>顯示名</Text>
							{isEditing ? <TextInput style={styles.cell} value={display_name} onChangeText={(event)=>{
								dispatch(updateDisplayName(event))
							}}></TextInput> :
							<TextInput editable={false} value={display_name}></TextInput>
							}
						</View>
						<View style={isEditing ? styles.profileRow : styles.noProfileRow}>
							<Text style={styles.title}>電郵</Text>
							{isEditing ? <TextInput style={styles.cell} value={email} onChangeText={(event)=>{
								dispatch(updateEmail(event))
							}}></TextInput> :
							<TextInput editable={false} value={email}></TextInput>
							}
						</View>
						<View style={isEditing ? styles.profileRow : styles.noProfileRow}>
							<Text style={styles.title}>手機號碼</Text>
							{isEditing ? <TextInput style={styles.cell} value={telephone} keyboardType="numeric"
							onChangeText={(event)=>{
								dispatch(updateMobile(event))
							}}></TextInput> :
							<TextInput editable={false} value={telephone}></TextInput>
							}
						</View>
						<View style={isEditing ? styles.profileRow : styles.noProfileRow}>
							<Text style={styles.title}>生日</Text>
							{isEditing ? <TextInput style={styles.cell} value={birthday ? birthday.slice(0,10) : birthday} onChangeText={(event)=>{
								dispatch(updateBirthday(event))
							}}></TextInput> :
							<TextInput editable={false} value={birthday ? birthday.slice(0,10) : birthday}></TextInput>
							}
						</View>
						<View style={isEditing ? styles.profileRow : styles.noProfileRow}>
							<Text style={styles.title}>性別</Text>
							{isEditing ? <MiddleCriteriaPickerForGender defaultValue={gender} label={"你的性別"} items={genderList}
							criteria={setGenderId} reactDispatch={null}/> :
							<TextInput editable={false} value={(genderList.filter(item=>item.value === genderId).
							map(item=>item.label))[0]}></TextInput>
							}
						</View>
						<View style={isEditing ? styles.profileRow : styles.noProfileRow}>
							<Text style={styles.title}>密碼</Text>
							{isEditing ? <TextInput style={styles.cell} value={password} onChangeText={setPassword}></TextInput> :
							<TextInput editable={false}></TextInput>
							}
						</View>
						<View style={isEditing ? styles.profileRow : styles.noProfileRow}>
							<Text style={styles.title}>再次輸入密碼</Text>
							{isEditing ? <TextInput style={styles.cell} value={passwordAgain} onChangeText={setPasswordAgain}/> :
							<TextInput editable={false}></TextInput>
							}
						</View>
						<View style={styles.warningContainer}>
							{isEditing && password !== passwordAgain && wrongPassword &&
							<Text style={styles.warning}>兩個密碼不一樣, 請重新輸入</Text>}
							{!!telephone && telephone.length !== 8 && wrongPhone &&
							<Text style={styles.warning}>電話號碼不正確</Text>}
							{!!email && !email.includes("@") && wrongEmail && <Text style={styles.warning}>電郵不正確</Text>}
						</View>
					</ScrollView>
				</View>
				<View style={styles.buttonsContainer}>
					{isEditing ? (<View>
						<TouchableOpacity style={[styles.button,styles.renew]} onPress={() => {
							let isNumber = checkMobile(telephone)
							{password === passwordAgain && telephone && telephone.length === 8 && email.includes("@") && isNumber &&
							dispatch(updateUserProfile(username,password,email,parseInt(telephone),
							birthday,display_name,parseInt(genderId),(genderList.filter(item=>item.value === genderId)
							.map(item=>item.label))[0]))
							}
							{password === passwordAgain && telephone && telephone.length === 8 && email.includes("@") && isNumber &&
							setIsEditing(false)}
							{password !== passwordAgain && setWrongPassword(true)}
							{telephone && telephone.length !== 8 && setWrongPhone(true)}
							{!isNumber && setWrongPhone(true)}
							{!email.includes("@") && setWrongEmail(true)}
						}}>
							<Text style={styles.editButtonText}>更新</Text>
						</TouchableOpacity>
						<TouchableOpacity style={[styles.button,styles.cancel]} onPress={() => {
							setIsEditing(false)
							setPassword("")
							setPasswordAgain("")
							setWrongPassword(false)
							setWrongPhone(false)
							setWrongEmail(false)
						}}>
							<Text style={styles.editButtonText}>取消</Text>
						</TouchableOpacity>
					</View>) :
					(<View>
						<TouchableOpacity style={styles.editButton} onPress={() => {
							setIsEditing(true);
							setPassword("")
							setPasswordAgain("")
							setWrongPassword(false)
							setWrongPhone(false)
							setWrongEmail(false)
						}}>
							<Text style={styles.editButtonText}>編輯</Text>
						</TouchableOpacity>
						<TouchableOpacity style={styles.backButton} onPress={() => {
							{isAuthenticated && navigation.navigate("Home",{screen:"HomeScreen"})}
							{isAuthenticatedAgent && navigation.navigate("AgentHome",{screen:"AgentHomeScreen"})}
						}}>
							<Text style={styles.backButtonText}>返回首頁</Text>
						</TouchableOpacity>
					</View>)}
					{!isEditing && <TouchableOpacity style={styles.logoutButton} onPress={()=>{
						dispatch(logout());
						dispatch(agentLogout())
					}}>
						<Text style={styles.editButtonText}>登出</Text>
					</TouchableOpacity>}
				</View>
				</View>
			</KeyboardAvoidingView>
		</TouchableWithoutFeedback>
    )
}

export default CustomerProfileScreen;