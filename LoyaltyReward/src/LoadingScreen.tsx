import {StackActions,useNavigation} from "@react-navigation/native";
import React,{useEffect} from "react";
import {SafeAreaView,View,Text,ImageBackground,Image} from "react-native";
import {useDispatch,useSelector} from "react-redux";
import {checkAgentUser} from "./redux/agentAuth/thunks";
import {checkCurrentUser} from "./redux/auth/thunks";
import {IRootState} from "../store";
import {backgroundStyles} from "./styles/LoadingScreenStyles";
import {styles} from "./styles/LoadingScreenStyles";

function LoadingScreen(){
	const dispatch = useDispatch();
	const navigation = useNavigation();
	const isAuthenticated = useSelector((state:IRootState)=>state.auth.isAuthenticated);
	const isAuthenticatedAgent = useSelector((state:IRootState)=>state.agentAuth.isAuthenticated);

	useEffect(()=>{
		dispatch(checkCurrentUser());
		dispatch(checkAgentUser());
	})

	useEffect(()=>{
		if(isAuthenticated === null){
			return;
		}
		if(isAuthenticatedAgent === null){
			return;
		}
		if(isAuthenticated){
			setTimeout(() => {
				navigation.dispatch(
					StackActions.replace("Main")
				)
			},1000)
		}else if(isAuthenticatedAgent){
			setTimeout(() => {
				navigation.dispatch(
					StackActions.replace("AgentMain")
				)
			},1000)
		}else{
			setTimeout(() => {
				navigation.dispatch(
					StackActions.replace("Login")
				)
			},1000)
		}
	},[isAuthenticated,isAuthenticatedAgent])

	return(
		<ImageBackground source={require("../images/simpleBackground.png")} style={backgroundStyles.image}>
			<SafeAreaView style={styles.main}>
				<View>
					<Image source={require("../images/penguin.png")} style={styles.image}/>
					<Text style={styles.title}>Loyalty&Reward</Text>
					<Text style={styles.subTitle}>Your Royalty App</Text>
				</View>
			</SafeAreaView>
		</ImageBackground>
	)
}

export default LoadingScreen;