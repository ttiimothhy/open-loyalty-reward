/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import {useNavigation,StackActions} from "@react-navigation/native";
import React,{useEffect} from 'react';
import {SafeAreaView,ScrollView,StatusBar,Text,useColorScheme,View,TouchableOpacity,ImageBackground} from 'react-native';
import {Colors} from 'react-native/Libraries/NewAppScreen';
import {useDispatch, useSelector} from "react-redux";
import {CarouselSlide,CarouselBar} from "./components/CarouselSlide";
import {ImageCard} from "./components/ImageCard";
import {IRootState} from "../store";
import {carouselContainerStandard, styles} from "./styles/HomeScreenStyles";
import {currentPoint,getAdvertisement,getPredictedTreatment,getTopSurgery,service} from "./redux/home/thunks";
import envfile from "../envfile";
import {useBottomTabBarHeight} from '@react-navigation/bottom-tabs';
import {pendingSchedule} from "./redux/myAppointments/thunks";
import {getAllTreatments} from "./redux/availableAppointments/thunks";

const HomeScreen = () => {
	const isDarkMode = useColorScheme() === "dark";
	const backgroundStyle = {backgroundColor:Colors.white};
	const advertisementCard = useSelector((state:IRootState)=>state.homePage.advertisement);
	const topSurgery = useSelector((state:IRootState)=>state.homePage.surgery);
	const predictSurgery = useSelector((state:IRootState)=>state.homePage.predictedTreatment);
	const serviceType = useSelector((state:IRootState)=>state.homePage.service);
	const point = useSelector((state:IRootState)=>state.homePage.point);
	const pending = useSelector((state:IRootState)=>state.myAppointmentsPage.pendingSchedule);
	const navigation = useNavigation();
	const isAuthenticated = useSelector((state:IRootState)=>state.auth.isAuthenticated);
	const tabBarHeight = useBottomTabBarHeight();
	const scrollViewContainer = carouselContainerStandard(tabBarHeight);
	const dispatch = useDispatch();
	// console.log(serviceType)
	useEffect(() => {
		if(!isAuthenticated){
			navigation.dispatch(
				StackActions.replace("Login")
			)
		}
	},[isAuthenticated])
	useEffect(() => {
		dispatch(pendingSchedule());
		dispatch(getAdvertisement());
		dispatch(getTopSurgery());
		dispatch(service());
		dispatch(currentPoint())
		dispatch(getPredictedTreatment())
	},[dispatch])

	useEffect(() => {
		dispatch(getAllTreatments())
	},[dispatch])

	return(
		<SafeAreaView style={[styles.flexTopContainer,backgroundStyle]}>
			<StatusBar barStyle={isDarkMode ? "light-content" : "dark-content"} />
			<View style={[styles.flexTopContainer,{backgroundColor:Colors.white}]}>
				{/* <View style={styles.scrollViewContainer}> */}
				<ScrollView style={[scrollViewContainer.scrollViewContainer,styles.whiteBackground,backgroundStyle]}>
					<CarouselSlide carouselItems={advertisementCard}/>
					<View style={styles.flexContainer}>
						<View style={styles.mainTextContainer}><Text style={styles.mainText}>熱門療程</Text></View>
						<ScrollView horizontal style={styles.imageContainer}>
							{topSurgery.map((surgery,index)=>(
								<ImageCard imageSource={`${envfile.REACT_APP_IMAGE_PATH}/treatments/${surgery.image}`}
								key={index} id={surgery.id}>{surgery.title}</ImageCard>
							))}
						</ScrollView>
						<View style={styles.mainTextContainer}><Text style={styles.mainText}>推介療程</Text></View>
						<ScrollView horizontal style={styles.imageContainer}>
							{predictSurgery && predictSurgery.length > 0 && predictSurgery.map((surgery,index)=>(
								<ImageCard imageSource={`${envfile.REACT_APP_IMAGE_PATH}/treatments/${surgery.image}`}
								key={index} id={surgery.id}>{surgery.title}</ImageCard>
							))}
						</ScrollView>
						<View style={styles.mainTextContainer}><Text style={styles.mainText}>探索服務</Text></View>
						<View style={styles.serviceContainer}>
							<View style={styles.serviceRowContainer}>
								{serviceType.length > 0 && (serviceType.map((type,index)=>
								(<TouchableOpacity style={styles.service} key={index} onPress={() => {
									navigation.navigate("AvailableAppointments",{screen:"AvailableAppointmentsScreen",
										params:{typeCriteria:type.id}
									})
								}}>
								<ImageBackground source={{uri:`${envfile.REACT_APP_IMAGE_PATH}/services/${type.image}`}}
								style={styles.imageBackground} imageStyle={styles.imageBackgroundBorderRadius}>
									<View style={styles.serviceTextContainer}>
										<Text style={styles.serviceText}>{type.service}</Text>
									</View>
								</ImageBackground>
								</TouchableOpacity>)))}
							</View>
						</View>
						<View style={styles.mainTextContainer}><Text style={styles.mainText}>我的積分</Text></View>
						<View style={styles.pointContainer}>
							<View style={styles.pointTextContainer}>
								<Text style={styles.yourPointText}>現擁有積分</Text>
								{point ? <Text>{point} pt</Text> : <Text>0</Text>}
							</View>
							<View style={styles.pointSwitchContainer}>
								<TouchableOpacity style={styles.firstPoint} onPress={() => {
									navigation.navigate("AvailableAppointments",{screen:"AvailableAppointmentsScreen",
									params:{typeCriteria:null}
								})
								}}>
									<Text style={styles.pointText}>賺取積分</Text>
								</TouchableOpacity>
								<TouchableOpacity style={styles.point} onPress={() => {
									navigation.navigate("Gift",{Screen:"GiftScreen"})
								}}>
									<Text style={styles.pointText}>兌換積分</Text>
								</TouchableOpacity>
							</View>
						</View>
					</View>
				</ScrollView>
				{/* </View> */}
				<View style={styles.carouselContainer}>
					<CarouselBar carouselItems={pending}/>
				</View>
			</View>
		</SafeAreaView>
	)
}

export default HomeScreen;