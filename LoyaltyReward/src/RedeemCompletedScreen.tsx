import {useNavigation} from "@react-navigation/native";
import React,{useEffect} from "react";
import {ScrollView, TouchableOpacity} from "react-native";
import {View,Text,Image} from "react-native";
import {useDispatch,useSelector} from "react-redux";
import { IRootState } from "../store";
import { getOrderDetail, getPointValue } from "./redux/redeem/thunks";
import {styles} from "./styles/RedeemCompletedScreenStyles";
import QRCode from 'react-native-qrcode-svg';

export const RewardRedeemed:React.FC<{name:string,amount:string}> = ({name,amount}) => {
    return(
        <View>
            <Text style={styles.text}>{name}</Text>
            <Text style={styles.bottomText}>數量: {amount}</Text>
        </View>
    )
}

function RedeemCompletedScreen(){
	const navigation = useNavigation();
    const dispatch = useDispatch();
    const checkoutResult = useSelector((state:IRootState)=>state.redeemPage.checkoutResult)
    const rewardRedeemedList = useSelector((state:IRootState)=>state.redeemPage.checkoutResult?.redeem_items)
    const myPoint = useSelector((state:IRootState)=>state.redeemPage.currentPoint);
	let newPoint = 0;
	// console.log(rewardRedeemedList)

    useEffect(() => {
        if(checkoutResult && checkoutResult.branch){
            dispatch(getOrderDetail(checkoutResult.branch.order_ref));
        }
    },[dispatch,checkoutResult])

	useEffect(() => {
		dispatch(getPointValue());
	},[dispatch])

	// if(myPoint){
	// 	newPoint = myPoint - parseInt(checkoutResult?.branch.points)
	// }

    return(
        <ScrollView style={styles.container}>
            <View style={styles.redeemOrderCompletedContainer}>
                <View style={styles.redeemOrderCompletedImageContainer}>
					<Image source={require("../images/trophy.png")} style={styles.image}/>
				</View>
                <Text style={styles.redeemStatus}>換領成功</Text>
                <Text style={styles.redeemNumber}>換領編號: #{checkoutResult && checkoutResult.branch.order_ref}</Text>
            </View>

            <View style={styles.redeemDetailContainer}>
				<View style={styles.redeemDetailRow}>
					<Text style={styles.titleText}>已換領的禮品</Text>
                    <View style={styles.giftContentContainer}>
						{rewardRedeemedList && rewardRedeemedList.length > 0 && rewardRedeemedList.map((rewardRedeemed,index)=>{
							return (<RewardRedeemed key={index} name={rewardRedeemed.name} amount={rewardRedeemed.amount}/>)
						})}
					</View>
                </View>
				<View style={styles.redeemDetailRow}>
                    <Text style={styles.titleText}>領取分店地點</Text>
                    {checkoutResult && checkoutResult.branch && <Text style={styles.text}>{checkoutResult.branch.location}</Text>}
                </View>
                <View style={styles.redeemDetailRow}>
                    <Text style={styles.titleText}>換領所需分數</Text>
                    <Text style={styles.text}>{checkoutResult?.branch.points} pt</Text>
                </View>
                <View style={styles.redeemDetailRow}>
                    <Text style={styles.titleText}>剩餘分數</Text>
                    <Text style={styles.text}>{!!myPoint && myPoint} pt</Text>
                    {/* tell kevin to return to me */}
                </View>
            </View>
			{checkoutResult && checkoutResult.branch && !!checkoutResult.branch.qr_code && <View style={styles.qrcodeContainer}>
                <QRCode value={checkoutResult.branch.qr_code}
				// logo={require("../images/badge.png")} logoSize={100}
				></QRCode>
            </View>}
            <View>
                <TouchableOpacity style={styles.button} onPress={() => {
					navigation.navigate("GiftScreen")
				}}>
					<Text style={styles.buttonText}>返回禮物頁面</Text>
                </TouchableOpacity>
            </View>
        </ScrollView>
    )

}

export default RedeemCompletedScreen;