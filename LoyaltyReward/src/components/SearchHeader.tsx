import {FontAwesomeIcon} from "@fortawesome/react-native-fontawesome";
import {useNavigation} from "@react-navigation/native";
import React,{useState} from "react";
import {TextInput,TouchableOpacity} from "react-native";
import {View} from "react-native-animatable";
import FontAwesome5 from "react-native-vector-icons/FontAwesome5";
import Ionicon from "react-native-vector-icons/Ionicons";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import {useDispatch,useSelector} from "react-redux";
import {IRootState} from "../../store";
import {updateSearchWord} from "../redux/searchHeader/actions";
import {searchKeyword} from "../redux/searchHeader/thunks";
import {styles} from "../styles/SearchHeaderStyles";

function searchHeader(){
	const [search,setSearch] = useState(false);
	const navigation = useNavigation();
	const dispatch = useDispatch();
	const searchWord = useSelector((state:IRootState)=>state.searchHeader.searchWord);
	const isAuthenticated = useSelector((state:IRootState)=>state.auth.isAuthenticated);
	const isAuthenticatedAgent = useSelector((state:IRootState)=>state.agentAuth.isAuthenticated);
	// console.log(searchWord)
	// console.log(isAuthenticated + "1");
	// console.log(isAuthenticatedAgent + "2")
	return(
		<View style={styles.iconContainer}>
			{search && <View style={styles.search}>
				<TouchableOpacity onPress={() => {
					setSearch(false)
					dispatch(updateSearchWord(""))
				}}>
					<FontAwesomeIcon size={28} color={"#cccccc"} icon="times"/>
				</TouchableOpacity>
				<View style={styles.searchInputWidth}>
					<TextInput value={searchWord} style={styles.searchInput} onChangeText={(event)=>{
						dispatch(updateSearchWord(event))
					}}/>
				</View>
				<TouchableOpacity onPress={() => {
					setSearch(false)
					dispatch(searchKeyword(searchWord,isAuthenticated,isAuthenticatedAgent))
					navigation.navigate("SearchScreen")
				}}>
					<FontAwesome5 size={26} color={"#11bb00"} name="searchengin"/>
				</TouchableOpacity>
			</View>}
			{!search && <TouchableOpacity onPress={() => {
				setSearch(true)
			}}>
				<Ionicon style={styles.headerIcon} size={26} name="search-sharp"/>
			</TouchableOpacity>}
			<MaterialIcons size={28} name="notifications-none"/>
		</View>
	)
}

export default searchHeader;