import React from "react";
import {View,Dimensions} from "react-native";
import {BarChart} from "react-native-chart-kit";
import {ChartData} from "react-native-chart-kit/dist/HelperTypes";
import {styles} from "../styles/BarChartGraphStyles";

const screenWidth = Dimensions.get('window').width * 0.9;
const chartConfig = {
	backgroundGradientFrom:"#ffffff",
	backgroundGradientFromOpacity:1,
	backgroundGradientTo:"#ffffff",
	backgroundGradientToOpacity:1,
	fillShadowGradient:"#119900",
	fillShadowGradientOpacity:1,
	color:(opacity = 0.5) => `rgba(0,0,0,${opacity})`,
	strokeWidth:2, // optional, default 3
	barPercentage:0.6,
	useShadowColorFromDataset:false, // optional
	decimalPlaces:0,
	propsForVerticalLabels:{fontSize:8},
	// propsForHorizontalLabels:{justifyContent:"center"},
	propsForBackgroundLines:{color:"#ffffff"}
}

const BarChartGraph:React.FC<{datasets:ChartData,yLabel:string,ySuffix:string}> = ({datasets,yLabel,ySuffix}) => {
	return(
		<View>
			<BarChart
				style={styles.barChart}
				data={datasets}
				width={screenWidth}
				height={220}
				yAxisLabel={yLabel}
				chartConfig={chartConfig}
				yAxisSuffix={ySuffix}
			/>
		</View>
	)
}

export default BarChartGraph;