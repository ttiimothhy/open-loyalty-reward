import CheckBox from "@react-native-community/checkbox";
import React,{useState} from "react";
import {View,Text} from "react-native-animatable";
import {useDispatch,useSelector} from "react-redux";
import {changeRole} from "../redux/role/thunks";
import {IRootState} from "../../store";
import {agentStyles,customerStyles,roleCheckBoxStyles,styles} from "../styles/CheckBoxStyles";
import {loginNull} from "../redux/agentAuth/thunks";
import {resetNull} from "../redux/agentUser/thunks";
import {loginNull as customerLoginNull}from "../redux/auth/thunks";
import {resetNull as customerResetNull} from "../redux/user/thunks"

export function CheckBoxDemo(){
	const [toggleCheckBox, setToggleCheckBox] = useState(false);
	return(
		<View style={styles.checkBoxContainer}>
			<CheckBox
				disabled={false}
				value={toggleCheckBox}
				onValueChange={(value) => setToggleCheckBox(value)}
				animationDuration={0}
				boxType={"square"}
				lineWidth={2}
				style={styles.checkBox}
				onCheckColor={"#ff0000"}
				onTintColor={"#555555"}
				tintColor={"#cccccc"}
			/>
		</View>
	)
}

export function CustomerRoleCheckBox(){
	const dispatch = useDispatch();
	const [customerToggleCheckBox,setCustomerToggleCheckBox] = useState(true);
	const [agentToggleCheckBox,setAgentToggleCheckBox] = useState(false);
	const role = useSelector((state:IRootState)=>state.role.role);
	return(
		<View style={styles.sliderContainer}>
			<Text style={customerStyles.role}>Customer</Text>
			<View style={roleCheckBoxStyles.checkBoxContainer}>
				<CheckBox
					disabled={role === "customer" ? true : false}
					value={customerToggleCheckBox}
					onValueChange={(value) => {
						setCustomerToggleCheckBox(value);
						setAgentToggleCheckBox(false);
						{role === "agent" && dispatch(changeRole("customer"))}
					}}
					animationDuration={0}
					boxType={"square"}
					lineWidth={2}
					style={roleCheckBoxStyles.checkBox}
					onCheckColor={"#ff0000"}
					onTintColor={"#222222"}
					tintColor={"#aaaaaa"}
				/>
			</View>
			<Text style={customerStyles.role}>Agent</Text>
			<View style={roleCheckBoxStyles.checkBoxContainer}>
				<CheckBox
					disabled={role === "agent" ? true : false}
					value={agentToggleCheckBox}
					onValueChange={(value) => {
						setCustomerToggleCheckBox(false);
						setAgentToggleCheckBox(value);
						{role === "customer" && dispatch(changeRole("agent"))}
						dispatch(customerLoginNull())
						dispatch(customerResetNull())
					}}
					animationDuration={0}
					boxType={"square"}
					lineWidth={2}
					style={roleCheckBoxStyles.checkBox}
					onCheckColor={"#ff0000"}
					onTintColor={"#222222"}
					tintColor={"#aaaaaa"}
				/>
			</View>
		</View>
	)
}

export function AgentRoleCheckBox(){
	const dispatch = useDispatch();
	const [customerToggleCheckBox,setCustomerToggleCheckBox] = useState(false);
	const [agentToggleCheckBox,setAgentToggleCheckBox] = useState(true);
	const role = useSelector((state:IRootState)=>state.role.role);
	return(
		<View style={styles.sliderContainer}>
			<Text style={agentStyles.role}>Customer</Text>
			<View style={roleCheckBoxStyles.checkBoxContainer}>
				<CheckBox
					disabled={role === "customer" ? true : false}
					value={customerToggleCheckBox}
					onValueChange={(value) => {
						setCustomerToggleCheckBox(value);
						setAgentToggleCheckBox(false);
						{role === "agent" && dispatch(changeRole("customer"))}
						dispatch(loginNull())
						dispatch(resetNull())
					}}
					animationDuration={0}
					boxType={"square"}
					lineWidth={2}
					style={roleCheckBoxStyles.checkBox}
					onCheckColor={"#ff0000"}
					onTintColor={"#eeeeee"}
					tintColor={"#999999"}
				/>
			</View>
			<Text style={agentStyles.role}>Agent</Text>
			<View style={roleCheckBoxStyles.checkBoxContainer}>
				<CheckBox
					disabled={role === "agent" ? true : false}
					value={agentToggleCheckBox}
					onValueChange={(value) => {
						setCustomerToggleCheckBox(false);
						setAgentToggleCheckBox(value);
						{role === "customer" && dispatch(changeRole("agent"))}
					}}
					animationDuration={0}
					boxType={"square"}
					lineWidth={2}
					style={roleCheckBoxStyles.checkBox}
					onCheckColor={"#ff0000"}
					onTintColor={"#eeeeee"}
					tintColor={"#999999"}
				/>
			</View>
		</View>
	)
}