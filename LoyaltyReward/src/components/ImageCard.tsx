import {useNavigation} from "@react-navigation/native";
import React from "react";
import {View,Text,Image,TouchableOpacity} from "react-native";
import {styles} from "../styles/ImageCardStyles";

export const ImageCard:React.FC<{imageSource:string,id:number}> = ({imageSource,children,id}) => {
	const navigation = useNavigation();
	return(
		<View style={styles.imageCardContainer}>
			<TouchableOpacity style={styles.imageBorder} onPress={() => {
				navigation.navigate("AvailableAppointments",{
					screen:"AppointmentDetailsScreen",
					params:{treatmentId:id}
				})
			}}>
				<Image source={{uri:imageSource}} style={styles.image}/>
				{/* <TouchableOpacity style={styles.editIcon}>
					<MaterialIcons size={20} name="edit"/>
				</TouchableOpacity> */}
			</TouchableOpacity>
			<Text style={styles.imageText}>{children}</Text>
		</View>
	)
}