import Carousel from "react-native-snap-carousel";
import React,{useRef, useState} from "react";
import {View,Text,useWindowDimensions,TouchableOpacity,ImageBackground,Platform,Linking,TouchableWithoutFeedback}
from "react-native";
import {styles} from "../styles/CarouselSlideStyles";
import {CarouselCard,ContactCarouselCard,CarouselCardRender,CarouselBarCard,ContactCard, Schedule} from "../../models";
import FontAwesome5 from "react-native-vector-icons/FontAwesome5";
import envfile from "../../envfile";
import {useDispatch, useSelector} from "react-redux";
import {updateCustomerId} from "../redux/agentTreatment/actions";
import {IRootState} from "../../store";
import {useNavigation} from "@react-navigation/native";

const renderCarouselItem = (card:CarouselCard) => {
	return(
		<ImageBackground key={card.index} source={{uri:`${envfile.REACT_APP_IMAGE_PATH}/advertisements/${card.item.image}`}}
		style={styles.image}>
			{/* <View style={styles.carouselCard}>
				<Text style={styles.carouselCardText}>fuck</Text>
				<Text style={styles.carouselCardSubText}>fuck</Text>
			</View> */}
		</ImageBackground>
	)
}

const renderContactCarouselItem = (card:ContactCarouselCard) => {
	const today = new Date().toISOString().slice(0,10);
	let age;
	if((parseInt(today.slice(6,8)) - parseInt(card.item.date_of_birth.slice(6,8)) < 0)){
		age = parseInt(today.slice(0,4)) - parseInt(card.item.date_of_birth.slice(0,4)) - 1
	}else if((parseInt(today.slice(6,8)) - parseInt(card.item.date_of_birth.slice(6,8))) === 0){
		if((parseInt(today.slice(10,12)) - parseInt(card.item.date_of_birth.slice(10,12)) < 0)){
			age = parseInt(today.slice(0,4)) - parseInt(card.item.date_of_birth.slice(0,4)) - 1
		}else{
			age = parseInt(today.slice(0,4)) - parseInt(card.item.date_of_birth.slice(0,4))
		}
	}else{
		age = parseInt(today.slice(0,4)) - parseInt(card.item.date_of_birth.slice(0,4))
	}
	// console.log(card.item.date_of_birth);
	// console.log(card.item.gender)
	// const age = parseInt(today.slice(0,4)) - parseInt(card.item.date_of_birth.slice(0,4))
	return(
		<View key={card.index} style={styles.contactCarouselCard}>
			<View style={styles.mainRowContainer}>
				<View>
					<View style={styles.rowContainer}>
						<Text style={styles.contactTitle}>姓名</Text>
						<Text style={styles.carouselBarText}>{card.item.display_name}</Text>
					</View>
					<View style={styles.rowContainer}>
						<Text style={styles.contactTitle}>年齡</Text>
						<Text style={styles.carouselBarSubAgentText}>{age}</Text>
					</View>
					<View style={styles.rowContainer}>
						<Text style={styles.contactTitle}>性別</Text>
						<Text style={styles.carouselBarSubAgentText}>{card.item.gender}</Text>
					</View>
				</View>
				<TouchableOpacity style={styles.contactButton} onPress={() => {
					let phoneNumber = "";
					if(Platform.OS === "android"){
						phoneNumber = `tel:${card.item.mobile}`;
					}else{
						phoneNumber = `telprompt:${card.item.mobile}`;
					}
					Linking.openURL(phoneNumber);
				}}>
					<Text style={styles.contactText}>聯絡</Text>
				</TouchableOpacity>
			</View>
		</View>
	)
}

export const CarouselSlide:React.FC<{carouselItems:CarouselCardRender[]}> = ({carouselItems}) => {
	const isCarousel = useRef(null);
	const windowDimension = useWindowDimensions();
	return(
		<View>
			<Carousel
				layout={"default"}
				ref={isCarousel}
				data={carouselItems}
				sliderWidth={windowDimension.width}
				itemWidth={windowDimension.width}
				renderItem={renderCarouselItem}
				// onSnapToItem={(index) => setIndex(index)}
				inactiveSlideShift={0}
				useScrollView={true}
				loop={true}
				loopClonesPerSide={2}
				autoplay={true}
				autoplayDelay={400}
				autoplayInterval={5000}
			/>
		</View>
	)
}

export const CarouselBar:React.FC<{carouselItems:Schedule[]}> = ({carouselItems}) =>{
	const isCarousel = useRef(null);
	const windowDimension = useWindowDimensions();
	const navigation = useNavigation()
	return(
		<View>
			<Carousel
				layout={"stack"}
				ref={isCarousel}
				data={carouselItems}
				sliderWidth={windowDimension.width}
				itemWidth={windowDimension.width}
				renderItem={(card:CarouselBarCard) => {
				return(
					<TouchableOpacity key={card.index} style={styles.carouselBarCard} onPress={() => {
						navigation.navigate("MyAppointments",{screen:"AlreadyAppointmentDetailsScreen",
							params:{appointmentId:card.item.id}
						})
					}}>
						<Text style={styles.carouselBarText}>{card.item.title}</Text>
						<Text style={styles.carouselBarSubText}>{card.item.timeslot}</Text>
						<Text style={styles.carouselBarSubText}>{card.item.name}</Text>
					</TouchableOpacity>
				)}}
				inactiveSlideShift={0}
				useScrollView={true}
				loop={true}
				loopClonesPerSide={2}
				autoplay={true}
				autoplayDelay={400}
				autoplayInterval={10000}
			/>
		</View>
	)
}

export const ContactCarousel:React.FC<{carouselItems:ContactCard[]}> = ({carouselItems}) =>{
	const [isCarousel,setIsCarousel] = useState<Carousel<any>|null>();
	const windowDimension = useWindowDimensions();
	const [index,setIndex] = useState(0);
	return(
		<View style={styles.carouselContainer}>
			<TouchableOpacity onPress={()=>{isCarousel?.snapToPrev()}}>
				<FontAwesome5 name="chevron-left" style={styles.arrow} size={28}/>
			</TouchableOpacity>
			<View style={styles.carouselImage}>
				<Carousel
					layout={"tinder"}
					ref={c => setIsCarousel(c)}
					data={carouselItems}
					sliderWidth={windowDimension.width - 60}
					itemWidth={windowDimension.width * 0.7}
					renderItem={renderContactCarouselItem}
					onSnapToItem={(index) => setIndex(index)}
					inactiveSlideShift={0}
					useScrollView={true}
					loop={true}
					loopClonesPerSide={2}
					autoplay={false}
					autoplayDelay={400}
					autoplayInterval={10000}
				/>
			</View>
			<TouchableOpacity onPress={()=>{isCarousel?.snapToNext()}}>
				<FontAwesome5 name="chevron-right" size={28}/>
			</TouchableOpacity>
		</View>
	)
}

export const ContactAppointmentCarousel:React.FC<{carouselItems:ContactCard[]}> = ({carouselItems}) =>{
	const [isCarousel,setIsCarousel] = useState<Carousel<any>|null>();
	const windowDimension = useWindowDimensions();
	const [index,setIndex] = useState(0);
	const dispatch = useDispatch();
	const today = new Date().toISOString().slice(0,10);
	const id  = useSelector((state:IRootState)=>state.agentTreatmentPage.customerId);

	return(
		<View style={styles.carouselContainer}>
			<TouchableOpacity onPress={()=>{isCarousel?.snapToPrev()}}>
				<FontAwesome5 name="chevron-left" style={styles.arrow} size={28}/>
			</TouchableOpacity>
			<View style={styles.carouselImage}>
				<Carousel
					layout={"tinder"}
					ref={c => setIsCarousel(c)}
					data={carouselItems}
					sliderWidth={windowDimension.width - 60}
					itemWidth={windowDimension.width * 0.7}
					renderItem={(card:ContactCarouselCard,) => {
						let age;
						if((parseInt(today.slice(6,8)) - parseInt(card.item.date_of_birth.slice(6,8)) < 0)){
							age = parseInt(today.slice(0,4)) - parseInt(card.item.date_of_birth.slice(0,4)) - 1
						}else if((parseInt(today.slice(6,8)) - parseInt(card.item.date_of_birth.slice(6,8))) === 0){
							if((parseInt(today.slice(10,12)) - parseInt(card.item.date_of_birth.slice(10,12)) < 0)){
								age = parseInt(today.slice(0,4)) - parseInt(card.item.date_of_birth.slice(0,4)) - 1
							}else{
								age = parseInt(today.slice(0,4)) - parseInt(card.item.date_of_birth.slice(0,4))
							}
						}else{
							age = parseInt(today.slice(0,4)) - parseInt(card.item.date_of_birth.slice(0,4))
						}
						return(
							<View key={card.index} style={styles.contactCarouselCard}>
								<View style={styles.mainRowContainer}>
									<View>
										<View style={styles.rowContainer}>
											<Text style={styles.contactTitle}>姓名</Text>
											<Text style={styles.carouselBarText}>{card.item.display_name}</Text>
										</View>
										<View style={styles.rowContainer}>
											<Text style={styles.contactTitle}>年齡</Text>
											<Text style={styles.carouselBarSubAgentText}>{age}</Text>
										</View>
										<View style={styles.rowContainer}>
											<Text style={styles.contactTitle}>性別</Text>
											<Text style={styles.carouselBarSubAgentText}>{card.item.gender}</Text>
										</View>
									</View>
									<View style={styles.buttonContainer}>
										<TouchableOpacity style={id === card.item.customer_id ? styles.finishConfirm :
										styles.confirmButton} onPress={() =>
										{dispatch(updateCustomerId(card.item.customer_id))}
										}>
											<Text style={id === card.item.customer_id ? styles.contactText :
											styles.finishConfirmText}>選擇</Text>
										</TouchableOpacity>
										<TouchableOpacity style={styles.contactButton} onPress={() => {
											let phoneNumber = "";
											if(Platform.OS === "android"){
												phoneNumber = `tel:${card.item.mobile}`;
											}else{
												phoneNumber = `telprompt:${card.item.mobile}`;
											}
											Linking.openURL(phoneNumber);
										}}>
											<Text style={styles.contactText}>聯絡</Text>
										</TouchableOpacity>
									</View>
								</View>
							</View>
						)
					}}
					onSnapToItem={(index) => setIndex(index)}
					inactiveSlideShift={0}
					useScrollView={true}
					loop={true}
					loopClonesPerSide={2}
					autoplay={false}
					autoplayDelay={400}
					autoplayInterval={10000}
				/>
			</View>
			<TouchableOpacity onPress={()=>{isCarousel?.snapToNext()}}>
				<FontAwesome5 name="chevron-right" size={28}/>
			</TouchableOpacity>
		</View>
	)
}