import {useNavigation} from "@react-navigation/native";
import React,{useState,useEffect} from "react";
import {View,Text,TouchableWithoutFeedback,useColorScheme,TouchableOpacity,FlatList} from "react-native";
import {styles} from "./styles/MyAppointmentsScreenStyles";
import {Colors} from "react-native/Libraries/NewAppScreen";
import {useDispatch,useSelector} from "react-redux";
import Modal from "react-native-modal";
import {completedScoredSchedule,completedUnScoredSchedule,cancelledSchedule,pendingSchedule,cancelAppointment,
purchasedSchedule,getAllTreatments} from "./redux/myAppointments/thunks";
import {IRootState} from "../store";
import {CompletedScoredSchedule,PurchasedSchedule,Schedule} from "../models";

const MyAppointmentsScreen = () => {
    const isDarkMode = useColorScheme() === "dark";
	const [isActive,setIsActive] = useState(true);
	const [isFinished,setIsFinished] = useState(false);
	const [isCancelled,setIsCancelled] = useState(false);
	const pending = useSelector((state:IRootState)=>state.myAppointmentsPage.pendingSchedule);
	const completedScore = useSelector((state:IRootState)=>state.myAppointmentsPage.completedScoredSchedule);
	const completedUnScore = useSelector((state:IRootState)=>state.myAppointmentsPage.completedUnScoredSchedule);
	const cancelled = useSelector((state:IRootState)=>state.myAppointmentsPage.cancelledSchedule);
	const purchased = useSelector((state:IRootState)=>state.myAppointmentsPage.purchasedSchedule);
	const [visible,setVisible] = useState(false);
    const navigation = useNavigation();
	const [cancelId,setCancelId] = useState(0);
	const dispatch = useDispatch();
	// console.log(completedScore)

	useEffect(()=>{
		dispatch(pendingSchedule());
		dispatch(completedScoredSchedule());
		dispatch(completedUnScoredSchedule());
		dispatch(cancelledSchedule());
		dispatch(purchasedSchedule())
	},[dispatch])

	useEffect(() => {
		dispatch(getAllTreatments());
	},[dispatch])
	// useEffect(()=>{
	// 	pending = useSelector((state:IRootState)=>state.myAppointmentsPage.pendingSchedule);
	// },[])

    return(
        <View style={[styles.flexContainer,{backgroundColor:Colors.white}]}>
			<View style={styles.titleContainer}>
				<TouchableWithoutFeedback onPress={() => {
					setIsActive(true);
					setIsFinished(false);
					setIsCancelled(false);
				}}>
					<View style={isActive && styles.title}>
						<Text style={isActive ? styles.titleFocusedText : styles.titleText}>將進行的療程</Text>
					</View>
				</TouchableWithoutFeedback>
				<TouchableWithoutFeedback onPress={() => {
					setIsActive(false);
					setIsFinished(true);
					setIsCancelled(false);
				}}>
					<View style={isFinished && styles.title}>
						<Text style={isFinished ? styles.titleFocusedText : styles.titleText}>已完成的療程</Text>
					</View>
				</TouchableWithoutFeedback>
				<TouchableWithoutFeedback onPress={() => {
					setIsActive(false);
					setIsFinished(false);
					setIsCancelled(true);
				}}>
					<View style={isCancelled && styles.title}>
						<Text style={isCancelled ? styles.titleFocusedText : styles.titleText}>已取消的療程</Text>
					</View>
				</TouchableWithoutFeedback>
			</View>
			{isActive && (<View style={styles.contentContainer}>
				<View style={styles.subHeadRow}>
					<Text style={styles.header}>下一次預約會在</Text>
				</View>
				<View style={styles.flatListContainer}>
				<FlatList<Schedule> data={pending} renderItem={(row)=>(
				<View key={row.item.id} style={styles.detailRow}>
					{/* <Text>{row.item.id}</Text> */}
					<View style={styles.row}>
						<View style={styles.placeDetails}>
							<Text>{row.item.timeslot}</Text>
							<Text>{row.item.name}</Text>
						</View>
						<View style={styles.flexWrap}>
							<Text style={styles.titleRowText}>{row.item.title}</Text>
						</View>
					</View>
					<View style={styles.buttonContainer}>
						<TouchableOpacity style={styles.confirmButton} onPress={()=>{
							navigation.navigate("AlreadyAppointmentDetailsScreen",{
								appointmentId:row.item.id
							})
						}}>
							<Text style={styles.confirmText}>詳情</Text>
						</TouchableOpacity>
						<TouchableOpacity style={styles.cancelButton} onPress={()=>{
							setVisible(true)
							setCancelId(row.item.id)
						}}>
							<Text style={styles.confirmText}>取消</Text>
						</TouchableOpacity>
					</View>
				</View>)}/>
				</View>
				<View style={styles.titleMiddleContainer}>
					<View style={isActive && styles.title}>
						<Text style={styles.titleFocusedText}>已購買的療程</Text>
					</View>
				</View>
				<View style={styles.flatLongListContainer}>
				<FlatList<PurchasedSchedule> data={purchased} renderItem={(row)=>(
				<View key={row.item.id} style={styles.detailRow}>
					{/* <Text>{row.item.id}</Text> */}
					<View style={styles.row}>
						<View style={styles.numberDetails}>
							<Text>{row.item.remaining_amount}次</Text>
						</View>
						<View style={styles.flexWrap}>
							<Text style={styles.titleRowText}>{row.item.title}</Text>
						</View>
					</View>
					<View style={styles.buttonContainer}>
						<TouchableOpacity style={styles.confirmButton} onPress={()=>{
							navigation.navigate("PurchasedTreatmentDetailsScreen",{
								purchasedId:row.item.id,
								remaining:row.item.remaining_amount
							})
						}}>
							<Text style={styles.confirmText}>詳情</Text>
						</TouchableOpacity>
					</View>
				</View>)}/>
				</View>
			</View>)}
			{isFinished && (<View style={styles.contentContainer}>
				<View style={styles.scoreContainer}>
					<View style={styles.subHeadRow}>
						<Text style={styles.header}>待評分的療程</Text>
					</View>
					<FlatList<Schedule> data={completedUnScore} renderItem={(row)=>(
					<View key={row.item.id} style={styles.detailRow}>
						<View style={styles.row}>
							<View style={styles.placeDetails}>
								<Text>{row.item.timeslot}</Text>
								<Text>{row.item.name}</Text>
							</View>
							<View style={styles.flexWrap}>
								<Text style={styles.titleRowText}>{row.item.title}</Text>
							</View>
						</View>
						<TouchableOpacity style={styles.confirmButton} onPress={()=>{
							navigation.navigate("AppointmentScoringScreen",{
								appointmentId:row.item.id
							})
						}}>
							<Text style={styles.confirmText}>評分</Text>
						</TouchableOpacity>
					</View>)}/>
				</View>
				<View style={styles.subHeadRow}>
					<Text style={styles.header}>已評分的療程</Text>
				</View>
				{<FlatList<CompletedScoredSchedule> data={completedScore} renderItem={(row)=>{
				return(
				<View key={row.item.id} style={styles.detailHavelRow}>
					<View style={styles.row}>
						<View style={styles.placeDetails}>
							<Text>{row.item.timeslot}</Text>
							<Text>{row.item.name}</Text>
						</View>
						<View style={styles.longFlexWrap}>
							<Text style={styles.titleRowText}>{row.item.title}</Text>
						</View>
						<View style={styles.flexRow}>
							<Text style={styles.scoreTitle}>分數:</Text>
							<View style={styles.scoreContentContainer}>
								<Text style={styles.scoreContent}>{row.item.score}</Text>
							</View>
						</View>
					</View>
				</View>)}}/>}
			</View>)}
			{isCancelled && (<View style={styles.contentContainer}>
				<View style={styles.subHeadRow}>
					<Text style={styles.cancelHeader}>你已取消的預約</Text>
				</View>
				<FlatList<Schedule> data={cancelled} renderItem={(row)=>(
				<View key={row.item.id} style={styles.detailSpaceBetweenRow}>
					<View style={styles.row}>
						<View style={styles.placeDetails}>
							<Text>{row.item.timeslot}</Text>
							<Text>{row.item.name}</Text>
						</View>
						<View style={styles.longFlexWrap}>
							<Text style={styles.titleRowText}>{row.item.title}</Text>
						</View>
					</View>
				</View>)}/>
			</View>)}
			<Modal isVisible={visible}>
				<View style={styles.modalContainer}>
					<View style={styles.modal}>
						<Text style={styles.deleteText}>確定刪除?</Text>
						<View style={styles.deleteButtonContainer}>
							<TouchableOpacity style={styles.deleteButton} onPress={() => {
								dispatch(cancelAppointment(cancelId))
								setCancelId(0)
								setVisible(false)
							}}>
								<Text style={styles.deleteButtonText}>確定</Text>
							</TouchableOpacity>
							<TouchableOpacity style={styles.cancelDeleteButton} onPress={() => {
								setVisible(false)
							}}>
								<Text style={styles.deleteButtonText}>取消</Text>
							</TouchableOpacity>
						</View>
					</View>
				</View>
			</Modal>
		</View>
    )
}

export default MyAppointmentsScreen;