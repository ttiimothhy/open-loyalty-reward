import {useNavigation} from "@react-navigation/native";
import React,{useEffect} from "react";
import {ScrollView,useColorScheme,TouchableOpacity,Text,View, FlatList} from "react-native";
import {Colors} from "react-native/Libraries/NewAppScreen";
import {useDispatch,useSelector} from "react-redux";
import {KeywordResultAgentContacts,KeywordResultAgentTreatments,KeywordResultRewards,KeywordResultTreatments} from "../models";
import {IRootState} from "../store";
import {updateSearchWord,resultNull} from "./redux/searchHeader/actions";
import {styles} from "./styles/SearchScreenStyles";

function SearchScreen(){
	const isDarkMode = useColorScheme() === "dark";
	const searchWord = useSelector((state:IRootState)=>state.searchHeader.searchWord);
	const dispatch = useDispatch();
	const navigation = useNavigation();
	const searchResult = useSelector((state:IRootState)=>state.searchHeader.result);
	const searchResultAgent = useSelector((state:IRootState)=>state.searchHeader.resultAgent);
	// console.log(searchResult);
	// console.log(searchResultAgent);

	useEffect(() => {
		const unsubscribe = navigation.addListener("beforeRemove",(event) => {
			event.preventDefault(); // Prevent default action
			unsubscribe() // Unsubscribe the event on first call to prevent infinite loop
			navigation.goBack() // Navigate to your desired screen
			dispatch(updateSearchWord(""))
			dispatch(resultNull());
		})

		return ()=>{
			// Do everything before unmount here
		}
	},[])

	return(
		<View style={[styles.flexContainer,{backgroundColor:Colors.white}]}>
			<View style={styles.titleContainer}>
				<Text style={styles.titleText}>搜索字眼</Text>
				<Text style={styles.search}>{searchWord}</Text>
			</View>
			{searchResult && searchResult.treatments && searchResult.treatments.length > 0 && <View style={styles.height}>
				<View style={styles.titleTextContainer}>
					<Text style={styles.title}>療程</Text>
				</View>
				<FlatList<KeywordResultTreatments>
				data={searchResult.treatments} renderItem={(row)=>(
				<View key={row.item.id} style={styles.contentTextContainer}>
					<Text style={styles.contentText}>{row.item.title}</Text>
					<View style={styles.contentDescriptionContainer}>
						<Text style={styles.contentDescription}>內容</Text>
					</View>
					<TouchableOpacity style={styles.contentContainer} onPress={() => {
						navigation.navigate("Main",{
							screen:"MainMinor",
							params:{
								screen:"AvailableAppointments",
								initial:false,
								params:{
									screen:"AppointmentDetailsScreen",
									initial:false,
									params:{treatmentId:row.item.id},
								}
							}
						})
					}}>
						<Text>{row.item.detail}</Text>
					</TouchableOpacity>
				</View>
				)}/>
			</View>}
			{searchResult && searchResult.rewards && searchResult.rewards.length > 0 && <View style={styles.height}>
				<View style={styles.titleTextContainer}>
					<Text style={styles.title}>禮物</Text>
				</View>
				<FlatList<KeywordResultRewards>
				data={searchResult.rewards} renderItem={(row)=>(
				<View key={row.item.id} style={styles.contentTextContainer}>
					<Text style={styles.contentText}>{row.item.name}</Text>
					<View style={styles.contentDescriptionContainer}>
						<Text style={styles.contentDescription}>內容</Text>
					</View>
					<TouchableOpacity style={styles.contentContainer} onPress={() => {
						navigation.navigate("Main",{
							screen:"MainMinor",
							params:{
								screen:"Gift",
								initial:false,
								params:{
									screen:"GiftDetailsScreen",
									initial:false,
									params:{rewardId:row.item.id},
								}
							}
						})
					}}>
						<Text>{row.item.detail}</Text>
					</TouchableOpacity>
				</View>
				)}/>
			</View>}
			{searchResultAgent && searchResultAgent.treatments && searchResultAgent.treatments.length > 0 && <View style={styles.height}>
				<View style={styles.titleTextContainer}>
					<Text style={styles.title}>療程</Text>
				</View>
				<FlatList<KeywordResultAgentTreatments>
				data={searchResultAgent.treatments} renderItem={(row)=>(
				<View key={row.item.id} style={styles.contentTextContainer}>
					<Text style={styles.contentText}>{row.item.title}</Text>
					<View style={styles.contentDescriptionContainer}>
						<Text style={styles.contentDescription}>內容</Text>
					</View>
					<TouchableOpacity style={styles.contentContainer} onPress={() => {
						navigation.navigate("AgentMain",{
							screen:"AgentMainMinor",
							params:{
								screen:"AgentAvailableAppointments",
								initial:false,
								params:{
									screen:"AgentAppointmentDetailsScreen",
									initial:false,
									params:{treatmentId:row.item.id},
								}
							}
						})
					}}>
						<Text>{row.item.detail}</Text>
					</TouchableOpacity>
				</View>
				)}/>
			</View>}
			{searchResultAgent && searchResultAgent.contact && searchResultAgent.contact.length > 0 && <View style={styles.height}>
				<View style={styles.titleTextContainer}>
					<Text style={styles.title}>客戶</Text>
				</View>
				<FlatList<KeywordResultAgentContacts>
				data={searchResultAgent.contact} renderItem={(row)=>{
					const today = new Date().toISOString().slice(0,10);
					let age;
					if((parseInt(today.slice(6,8)) - parseInt(row.item.date_of_birth.slice(6,8)) < 0)){
						age = parseInt(today.slice(0,4)) - parseInt(row.item.date_of_birth.slice(0,4)) - 1
					}else if((parseInt(today.slice(6,8)) - parseInt(row.item.date_of_birth.slice(6,8))) === 0){
						if((parseInt(today.slice(10,12)) - parseInt(row.item.date_of_birth.slice(10,12)) < 0)){
							age = parseInt(today.slice(0,4)) - parseInt(row.item.date_of_birth.slice(0,4)) - 1
						}else{
							age = parseInt(today.slice(0,4)) - parseInt(row.item.date_of_birth.slice(0,4))
						}
					}else{
						age = parseInt(today.slice(0,4)) - parseInt(row.item.date_of_birth.slice(0,4))
					}
					return(
						<View key={row.item.id} style={styles.contentTextContainer}>
							<Text style={styles.contentText}>{row.item.display_name}</Text>
							<View style={styles.contentDescriptionContainer}>
								<Text style={styles.contentDescription}>資料</Text>
							</View>
							<TouchableOpacity style={styles.contentContactContainer}>
								<Text>姓名: {row.item.display_name}</Text>
								<Text>年齡: {age}</Text>
								<Text>性別: {row.item.gender}</Text>
							</TouchableOpacity>
						</View>
					)
				}}/>
			</View>}
		</View>
	)
}

export default SearchScreen;