import {RouteProp,useNavigation,useRoute} from "@react-navigation/native";
import React,{useEffect} from "react";
import {View,Image,Text,useColorScheme,TouchableOpacity} from "react-native";
import {Colors} from "react-native/Libraries/NewAppScreen";
import {useDispatch,useSelector} from "react-redux";
import {IRootState} from "../store";
import {styles} from "./styles/GiftDetailsScreenStyles";
import {RewardList} from "../models";
import envfile from "../envfile";
import {addToCart} from "./redux/redeem/thunks";
import {updateSearchWord} from "./redux/searchHeader/actions";

function GiftDetailsScreen(){
	const isDarkMode = useColorScheme() === "dark";
	const navigation = useNavigation();
	const dispatch = useDispatch();
	const route = useRoute<RouteProp<RewardList,"Detail">>();
	const rewards = useSelector((state:IRootState)=>state.redeemPage.reward);

	useEffect(() => {
		dispatch(updateSearchWord(""))
	},[dispatch])

	return(
		<View style={[styles.pageContainer,{backgroundColor:Colors.white}]}>
			<View style={styles.imageContainer}>
				<Image source={{uri:`${envfile.REACT_APP_IMAGE_PATH}/rewards/${rewards.filter
				(reward=>reward.id === route.params.rewardId).map(reward=>reward.image)}`}} style={styles.image}/>
				{/* match the specific id and replace array with the image of that id */}
			</View>
			<View style={styles.timeContainer}>
				<View style={styles.titleTextContainer}>
					<Text style={styles.title}>{rewards.filter(reward=>reward.id === route.params.rewardId)
					.map(reward=>reward.name)}</Text>
				</View>
				<Text style={styles.pointText}>{rewards.filter(reward=>reward.id === route.params.rewardId)
				.map(reward=>reward.type)}</Text>
			</View>
			<View style={styles.timeContainer}>
				<Text style={styles.introductionText}>所需積分</Text>
				<Text style={styles.timeText}>{rewards.filter(reward=>reward.id === route.params.rewardId)
				.map(reward=>reward.redeem_points)} pts</Text>
			</View>
			<TouchableOpacity style={styles.confirmButton} onPress={() => {
				{route.params.rewardId && dispatch(addToCart(route.params.rewardId))}
				{route.params.rewardId && navigation.navigate("GiftScreen")}
			}}>
				<Text style={styles.confirmText}>加入禮品車</Text>
			</TouchableOpacity>
			<TouchableOpacity style={styles.backButton} onPress={() => {
				navigation.navigate("GiftScreen")
			}}>
				<Text style={styles.backText}>返回</Text>
			</TouchableOpacity>
		</View>
	)
}

export default GiftDetailsScreen;