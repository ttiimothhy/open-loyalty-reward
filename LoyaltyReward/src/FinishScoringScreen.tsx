import {useNavigation} from "@react-navigation/native";
import React from "react";
import {SafeAreaView,View,Image,Text,TouchableOpacity} from "react-native";
import {styles} from "./styles/FinishScoringScreenStyles";

function FinishScoringScreen(){
	const navigation = useNavigation();

	return(
		<SafeAreaView>
			<View style={styles.imageContainer}>
				<Image style={styles.image} source={require("../images/trophy.png")}/>
				<Text style={styles.successText}>感謝你的評分</Text>
			</View>
			<TouchableOpacity style={styles.confirmButton} onPress={() => {
				navigation.navigate("MyAppointmentsScreen")
			}}>
				<Text style={styles.confirmText}>返回</Text>
			</TouchableOpacity>
		</SafeAreaView>
	)
}

export default FinishScoringScreen;