import AsyncStorage from "@react-native-community/async-storage";
import {Dispatch} from "redux";
import envfile from "../../../envfile";
import {getAllRewardsSuccess,getAllRewardTypesSuccess,getALLPickupLocationsSuccess,getFilteredRewardsSuccess,
getRewardWithIdSuccess,getPointValueSuccess,failed,IRedeemActions, addToCartSuccess, minusFromCartSuccess,
deleteFromCartSuccess, getInStockItemSuccess, getOutOfStockItemSuccess,checkoutCartSuccess,
getOrderDetailSuccess,
updateCartList,
resetAdd,
resetMinus,
resetDelete,
noEnoughPoint} from "./actions";

export function getAllRewards() {
	return async (dispatch:Dispatch<IRedeemActions>) => {
		const token = await AsyncStorage.getItem("token");
		// console.log(token)
		const res = await fetch(`${envfile.REACT_APP_BACKEND_PORT}/reward/all`, {
			headers: {
				Authorization: `Bearer ${token}`
			}
		})
		const result = await res.json();
		// console.log(result);
		if(!result.message){
			dispatch(getAllRewardsSuccess(result));
		} else {
			dispatch(failed("Get_all_rewards_failed", result.message));
		}
	}
}

export function getAllRewardTypes() {
	return async (dispatch: Dispatch<IRedeemActions>) => {
		const token = await AsyncStorage.getItem("token");
		// console.log(token)
		const res = await fetch(`${envfile.REACT_APP_BACKEND_PORT}/reward/type`, {
			headers: {
				Authorization: `Bearer ${token}`
			}
		})
		const result = await res.json();
		// console.log(result);
		if (!result.message) {
			dispatch(getAllRewardTypesSuccess(result));
		} else {
			dispatch(failed("Get_all_reward_types_failed", result.message));
		}
	}
}

export function getAllPickupLocations() {
	return async (dispatch: Dispatch<IRedeemActions>) => {
		const token = await AsyncStorage.getItem("token");
		// console.log(token)
		const res = await fetch(`${envfile.REACT_APP_BACKEND_PORT}/reward/location`, {
			headers: {
				Authorization: `Bearer ${token}`
			}
		})
		const result = await res.json();
		// console.log(result);
		if (!result.message) {
			dispatch(getALLPickupLocationsSuccess(result));
		} else {
			dispatch(failed("Get_all_pickup_locations_failed", result.message));
		}
	}
}

export function getFilteredRewards(type_id:number|null) {
	return async (dispatch: Dispatch<IRedeemActions>) => {
		const token = await AsyncStorage.getItem("token");
		// console.log(token)
		if(type_id){
			const res = await fetch(`${envfile.REACT_APP_BACKEND_PORT}/reward?type=${type_id}&location=0`, {
				headers: {
					Authorization: `Bearer ${token}`
				}
			})
			const result = await res.json();
			// console.log(result);
			if(!result.message){
				dispatch(getFilteredRewardsSuccess(result));
			}else{
				dispatch(failed("Get_filtered_rewards_failed", result.message));
			}
		}
	}
}

export function getRewardWithId(reward_id: string) {
	return async (dispatch: Dispatch<IRedeemActions>) => {
		const token = await AsyncStorage.getItem("token");
		// console.log(token)
		const res = await fetch(`${envfile.REACT_APP_BACKEND_PORT}/reward/${reward_id}`, {
			headers: {
				Authorization: `Bearer ${token}`
			}
		})
		const result = await res.json();
		// console.log(result);
		if(!result.message){
			dispatch(getRewardWithIdSuccess(result));
		}else{
			dispatch(failed("Get_reward_with_id_failed", result.message));
		}
	}
}

export function getPointValue() {
	return async (dispatch: Dispatch<IRedeemActions>) => {
		const token = await AsyncStorage.getItem("token");
		// console.log(token)
		const res = await fetch(`${envfile.REACT_APP_BACKEND_PORT}/index/points`, {
			headers: {
				Authorization: `Bearer ${token}`
			}
		})
		const result = await res.json();
		// console.log(result);
		if(result.success){
			// console.log(result.current_point)
			dispatch(getPointValueSuccess(result.current_point));
		}else{
			dispatch(failed("Get_point_value_failed", result.message));
		}
	}
}

export function addToCart(rewardId:number|null){
	return async (dispatch: Dispatch<IRedeemActions>) => {
		const token = await AsyncStorage.getItem("token");
		// console.log(rewardId)
		if(rewardId){
			const res = await fetch(`${envfile.REACT_APP_BACKEND_PORT}/cart/add/${rewardId}`, {
				method:"post",
				headers: {
					Authorization: `Bearer ${token}`,
					"Content-Type": "application/json"
				},
				body:JSON.stringify({rewardId,role:"customer"})
			});
			const result = await res.json();
			// console.log(result);
			if(result.success){
				dispatch(addToCartSuccess(result.success));
				// dispatch(updateAddToCart(rewardId))
			}else{
				dispatch(failed("Add_to_cart_failed", result.message));
			}
		}
	}
}

export function minusFromCart(rewardId:number|null){
	return async (dispatch: Dispatch<IRedeemActions>) => {
		const token = await AsyncStorage.getItem("token");
		// console.log(token)
		if(rewardId){
			const res = await fetch(`${envfile.REACT_APP_BACKEND_PORT}/cart/minus/${rewardId}`, {
				method:"put",
				headers: {
					Authorization: `Bearer ${token}`,
					"Content-Type": "application/json"
				},
				body:JSON.stringify({rewardId,role:"customer"})
			});
			const result = await res.json();
			// console.log(result);
			if(result.success){
				dispatch(minusFromCartSuccess(result.success));
				// dispatch(updateMinusCart(rewardId))
			}else{
				dispatch(failed("Minus_from_cart_failed", result.message));
			}
		}
	}
}

export function deleteFromCart(rewardId:number|null){
	return async (dispatch: Dispatch<IRedeemActions>) => {
		const token = await AsyncStorage.getItem("token");
		// console.log(token)
		if(rewardId){
			const res = await fetch(`${envfile.REACT_APP_BACKEND_PORT}/cart/delete/${rewardId}`, {
				method:"put",
				headers: {
					Authorization: `Bearer ${token}`,
					"Content-Type": "application/json"
				},
				body:JSON.stringify({rewardId,role:"customer"})
			});
			const result = await res.json();
			// console.log(result);
			if(result.success){
				dispatch(deleteFromCartSuccess(result.success));
				// dispatch(updateCartList(rewardId))
			}else{
				dispatch(failed("Delete_from_cart_failed", result.message));
			}
		}
	}
}


export function getInStockItem(){
	return async (dispatch: Dispatch<IRedeemActions>) => {
		const token = await AsyncStorage.getItem("token");
		// console.log(token)
		const res = await fetch(`${envfile.REACT_APP_BACKEND_PORT}/cart/inStock`, {
			headers: {
				Authorization: `Bearer ${token}`
			}
		})
		const result = await res.json();
		// console.log(result);
		if(!result.message){
			dispatch(getInStockItemSuccess(result));
			dispatch(resetAdd())
			dispatch(resetMinus())
			dispatch(resetDelete())
		}else{
			dispatch(failed("Get_in_stock_item_failed", result.message));
		}
	}
}

export function getOutOfStockItem(){
	return async (dispatch: Dispatch<IRedeemActions>) => {
		const token = await AsyncStorage.getItem("token");
		// console.log(token)
		const res = await fetch(`${envfile.REACT_APP_BACKEND_PORT}/cart/outOfStock`, {
			headers: {
				Authorization: `Bearer ${token}`
			}
		})
		const result = await res.json();
		// console.log(result);
		if(!result.message){
			dispatch(getOutOfStockItemSuccess(result));
			dispatch(resetAdd())
			dispatch(resetMinus())
			dispatch(resetDelete())
		}else{
			dispatch(failed("Get_out_of_stock_item_failed", result.message));
		}
	}
}

export function checkoutCart(pickup_location_id:number|null,total_redeem_points:number|null){
	return async (dispatch: Dispatch<IRedeemActions>) => {
		const token = await AsyncStorage.getItem("token");
		// console.log(token)
		const res = await fetch(`${envfile.REACT_APP_BACKEND_PORT}/cart/redeem`, {
			method:"post",
			headers: {
				Authorization: `Bearer ${token}`,
				"Content-Type": "application/json"
			},
			body:JSON.stringify({pickup_location_id,total_redeem_points,role:"customer"})
		});
		const result = await res.json();
		// console.log(result);
		if(!result.message){
			dispatch(checkoutCartSuccess(result));
		}else{
			dispatch(failed("Checkout_cart_failed", result.message));
			if(result.message === "not enough points"){
				dispatch(noEnoughPoint());
			}
		}
	}
}

export function getOrderDetail(orderRef:string|undefined){
	return async (dispatch: Dispatch<IRedeemActions>) => {
		const token = await AsyncStorage.getItem("token");
		// console.log(token)
		const res = await fetch(`${envfile.REACT_APP_BACKEND_PORT}/points/history/redeemed/${orderRef}`, {
			headers: {
				Authorization: `Bearer ${token}`
			}
		})
		const result = await res.json();
		// console.log(result);
		if(!result.message){
			dispatch(getOrderDetailSuccess(result));
		}else{
			dispatch(failed("Get_order_detail_failed", result.message));
		}
	}
}