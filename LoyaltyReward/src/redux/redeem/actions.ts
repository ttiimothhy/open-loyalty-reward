import {InStockItemDetail,RedeemOrderDetails,OutOfStockItemDetail,Reward,RewardPickupLocation,RewardType} from "../../../models"

export function getAllRewardsSuccess(reward:Reward[]){
    return{
        type:"@@redeem/Get_all_rewards" as const,
        reward,
    }
}

export function getAllRewardTypesSuccess(rewardType:RewardType[]){
    return{
        type:"@@redeem/Get_all_reward_types" as const,
        rewardType
    }
}

export function getALLPickupLocationsSuccess(rewardPickupLocation:RewardPickupLocation[]){
    return{
        type:"@@redeem/Get_all_pickup_locations" as const,
        rewardPickupLocation
    }
}

export function getFilteredRewardsSuccess(reward:Reward[]){
    return{
        type:"@@redeem/Get_filtered_rewards" as const,
        reward
    }
}

export function getRewardWithIdSuccess(reward:Reward[]){
    return{
        type:"@@redeem/Get_reward_with_id" as const,
        reward
    }
}

export function getPointValueSuccess(point:number){
    return{
        type:"@@redeem/Get_point_value" as const,
        point
    }
}

export function addToCartSuccess(addToCartResult:boolean|null){
    return{
        type:"@@redeem/Add_to_cart" as const,
        addToCartResult
    }
}

export function minusFromCartSuccess(minusFromCartResult:boolean|null){
    return{
        type:"@@redeem/Minus_from_cart" as const,
        minusFromCartResult
    }
}

export function deleteFromCartSuccess(deleteFromCartResult:boolean|null){
    return{
        type:"@@redeem/Delete_from_cart" as const,
        deleteFromCartResult
    }
}

export function getInStockItemSuccess(inStockItemDetail:InStockItemDetail[]){
    return{
        type:"@@redeem/Get_in_stock_item" as const,
        inStockItemDetail
    }
}

export function getOutOfStockItemSuccess(outOfStockItemDetail:OutOfStockItemDetail[]){
    return{
        type:"@@redeem/Get_out_of_stock_item" as const,
        outOfStockItemDetail
    }
}

export function checkoutCartSuccess(checkoutResult:RedeemOrderDetails){
    return{
        type:"@@redeem/Checkout_cart" as const,
        checkoutResult
    }
}

export function getOrderDetailSuccess(orderDetail:RedeemOrderDetails){
    return{
        type:"@@redeem/Get_order_detail" as const,
        orderDetail
    }
}

export function updateCartList(rewardId:number){
	return{
		type:"@@redeem/Update_cart_list" as const,
		rewardId
	}
}

export function updateAddCart(rewardId:number){
	return{
		type:"@@redeem/Update_add_cart" as const,
		rewardId
	}
}

export function updateMinusCart(rewardId:number){
	return{
		type:"@@redeem/Update_minus_cart" as const,
		rewardId
	}
}

export function resetAdd(){
	return{
		type:"@@redeem/Reset_add_result" as const,
	}
}

export function resetMinus(){
	return{
		type:"@@redeem/Reset_minus_result" as const,
	}
}

export function resetDelete(){
	return{
		type:"@@redeem/Reset_delete_result" as const,
	}
}

export function changeNoLocationToFalse(){
	return{
		type:"@@redeem/Change_no_location_to_false" as const,
	}
}

export function changeNoLocationToNull(){
	return{
		type:"@@redeem/Change_no_location_to_null" as const,
	}
}

export function noEnoughPoint(){
	return{
		type:"@@redeem/No_enough_point" as const,
	}
}

export function noEnoughPointToFalse(){
	return{
		type:"@@redeem/No_enough_point_false" as const,
	}
}

type Failed = "Get_all_rewards_failed"|"Get_all_reward_types_failed"|"Get_all_pickup_locations_failed"|
"Get_filtered_rewards_failed"|"Get_reward_with_id_failed"|"Get_point_value_failed"|"Add_to_cart_failed"|
"Minus_from_cart_failed"|"Delete_from_cart_failed"|"Get_in_stock_item_failed"|"Get_out_of_stock_item_failed"|
"Checkout_cart_failed"|"Get_order_detail_failed";

export function failed(type:Failed,message:string){
	return {
		type,
		message
	}
}

export type IRedeemActions = ReturnType<typeof getAllRewardsSuccess|typeof getAllRewardTypesSuccess|
typeof getALLPickupLocationsSuccess|typeof getFilteredRewardsSuccess|typeof getRewardWithIdSuccess|
typeof getPointValueSuccess|typeof addToCartSuccess|typeof minusFromCartSuccess|typeof deleteFromCartSuccess|
typeof getInStockItemSuccess|typeof getOutOfStockItemSuccess|typeof checkoutCartSuccess|
typeof getOrderDetailSuccess|typeof updateCartList|typeof resetAdd|typeof resetMinus|typeof resetDelete|
typeof changeNoLocationToFalse|typeof changeNoLocationToNull|typeof noEnoughPoint|
typeof noEnoughPointToFalse|typeof failed>