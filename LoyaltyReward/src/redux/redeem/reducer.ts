import {InStockItemDetail,OutOfStockItemDetail,Reward,RewardPickupLocation,RewardType,RedeemOrderDetails} from "../../../models";
import {IRedeemActions} from "./actions";

export interface IRedeemState{
    reward:Reward[];
    currentPoint:number|null;
    rewardType:RewardType[];
    rewardPickupLocation:RewardPickupLocation[];
	addToCartResult:boolean|null;
	minusFromCartResult:boolean|null;
	deleteFromCartResult:boolean|null;
	inStockItemDetail:InStockItemDetail[];
	outOfStockItemDetail:OutOfStockItemDetail[];
	checkoutResult:RedeemOrderDetails;
	orderDetail:RedeemOrderDetails;
	noPickLocation:boolean|null;
	noEnoughPoint:boolean;
}

const initialState = {
    reward:[],
    currentPoint:null,
    rewardType:[],
    rewardPickupLocation:[],
	addToCartResult:null,
	minusFromCartResult:null,
	deleteFromCartResult:null,
	inStockItemDetail:[],
	outOfStockItemDetail:[],
	checkoutResult:{
		redeem_items:[],
		branch:{
			order_ref:"",
			date:"",
			location:"",
			points:"",
			qr_code:"",
			is_pickup:""
		}
	},
	orderDetail:{
		redeem_items:[],
		branch:{
			order_ref:"",
			date:"",
			location:"",
			points:"",
			qr_code:"",
			is_pickup:""
		}
	},
	noPickLocation:null,
	noEnoughPoint:false
}

const redeemReducer = (state: IRedeemState = initialState,action:IRedeemActions):IRedeemState => {
	switch(action.type){
		case "@@redeem/Get_all_rewards":
            return{
                ...state,
				reward:action.reward
            }
		case "@@redeem/Get_all_reward_types":
			return{
				...state,
				rewardType:action.rewardType
			}
		case "@@redeem/Get_all_pickup_locations":
			return{
				...state,
                rewardPickupLocation:action.rewardPickupLocation
			}
		case "@@redeem/Get_filtered_rewards":
			return{
				...state,
				reward:action.reward
			}
		case "@@redeem/Get_reward_with_id":
			return{
				...state,
                reward:action.reward
			}
		case "@@redeem/Get_point_value":
			return{
				...state,
                currentPoint:action.point
			}
		case "@@redeem/Add_to_cart":
			return{
				...state,
				addToCartResult:action.addToCartResult
			}
		case "@@redeem/Minus_from_cart":
			return{
				...state,
				minusFromCartResult:action.minusFromCartResult
			}
		case "@@redeem/Delete_from_cart":
			return{
				...state,
				deleteFromCartResult:action.deleteFromCartResult
			}
		case "@@redeem/Get_in_stock_item":
			return{
				...state,
				inStockItemDetail:action.inStockItemDetail
			}
		case "@@redeem/Get_out_of_stock_item":
			return{
				...state,
				outOfStockItemDetail:action.outOfStockItemDetail
			}
		case "@@redeem/Checkout_cart":
			return{
				...state,
				checkoutResult:action.checkoutResult
			}
		case "@@redeem/Get_order_detail":
			return{
				...state,
				orderDetail:action.orderDetail
			}
		case "@@redeem/Update_cart_list":
			return{
				...state,
				inStockItemDetail:state.inStockItemDetail.filter(stock=>parseInt(stock.id) !== action.rewardId),
				outOfStockItemDetail:state.outOfStockItemDetail.filter(stock=>parseInt(stock.id) !== action.rewardId)
			}
		case "@@redeem/Reset_add_result":
			return{
				...state,
				addToCartResult:null
			}
		case "@@redeem/Reset_minus_result":
			return{
				...state,
				minusFromCartResult:null
			}
		case "@@redeem/Reset_delete_result":
			return{
				...state,
				deleteFromCartResult:null
			}
		case "@@redeem/Change_no_location_to_false":
			return{
				...state,
				noPickLocation:false
			}
		case "@@redeem/Change_no_location_to_null":
			return{
				...state,
				noPickLocation:null
			}
		case "@@redeem/No_enough_point":
			return{
				...state,
				noEnoughPoint:true
			}
		case "@@redeem/No_enough_point_false":
			return{
				...state,
				noEnoughPoint:false
			}
		default:
			return state;
	}
}

export default redeemReducer;