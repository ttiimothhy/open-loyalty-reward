import AsyncStorage from "@react-native-community/async-storage";
import {Dispatch} from "redux";
import envfile from "../../../envfile";
import {IAuthActions,loginFailure,loginSuccess} from "../agentAuth/actions";
import {ISocialAuthActions} from "./actions";

export function handleAgentGoogleAuthorize(accessToken:string){
	return async(dispatch:Dispatch<IAuthActions|ISocialAuthActions>) => {
		const res = await fetch(`${envfile.REACT_APP_BACKEND_PORT}/users/googleLogin/agents`,{
			method:"post",
			headers:{
				"Content-Type":"application/json"
			},
			body:JSON.stringify({accessToken,role:"agent"})
		});
		const result = await res.json();
		// console.log(result);
		if(result.success){
			dispatch(loginSuccess(result.id,result.data));
			await AsyncStorage.setItem("token",result.data)
		}else{
			dispatch(loginFailure(result.message));
		}
	}
}

export function handleAgentFacebookAuthorize(token:string|undefined,email:string|undefined){
	return async(dispatch:Dispatch<IAuthActions|ISocialAuthActions>) => {
		if(token && email){
			// console.log(accessToken)
			const res = await fetch(`${envfile.REACT_APP_BACKEND_PORT}/users/facebookLogin/agents`,{
				method:"post",
				headers:{
					"Content-Type":"application/json"
				},
				body:JSON.stringify({token,email})
			});
			const result = await res.json();
			// console.log(result);
			if(result.success){
				dispatch(loginSuccess(result.id,result.data));
				await AsyncStorage.setItem("token",result.data)
			}else{
				dispatch(loginFailure(result.message));
			}
		}
	}
}

export function handleAppleAuthorize(clientId:string,identityToken:string|null,authorizationCode:string|null){
	return async(dispatch:Dispatch<IAuthActions|ISocialAuthActions>) => {
		// console.log(accessToken)
		// console.log(email)
		// console.log(accessToken)
		if(identityToken && authorizationCode){
			const res = await fetch(`${envfile.REACT_APP_BACKEND_PORT}/users/appleLogin/agents`,{
				method:"post",
				headers:{
					"Content-Type":"application/json"
				},
				body:JSON.stringify({clientId,identityToken,authorizationCode})
			});
			const result = await res.json();
			// console.log(result);
			if(result.success){
				dispatch(loginSuccess(result.id,result.data));
				await AsyncStorage.setItem("token",result.data)
			}else{
				dispatch(loginFailure(result.message));
			}
		}
	}
}
