import {IReactNativeAuthToken} from "./reducer";

export function loginAgentGoogleSuccess(token:IReactNativeAuthToken){
	return{
		type:"@@socialAgentAuth/Login_google" as const,
		token
	}
}

export type ISocialAuthActions = ReturnType<typeof loginAgentGoogleSuccess>