import {KeywordResult,KeywordResultAgent} from "../../../models";

export function updateSearchWord(search:string){
	return{
		type:"@@searchHeader/Update_search_word" as const,
		search
	}
}

export function searchKeywordSuccess(search:KeywordResult){
	return{
		type:"@@searchHeader/Search_keyword" as const,
		search
	}
}

export function searchKeywordSuccessAgent(search:KeywordResultAgent){
	return{
		type:"@@searchHeader/Search_keyword_agent" as const,
		search
	}
}

export function searchKeywordFail(){
	return{
		type:"@@searchHeader/Search_keyword_fail" as const,
	}
}

export function searchKeywordFailAgent(){
	return{
		type:"@@searchHeader/Search_keyword_agent_fail" as const,
	}
}

export function resultNull(){
	return{
		type:"@@searchHeader/Set_result_null" as const,
	}
}

type Failed = "Search_keyword_failed"
export function failed(type:Failed,message:string){
	return{
		type,
		message
	}
}

export type ISearchHeaderActions = ReturnType<typeof updateSearchWord|typeof searchKeywordSuccess|typeof searchKeywordSuccessAgent|
typeof resultNull|typeof searchKeywordFail|typeof searchKeywordFailAgent|typeof failed>