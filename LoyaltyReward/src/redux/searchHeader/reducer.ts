import {KeywordResult, KeywordResultAgent} from "../../../models";
import {ISearchHeaderActions} from "./actions";

export interface ISearchHeaderState{
	searchWord:string;
	result:KeywordResult|null;
	resultAgent:KeywordResultAgent|null;
}

const initialState = {
	searchWord:"",
	result:null,
	resultAgent:null
}

export const searchHeaderReducer = (state:ISearchHeaderState = initialState,action:ISearchHeaderActions):ISearchHeaderState => {
	switch(action.type){
		case "@@searchHeader/Update_search_word":
			return{
				...state,
				searchWord:action.search
			}
		case "@@searchHeader/Search_keyword":
			return{
				...state,
				result:action.search
			}
		case "@@searchHeader/Search_keyword_agent":
			return{
				...state,
				resultAgent:action.search
			}
		case "@@searchHeader/Set_result_null":
			return{
				...state,
				result:null,
				resultAgent:null
			}
		case "@@searchHeader/Search_keyword_fail":
			return{
				...state,
				result:null
			}
		case "@@searchHeader/Search_keyword_agent_fail":
			return{
				...state,
				resultAgent:null
			}
		default:
			return state
	}
}