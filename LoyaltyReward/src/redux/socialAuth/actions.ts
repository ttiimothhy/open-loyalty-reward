import {IReactNativeAuthToken} from "./reducer";

export function loginGoogleSuccess(token:IReactNativeAuthToken){
	return{
		type:"@@socialAuth/Login_google" as const,
		token
	}
}

export type ISocialAuthActions = ReturnType<typeof loginGoogleSuccess>