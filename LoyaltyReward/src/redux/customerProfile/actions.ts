import {UserProfile} from "../../../models";

export function getUserProfileSuccess(userProfile:UserProfile){
    return{
        type:"@@customerProfile/Get_user_profile" as const,
        userProfile
    }
}

export function updateUserProfileSuccess(username:string,email:string,mobile:string,date_of_birth:string,display_name:string,gender:string){
    return{
        type:"@@customerProfile/Update_user_profile" as const, // to confirm whether code as this way
        username,
		email,
		mobile,
		date_of_birth,
		display_name,
		gender
    }
}

export function updateUsername(username:string){
	return{
		type:"@@customerProfile/Update_username" as const,
		username
	}
}

export function updateDisplayName(display_name:string){
	return{
		type:"@@customerProfile/Update_display_name" as const,
		display_name
	}
}

export function updatePassword(password:string){
	return{
		type:"@@customerProfile/Update_password" as const,
		password
	}
}

export function updateEmail(email:string){
	return{
		type:"@@customerProfile/Update_email" as const,
		email
	}
}

export function updateMobile(mobile:string){
	return{
		type:"@@customerProfile/Update_mobile" as const,
		mobile
	}
}

export function updateBirthday(birthday:string){
	return{
		type:"@@customerProfile/Update_birthday" as const,
		birthday
	}
}

export function updateGender(gender:string){
	return{
		type:"@@customerProfile/Update_gender" as const,
		gender
	}
}

export function updateProfileFailure(){
	return{
		type:"@@customerProfile/Update_profile_fail" as const,
	}
}

type Failed = "Get_user_profile_failed"|"Update_user_profile_failed";
export function failed(type:Failed,message:string){
	return {
		type,
		message
	}
}

export type ICustomerProfileActions = ReturnType<typeof getUserProfileSuccess|typeof updateUserProfileSuccess|typeof updateUsername|typeof updateDisplayName|
typeof updatePassword|typeof updateEmail|typeof updateMobile|typeof updateBirthday|typeof updateGender|
typeof updateProfileFailure|typeof failed>