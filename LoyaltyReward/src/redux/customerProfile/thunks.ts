import AsyncStorage from "@react-native-community/async-storage";
import {Dispatch} from "redux";
import envfile from "../../../envfile";
import {getUserProfileSuccess,failed,updateUserProfileSuccess,ICustomerProfileActions,updateProfileFailure} from "./actions";

export function getUserProfile(){
	return async(dispatch:Dispatch<ICustomerProfileActions>) => {
		const token = await AsyncStorage.getItem("token");
		// console.log(token)
		const res = await fetch(`${envfile.REACT_APP_BACKEND_PORT}/profile/user`,{
			headers:{
				Authorization:`Bearer ${token}`
			}
		})
		const result = await res.json();
		// console.log(result);
		if(!result.message){
			dispatch(getUserProfileSuccess(result))
		}else{
			dispatch(failed("Get_user_profile_failed",result.message))
		}
	}
}

export function updateUserProfile(username:string,password:string,email:string,mobile:number,birthday:string,
display_name:string,gender_id:number,gender:string){ // to confirm format of date_of_birth
	return async(dispatch:Dispatch<ICustomerProfileActions>) => {
		// console.log(username,password,email,mobile,birthday,display_name,gender_id,gender)
		let date_of_birth = birthday + " 00:00:00"
		const token = await AsyncStorage.getItem("token") // need this line?
		const res = await fetch(`${envfile.REACT_APP_BACKEND_PORT}/profile/update`,{
            method:"post",
			headers:{
				Authorization:`Bearer ${token}`,
				"Content-Type":"application/json" // need to change? "Content-Type":"application/json",???
			},
            body:JSON.stringify({username,password,email,mobile,date_of_birth,display_name,gender_id}) // to be changed
		})
		const result = await res.json();
		// console.log(result);
		if(result.success){
			dispatch(updateUserProfileSuccess(username,email,mobile + "",birthday,display_name,gender))
		}else{
			dispatch(failed("Update_user_profile_failed",result.message))
			dispatch(updateProfileFailure())
		}
	}
}