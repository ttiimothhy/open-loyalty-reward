import AsyncStorage from "@react-native-community/async-storage";
import {Dispatch} from "redux";
import envfile from "../../../envfile";
import {IPointsActions,getPointsEarnedHistorySuccess,failed,getPointsRedeemedHistorySuccess, getExpiredPointsHistorySuccess} from "./actions";

export function getPointsEarnedHistory(){
	return async(dispatch:Dispatch<IPointsActions>) => {
		const token = await AsyncStorage.getItem("token");
		// console.log(token)
		const res = await fetch(`${envfile.REACT_APP_BACKEND_PORT}/points/history/earned`,{
			headers:{
				Authorization:`Bearer ${token}`
			}
		})
		const result = await res.json();
		// console.log(result)
		if(!result.message){
			dispatch(getPointsEarnedHistorySuccess(result));
		}else{
			dispatch(failed("Get_points_earned_history_failed",result.message));
		}
	}
}

export function getPointsRedeemedHistory(){
	return async(dispatch:Dispatch<IPointsActions>) => {
		const token = await AsyncStorage.getItem("token");
		// console.log(token)
		const res = await fetch(`${envfile.REACT_APP_BACKEND_PORT}/points/history/redeemed`,{
			headers:{
				Authorization:`Bearer ${token}`
			}
		})
		const result = await res.json();
		// console.log(result)
		if(!result.message){
			dispatch(getPointsRedeemedHistorySuccess(result));
		}else{
			dispatch(failed("Get_points_redeemed_history_failed",result.message));
		}
	}
}

export function getExpiredPointsHistory(){
	return async(dispatch:Dispatch<IPointsActions>) => {
		const token = await AsyncStorage.getItem("token");
		const res = await fetch(`${envfile.REACT_APP_BACKEND_PORT}/points/history/expired`,{
			headers:{
				Authorization:`Bearer ${token}`
			}
		})
		const result = await res.json();
		// console.log(result)
		if(!result.message){
			dispatch(getExpiredPointsHistorySuccess(result));
		}else{
			dispatch(failed("Get_expired_points_history_failed",result.message));
		}
	}
}