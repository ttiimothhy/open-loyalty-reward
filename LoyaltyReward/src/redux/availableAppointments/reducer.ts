import {PlaceCriteria, Treatment,TypeCriteria} from "../../../models";
import {IAvailableAppointmentsActions} from "./actions";

export interface IAvailableAppointmentsState{
	treatment:Treatment[];
	type:TypeCriteria[];
	place:PlaceCriteria[];
	timeslotSuccess:boolean|null;
}

const initialState = {
	treatment:[],
	type:[],
	place:[],
	timeslotSuccess:null
}

const availableAppointmentsReducer = (state:IAvailableAppointmentsState = initialState,action:IAvailableAppointmentsActions):
IAvailableAppointmentsState => {
	switch(action.type){
		case "@@availableAppointments/Get_all_treatments":
			return{
				...state,
				treatment:action.treatment
		}
		case "@@availableAppointments/Get_type_criteria":
			return{
				...state,
				type:action.criteria
			}
		case "@@availableAppointments/Get_place_criteria":
			return{
				...state,
				place:action.criteria
			}
		case "@@availableAppointments/Get_treatments":
			return{
				...state,
				treatment:action.treatment
			}
		case "@@availableAppointments/Send_timeslot":
			return{
				...state,
				timeslotSuccess:true
			}
		case "@@availableAppointments/Send_timeslot_fail":
			return{
				...state,
				timeslotSuccess:false
			}
		case "@@availableAppointments/Set_timeslot_null":
			return{
				...state,
				timeslotSuccess:null
			}
		default:{
			return state
		}
	}
}

export default availableAppointmentsReducer;