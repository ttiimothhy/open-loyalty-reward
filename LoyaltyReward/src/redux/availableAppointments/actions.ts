import {PlaceCriteria, Treatment,TypeCriteria} from "../../../models";

export function getAllTreatmentsSuccess(treatment:Treatment[]){
	return{
		type:"@@availableAppointments/Get_all_treatments" as const,
		treatment
	}
}

export function typeCriteriaSuccess(criteria:TypeCriteria[]){
	return{
		type:"@@availableAppointments/Get_type_criteria" as const,
		criteria
	}
}

export function placeCriteriaSuccess(criteria:PlaceCriteria[]){
	return{
		type:"@@availableAppointments/Get_place_criteria" as const,
		criteria
	}
}

export function getTreatmentsSuccess(treatment:Treatment[]){
	return{
		type:"@@availableAppointments/Get_treatments" as const,
		treatment
	}
}

export function sendTimeslotSuccess(){
	return{
		type:"@@availableAppointments/Send_timeslot" as const,
	}
}

export function sendTimeslotFail(){
	return{
		type:"@@availableAppointments/Send_timeslot_fail" as const,
	}
}

export function setTimeslotNull(){
	return{
		type:"@@availableAppointments/Set_timeslot_null" as const,
	}
}

type Failed = "Get_all_treatments_failed"|"Get_type_criteria_failed"|"Get_place_criteria_failed"|"Get_treatments_failed"|
"Send_timeslot_failed";
export function failed(type:Failed,message:string){
	return{
		type,
		message
	}
}

export type IAvailableAppointmentsActions = ReturnType<typeof getAllTreatmentsSuccess|typeof typeCriteriaSuccess|
typeof getTreatmentsSuccess|typeof placeCriteriaSuccess|typeof sendTimeslotSuccess|typeof sendTimeslotFail|
typeof setTimeslotNull|typeof failed>