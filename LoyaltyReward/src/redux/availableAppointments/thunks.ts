import AsyncStorage from "@react-native-community/async-storage";
import {Dispatch} from "redux";
import envfile from "../../../envfile";
import {addPendingSchedule,IMyAppointmentsActions} from "../myAppointments/actions";
import {IAvailableAppointmentsActions,getAllTreatmentsSuccess,failed,typeCriteriaSuccess,getTreatmentsSuccess,
placeCriteriaSuccess,sendTimeslotSuccess,sendTimeslotFail} from "./actions";

export function getAllTreatments(){
	return async(dispatch:Dispatch<IAvailableAppointmentsActions>) => {
		const token = await AsyncStorage.getItem("token");
		const res = await fetch(`${envfile.REACT_APP_BACKEND_PORT}/treatment/all`,{
			headers:{
				Authorization:`Bearer ${token}`
			}
		})
		const result = await res.json();
		// console.log(result)
		if(!result.message){
			dispatch(getAllTreatmentsSuccess(result))
		}else{
			dispatch(failed("Get_all_treatments_failed",result.message))
		}
	}
}

export function typeCriteria(){
	return async(dispatch:Dispatch<IAvailableAppointmentsActions>) => {
		const token = await AsyncStorage.getItem("token");
		const res = await fetch(`${envfile.REACT_APP_BACKEND_PORT}/treatment/service`,{
			headers:{
				Authorization:`Bearer ${token}`
			}
		})
		const result = await res.json();
		// console.log(result)
		if(!result.message){
			dispatch(typeCriteriaSuccess(result))
		}else{
			dispatch(failed("Get_type_criteria_failed",result.message))
		}
	}
}

export function placeCriteria(){
	return async(dispatch:Dispatch<IAvailableAppointmentsActions>) => {
		const token = await AsyncStorage.getItem("token");
		const res = await fetch(`${envfile.REACT_APP_BACKEND_PORT}/treatment/branch`,{
			headers:{
				Authorization:`Bearer ${token}`
			}
		})
		const result = await res.json();
		// console.log(result)
		if(!result.message){
			dispatch(placeCriteriaSuccess(result))
		}else{
			dispatch(failed("Get_place_criteria_failed",result.message))
		}
	}
}

export function getTreatments(typeId:number|null,placeId:number|null){
	return async(dispatch:Dispatch<IAvailableAppointmentsActions>) => {
		const token = await AsyncStorage.getItem("token");
		if(typeId || placeId){
			if(typeId){
				const res = await fetch(`${envfile.REACT_APP_BACKEND_PORT}/treatment?service=${typeId}&branch=0`,{
					headers:{
						Authorization:`Bearer ${token}`
					}
				})
				const result = await res.json();
				// console.log(result)
				if(!result.message){
					dispatch(getTreatmentsSuccess(result))
				}else{
					dispatch(failed("Get_treatments_failed",result.message))
				}
			}else if(placeId){
				const res = await fetch(`${envfile.REACT_APP_BACKEND_PORT}/treatment?service=0&branch=${placeId}`,{
					headers:{
						Authorization:`Bearer ${token}`
					}
				})
				const result = await res.json();
				// console.log(result)
				if(!result.message){
					dispatch(getTreatmentsSuccess(result))
				}else{
					dispatch(failed("Get_treatments_failed",result.message))
				}
			}
		}
	}
}

export function sendTimeslot(timeId:string,placeId:string,treatmentId:number|undefined,timeConstraint:string,placeConstraint:string){
	return async(dispatch:Dispatch<IMyAppointmentsActions|IAvailableAppointmentsActions>) => {
		const token = await AsyncStorage.getItem("token");
		if(treatmentId){
			const res = await fetch(`${envfile.REACT_APP_BACKEND_PORT}/appointment/${treatmentId}`,{
				method:"post",
				headers:{
					Authorization:`Bearer ${token}`,
					"Content-Type":"application/json"
				},
				body:JSON.stringify({branch_id:parseInt(placeId),timeslot:timeId})
			})
			const result = await res.json();
			// console.log(result)
			if(result.success){
				dispatch(sendTimeslotSuccess())
				dispatch(addPendingSchedule(treatmentId,timeConstraint,placeConstraint))
			}else{
				dispatch(sendTimeslotFail())
				dispatch(failed("Send_timeslot_failed",result.message))
			}
		}
	}
}