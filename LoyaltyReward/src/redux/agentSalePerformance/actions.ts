import {RankingAppointment, RankingReferral} from "../../../models";

export function getMyPerformanceSuccess(referral:string,appointment:string){
	return{
		type:"@@agentSalePerformance/Get_my_performance" as const,
		referral,
		appointment
	}
}

export function getDayReferralSuccess(rankReferral:RankingReferral[]){
	return{
		type:"@@agentSalePerformance/Get_day_referral" as const,
		rankReferral,
	}
}

export function getMonthReferralSuccess(rankReferral:RankingReferral[]){
	return{
		type:"@@agentSalePerformance/Get_month_referral" as const,
		rankReferral,
	}
}

export function getYearReferralSuccess(rankReferral:RankingReferral[]){
	return{
		type:"@@agentSalePerformance/Get_year_referral" as const,
		rankReferral,
	}
}

export function getDayAppointmentSuccess(rankAppointment:RankingAppointment[]){
	return{
		type:"@@agentSalePerformance/Get_day_appointment" as const,
		rankAppointment,
	}
}

export function getMonthAppointmentSuccess(rankAppointment:RankingAppointment[]){
	return{
		type:"@@agentSalePerformance/Get_month_appointment" as const,
		rankAppointment,
	}
}

export function getYearAppointmentSuccess(rankAppointment:RankingAppointment[]){
	return{
		type:"@@agentSalePerformance/Get_year_appointment" as const,
		rankAppointment,
	}
}

type Failed = "Get_my_performance_failed"|"Get_day_referral_failed"|"Get_month_referral_failed"|"Get_year_referral_failed"
|"Get_day_appointment_failed"|"Get_month_appointment_failed"|"Get_year_appointment_failed"
export function failed(type:Failed,message:string){
	return{
		type,
		message
	}
}

export type IAgentSalePerformanceActions = ReturnType<typeof getMyPerformanceSuccess|typeof getDayReferralSuccess|
typeof getMonthReferralSuccess|typeof getYearReferralSuccess|typeof getDayAppointmentSuccess|
typeof getMonthAppointmentSuccess|typeof getYearAppointmentSuccess|typeof failed>