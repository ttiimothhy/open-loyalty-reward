import AsyncStorage from "@react-native-community/async-storage";
import {Dispatch} from "redux";
import envfile from "../../../envfile";
import {getMyPerformanceSuccess,failed,IAgentSalePerformanceActions, getMonthReferralSuccess, getDayReferralSuccess, getYearReferralSuccess, getDayAppointmentSuccess, getMonthAppointmentSuccess, getYearAppointmentSuccess} from "./actions";

export function getMyPerformance(){
	return async(dispatch:Dispatch<IAgentSalePerformanceActions>) => {
		const token = await AsyncStorage.getItem("token")
		const res = await fetch(`${envfile.REACT_APP_BACKEND_PORT}/agents/index/performance`,{
			headers:{
				Authorization:`Bearer ${token}`
			}
		})
		const result = await res.json()
		if(result.success){
			dispatch(getMyPerformanceSuccess(result.referral_count,result.appointment_count))
		}else{
			dispatch(failed("Get_my_performance_failed",result.message))
		}
	}
}

export function getDayReferral(){
	return async(dispatch:Dispatch<IAgentSalePerformanceActions>) => {
		const token = await AsyncStorage.getItem("token")
		const res = await fetch(`${envfile.REACT_APP_BACKEND_PORT}/agents/performance/referral/day`,{
			headers:{
				Authorization:`Bearer ${token}`
			}
		})
		const result = await res.json()
		console.log(result)
		if(!result.message){
			dispatch(getDayReferralSuccess(result))
		}else{
			dispatch(failed("Get_day_referral_failed",result.message))
		}
	}
}

export function getMonthReferral(){
	return async(dispatch:Dispatch<IAgentSalePerformanceActions>) => {
		const token = await AsyncStorage.getItem("token")
		const res = await fetch(`${envfile.REACT_APP_BACKEND_PORT}/agents/performance/referral/month`,{
			headers:{
				Authorization:`Bearer ${token}`
			}
		})
		const result = await res.json()
		if(!result.message){
			dispatch(getMonthReferralSuccess(result))
		}else{
			dispatch(failed("Get_month_referral_failed",result.message))
		}
	}
}

export function getYearReferral(){
	return async(dispatch:Dispatch<IAgentSalePerformanceActions>) => {
		const token = await AsyncStorage.getItem("token")
		const res = await fetch(`${envfile.REACT_APP_BACKEND_PORT}/agents/performance/referral/year`,{
			headers:{
				Authorization:`Bearer ${token}`
			}
		})
		const result = await res.json()
		if(!result.message){
			dispatch(getYearReferralSuccess(result))
		}else{
			dispatch(failed("Get_year_referral_failed",result.message))
		}
	}
}

export function getDayAppointment(){
	return async(dispatch:Dispatch<IAgentSalePerformanceActions>) => {
		const token = await AsyncStorage.getItem("token")
		const res = await fetch(`${envfile.REACT_APP_BACKEND_PORT}/agents/performance/appointment/day`,{
			headers:{
				Authorization:`Bearer ${token}`
			}
		})
		const result = await res.json()
		if(!result.message){
			dispatch(getDayAppointmentSuccess(result))
		}else{
			dispatch(failed("Get_day_appointment_failed",result.message))
		}
	}
}

export function getMonthAppointment(){
	return async(dispatch:Dispatch<IAgentSalePerformanceActions>) => {
		const token = await AsyncStorage.getItem("token")
		const res = await fetch(`${envfile.REACT_APP_BACKEND_PORT}/agents/performance/appointment/month`,{
			headers:{
				Authorization:`Bearer ${token}`
			}
		})
		const result = await res.json()
		if(!result.message){
			dispatch(getMonthAppointmentSuccess(result))
		}else{
			dispatch(failed("Get_month_appointment_failed",result.message))
		}
	}
}

export function getYearAppointment(){
	return async(dispatch:Dispatch<IAgentSalePerformanceActions>) => {
		const token = await AsyncStorage.getItem("token")
		const res = await fetch(`${envfile.REACT_APP_BACKEND_PORT}/agents/performance/appointment/year`,{
			headers:{
				Authorization:`Bearer ${token}`
			}
		})
		const result = await res.json()
		if(!result.message){
			dispatch(getYearAppointmentSuccess(result))
		}else{
			dispatch(failed("Get_year_appointment_failed",result.message))
		}
	}
}