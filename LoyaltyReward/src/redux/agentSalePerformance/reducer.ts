import {RankingAppointment, RankingReferral} from "../../../models";
import {IAgentSalePerformanceActions} from "./actions";

export interface IAgentSalePerformanceState{
	referral:string;
	appointment:string;
	dayRankReferral:RankingReferral[];
	dayRankAppointment:RankingAppointment[];
	monthRankReferral:RankingReferral[];
	monthRankAppointment:RankingAppointment[];
	yearRankReferral:RankingReferral[];
	yearRankAppointment:RankingAppointment[];
}

const initialState = {
	referral:"",
	appointment:"",
	dayRankReferral:[],
	dayRankAppointment:[],
	monthRankReferral:[],
	monthRankAppointment:[],
	yearRankReferral:[],
	yearRankAppointment:[],
}

const agentSalePerformanceReducer = (state:IAgentSalePerformanceState = initialState,action:IAgentSalePerformanceActions):IAgentSalePerformanceState => {
	switch(action.type){
		case "@@agentSalePerformance/Get_my_performance":
			return{
				...state,
				referral:action.referral,
				appointment:action.appointment
			}
		case "@@agentSalePerformance/Get_day_referral":
			return{
				...state,
				dayRankReferral:action.rankReferral
			}
		case "@@agentSalePerformance/Get_month_referral":
			return{
				...state,
				monthRankReferral:action.rankReferral
			}
		case "@@agentSalePerformance/Get_year_referral":
			return{
				...state,
				yearRankReferral:action.rankReferral
			}
		case "@@agentSalePerformance/Get_day_appointment":
			return{
				...state,
				dayRankAppointment:action.rankAppointment
			}
		case "@@agentSalePerformance/Get_month_appointment":
			return{
				...state,
				monthRankAppointment:action.rankAppointment
			}
		case "@@agentSalePerformance/Get_year_appointment":
			return{
				...state,
				yearRankAppointment:action.rankAppointment
			}
		default:
			return state
	}
}

export default agentSalePerformanceReducer;