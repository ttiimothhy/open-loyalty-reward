import {PurchasedSchedule,CompletedScoredSchedule,Schedule, Treatment} from "../../../models";
import {IMyAppointmentsActions} from "./actions";
export interface IMyAppointmentsState{
	treatments:Treatment[];
	pendingSchedule:Schedule[];
	completedScoredSchedule:CompletedScoredSchedule[];
	completedUnScoredSchedule:Schedule[];
	cancelledSchedule:Schedule[];
	purchasedSchedule:PurchasedSchedule[];
}

const initialState = {
	treatments:[],
	pendingSchedule:[],
	completedScoredSchedule:[],
	completedUnScoredSchedule:[],
	cancelledSchedule:[],
	purchasedSchedule:[]
}

const myAppointmentsReducer = (state:IMyAppointmentsState = initialState,action:IMyAppointmentsActions):IMyAppointmentsState => {
	switch(action.type){
		case "@@myAppointments/Get_all_treatments":
			return{
				...state,
				treatments:action.treatment
			}
		case "@@myAppointments/Get_pending_schedule":
			return{
				...state,
				pendingSchedule:action.schedule
			}
		case "@@myAppointments/Get_completed_scored_schedule":
			return{
				...state,
				completedScoredSchedule:action.schedule
			}
		case "@@myAppointments/Get_completed_unScored_schedule":
			return{
				...state,
				completedUnScoredSchedule:action.schedule
			}
		case "@@myAppointments/Get_cancelled_schedule":
			return{
				...state,
				cancelledSchedule:action.schedule
			}
		case "@@myAppointments/Cancel_appointment":
			let cancel = state.pendingSchedule.filter(schedule => schedule.id === action.cancelId);
			let newCancelledSchedule = state.cancelledSchedule.slice();
			if(cancel.length > 0){
				newCancelledSchedule.push(cancel[0])
			}
			return{
				...state,
				pendingSchedule:state.pendingSchedule.filter(schedule => schedule.id !== action.cancelId),
				cancelledSchedule:newCancelledSchedule
			}
		case "@@myAppointments/Mark_score":
			let scoredAppointment = state.completedUnScoredSchedule.filter(schedule => schedule.id === action.appointmentId);
			let newCompletedScoredSchedule = state.completedScoredSchedule.slice();
			if(scoredAppointment.length > 0){
				scoredAppointment[0]
				newCompletedScoredSchedule.push({...scoredAppointment[0],score:action.score})
			}
			return{
				...state,
				completedScoredSchedule:newCompletedScoredSchedule,
				completedUnScoredSchedule:state.completedUnScoredSchedule.filter(schedule => schedule.id !== action.appointmentId)
			}
		case "@@myAppointments/Get_purchased_schedule":
			return{
				...state,
				purchasedSchedule:action.schedule
			}
		case "@@myAppointments/Add_pending_schedule":
			let newPendingSchedule = state.pendingSchedule.slice();
			// console.log(action.treatmentId)
			// console.log(state.treatments)
			let pending = (state.treatments.filter(treatment=>treatment.id === action.treatmentId).map(treatment=>treatment))[0]
			// console.log(pending)
			let myPending = {
				id:0,
				image:"",
				title:""
			}
			if(pending){
				myPending = {
					id:pending.id,
					image:pending.image,
					title:pending.title
				}
			}

			newPendingSchedule.push({...myPending,timeslot:action.time,treatment_id:myPending.id,name:action.place})
			return{
				...state,
				pendingSchedule:newPendingSchedule
			}
		case "@@myAppointments/Add_purchased_schedule":
			let newPurchasedSchedule = state.purchasedSchedule.slice();
			// console.log(newPurchasedSchedule)
			let alreadyPurchaseTreatment = newPurchasedSchedule.filter(schedule=>schedule.id === action.id)
			// console.log(alreadyPurchaseTreatment.length)
			// console.log(action.id)
			if(alreadyPurchaseTreatment.length > 0){
				// console.log(alreadyPurchaseTreatment[0]);
				// console.log(alreadyPurchaseTreatment[0].id);
				// console.log(action.purchase);
				// console.log((newPurchasedSchedule.filter(schedule=>schedule.id === action.id).map(schedule=>schedule.remaining_amount))[0]);
				let amount = parseInt((newPurchasedSchedule.filter(schedule=>schedule.id === action.id))[0].remaining_amount)
				amount += action.purchase;
				(newPurchasedSchedule.filter(schedule=>schedule.id === action.id))[0].remaining_amount = amount + ""
				// console.log((newPurchasedSchedule.filter(schedule=>schedule.id === action.id).map(schedule=>schedule.remaining_amount))[0]);
			}else{
				let purchase = {
					id:action.id,
					remaining_amount:action.purchase + "",
					detail:action.treatment.detail,
					image:action.treatment.image,
					title:action.treatment.title
				}
				// console.log(purchase)
				newPurchasedSchedule.push(purchase)
			}
			// console.log(newPurchasedSchedule)
			return{
				...state,
				purchasedSchedule:newPurchasedSchedule
			}
		default:
			return state
	}
}

export default myAppointmentsReducer;