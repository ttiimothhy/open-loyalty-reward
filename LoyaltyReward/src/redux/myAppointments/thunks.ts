import AsyncStorage from "@react-native-community/async-storage";
import {Dispatch} from "redux";
import envfile from "../../../envfile";
import {IMyAppointmentsActions,pendingScheduleSuccess,failed,cancelledScheduleSuccess,completedScoredScheduleSuccess,
completedUnScoredScheduleSuccess,cancelAppointmentSuccess,markScoreSuccess,purchasedScheduleSuccess,
getAllTreatmentsSuccess} from "./actions";

export function getAllTreatments(){
	return async(dispatch:Dispatch<IMyAppointmentsActions>) => {
		const token = await AsyncStorage.getItem("token");
		const res = await fetch(`${envfile.REACT_APP_BACKEND_PORT}/treatment/all`,{
			headers:{
				Authorization:`Bearer ${token}`
			}
		})
		const result = await res.json();
		// console.log(result)
		if(!result.message){
			dispatch(getAllTreatmentsSuccess(result))
		}else{
			dispatch(failed("Get_all_treatments_failed",result.message))
		}
	}
}

export function pendingSchedule(){
	return async(dispatch:Dispatch<IMyAppointmentsActions>) => {
		const token = await AsyncStorage.getItem("token");
		// console.log(token)
		const res = await fetch(`${envfile.REACT_APP_BACKEND_PORT}/schedule?status=pending`,{
			headers:{
				Authorization:`Bearer ${token}`
			}
		})
		const result = await res.json();
		// console.log(result)
		if(!result.message){
			dispatch(pendingScheduleSuccess(result));
		}else{
			dispatch(failed("Get_pending_schedule_failed",result.message));
		}
	}
}

export function completedScoredSchedule(){
	return async(dispatch:Dispatch<IMyAppointmentsActions>) => {
		const token = await AsyncStorage.getItem("token");
		// console.log(token)
		const res = await fetch(`${envfile.REACT_APP_BACKEND_PORT}/schedule?status=completedScored`,{
			headers:{
				Authorization:`Bearer ${token}`
			}
		})
		const result = await res.json();
		// console.log(result)
		if(!result.message){
			dispatch(completedScoredScheduleSuccess(result));
		}else{
			dispatch(failed("Get_completed_scored_schedule_failed",result.message));
		}
	}
}

export function completedUnScoredSchedule(){
	return async(dispatch:Dispatch<IMyAppointmentsActions>) => {
		const token = await AsyncStorage.getItem("token");
		// console.log(token)
		const res = await fetch(`${envfile.REACT_APP_BACKEND_PORT}/schedule?status=completedUnScored`,{
			headers:{
				Authorization:`Bearer ${token}`
			}
		})
		const result = await res.json();
		// console.log(result)
		if(!result.message){
			dispatch(completedUnScoredScheduleSuccess(result));
		}else{
			dispatch(failed("Get_completed_unScored_schedule_failed",result.message));
		}
	}
}

export function cancelledSchedule(){
	return async(dispatch:Dispatch<IMyAppointmentsActions>) => {
		const token = await AsyncStorage.getItem("token");
		// console.log(token)
		const res = await fetch(`${envfile.REACT_APP_BACKEND_PORT}/schedule?status=cancelled`,{
			headers:{
				Authorization:`Bearer ${token}`
			}
		})
		const result = await res.json();
		// console.log(result)
		if(!result.message){
			dispatch(cancelledScheduleSuccess(result));
		}else{
			dispatch(failed("Get_cancelled_schedule_failed",result.message));
		}
	}
}

export function purchasedSchedule(){
	return async(dispatch:Dispatch<IMyAppointmentsActions>) => {
		const token = await AsyncStorage.getItem("token");
		// console.log(token)
		const res = await fetch(`${envfile.REACT_APP_BACKEND_PORT}/purchase/item`,{
			headers:{
				Authorization:`Bearer ${token}`
			}
		})
		const result = await res.json();
		// console.log(result)
		if(!result.message){
			dispatch(purchasedScheduleSuccess(result));
		}else{
			dispatch(failed("Get_purchased_schedule_failed",result.message));
		}
	}
}

export function cancelAppointment(cancelId:number){
	return async(dispatch:Dispatch<IMyAppointmentsActions>) => {
		const token = await AsyncStorage.getItem("token");
		// console.log(token)
		const res = await fetch(`${envfile.REACT_APP_BACKEND_PORT}/schedule/cancel?appointment_id=${cancelId}`,{
			headers:{
				Authorization:`Bearer ${token}`,
			},
		})
		const result = await res.json();
		// console.log(result)
		if(!result.message){
			dispatch(cancelAppointmentSuccess(cancelId));
		}else{
			dispatch(failed("Cancel_appointment_failed",result.message));
		}
	}
}

export function markScore(appointmentId:number|undefined,score:number){
	return async(dispatch:Dispatch<IMyAppointmentsActions>) => {
		const token = await AsyncStorage.getItem("token");
		if(appointmentId){
			const res = await fetch(`${envfile.REACT_APP_BACKEND_PORT}/schedule/score?appointment_id=${appointmentId}&score=${score}`,{
				headers:{
					Authorization:`Bearer ${token}`
				}
			})
			const result = await res.json();
			// console.log(result)
			if(!result.message){
				dispatch(markScoreSuccess(appointmentId,score))
			}else{
				dispatch(failed("Mark_score_failed",result.message))
			}
		}
	}
}