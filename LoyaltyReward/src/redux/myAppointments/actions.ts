import {Schedule,PurchasedSchedule,CompletedScoredSchedule,Treatment, TreatmentWithId} from "../../../models";

export function getAllTreatmentsSuccess(treatment:Treatment[]){
	return{
		type:"@@myAppointments/Get_all_treatments" as const,
		treatment
	}
}

export function pendingScheduleSuccess(schedule:Schedule[]){
	return{
		type:"@@myAppointments/Get_pending_schedule" as const,
		schedule
	}
}

export function completedScoredScheduleSuccess(schedule:CompletedScoredSchedule[]){
	return{
		type:"@@myAppointments/Get_completed_scored_schedule" as const,
		schedule
	}
}

export function completedUnScoredScheduleSuccess(schedule:Schedule[]){
	return{
		type:"@@myAppointments/Get_completed_unScored_schedule" as const,
		schedule
	}
}

export function cancelledScheduleSuccess(schedule:Schedule[]){
	return{
		type:"@@myAppointments/Get_cancelled_schedule" as const,
		schedule
	}
}

export function purchasedScheduleSuccess(schedule:PurchasedSchedule[]){
	return{
		type:"@@myAppointments/Get_purchased_schedule" as const,
		schedule
	}
}

export function cancelAppointmentSuccess(cancelId:number){
	return{
		type:"@@myAppointments/Cancel_appointment" as const,
		cancelId
	}
}

export function markScoreSuccess(appointmentId:number,score:number){
	return{
		type:"@@myAppointments/Mark_score" as const,
		appointmentId,
		score
	}
}

export function addPendingSchedule(treatmentId:number,time:string,place:string){
	return{
		type:"@@myAppointments/Add_pending_schedule" as const,
		treatmentId,
		time,
		place
	}
}

export function addPurchasedScheduleSuccess(treatment:TreatmentWithId,id:number,purchase:number){
	return{
		type:"@@myAppointments/Add_purchased_schedule" as const,
		treatment,
		id,
		purchase
	}
}

type Failed = "Get_all_treatments_failed"|"Get_pending_schedule_failed"|"Get_completed_scored_schedule_failed"|"Get_completed_unScored_schedule_failed"|
"Get_cancelled_schedule_failed"|"Cancel_appointment_failed"|"Mark_score_failed"|"Get_purchased_schedule_failed";
export function failed(type:Failed,message:string){
	return{
		type,
		message
	}
}

export type IMyAppointmentsActions = ReturnType<typeof getAllTreatmentsSuccess|typeof pendingScheduleSuccess|typeof completedScoredScheduleSuccess|
typeof completedUnScoredScheduleSuccess|typeof cancelAppointmentSuccess|typeof cancelledScheduleSuccess|
typeof markScoreSuccess|typeof purchasedScheduleSuccess|typeof addPendingSchedule|
typeof addPurchasedScheduleSuccess|typeof failed>