export function resetPasswordSuccess(){
	return{
		type:"@@user/Reset_password_success" as const,
	}
}

export function resetPasswordFailure(message:string){
	return{
		type:"@@user/Reset_password_failure" as const,
		message
	}
}

export function resetNullSuccess(){
	return{
		type:"@@user/Reset_null" as const,
	}
}

export type IAgentUserActions = ReturnType<typeof resetPasswordSuccess|typeof resetPasswordFailure|typeof resetNullSuccess>
