import AsyncStorage from "@react-native-community/async-storage";
import {Asset} from "react-native-image-picker";
import {Dispatch} from "redux";
import envfile from "../../../envfile";
import {uploadPhotoSuccess,failed,IAdminActions} from "./actions";

export function uploadPhoto(photo:Asset[]){
	return async(dispatch:Dispatch<IAdminActions>) => {
		const token = await AsyncStorage.getItem("token");
		if(photo[0].uri){
			const resFile = await fetch(photo[0].uri);
			const blob = await resFile.blob();
			const file = new File([blob],"filename.png",{
				type:"image/png",
			})
			// console.log(file)
			const photoData = new FormData();
			photoData.append("photo",file)
			const res = await fetch(`${envfile.REACT_APP_BACKEND_PORT}/admin/demo`,{
				method:"post",
				headers:{
					Authorization:`Bearer ${token}`
				},
				body:photoData
			})
			const result = await res.json();
			console.log(result)
			if(result.success){
				dispatch(uploadPhotoSuccess())
			}else{
				dispatch(failed("Upload_photo_failed",result.message))
			}
		}
	}
}