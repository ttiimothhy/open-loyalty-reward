import {IAdminActions} from "./actions";

export interface IAdminState{
	upload:boolean|null
}

const initialState = {
	upload:null
}

// Can use immer
const adminReducer = (state:IAdminState = initialState,action:IAdminActions):IAdminState => {
	switch(action.type){
		case "@@admin/Upload_photo":
			return{
				...state,
				upload:true
			}
		default:
			return state
	}
}

export default adminReducer;