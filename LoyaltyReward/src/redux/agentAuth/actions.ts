export function loginSuccess(id:number,token:string){
	return{
		type:"@@agentAuth/Login_success" as const,
		id,
		token
	}
}

export function loginFailure(message:string){
	return{
		type:"@@agentAuth/Login_failure" as const,
		message
	}
}

export function loginNullSuccess(){
	return{
		type:"@@agentAuth/Login_null" as const,
	}
}

export function logoutSuccess(){
	return{
		type:"@@agentAuth/Logout_success" as const
	}
}

export type IAuthActions = ReturnType<typeof loginSuccess|typeof loginFailure|typeof loginNullSuccess|typeof logoutSuccess>