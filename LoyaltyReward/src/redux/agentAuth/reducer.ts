import {IAuthActions} from "./actions";

export interface IAgentAuthState{
	id:number;
	isAuthenticated:boolean|null;
	isLoggedIn:boolean|null;
	token:string;
	message:string;
}

export const initialState = {
	id:0,
	isAuthenticated:null,
	isLoggedIn:null,
	token:"",
	message:""
}

const authAgentReducer = (state:IAgentAuthState = initialState,action:IAuthActions):IAgentAuthState => {
	switch(action.type){
		case "@@agentAuth/Login_success":
			return{
				...state,
				isAuthenticated:true,
				id:action.id,
				token:action.token
			}
		case "@@agentAuth/Login_failure":
			return{
				...state,
				isAuthenticated:false,
				isLoggedIn:false,
				message:action.message
			}
		case "@@agentAuth/Login_null":
			return{
				...state,
				isLoggedIn:null
			}
		case "@@agentAuth/Logout_success":
			return{
				...state,
				isAuthenticated:false,
				isLoggedIn:null,
				token:"",
				message:""
			}
		default:
			return state
	}
}

export default authAgentReducer;