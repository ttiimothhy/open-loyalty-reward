import asyncStorage from "@react-native-community/async-storage";
import {Dispatch} from "redux";
import {changeRoleSuccess,IRoleActions} from "./actions";

export function changeRole(role:string){
	return async(dispatch:Dispatch<IRoleActions>) => {
		await asyncStorage.setItem("role",role);
		dispatch(changeRoleSuccess(role));
	}
}