export function changeRoleSuccess(role:string){
	return{
		type:"@@role/Change_role" as const,
		role
	}
}

export type IRoleActions = ReturnType<typeof changeRoleSuccess>