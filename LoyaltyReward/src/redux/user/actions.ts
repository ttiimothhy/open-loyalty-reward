import { ResendSecurityCodeResult, VerifySecurityCodeResult } from "../../../models"

export function registerSuccess(id:string){
	return{
		type:"@@user/Register_success" as const,
		id
	}
}

export function registerFailure(message:string){
	return{
		type:"@@user/Register_failure" as const,
		message
	}
}

export function registerNullSuccess(){
	return{
		type:"@@user/Register_null" as const,
	}
}

export function resetPasswordSuccess(){
	return{
		type:"@@user/Reset_password_success" as const
	}
}

export function resetPasswordFailure(message:string){
	return{
		type:"@@user/Reset_password_failure" as const,
		message
	}
}

export function resetNullSuccess(){
	return{
		type:"@@user/Reset_null" as const,
	}
}

export function verifySecurityCodeSuccess(verifySecurityCodeResult:VerifySecurityCodeResult){
	return{
		type:"@@user/Verify_security_code_success" as const,
		verifySecurityCodeResult
	}
}

export function verifySecurityCodeFailure(message:string){
	return{
		type:"@@user/Verify_security_code_failure" as const,
		message
	}
}

export function resendSecurityCodeSuccess(resendSecurityCodeResult:ResendSecurityCodeResult){
	return{
		type:"@@user/Resend_security_code_success" as const,
		resendSecurityCodeResult
	}
}

export function resendSecurityCodeFailure(message:string){
	return{
		type:"@@user/Resend_security_code_failure" as const,
		message
	}
}

export type IUserActions = ReturnType<typeof registerSuccess|typeof registerFailure|typeof registerNullSuccess|typeof resetPasswordSuccess|
typeof resetPasswordFailure|typeof verifySecurityCodeSuccess|typeof verifySecurityCodeFailure|typeof resendSecurityCodeSuccess|
typeof resendSecurityCodeFailure|typeof resetNullSuccess>