import {IUserActions} from "./actions";

export interface IUserState{
	isRegistered:boolean|null;
	isReset:boolean|null;
	message:string;
	verifySecurityCodeResult:{
		success:boolean|null,
		message:string
	};
	resendSecurityCodeResult:{
		success:boolean|null,
		message:string
	};
	userId:string
}

export const initialState = {
	isRegistered:null,
	isReset:null,
	message:"",
	verifySecurityCodeResult:{
		success:null,
		message:""
	},
	resendSecurityCodeResult:{
		success:null,
		message:""
	},
	userId:""
}

const userReducer = (state:IUserState = initialState,action:IUserActions):IUserState => {
	switch(action.type){
		case "@@user/Register_success":
			return{
				...state,
				isRegistered:true,
				userId:action.id
			}
		case "@@user/Register_failure":
			return{
				...state,
				isRegistered:false,
				message:action.message
			}
		case "@@user/Register_null":
			return{
				...state,
				isRegistered:null
			}
		case "@@user/Reset_password_success":
			return{
				...state,
				isReset:true
			}
		case "@@user/Reset_password_failure":
			return{
				...state,
				isReset:false,
				message:action.message
			}
		case "@@user/Reset_null":
			return{
				...state,
				isReset:null
			}
		case "@@user/Verify_security_code_success":
			return{
				...state,
				verifySecurityCodeResult:{
					success:true,
					message:""
				}
			}
		case "@@user/Verify_security_code_failure":
			return{
				...state,
				verifySecurityCodeResult:{
					success:false,
					message:action.message
				}
			}
		case "@@user/Resend_security_code_success":
			return{
				...state,
				resendSecurityCodeResult:{
					success:true,
					message:""
				}
			}
		case "@@user/Resend_security_code_failure":
			return{
				...state,
				resendSecurityCodeResult:{
					success:false,
					message:action.message
				}
			}
		default:
			return state
	}
}

export default userReducer;