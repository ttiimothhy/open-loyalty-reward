import AsyncStorage from "@react-native-community/async-storage";
import {Dispatch} from "redux";
import envfile from "../../../envfile";
import {failed,getAdvertisementSuccess,getContactCardSuccess,IAgentHomeActions,myPerformanceSuccess} from "./actions";

export function getAdvertisement(){
	return async(dispatch:Dispatch<IAgentHomeActions>) => {
		const token = await AsyncStorage.getItem("token");
		// console.log(token)
		const res = await fetch(`${envfile.REACT_APP_BACKEND_PORT}/agents/index/advertisements`,{
			headers:{
				Authorization:`Bearer ${token}`
			}
		})
		const result = await res.json();
		// console.log(result);
		if(!result.message){
			dispatch(getAdvertisementSuccess(result))
		}else{
			dispatch(failed("Get_advertisement_failed",result.message))
		}
	}
}

export function getContactCard(){
	return async(dispatch:Dispatch<IAgentHomeActions>) => {
		const token = await AsyncStorage.getItem("token")
		const res = await fetch(`${envfile.REACT_APP_BACKEND_PORT}/agents/index/contact`,{
			headers:{
				Authorization:`Bearer ${token}`
			}
		})
		const result = await res.json();
		// console.log(result);
		if(!result.message){
			dispatch(getContactCardSuccess(result))
		}else{
			dispatch(failed("Get_contact_card_failed",result.message))
		}
	}
}
// Get my performance
export function myPerformance(){
	return async(dispatch:Dispatch<IAgentHomeActions>) => {
		const token = await AsyncStorage.getItem("token")
		const res = await fetch(`${envfile.REACT_APP_BACKEND_PORT}/agents/index/performance`,{
			headers:{
				Authorization:`Bearer ${token}`
			}
		})
		const result = await res.json();
		// console.log(result);
		if(!result.message){
			dispatch(myPerformanceSuccess(result))
		}else{
			dispatch(failed("Get_my_performance_failed",result.message))
		}
	}
}
