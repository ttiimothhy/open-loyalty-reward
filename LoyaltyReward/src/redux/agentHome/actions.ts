import {Advertisement,ContactCard,PerformanceCount} from "../../../models";

export function getAdvertisementSuccess(advertisement:Advertisement[]){
	return{
		type:"@@agentHome/Get_advertisement" as const,
		advertisement
	}
}

export function getContactCardSuccess(contactCard:ContactCard[]){
	return{
		type:"@@agentHome/Get_contact_card" as const,
		contactCard
	}
}

export function myPerformanceSuccess(count:PerformanceCount){
	return{
		type:"@@agentHome/Get_my_performance" as const,
		count
	}
}


type Failed = "Get_advertisement_failed"|"Get_contact_card_failed"|"Get_my_performance_failed"|"Get_point_failed";
export function failed(type:Failed,message:string){
	return {
		type,
		message
	}
}

export type IAgentHomeActions = ReturnType<typeof getAdvertisementSuccess|typeof getContactCardSuccess|typeof myPerformanceSuccess
|typeof failed>