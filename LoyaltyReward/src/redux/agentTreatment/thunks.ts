import AsyncStorage from "@react-native-community/async-storage";
import {Dispatch} from "redux";
import envfile from "../../../envfile";
import {failed, getAllTreatmentsSuccess,IAgentTreatmentActions,placeCriteriaSuccess,typeCriteriaSuccess,getTreatmentsSuccess,
sendTimeslotSuccess,sendTimeslotFail}from "./actions";

export function getAllTreatments(){
	return async(dispatch:Dispatch<IAgentTreatmentActions>) => {
		const token = await AsyncStorage.getItem("token")
		const res = await fetch(`${envfile.REACT_APP_BACKEND_PORT}/agents/treatment/all`,{
			headers:{
				Authorization:`Bearer ${token}`
			}
		})
		const result = await res.json();
		if(!result.message){
			dispatch(getAllTreatmentsSuccess(result));
		}else{
			dispatch(failed("Get_all_treatments_failed",result.message))
		}
	}
}

export function typeCriteria(){
	return async(dispatch:Dispatch<IAgentTreatmentActions>) => {
		const token = await AsyncStorage.getItem("token");
		const res = await fetch(`${envfile.REACT_APP_BACKEND_PORT}/agents/treatment/service`,{
			headers:{
				Authorization:`Bearer ${token}`
			}
		})
		const result = await res.json();
		// console.log(result)
		if(!result.message){
			dispatch(typeCriteriaSuccess(result))
		}else{
			dispatch(failed("Get_type_criteria_failed",result.message))
		}
	}
}

export function placeCriteria(){
	return async(dispatch:Dispatch<IAgentTreatmentActions>) => {
		const token = await AsyncStorage.getItem("token");
		const res = await fetch(`${envfile.REACT_APP_BACKEND_PORT}/agents/treatment/branch`,{
			headers:{
				Authorization:`Bearer ${token}`
			}
		})
		const result = await res.json();
		// console.log(result)
		if(!result.message){
			dispatch(placeCriteriaSuccess(result))
		}else{
			dispatch(failed("Get_place_criteria_failed",result.message))
		}
	}
}

// optional parameters -> undefined
// need the params, but nothing to give you -> null
export function getTreatments(typeId:number|null,placeId:number|null){
	return async(dispatch:Dispatch<IAgentTreatmentActions>) => {
		const token = await AsyncStorage.getItem("token");
		if(typeId || placeId){
			if(typeId){
				const res = await fetch(`${envfile.REACT_APP_BACKEND_PORT}/agents/treatment?service=${typeId}&branch=0`,{
					headers:{
						Authorization:`Bearer ${token}`
					}
				})
				const result = await res.json();
				// console.log(result)
				if(!result.message){
					dispatch(getTreatmentsSuccess(result))
				}else{
					dispatch(failed("Get_treatments_failed",result.message))
				}
			}else if(placeId){
				const res = await fetch(`${envfile.REACT_APP_BACKEND_PORT}/agents/treatment?service=${typeId}&branch=0`,{
					headers:{
						Authorization:`Bearer ${token}`
					}
				})
				const result = await res.json();
				// console.log(result)
				if(!result.message){
					dispatch(getTreatmentsSuccess(result))
				}else{
					dispatch(failed("Get_treatments_failed",result.message))
				}
			}
		}
	}
}

export function sendTimeslot(timeId:string,placeId:string,customerId:number,treatmentId:number|undefined){
	return async(dispatch:Dispatch<IAgentTreatmentActions>) => {
		const token = await AsyncStorage.getItem("token");
		if(treatmentId){
			const res = await fetch(`${envfile.REACT_APP_BACKEND_PORT}/agents/appointment/${treatmentId}`,{
				method:"post",
				headers:{
					Authorization:`Bearer ${token}`,
					"Content-Type":"application/json"
				},
				body:JSON.stringify({branch_id:parseInt(placeId),timeslot:timeId,customer_id:customerId})
			})
			const result = await res.json();
			// console.log(result)
			if(result.success){
				dispatch(sendTimeslotSuccess())
			}else{
				dispatch(sendTimeslotFail())
				dispatch(failed("Send_timeslot_failed",result.message))
			}
		}
	}
}