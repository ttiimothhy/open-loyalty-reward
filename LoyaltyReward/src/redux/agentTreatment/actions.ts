import {AgentAvailableTreatment, PlaceCriteria, TypeCriteria} from "../../../models";

export function getAllTreatmentsSuccess(treatment:AgentAvailableTreatment[]){
	return{
		type:"@@agentTreatment/Get_all_treatments" as const,
		treatment
	}
}

export function typeCriteriaSuccess(criteria:TypeCriteria[]){
	return{
		type:"@@agentTreatment/Get_type_criteria" as const,
		criteria
	}
}

export function placeCriteriaSuccess(criteria:PlaceCriteria[]){
	return{
		type:"@@agentTreatment/Get_place_criteria" as const,
		criteria
	}
}

export function getTreatmentsSuccess(treatment:AgentAvailableTreatment[]){
	return{
		type:"@@agentTreatment/Get_treatments" as const,
		treatment
	}
}

export function sendTimeslotSuccess(){
	return{
		type:"@@agentTreatment/Send_timeslot" as const,
	}
}

export function sendTimeslotFail(){
	return{
		type:"@@agentTreatment/Send_timeslot_fail" as const,
	}
}

export function setTimeslotNull(){
	return{
		type:"@@agentTreatment/Set_timeslot_null" as const,
	}
}

export function updateCustomerId(customerId:number|null){
	return{
		type:"@@agentTreatment/Update_customer_id" as const,
		customerId
	}
}

type Failed = "Get_all_treatments_failed"|"Get_type_criteria_failed"|"Get_place_criteria_failed"|"Get_treatments_failed"|
"Send_timeslot_failed"
export function failed(type:Failed,message:string){
	return{
		type,
		message
	}
}

export type IAgentTreatmentActions = ReturnType<typeof getAllTreatmentsSuccess|typeof typeCriteriaSuccess|
typeof placeCriteriaSuccess|typeof getTreatmentsSuccess|typeof sendTimeslotSuccess|typeof sendTimeslotFail|
typeof setTimeslotNull|typeof updateCustomerId|typeof failed>