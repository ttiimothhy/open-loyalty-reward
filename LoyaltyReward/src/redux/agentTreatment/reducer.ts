import {AgentAvailableTreatment, PlaceCriteria, TypeCriteria} from "../../../models";
import {IAgentTreatmentActions} from "./actions";

export interface IAgentTreatmentState{
	treatment:AgentAvailableTreatment[];
	type:TypeCriteria[];
	place:PlaceCriteria[];
	timeslotSuccess:boolean|null;
	customerId:number|null
}

const initialState = {
	treatment:[],
	type:[],
	place:[],
	timeslotSuccess:null,
	customerId:null
}

const agentAvailableTreatmentReducer = (state:IAgentTreatmentState = initialState,action:IAgentTreatmentActions)
:IAgentTreatmentState => {
	switch(action.type){
		case "@@agentTreatment/Get_all_treatments":
			return{
				...state,
				treatment:action.treatment
			}
		case "@@agentTreatment/Get_type_criteria":
			return{
				...state,
				type:action.criteria
			}
		case "@@agentTreatment/Get_place_criteria":
			return{
				...state,
				place:action.criteria
			}
		case "@@agentTreatment/Get_treatments":
			return{
				...state,
				treatment:action.treatment,
			}
		case "@@agentTreatment/Send_timeslot":
			return{
				...state,
				timeslotSuccess:true
			}
		case "@@agentTreatment/Send_timeslot_fail":
			return{
				...state,
				timeslotSuccess:false
			}
		case "@@agentTreatment/Set_timeslot_null":
			return{
				...state,
				timeslotSuccess:null
			}
		case "@@agentTreatment/Update_customer_id":
			return{
				...state,
				customerId:action.customerId
			}
		default:
			return state
	}
}

export default agentAvailableTreatmentReducer;