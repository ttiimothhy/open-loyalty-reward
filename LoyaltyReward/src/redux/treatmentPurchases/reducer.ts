import {PlaceCriteria,Treatment,TypeCriteria} from "../../../models";
import {ITreatmentPurchasesActions} from "./actions";

export interface ITreatmentPurchasesState{
	purchaseSuccess:boolean|null;
	treatment:Treatment[];
	type:TypeCriteria[];
	place:PlaceCriteria[];
}

const initialState = {
	purchaseSuccess:null,
	treatment:[],
	type:[],
	place:[]
}

const treatmentPurchasesReducer = (state:ITreatmentPurchasesState = initialState,action:ITreatmentPurchasesActions):ITreatmentPurchasesState => {
	switch(action.type){
		case "@@treatmentPurchases/Confirm_purchase":
			return{
				...state,
				purchaseSuccess:true
			}
		case "@@treatmentPurchases/Get_all_treatments":
			return{
				...state,
				treatment:action.treatment
		}
		case "@@treatmentPurchases/Get_type_criteria":
			return{
				...state,
				type:action.criteria
			}
		case "@@treatmentPurchases/Get_place_criteria":
			return{
				...state,
				place:action.criteria
			}
		case "@@treatmentPurchases/Get_treatments":
			return{
				...state,
				treatment:action.treatment
			}
		case "@@treatmentPurchases/Confirm_purchase":
			return{
				...state,
				purchaseSuccess:true
			}
		case "@@treatmentPurchases/Set_purchase_fail":
			return{
				...state,
				purchaseSuccess:false
			}
		case "@@treatmentPurchases/Set_purchase_null":
			return{
				...state,
				purchaseSuccess:null
			}
		default:
			return state
	}
}

export default treatmentPurchasesReducer;