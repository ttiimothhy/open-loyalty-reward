import AsyncStorage from "@react-native-community/async-storage";
import {Dispatch} from "redux";
import envfile from "../../../envfile";
import {addPurchasedScheduleSuccess,IMyAppointmentsActions} from "../myAppointments/actions";
import {confirmPurchasesSuccess,failed,ITreatmentPurchasesActions,getAllTreatmentsSuccess,typeCriteriaSuccess,
placeCriteriaSuccess,getTreatmentsSuccess} from "./actions";

export function getAllTreatments(){
	return async(dispatch:Dispatch<ITreatmentPurchasesActions>) => {
		const token = await AsyncStorage.getItem("token");
		const res = await fetch(`${envfile.REACT_APP_BACKEND_PORT}/treatment/all`,{
			headers:{
				Authorization:`Bearer ${token}`
			}
		})
		const result = await res.json();
		// console.log(result + " a")
		if(!result.message){
			dispatch(getAllTreatmentsSuccess(result))
		}else{
			dispatch(failed("Get_all_treatments_failed",result.message))
		}
	}
}

export function typeCriteria(){
	return async(dispatch:Dispatch<ITreatmentPurchasesActions>) => {
		const token = await AsyncStorage.getItem("token");
		const res = await fetch(`${envfile.REACT_APP_BACKEND_PORT}/treatment/service`,{
			headers:{
				Authorization:`Bearer ${token}`
			}
		})
		const result = await res.json();
		// console.log(result)
		if(!result.message){
			dispatch(typeCriteriaSuccess(result))
		}else{
			dispatch(failed("Get_type_criteria_failed",result.message))
		}
	}
}

export function placeCriteria(){
	return async(dispatch:Dispatch<ITreatmentPurchasesActions>) => {
		const token = await AsyncStorage.getItem("token");
		const res = await fetch(`${envfile.REACT_APP_BACKEND_PORT}/treatment/branch`,{
			headers:{
				Authorization:`Bearer ${token}`
			}
		})
		const result = await res.json();
		// console.log(result)
		if(!result.message){
			dispatch(placeCriteriaSuccess(result))
		}else{
			dispatch(failed("Get_place_criteria_failed",result.message))
		}
	}
}


export function getTreatments(typeId:number|null,placeId:number|null){
	return async(dispatch:Dispatch<ITreatmentPurchasesActions>) => {
		const token = await AsyncStorage.getItem("token");
		if(typeId || placeId){
			if(typeId){
				const res = await fetch(`${envfile.REACT_APP_BACKEND_PORT}/treatment?service=${typeId}&branch=0`,{
					headers:{
						Authorization:`Bearer ${token}`
					}
				})
				const result = await res.json();
				// console.log(result)
				if(!result.message){
					dispatch(getTreatmentsSuccess(result))
				}else{
					dispatch(failed("Get_treatments_failed",result.message))
				}
			}else if(placeId){
				const res = await fetch(`${envfile.REACT_APP_BACKEND_PORT}/treatment?service=0&branch=${placeId}`,{
					headers:{
						Authorization:`Bearer ${token}`
					}
				})
				const result = await res.json();
				// console.log(result)
				if(!result.message){
					dispatch(getTreatmentsSuccess(result))
				}else{
					dispatch(failed("Get_treatments_failed",result.message))
				}
			}
		}
	}
}

export function confirmPurchases(purchased_amount:number,treatment_id:number|undefined){
	return async(dispatch:Dispatch<any>) => {
		const token = await AsyncStorage.getItem("token");
		if(treatment_id){
			const res = await fetch(`${envfile.REACT_APP_BACKEND_PORT}/purchase/confirm`,{
				method:"post",
				headers:{
					Authorization:`Bearer ${token}`,
					"Content-type":"application/json"
				},
				body:JSON.stringify({purchased_amount,treatment_id})
			})
			const result = await res.json()
			// console.log(result)
			if(result.success){
				dispatch(confirmPurchasesSuccess())
				dispatch(addPurchasedSchedule(treatment_id,purchased_amount))
			}else{
				dispatch(failed("Confirm_purchase_failed",result.message))
			}
		}
	}
}

function addPurchasedSchedule(treatment_id:number,purchased_amount:number){
	return async(dispatch:Dispatch<ITreatmentPurchasesActions|IMyAppointmentsActions>) => {
		const token = await AsyncStorage.getItem("token");
		const res = await fetch(`${envfile.REACT_APP_BACKEND_PORT}/treatment/${treatment_id}`,{
			headers:{
				Authorization:`Bearer ${token}`
			}
		})
		const result = await res.json();
		// console.log(result)
		if(!result.message){
			dispatch(addPurchasedScheduleSuccess(result,treatment_id,purchased_amount))
		}else{
			dispatch(failed("Add_pending_schedule_failed",result.message));
		}
	}
}