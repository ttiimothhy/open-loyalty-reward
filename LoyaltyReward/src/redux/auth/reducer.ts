import {IAuthActions} from "./actions";
export interface IAuthState{
	id:number;
	isAuthenticated:boolean|null;
	isLoggedIn:boolean|null;
	token:string;
	message:string;
}

export const initialState = {
	id:0,
	isAuthenticated:null,
	isLoggedIn:null,
	token:"",
	message:""
}

const authReducer = (state:IAuthState = initialState,action:IAuthActions):IAuthState => {
	switch(action.type){
		case "@@auth/Login_success":
			return{
				...state,
				isAuthenticated:true,
				id:action.id,
				token:action.token
			}
		case "@@auth/Login_failure":
			return{
				...state,
				isAuthenticated:false,
				isLoggedIn:false,
				message:action.message
			}
		case "@@auth/Login_null":
			return{
				...state,
				isLoggedIn:null
			}
		case "@@auth/Logout_success":
			return{
				...state,
				isAuthenticated:false,
				isLoggedIn:null,
				token:"",
				message:""
			}
		default:
			return state
	}
}

export default authReducer;