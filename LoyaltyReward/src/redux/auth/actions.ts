export function loginSuccess(id:number,token:string){
	return{
		type:"@@auth/Login_success" as const,
		id,
		token
	}
}

export function loginFailure(message:string){
	return{
		type:"@@auth/Login_failure" as const,
		message
	}
}

export function loginNullSuccess(){
	return{
		type:"@@auth/Login_null" as const,
	}
}

export function logoutSuccess(){
	return{
		type:"@@auth/Logout_success" as const
	}
}

export type IAuthActions = ReturnType<typeof loginSuccess|typeof loginFailure|typeof loginNullSuccess|typeof logoutSuccess>