import {ContactCard} from "../../../models";
import {IAgentCallListActions} from "./actions";

export interface IAgentCallListState{
	call:boolean|null;
	contactList:ContactCard[];
	addNewContact:boolean|null;
	canWhatsApp:boolean|null
}

const initialState = {
	call:null,
	contactList:[],
	addNewContact:null,
	canWhatsApp:null
}

const agentCallListReducer = (state:IAgentCallListState = initialState,action:IAgentCallListActions):IAgentCallListState => {
	switch(action.type){
		case "@@agentCallList/Call_success":
			return{
				...state,
				call:true
			}
		case "@@agentCallList/Call_fail":
			return{
				...state,
				call:false
			}
		case "@@agentCallList/Call_null":
			return{
				...state,
				call:null
			}
		case "@@agentCallList/Get_all_call_list":
			return{
				...state,
				contactList:action.callList
			}
		case "@@agentCallList/Add_new_contact":
			return{
				...state,
				addNewContact:true
			}
		case "@@agentCallList/Add_new_contact_fail":
			return{
				...state,
				addNewContact:false
			}
		case "@@agentCallList/Add_new_contact_null":
			return{
				...state,
				addNewContact:null
			}
		case "@@agentCallList/Whats_app_new_number":
			return{
				...state,
				canWhatsApp:true
			}
		case "@@agentCallList/Whats_app_new_number_null":
			return{
				...state,
				canWhatsApp:null
			}
		default:
			return state
	}
}

export default agentCallListReducer;