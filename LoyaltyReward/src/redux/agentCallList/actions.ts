import {ContactCard} from "../../../models";

export function callSuccessSuccess(){
	return{
		type:"@@agentCallList/Call_success" as const
	}
}

export function callFailSuccess(){
	return{
		type:"@@agentCallList/Call_fail" as const
	}
}

export function callNull(){
	return{
		type:"@@agentCallList/Call_null" as const
	}
}

export function getAllCallListSuccess(callList:ContactCard[]){
	return{
		type:"@@agentCallList/Get_all_call_list" as const,
		callList
	}
}

export function addNewContactSuccess(){
	return{
		type:"@@agentCallList/Add_new_contact" as const,
	}
}

export function addNewContactFail(){
	return{
		type:"@@agentCallList/Add_new_contact_fail" as const,
	}
}

export function addNewContactNull(){
	return{
		type:"@@agentCallList/Add_new_contact_null" as const,
	}
}

export function whatsAppNewNumber(){
	return{
		type:"@@agentCallList/Whats_app_new_number" as const,
	}
}

export function setWhatsAppNull(){
	return{
		type:"@@agentCallList/Whats_app_new_number_null" as const,
	}
}

type Failed = "Call_success_failed"|"Call_fail_failed"|"Get_all_call_list_failed"|"Add_new_contact_failed"
export function failed(type:Failed,message:string){
	return{
		type,
		message
	}
}

export type IAgentCallListActions = ReturnType<typeof callSuccessSuccess|typeof callFailSuccess|typeof callNull|
typeof getAllCallListSuccess|typeof addNewContactSuccess|typeof addNewContactFail|typeof addNewContactNull|
typeof whatsAppNewNumber|typeof setWhatsAppNull|typeof failed>