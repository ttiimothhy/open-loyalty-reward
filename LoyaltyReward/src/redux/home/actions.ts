import {Advertisement,Surgery,Service} from "../../../models";

export function getAdvertisementSuccess(advertisement:Advertisement[]){
	return{
		type:"@@home/Get_advertisement" as const,
		advertisement
	}
}

export function getTopSurgerySuccess(surgery:Surgery[]){
	return{
		type:"@@home/Get_top_surgery" as const,
		surgery
	}
}

export function serviceSuccess(service:Service[]){
	return{
		type:"@@home/Get_service" as const,
		service
	}
}

export function currentPointSuccess(point:number){
	return{
		type:"@@home/Get_point" as const,
		point
	}
}

export function getPredictedTreatmentSuccess(surgery:Surgery[]){
	return{
		type:"@@home/Get_predicted_treatment" as const,
		surgery
	}
}

type Failed = "Get_advertisement_failed"|"Get_top_surgery_failed"|"Get_service_failed"|"Get_point_failed"|
"Get_predicted_treatment_failed";
export function failed(type:Failed,message:string){
	return {
		type,
		message
	}
}

export type IHomeActions = ReturnType<typeof getAdvertisementSuccess|typeof getTopSurgerySuccess|typeof serviceSuccess|
typeof currentPointSuccess|typeof getPredictedTreatmentSuccess|typeof failed>