import {Advertisement,Surgery,Service} from "../../../models";
import {IHomeActions} from "./actions";

export interface IHomeState{
	advertisement:Advertisement[];
	surgery:Surgery[];
	service:Service[];
	point:number|null;
	predictedTreatment:Surgery[]
}

const initialState = {
	advertisement:[],
	surgery:[],
	service:[],
	point:null,
	predictedTreatment:[]
}

const homeReducer = (state:IHomeState = initialState,action:IHomeActions):IHomeState => {
	switch(action.type){
		case "@@home/Get_advertisement":
			return{
				...state,
				advertisement:action.advertisement
			}
		case "@@home/Get_top_surgery":
			return{
				...state,
				surgery:action.surgery
			}
		case "@@home/Get_service":
			return{
				...state,
				service:action.service
			}
		case "@@home/Get_point":
			return{
				...state,
				point:action.point
			}
		case "@@home/Get_predicted_treatment":
			return{
				...state,
				predictedTreatment:action.surgery
			}
		default:
			return state
	}
}

export default homeReducer;