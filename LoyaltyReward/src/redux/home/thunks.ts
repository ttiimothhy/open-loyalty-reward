import AsyncStorage from "@react-native-community/async-storage";
import {Dispatch} from "redux";
import envfile from "../../../envfile";
import {currentPointSuccess,failed,getAdvertisementSuccess,getPredictedTreatmentSuccess,getTopSurgerySuccess,IHomeActions,serviceSuccess} from "./actions";

export function getAdvertisement(){
	return async(dispatch:Dispatch<IHomeActions>) => {
		const token = await AsyncStorage.getItem("token");
		// console.log(token)
		const res = await fetch(`${envfile.REACT_APP_BACKEND_PORT}/index/advertisements`,{
			headers:{
				Authorization:`Bearer ${token}`
			}
		})
		const result = await res.json();
		// console.log(result);
		if(!result.message){
			dispatch(getAdvertisementSuccess(result))
		}else{
			dispatch(failed("Get_advertisement_failed",result.message))
		}
	}
}

export function getTopSurgery(){
	return async(dispatch:Dispatch<IHomeActions>) => {
		const token = await AsyncStorage.getItem("token")
		const res = await fetch(`${envfile.REACT_APP_BACKEND_PORT}/index/treatments`,{
			headers:{
				Authorization:`Bearer ${token}`
			}
		})
		const result = await res.json();
		// console.log(result);
		if(!result.message){
			dispatch(getTopSurgerySuccess(result))
		}else{
			dispatch(failed("Get_top_surgery_failed",result.message))
		}
	}
}

export function getPredictedTreatment(){
	return async(dispatch:Dispatch<IHomeActions>) => {
		const token = await AsyncStorage.getItem("token");
		const res = await fetch(`${envfile.REACT_APP_BACKEND_PORT}/index/prediction`,{
			headers:{
				Authorization:`Bearer ${token}`
			}
		})
		const result = await res.json();
		if(!result.message){
			dispatch(getPredictedTreatmentSuccess(result));
		}else{
			dispatch(failed("Get_predicted_treatment_failed",result.message))
		}
	}
}

export function service(){
	return async(dispatch:Dispatch<IHomeActions>) => {
		const token = await AsyncStorage.getItem("token")
		const res = await fetch(`${envfile.REACT_APP_BACKEND_PORT}/index/services`,{
			headers:{
				Authorization:`Bearer ${token}`
			}
		})
		const result = await res.json();
		// console.log(result);
		if(!result.message){
			dispatch(serviceSuccess(result))
		}else{
			dispatch(failed("Get_service_failed",result.message))
		}
	}
}

export function currentPoint(){
	return async(dispatch:Dispatch<IHomeActions>) => {
		const token = await AsyncStorage.getItem("token")
		const res = await fetch(`${envfile.REACT_APP_BACKEND_PORT}/index/points`,{
			headers:{
				Authorization:`Bearer ${token}`
			}
		})
		const result = await res.json();
		// console.log(result);
		if(result.success){
			dispatch(currentPointSuccess(result.current_point));
		}else{
			dispatch(failed("Get_point_failed",result.error));
		}
	}
}