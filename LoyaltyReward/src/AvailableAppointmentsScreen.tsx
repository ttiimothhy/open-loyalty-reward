import {FontAwesomeIcon} from "@fortawesome/react-native-fontawesome";
import React,{useState,useEffect} from "react";
import {View,Image,Text,useColorScheme,TouchableOpacity,FlatList,} from "react-native";
import {Colors} from "react-native/Libraries/NewAppScreen";
import {styles} from "./styles/AvailableAppointmentsScreenStyles";
import {ParamList, Treatment} from "../models";
import {RouteProp, useNavigation, useRoute} from "@react-navigation/native";
import {MiddleCriteriaPicker} from "./components/Picker";
import {useDispatch,useSelector} from "react-redux";
import {getAllTreatments,getTreatments,placeCriteria,typeCriteria} from "./redux/availableAppointments/thunks";
import {IRootState} from "../store";
import envfile from "../envfile";

export const Appointment:React.FC<{imageSource:string,id:number}> = ({imageSource,id,children}) => {
	const navigation = useNavigation();
	return(
	<View style={styles.appointment}>
		<View style={styles.appointmentImageContainer}>
			<Image source={{uri:`${envfile.REACT_APP_IMAGE_PATH}/treatments/${imageSource}`}}
			style={styles.appointmentImage}/>
		</View>
		<TouchableOpacity style={styles.appointmentTextContainer} onPress={() => {
			navigation.navigate("AppointmentDetailsScreen",{
				treatmentId:id
			})
		}}>
			<Text style={styles.appointmentText}>{children}</Text>
		</TouchableOpacity>
	</View>
	)
}

function AvailableAppointmentsScreen(){
	const isDarkMode = useColorScheme() === "dark";
	const [filter,setFilter] = useState(false);
	const treatments = useSelector((state:IRootState)=>state.availableAppointmentsPage.treatment);
	const typeCriterion = useSelector((state:IRootState)=>state.availableAppointmentsPage.type);
	const placeCriterion = useSelector((state:IRootState)=>state.availableAppointmentsPage.place);
	const treatmentsLength = treatments.length
	const [typeId,setTypeId] = useState<number|null>(null);
	// console.log(typeId)
	const [placeId,setPlaceId] = useState<number|null>(null);
	const route = useRoute<RouteProp<ParamList,"Detail">>();
	// console.log(route.params)
	const dispatch = useDispatch();
	// const navigation = useNavigation();
	useEffect(() => {
		dispatch(getAllTreatments());
		dispatch(typeCriteria());
		dispatch(placeCriteria());
	},[dispatch])

	useEffect(() => {
		dispatch(getTreatments(typeId,placeId));
	},[dispatch,typeId,placeId])

	useEffect(() => {
		if(route.params){
			if(route.params.typeCriteria){
				// setFilter(true)
				setTypeId(route.params.typeCriteria)
			}else{
				dispatch(getAllTreatments())
				setTypeId(null)
			}
		}else{
			dispatch(getAllTreatments())
			setTypeId(null)
		}
	},[route.params])



	const bodyArea = typeCriterion.map((criterion) => ({label:criterion.service,value:criterion.id + ""}))
	const place = placeCriterion.map((criterion) => ({label:criterion.name,value:criterion.id + ""}))

	return(
		<View style={[(filter ? styles.flexContainer : styles.flex),{backgroundColor:Colors.white}]}>
			<View style={styles.titleContainer}>
				<View>
					<Text>{treatmentsLength}項</Text>
				</View>
				<View style={styles.selectedButtonContainer}>
					<TouchableOpacity style={styles.arrangeButton} onPress={() => {
						// setResetCriteria(!resetCriteria)
						setTypeId(null)
						setPlaceId(null)
						dispatch(getAllTreatments());
					}}>
						<View style={styles.filter}>
							<Text style={styles.notArrange}>重設條件</Text>
						</View>
					</TouchableOpacity>
					<TouchableOpacity style={styles.arrangeButton} onPress={() => {
						setFilter(!filter)
						setTypeId(null)
						setPlaceId(null)
						dispatch(getAllTreatments());
					}}>
						{filter && <FontAwesomeIcon icon="filter" size={12}></FontAwesomeIcon>}
						<View style={styles.filter}>
							<Text style={filter ? styles.arrangeNow : styles.notArrange}>篩選</Text>
						</View>
					</TouchableOpacity>
				</View>
			</View>
			{filter && <View style={styles.filterContainer}>
				<View style={styles.pickerContainer}>
					<Text style={styles.pickerTitle}>類型</Text>
					<MiddleCriteriaPicker defaultValue="" label={"選擇類型"} items={bodyArea} criteria={setTypeId}
					reactDispatch={getAllTreatments()} nearCriteria={placeId}/>
				</View>
				<View style={styles.pickerContainer}>
					<Text style={styles.pickerTitle}>地點</Text>
					<MiddleCriteriaPicker defaultValue="" label={"選擇地點"} items={place} criteria={setPlaceId}
					reactDispatch={getAllTreatments()} nearCriteria={typeId}/>
				</View>
			</View>}
			<View style={filter ? styles.longScrollView : styles.scrollView}>
				<FlatList<Treatment> data={treatments} renderItem={(row)=>(
					<Appointment imageSource={row.item.image} key={row.item.id} id={row.item.id}>{row.item.title}</Appointment>
				)}/>
			</View>
			{/* {appointmentList.map((appointment,index) => (
				<Appointment imageSource={appointment.image} key={index}>{appointment.text}</Appointment>
			))} */}
		</View>
	)
}

export default AvailableAppointmentsScreen;