import {RouteProp,useNavigation,useRoute} from "@react-navigation/native";
import React from "react";
import {useEffect} from "react";
import {SafeAreaView,View,Image,Text,TouchableOpacity} from "react-native";
import QRCode from "react-native-qrcode-svg";
import {useDispatch, useSelector} from "react-redux";
import {ParamList} from "../models";
import {IRootState} from "../store";
import {pendingSchedule} from "./redux/myAppointments/thunks";
import {setPurchaseNull} from "./redux/treatmentPurchases/actions";
import {styles} from "./styles/FinishTreatmentPurchaseScreenStyles";

function FinishTreatmentPurchaseScreen(){
	const navigation = useNavigation();
	const dispatch = useDispatch();
	const route = useRoute<RouteProp<ParamList,"Detail">>();
	const treatments = useSelector((state:IRootState)=>state.treatmentPurchasesPage.treatment);
	useEffect(() => {
		dispatch(pendingSchedule())
	})

	return(
		<SafeAreaView>
			<View style={styles.imageContainer}>
				<Image style={styles.image} source={require("../images/trophy.png")}/>
				<Text style={styles.successText}>購買成功</Text>
			</View>
			<View>
				<View style={styles.textContainer}>
					<Text style={styles.text}>療程名稱</Text>
					<Text>{(treatments.filter(treatment=>treatment.id === route.params.treatmentId)
					.map(treatment=>treatment.title))[0]}</Text>
				</View>
				<View style={styles.textContainer}>
					<Text style={styles.text}>購買數量</Text>
					<Text>{route.params.treatmentNumber}次</Text>
				</View>
			</View>
			{route.params && <View style={styles.qrcodeContainer}>
                <QRCode value={`{treatment_name:${(treatments.filter(treatment=>treatment.id === route.params.treatmentId)
				.map(treatment=>treatment.title))[0]},purchase_number:${route.params.treatmentNumber}}`}
				// logo={require("../images/badge.png")} logoSize={100}
				/>
            </View>}
			<TouchableOpacity style={styles.confirmButton} onPress={() => {
				dispatch(setPurchaseNull())
				navigation.navigate("AvailablePurchasesScreen")
			}}>
				<Text style={styles.confirmText}>返回</Text>
			</TouchableOpacity>
		</SafeAreaView>
	)
}

export default FinishTreatmentPurchaseScreen;