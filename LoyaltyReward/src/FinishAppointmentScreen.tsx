import {RouteProp,useNavigation,useRoute} from "@react-navigation/native";
import React from "react";
import {SafeAreaView,View,Image,Text,TouchableOpacity} from "react-native";
import {useDispatch,useSelector} from "react-redux";
import {ParamList} from "../models";
import {IRootState} from "../store";
import {setTimeslotNull} from "./redux/availableAppointments/actions";
import {styles} from "./styles/FinishAppointmentScreenStyles";

function FinishAppointmentScreen(){
	const navigation = useNavigation();
	const dispatch = useDispatch();
	const route = useRoute<RouteProp<ParamList,"Detail">>();
	const treatments = useSelector((state:IRootState)=>state.availableAppointmentsPage.treatment);
	// console.log(route.params)

	return(
		<SafeAreaView>
			<View style={styles.imageContainer}>
				<Image style={styles.image} source={require("../images/trophy.png")}/>
				<Text style={styles.successText}>預約成功</Text>
			</View>
			<View>
				<View style={styles.textContainer}>
					<Text style={styles.text}>療程名稱</Text>
					<Text>
						{(treatments.filter(id => id.id === route.params.treatmentId).map(id => id.title))[0]}
					</Text>
				</View>
				<View style={styles.textContainer}>
					<Text style={styles.text}>預約日期</Text>
					<Text>{route.params.time.slice(0,10)}</Text>
				</View>
				<View style={styles.textContainer}>
					<Text style={styles.text}>預約時間</Text>
					<Text>{route.params.time.slice(10)}</Text>
				</View><View style={styles.textContainer}>
					<Text style={styles.text}>預約分店</Text>
					<Text>{route.params.place}</Text>
				</View>
			</View>
			<TouchableOpacity style={styles.confirmButton} onPress={() => {
				dispatch(setTimeslotNull())
				navigation.navigate("AvailableAppointmentsScreen")
			}}>
				<Text style={styles.confirmText}>返回</Text>
			</TouchableOpacity>
		</SafeAreaView>
	)
}

export default FinishAppointmentScreen;