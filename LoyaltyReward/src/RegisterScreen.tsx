import {useNavigation} from "@react-navigation/native";
import React,{useEffect,useState} from "react";
import {SafeAreaView,View,ScrollView,Text,TextInput,TouchableOpacity,TouchableWithoutFeedback,Keyboard,KeyboardAvoidingView,
Platform,ImageBackground} from "react-native";
import {useDispatch,useSelector} from "react-redux";
import {register,registerNull} from "./redux/user/thunks";
import {IRootState} from "../store";
import {backgroundStyles,flexStyles} from "./styles/LoadingScreenStyles";
import {styles} from "./styles/RegisterScreenStyles";

function checkMobile(phone:string){
	if(phone.length > 0){
		for(let i = 0; i < phone.length; i++){
			if(isNaN(parseInt(phone[i]))){
				return false
			}
		}
		return true
	}
	return true
}

function RegisterScreen(){
	const [username,setUsername] = useState("");
	const [password,setPassword] = useState("");
	const [name,setName] = useState("");
	const [email,setEmail] = useState("");
	const [phone,setPhone] = useState("");
	const [suggestedPhone,setSuggestedPhone] = useState("");
	const [wrongPhone,setWrongPhone] = useState(false);
	const [wrongReferral,setWrongReferral] = useState(false)
	const [wrongEmail,setWrongEmail] = useState(false);
	// console.log(wrongPhone);
	// console.log(wrongEmail);
	const navigation = useNavigation();
	const dispatch = useDispatch();
	let isRegistered = useSelector((state:IRootState)=>state.user.isRegistered);

	// useEffect(() => {
	// 	if(isRegistered){
	// 		setTimeout(() => {
	// 			navigation.navigate("RoleLogin");
	// 		},1000)
	// 	}
	// },[isRegistered])

	useEffect(() => {
		if(isRegistered){
			setWrongPhone(false)
			setWrongEmail(false)
			setWrongReferral(false)
		}
	},[isRegistered])

	return(
		<KeyboardAvoidingView behavior={Platform.OS === "ios" ? "padding" : "height"} style={flexStyles.flex}>
			<TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
				<ScrollView style={styles.main} keyboardShouldPersistTaps="handled">
					<ImageBackground source={require("../images/simpleBackground.png")} style={backgroundStyles.image}>
						<SafeAreaView style={flexStyles.flex}>
							{/* <CustomerRoleCheckBox/> */}
							<View style={styles.register}>
								<Text style={styles.title}>用戶註冊</Text>
								<Text>用戶名</Text>
									<TextInput style={styles.textInput} value={username} onChangeText={setUsername}></TextInput>
								<Text>密碼</Text>
								<TextInput
									style={styles.textInput} value={password} onChangeText={setPassword}
									secureTextEntry
								></TextInput>
								<Text>姓名</Text>
								<TextInput style={styles.textInput} value={name} onChangeText={setName}></TextInput>
								<Text>電郵地址</Text>
								<TextInput
									style={styles.textInput} value={email} onChangeText={setEmail}
									keyboardType="email-address"
								></TextInput>
								<Text>聯絡電話</Text>
								<TextInput style={styles.textInput} value={phone} onChangeText={setPhone}
								keyboardType="numeric"/>
								<Text>推薦人的電話號碼(如有)</Text>
								<TextInput
									style={styles.textInput} value={suggestedPhone} onChangeText={setSuggestedPhone}
									keyboardType="numeric"
								></TextInput>
							</View>
							<View style={styles.notificationContainer}>
							{isRegistered && (
								<View style={styles.notification}>
									<Text style={styles.notificationText}>註冊成功</Text>
								</View>
							)}
							{isRegistered === false && (
							<View style={styles.dangerNotification}>
								<Text style={styles.dangerNotificationText}>此用戶名/電郵/電話已被使用</Text>
							</View>)}
							{(wrongPhone || wrongEmail || wrongReferral) &&
							<View style={styles.dangerNotification}>
								<Text style={styles.dangerNotificationText}>此電郵/電話/推薦人電話不正確</Text>
							</View>}
							</View>
							<View style={styles.registerContainer}>
								<TouchableOpacity onPress={()=>{
									navigation.navigate("RoleLogin");
									dispatch(registerNull());
									setWrongEmail(false)
									setWrongPhone(false)
									setWrongReferral(false)
								}}>
									<View>
										<Text style={styles.loginButton}>已註冊? 請登入</Text>
									</View>
								</TouchableOpacity>
								<TouchableOpacity onPress={()=>{
									let isNumber = checkMobile(phone)
									let isReferral = checkMobile(suggestedPhone)
									// console.log(isReferral)
									dispatch(registerNull())
									{phone.length === 8 && (suggestedPhone.length === 0 || suggestedPhone.length === 8) &&
									email.includes("@") && isNumber && isReferral &&
									dispatch(register(username,password,name,email,parseInt(phone),parseInt(suggestedPhone)))
									}
									setWrongEmail(false)
									setWrongPhone(false)
									setWrongReferral(false)
									{!email.includes("@") && setWrongEmail(true)}
									{phone.length !== 8 && setWrongPhone(true)}
									{!isNumber && setWrongPhone(true)}
									{(suggestedPhone.length !== 0 && suggestedPhone.length !== 8) && setWrongReferral(true)}
									{!isReferral && setWrongReferral(true)}
									{phone.length === 8 && (suggestedPhone.length === 0 || suggestedPhone.length === 8) &&
									email.includes("@") && isNumber && isReferral && navigation.navigate("Register",{screen:"SecurityCodeScreen"})}
								}}>
									<View style={styles.registerButton}>
										<Text style={styles.button}>註冊</Text>
									</View>
								</TouchableOpacity>
							</View>
						</SafeAreaView>
					</ImageBackground>
				</ScrollView>
			</TouchableWithoutFeedback>
		</KeyboardAvoidingView>
	)
}

export default RegisterScreen;