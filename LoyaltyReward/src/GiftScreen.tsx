import {FontAwesomeIcon} from "@fortawesome/react-native-fontawesome";
import {useNavigation} from "@react-navigation/native";
import React,{useEffect,useState} from "react";
import {SafeAreaView,ScrollView,useColorScheme,View,Text,TouchableOpacity,Image} from "react-native";
import {Colors} from "react-native/Libraries/NewAppScreen";
import {LongCriteriaPicker} from "./components/Picker";
import {styles} from "./styles/AvailableAppointmentsScreenStyles";
import {giftStandard, giftStyles} from "./styles/GiftScreenStyles";
import EvilIcons from "react-native-vector-icons/EvilIcons";
import {IRootState} from "../store";
import {useDispatch,useSelector} from "react-redux";
import {getAllRewards,getAllRewardTypes,getFilteredRewards,getPointValue,addToCart, getInStockItem, getOutOfStockItem}
from "./redux/redeem/thunks";
import envfile from "../envfile";
import {useBottomTabBarHeight} from "@react-navigation/bottom-tabs";
import FontAwesome5 from "react-native-vector-icons/FontAwesome5";

const placeholder = {
	label:"選擇數量",
	value:null,
	color:'#555555',
}

export const Gift:React.FC<{id:number,imageSource:string,text:string,point:number,type:string,setVisible:
React.Dispatch<React.SetStateAction<boolean>>|null}> = ({id,imageSource,text,point,type,setVisible}) => {
	const navigation = useNavigation();
	const dispatch = useDispatch();
	const [addSuccess,setAddSuccess] = useState(false);

	useEffect(() => {
		let timeOutId = setTimeout(() => {
			setAddSuccess(false)
		},3000)
		return(() => {
			clearTimeout(timeOutId)
		})
	},[addSuccess])

	return(
		<View style={giftStyles.gift}>
			<View style={giftStyles.giftImageContainer}>
				<TouchableOpacity style={styles.appointmentTextContainer} onPress={() => {
					navigation.navigate("GiftDetailsScreen",{
						rewardId:id
					})
				}}>
					<Image source={{uri:`${envfile.REACT_APP_IMAGE_PATH}/rewards/${imageSource}`}} style={giftStyles.giftImage}/>
				</TouchableOpacity>
			</View>
			<View style={giftStyles.giftDescriptionContainer}>
				<View style={giftStyles.pointContainer}>
					<View style={giftStyles.giftContainer}>
						<Text style={styles.appointmentText}>{text}</Text>
					</View>
					<Text style={giftStyles.pointText}>{point} pt</Text>
				</View>
				<View style={giftStyles.typeContainer}>
					<Text style={giftStyles.typeText}>{type}</Text>
					{addSuccess ? <View>
						<FontAwesome5 name="check" size={18}/>
					</View>
					: <TouchableOpacity onPress={() => {
						{setVisible && setVisible(true)};
						{dispatch(addToCart(id))};
						setAddSuccess(true)
					}}>
					<EvilIcons name="cart" style={giftStyles.cartIcon}/>
					</TouchableOpacity>}
				</View>
			</View>
		</View>
	)
}

function GiftScreen(){
	const isDarkMode = useColorScheme() === "dark";
	const [filter,setFilter] = useState(false);
	const navigation = useNavigation();
	const [type_id,setType_id] = useState<number|null>(null);
	// const [location_id,setLocation_id] = useState<number|null>(null);
	const giftList = useSelector((state:IRootState)=>state.redeemPage.reward);
	const typeCriteria = useSelector((state:IRootState)=>state.redeemPage.rewardType);
	const type = typeCriteria.map((criteria) => ({label:criteria.type,value:criteria.id + ""}))
	const myPoint = useSelector((state:IRootState)=>state.redeemPage.currentPoint);
	const numberOfGift = useSelector((state:IRootState)=>state.redeemPage.reward).length;
	const tabBarHeight = useBottomTabBarHeight();
	const giftContainerStyle = giftStandard(tabBarHeight);
	const dispatch = useDispatch();
	useEffect(() => {
		dispatch(getAllRewards());
		dispatch(getAllRewardTypes());
		dispatch(getPointValue());
	},[dispatch])

	useEffect(() => {
		dispatch(getFilteredRewards(type_id));
	},[dispatch,type_id])

	return(
		<SafeAreaView style={[giftStyles.flexContainer,{backgroundColor:Colors.white}]}>
			<View style={styles.titleContainer}>
				<View>
					<Text>{numberOfGift}項</Text>
				</View>
				<View style={styles.selectedButtonContainer}>
					<TouchableOpacity style={styles.arrangeButton} onPress={() => {
						setFilter(!filter)
						setType_id(null)
						// setLocation_id(null)
						dispatch(getAllRewards());
					}}>
						{filter && <FontAwesomeIcon icon="filter" size={12}></FontAwesomeIcon>}
						<View style={styles.filter}>
							<Text style={filter ? styles.arrangeNow : styles.notArrange}>篩選</Text>
						</View>
					</TouchableOpacity>
				</View>
			</View>
			{filter && <View style={styles.filterContainer}>
				<View style={styles.pickerContainer}>
					<Text style={styles.pickerTitle}>類型</Text>
					<LongCriteriaPicker label={"選擇類型"} items={type} criteria={setType_id}
					reactDispatch={getAllRewards()}/>
				</View>
			</View>}
			<View style={filter ? giftContainerStyle.longScrollViewContainer : giftContainerStyle.scrollViewContainer}>
				<ScrollView contentContainerStyle={filter ? giftContainerStyle.longScrollView : giftContainerStyle.scrollView}>
					{giftList.length > 0 && giftList.map((gift,index)=>{
					// let inStock = inStockItemList.find(stock=>stock.id === gift.id)
					// let outOfStock = outOfStockItemList.find(stock=>stock.id === gift.id)
					return(
						<Gift key={index} id={gift.id} imageSource={gift.image} text={gift.name} point={gift.redeem_points}
						type={gift.type} setVisible={null}/>
					)
				})}
					{giftList.length > 0 && giftList.length % 2 !== 0 && (
						<View style={giftStyles.empty}></View>
					)}
				</ScrollView>
			</View>
			<View style={!filter ? giftContainerStyle.myPointContainer : giftContainerStyle.myPointLongContainer}>
				<Text style={giftStyles.myPointText}>積分結餘: {myPoint && myPoint} pt</Text>
				<TouchableOpacity style={giftStyles.enquiryButton} onPress={() => {
					navigation.navigate("MyPointScreen")
				}}>
					<Text style={giftStyles.myPointText}>查詢結餘</Text>
				</TouchableOpacity>
			</View>
		</SafeAreaView>
	)
}

export default GiftScreen;