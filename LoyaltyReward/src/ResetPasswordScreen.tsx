import {useNavigation} from "@react-navigation/native";
import React,{useState} from "react";
import {useEffect} from "react";
import {SafeAreaView,Keyboard,TouchableWithoutFeedback,View,Text,TextInput,TouchableOpacity,ImageBackground} from "react-native";
import {useDispatch, useSelector} from "react-redux";
import {CustomerRoleCheckBox} from "./components/CheckBox";
import {resetNull,resetPasswordByEmail} from "./redux/user/thunks";
import {IRootState} from "../store";
import {backgroundStyles} from "./styles/LoadingScreenStyles";
import {styles} from "./styles/ResetPasswordScreenStyles";

function ResetPasswordScreen(){
	const [email,setEmail] = useState("");
	const [wrongEmail,setWrongEmail] = useState(false)
	const dispatch = useDispatch();
	const navigation = useNavigation();
	const isReset = useSelector((state:IRootState)=>state.user.isReset);
	// console.log(isReset)

	//autmatically nagivate to rolelogin after reseted
	useEffect(() => {
		if(isReset){
			setTimeout(() => {
				navigation.navigate("RoleLogin")
			},1000)
		}
	},[isReset])

	return(
		<TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>

			{/* background image */}
			<ImageBackground source={require("../images/simpleBackground.png")} style={backgroundStyles.image}>
				<SafeAreaView style={styles.main}>
					<CustomerRoleCheckBox/>

					{/* container */}
					<View style={styles.emailInput}>

						{/* input guidance text */}
						<View><Text style={styles.title}>重設密碼</Text></View>

						{/* input email */}
						<TextInput style={styles.textInput} placeholder="電郵" value={email} onChangeText={(event)=>{
							setEmail(event);
							setWrongEmail(false)
							dispatch(resetNull())
						}}></TextInput>

						{/* buttons container */}
						<View style={styles.buttonContainer}>

							{/* back to login page button */}
							<TouchableOpacity onPress={()=>{
								navigation.navigate("RoleLogin")
								setWrongEmail(false)
								dispatch(resetNull())
							}}>
								<View style={styles.sendButton}>
									<Text style={styles.button}>返回登入頁面</Text>
								</View>
							</TouchableOpacity>

							{/* send button */}
							<TouchableOpacity onPress={()=>{
								{!email.includes("@") && setWrongEmail(true)}
								{email.includes("@") && dispatch(resetPasswordByEmail(email))}
							}}>
								<View style={styles.sendButton}>
									<Text style={styles.button}>傳送</Text>
								</View>
							</TouchableOpacity>

						</View>

						{/* status container */}
						<View style={styles.notificationContainer}>

							{/* reset ed */}
							{isReset && (
								<View style={styles.notification}>
									<Text style={styles.notificationText}>已傳送</Text>
								</View>
							)}

							{/* wrong email */}
							{wrongEmail && (
								<View style={styles.dangerNotification}>
									<Text style={styles.dangerNotificationText}>這電郵不正確</Text>
								</View>
							)}

							{/* this email has not been registerd */}
							{isReset === false && (
								<View style={styles.dangerNotification}>
									<Text style={styles.dangerNotificationText}>此電郵沒有登記資料</Text>
								</View>
							)}
						</View>
					</View>
				</SafeAreaView>
			</ImageBackground>
		</TouchableWithoutFeedback>
	)
}

export default ResetPasswordScreen;