import {useNavigation,StackActions} from "@react-navigation/native";
import React,{useState,useEffect,useCallback} from "react";
import {ScrollView,View,TextInput,Text,TouchableOpacity,TouchableWithoutFeedback,Keyboard,KeyboardAvoidingView,
Platform,ImageBackground,SafeAreaView} from "react-native";
import {useDispatch,useSelector} from "react-redux";
import {login,loginNull} from "./redux/auth/thunks";
import {registerNull,resetNull} from "./redux/user/thunks";
import {IRootState} from "../store";
import {backgroundStyles,flexStyles} from "./styles/LoadingScreenStyles";
import {styles} from "./styles/LoginScreenStyles";
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome'
import {authorize,prefetchConfiguration} from "react-native-app-auth";
import {handleAppleAuthorize, handleFacebookAuthorize, handleGoogleAuthorize} from "./redux/socialAuth/thunks";
import {loginGoogleSuccess} from "./redux/socialAuth/actions";
import {configs} from "../auth.config";
import {CustomerRoleCheckBox} from "./components/CheckBox";
import {LoginButton,AccessToken,Profile,LoginManager} from 'react-native-fbsdk-next';
import appleAuth,{AppleButton} from "@invertase/react-native-apple-authentication";
import envfile from "../envfile";

function LoginScreen(){
	const navigation = useNavigation();
	const isAuthenticated = useSelector((state:IRootState)=>state.auth.isAuthenticated);
	const isLoggedIn = useSelector((state:IRootState)=>state.auth.isLoggedIn);
	const dispatch = useDispatch();
	const [username,setUsername] = useState("");
	const [password,setPassword] = useState("");
	const [keyboardShow,setKeyboardShow] = useState(false);
	// console.log(isLoggedIn)

	const handleAuthorize = useCallback(
		async(provider:any) => {
			try{
				const config = configs[provider];
				// console.log(provider)
				const newAuthState = await authorize(config);
				// console.log(newAuthState)
				if(provider === "google"){
					dispatch(handleGoogleAuthorize(newAuthState.accessToken))
					dispatch(loginGoogleSuccess(newAuthState))
				}
			}catch(e){
				console.log(e);
			}
		}
	,[dispatch]);

	useEffect(() => {
		if(isAuthenticated){
			navigation.dispatch(
				StackActions.replace("Main")
			)
		}
	},[isAuthenticated])

	useEffect(() => {
		prefetchConfiguration({
			warmAndPrefetchChrome:true,
			...configs.google
		})
	},[])

	async function onAppleButtonPress() {
		// performs login request
		const appleAuthRequestResponse = await appleAuth.performRequest({
			requestedOperation:appleAuth.Operation.LOGIN,
			requestedScopes:[appleAuth.Scope.EMAIL,appleAuth.Scope.FULL_NAME],
		})

		// get current authentication state for user
		// This method must be tested on a real device. On the iOS simulator it always throws an error.
		const credentialState = await appleAuth.getCredentialStateForUser(appleAuthRequestResponse.user);

		// use credentialState response to ensure the user is authenticated
		if(credentialState === appleAuth.State.AUTHORIZED){
			// console.log(appleAuthRequestResponse)
			// console.log(appleAuth)
		  	// user is authenticated
			dispatch(handleAppleAuthorize(`${envfile.REACT_APP_APPLE_APP_ID}`,appleAuthRequestResponse.identityToken,appleAuthRequestResponse.authorizationCode))
		}
	}

	return(
		<KeyboardAvoidingView behavior={Platform.OS === "ios" ? "padding" : "height"} style={flexStyles.flex}>
		<TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false} style={styles.flex}>
			<ScrollView scrollEnabled={keyboardShow} style={styles.main} keyboardShouldPersistTaps="handled">
				<ImageBackground source={require("../images/simpleBackground.png")} style={backgroundStyles.image}>
					<SafeAreaView style={flexStyles.flex}>
						<CustomerRoleCheckBox/>
						<View style={styles.login}>
							<View style={styles.loginSub}>
								<Text style={styles.title}>登入</Text>
								<Text>用戶名</Text>
								<TextInput onFocus = {() => setKeyboardShow(true)} style={styles.textInput} placeholder="username"
								value={username} onBlur = {() => {
									setKeyboardShow(false)
								}} onChangeText={setUsername}/>
								<Text>密碼</Text>
								<TextInput style={styles.textInput} placeholder="password" value={password}
								onChangeText={setPassword} secureTextEntry onBlur = {() => {
									setKeyboardShow(false)
								}} onFocus = {() => setKeyboardShow(true)}
								/>
								<TouchableOpacity onPress={()=>{
									navigation.navigate("ResetPassword");
									dispatch(loginNull());
									dispatch(resetNull());
								}}>
									<Text style={styles.resetButton}>忘記密碼</Text>
								</TouchableOpacity>
								<LoginButton
									// style={styles.socialFacebookLogin}
									onLoginFinished={async (error,result) => {
										if(!result.isCancelled)
										// {
										// 	console.log("login is cancelled");
										// }else
										{
											const token = await AccessToken.getCurrentAccessToken()
											const customerProfile = await Profile.getCurrentProfile()
											// console.log(customerProfile)
											dispatch(handleFacebookAuthorize(token?.accessToken,customerProfile?.email))
										}
										// console.log(customerProfile)
									}}
									onLogoutFinished={()=>{
										dispatch(loginNull())
										LoginManager.logOut()
									}}
								/>
								<View style={styles.socialLoginContainer}>
									<TouchableOpacity onPress={()=>{
										handleAuthorize("google")
									}}>
										<View style={styles.socialLogin}>
											<FontAwesomeIcon color={"#ffffff"} size={18} icon={["fab","google"]}/>
										</View>
									</TouchableOpacity>
									<AppleButton
										buttonStyle={AppleButton.Style.BLACK}
										buttonType={AppleButton.Type.SIGN_IN}
										style={styles.appleButton}
										onPress={onAppleButtonPress}
									/>
								</View>
							</View>
						</View>
						<View style={styles.notificationContainer}>
							{isLoggedIn === false && (
								<View style={styles.dangerNotification}>
									<Text style={styles.dangerNotificationText}>用戶名/密碼錯誤</Text>
								</View>
							)}
						</View>
						<View style={styles.loginContainer}>
							<TouchableOpacity onPress={()=>{
								navigation.navigate("Register",{screen:"RegisterScreen"})
								dispatch(loginNull());
								dispatch(registerNull());
							}}>
								<View>
									<Text style={styles.registerButton}>新用戶? 請註冊</Text>
								</View>
							</TouchableOpacity>
							<TouchableOpacity onPress={()=>{
								dispatch(login(username,password))
							}}>
								<View style={styles.loginButton}>
									<Text style={styles.button}>登入</Text>
								</View>
							</TouchableOpacity>
						</View>
					</SafeAreaView>
				</ImageBackground>
			</ScrollView>
		</TouchableWithoutFeedback>
		</KeyboardAvoidingView>
	)
}

export default LoginScreen;