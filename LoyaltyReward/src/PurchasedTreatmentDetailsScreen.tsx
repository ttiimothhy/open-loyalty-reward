import {RouteProp,useNavigation,useRoute} from "@react-navigation/native";
import React from "react";
import {View,Image,Text,useColorScheme,TouchableOpacity} from "react-native";
import QRCode from "react-native-qrcode-svg";
import {Colors} from "react-native/Libraries/NewAppScreen";
import {useSelector} from "react-redux";
import envfile from "../envfile";
import {ParamList} from "../models";
import {IRootState} from "../store";
import {styles} from "./styles/PurchasedTreatmentDetailsScreenStyles";

function PurchasedTreatmentDetailsScreen(){
	const isDarkMode = useColorScheme() === "dark";
	const navigation = useNavigation();
	const route = useRoute<RouteProp<ParamList,"Detail">>();
	// console.log(route.params)
	const purchased = useSelector((state:IRootState)=>state.myAppointmentsPage.purchasedSchedule);
	// console.log(purchased)
	return(
		<View style={[styles.pageContainer,{backgroundColor:Colors.white}]}>
			<View style={styles.imageContainer}>
				{route.params && <Image source={{uri:`${envfile.REACT_APP_IMAGE_PATH}/treatments/${(purchased.
				filter(schedule=>schedule.id === route.params.purchasedId).map(schedule=>schedule.image))[0]}`}}
				style={styles.image}/>}
			</View>
			{route.params && route.params.purchasedId && route.params.remaining && <View style={styles.qrcodeContainer}>
                <QRCode value={`{treatment_name:${(purchased.filter(treatment=>treatment.id === route.params.purchasedId)
				.map(treatment=>treatment.title))[0]},description:${(purchased.filter(schedule=>schedule.id
				=== route.params.purchasedId).map(schedule=>schedule.detail))[0]},
				purchase_number:${route.params.remaining}}`}
				// logo={require("../images/badge.png")} logoSize={100}
				size={80}
				/>
            </View>}
			{route.params && <Text style={styles.title}>{(purchased.filter(schedule=>schedule.id
			=== route.params.purchasedId).map(schedule=>schedule.title))[0]}</Text>}
			<View style={styles.timeContainer}>
				<Text style={styles.treatmentText}>療程介紹</Text>
				{route.params && <Text style={styles.treatmentContentText}>{(purchased.filter(schedule=>schedule.id
				=== route.params.purchasedId).map(schedule=>schedule.detail))[0]}</Text>}
			</View>
			<View style={styles.placeContainer}>
				<Text style={styles.timeText}>剩餘療程券數量</Text>
				{route.params && route.params.remaining && <Text style={styles.timeText}>{parseInt(route.params.remaining)}次</Text>}
			</View>
			<TouchableOpacity style={styles.backButton} onPress={() => {
				navigation.navigate("MyAppointmentsScreen")
			}}>
				<Text style={styles.backText}>返回</Text>
			</TouchableOpacity>
		</View>
	)
}

export default PurchasedTreatmentDetailsScreen;