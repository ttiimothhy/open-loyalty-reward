import {RouteProp,useNavigation,useRoute} from "@react-navigation/native";
import React from "react";
import {View,Image,Text,useColorScheme,TouchableOpacity} from "react-native";
import {Colors} from "react-native/Libraries/NewAppScreen";
import {useSelector} from "react-redux";
import envfile from "../envfile";
import {ParamList} from "../models";
import {IRootState} from "../store";
import {styles} from "./styles/AlreadyAppointmentDetailsScreenStyles";

function AlreadyAppointmentDetailsScreen(){
	const isDarkMode = useColorScheme() === "dark";
	const navigation = useNavigation();
	const route = useRoute<RouteProp<ParamList,"Detail">>();
	const pending = useSelector((state:IRootState)=>state.myAppointmentsPage.pendingSchedule);
	return(
		<View style={[styles.pageContainer,{backgroundColor:Colors.white}]}>
			<View style={styles.imageContainer}>
				{route.params && <Image source={{uri:`${envfile.REACT_APP_IMAGE_PATH}/treatments/${pending.
				filter(schedule=>schedule.id === route.params.appointmentId).map(schedule=>schedule.image)}`}}
				style={styles.image}/>}
			</View>
			{route.params && <Text style={styles.title}>{pending.filter(schedule=>schedule.id
			=== route.params.appointmentId).map(schedule=>schedule.title)}</Text>}
			<View style={styles.timeContainer}>
				<Text style={styles.timeText}>日期及時間</Text>
				{route.params && <Text style={styles.timeText}>{pending.filter(schedule=>schedule.id
				=== route.params.appointmentId).map(schedule=>schedule.timeslot)}</Text>}
			</View>
			<View style={styles.placeContainer}>
				<Text style={styles.timeText}>分店</Text>
				{route.params && <Text style={styles.timeText}>{pending.filter(schedule=>schedule.id
				=== route.params.appointmentId).map(schedule=>schedule.name)}</Text>}
			</View>
			<TouchableOpacity style={styles.backButton} onPress={() => {
				navigation.navigate("MyAppointmentsScreen")
			}}>
				<Text style={styles.backText}>返回</Text>
			</TouchableOpacity>
		</View>
	)
}

export default AlreadyAppointmentDetailsScreen;