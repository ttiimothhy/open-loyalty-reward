import React,{useEffect} from "react";
import {useColorScheme,View,Text,TouchableOpacity} from "react-native";
import {Colors} from "react-native/Libraries/NewAppScreen";
import {styles} from "./styles/MyPointScreenStyles";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import {useNavigation} from "@react-navigation/native";
import {useDispatch,useSelector} from "react-redux";
import { getPointValue } from "./redux/redeem/thunks";
import { IRootState } from "../store";
import {getExpiredPointsHistory, getPointsEarnedHistory} from "./redux/point/thunks";

export const PointsEarnedHistoryRow:React.FC<{date:string,summary:string,location:string,points:string}> = ({date,summary,location,points}) => {
	return(
		<View style={styles.content}>
			<View style={styles.dateText}>
				<View style={styles.year}><Text>{date.slice(0,10)}</Text></View>
				<View><Text>{date.slice(11,16)}</Text></View>
			</View>
			<View style={styles.middleText}><Text>{summary}</Text></View>
			<View style={styles.finalText}><Text>+{points}分</Text></View>
		</View>
	)
}

function MyPointScreen(){
	const isDarkMode = useColorScheme() === "dark";
	const navigation = useNavigation();
	const myPoint = useSelector((state:IRootState)=>state.redeemPage.currentPoint);
	const pointsEarnedHistoryRows = useSelector((state:IRootState)=>state.pointPage.pointsEarnedHistoryRow);
	const expiredPointHistory = useSelector((state:IRootState)=>state.pointPage.expiredPointHistory)
	const dispatch = useDispatch();
	useEffect(() => {
		dispatch(getPointsEarnedHistory());
		dispatch(getPointValue());
		dispatch(getExpiredPointsHistory());
	},[dispatch])

	return(
		<View style={[styles.flexContainer,{backgroundColor:Colors.white}]}>
			<View style={styles.titleContainer}>
				<View style={styles.titleRowContainer}>
					<View style={styles.titleFirstContainer}>
						<Text style={styles.titleGreyText}>現有</Text>
						<Text style={styles.titleText}>{myPoint}分</Text>
					</View>
				</View>
				<TouchableOpacity style={styles.pointIconContainer} onPress={() => {
					navigation.navigate("RedeemHistoryScreen")
				}}>
					<MaterialCommunityIcons name="shopping-outline" style={styles.pointIcon}/>
				</TouchableOpacity>
			</View>
			<View style={styles.contentContainer}>
				<Text style={styles.pointTitle}>積分紀錄</Text>
				{pointsEarnedHistoryRows.length > 0 && pointsEarnedHistoryRows.map((pointsEarnedHistoryRow,index)=>(
					<PointsEarnedHistoryRow key={index} date={pointsEarnedHistoryRow.date} summary={pointsEarnedHistoryRow.summary}
					location={pointsEarnedHistoryRow.location} points={pointsEarnedHistoryRow.points}/>
				))}
			</View>
			<View style={styles.expiredContentContainer}>
				<Text style={styles.pointTitle}>過期積分</Text>
				{expiredPointHistory && expiredPointHistory.length > 0 && expiredPointHistory.map((history,index)=>(
					<View key={index} style={styles.expiredContent}>
						<View style={styles.expiredDateText}>
							<View style={styles.year}><Text>{history.date.slice(0,10)}</Text></View>
							<View><Text>{history.date.slice(11,16)}</Text></View>
						</View>
						<View style={styles.finalText}><Text>{history.points}分</Text></View>
					</View>
				))}
			</View>
		</View>
	)
}

export default MyPointScreen;