import {StyleSheet} from "react-native";

export const styles = StyleSheet.create({
	checkBoxContainer:{
		height:90,
		// width:30,
		// flex:1,
		alignItems:"flex-start",
	},
	checkBox:{
		height:18,
		width:18,
		marginRight:10
	},
	sliderContainer:{
		flexDirection:"row",
		alignItems:"center",
		justifyContent:"flex-end",
		paddingHorizontal:10,
		marginTop:10
	},
	sliderStyle:{
		marginHorizontal:5,
		width:50,
		height:40
	},
})

export const roleCheckBoxStyles = StyleSheet.create({
	checkBoxContainer:{
		height:20,
		marginLeft:5,
		// width:30,
		// flex:1,
		alignItems:"flex-start",
	},
	checkBox:{
		height:18,
		width:18,
		marginRight:10
	},
})

export const customerStyles = StyleSheet.create({
	role:{
		color:"#555555",
		fontWeight:"600"
	},
})

export const agentStyles = StyleSheet.create({
	role:{
		color:"#cccccc",
		fontWeight:"600"
	},
})