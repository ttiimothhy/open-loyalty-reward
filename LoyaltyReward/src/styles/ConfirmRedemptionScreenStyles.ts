import {Dimensions, StyleSheet} from "react-native";

export const styles = StyleSheet.create({
	flexContainer:{
		height:Dimensions.get("window").height
	},
	scrollView:{
		height:Dimensions.get("window").height * 0.45,
		borderBottomWidth:1,
		borderColor:"#eeeeee",
		paddingBottom:10
	},
	image:{
		height:100,
		width:100,
		borderRadius:4
	},
	imageContainer:{
		justifyContent:"center"
	},
	description:{
		// flex:1,
		// marginVertical:10,
		flexDirection:"row",
		alignItems:"center",
	},
	name:{
		width:Dimensions.get("window").width * 0.55,
		fontSize:20,
		fontWeight:"800",
		marginBottom:10
	},
	giftType:{
		color:"#adadad",
		fontSize:16,
		fontWeight:"600",
		marginBottom:10
	},
	giftOtherContainer:{
		flexDirection:"row",
		justifyContent:"space-between",
		marginHorizontal:25,
		marginTop:15
	},
	payContainer:{
		paddingVertical:10,
		paddingHorizontal:20,
		borderBottomWidth:1,
		borderColor:"#eeeeee"
	},
	payTitle:{
		fontSize:20,
		color:"#adadad",
		marginBottom:10
	},
	payRowContainer:{
		flexDirection:"row",
		justifyContent:"space-between",
		alignItems:"center",
	},
	placeContainer:{
		paddingTop:10,
		paddingHorizontal:20,
	},
	confirmButton:{
		alignItems:"center",
		justifyContent:"center",
		backgroundColor:"#eedd8d",
		paddingVertical:15,
		marginHorizontal:20,
		marginTop:10,
		marginBottom:15,
		borderRadius:5
	},
	confirmText:{
		color:"#ffffff",
		fontWeight:"800",
		fontSize:18
	},
	backButton:{
		alignItems:"center",
		justifyContent:"center",
		borderColor:"#eedd8d",
		borderWidth:2,
		paddingVertical:15,
		marginHorizontal:20,
		borderRadius:5
	},
	backText:{
		color:"#eedd8d",
		fontWeight:"800",
		fontSize:18
	},
	pointRowContainer:{
		flexDirection:"row",
		width:Dimensions.get("window").width * 0.4,
		justifyContent:"space-between",
		alignItems:"center"
	},
	haveText:{
		color:"#8688bc"
	},
	warning:{
		marginTop:5,
		alignSelf:"center",
		color:"#ff0000",
		fontSize:12,
		fontWeight:"800"
	}
})