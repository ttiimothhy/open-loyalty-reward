import {StyleSheet,Dimensions} from "react-native";
// const windowDimension = useWindowDimensions();

export const notificationStyles = StyleSheet.create({
	commonNotification:{
		justifyContent:"center",
		alignItems:"center",
		borderRadius:5,
		padding:10
	},
	successColor:{
		backgroundColor:"#1e8449",
	},
	dangerColor:{
		backgroundColor:"#ff0000",
	},
	button:{
		flexDirection:"row",
		borderWidth:2,
		borderColor:"#adadad",
		paddingLeft:20,
		paddingRight:20,
		paddingTop:10,
		paddingBottom:10,
		borderRadius:5,
	},
	buttonText:{
		fontSize:20,
		fontWeight:"600",
		color:"#adadad"
	}
})

export const styles = StyleSheet.create({
	flex:{
		flex:1
	},
	main:{
		flexDirection:"column",
		flex:1,
		// backgroundColor:"#aaff00"
	},
	title:{
		fontSize:30,
		fontWeight:"600",
		marginBottom:15
	},
	login:{
		height:Dimensions.get("window").height * 0.66,
		justifyContent:"flex-end"
	},
	loginSub:{
		marginLeft:30,
		marginTop:10,
		// flex:1
	},
	textInput:{
		marginTop:3,
		marginBottom:10,
		fontSize:20,
		borderWidth:1,
		borderColor:"#adadad",
		width:300,
		height:40,
		borderRadius:5
	},
	socialLoginContainer:{
		marginTop:10,
		flexDirection:"row"
	},
	socialLogin:{
		backgroundColor:"#cccccc",
		height:36,
		width:36,
		borderRadius:3,
		justifyContent:"center",
		alignItems:"center",
		marginRight:5,
	},
	notificationContainer:{
		marginTop:20,
		height:40,
		marginBottom:45,
		// backgroundColor:"#ff0000"
	},
	dangerNotification:{
		marginLeft:30,
		marginRight:30,
		...notificationStyles.dangerColor,
		...notificationStyles.commonNotification
	},
	dangerNotificationText:{
		fontSize:20,
		color:"#ffffff",
		fontWeight:"600"
	},
	loginContainer:{
		justifyContent:"space-between",
		alignItems:"flex-end",
		flexDirection:"row",
		margin:30,
		// backgroundColor:"#ffcc00"
		// flex:0
	},
	resetButton:{
		marginBottom:10,
		fontSize:14,
		fontWeight:"600",
	},
	registerButton:{
		fontSize:14,
		fontWeight:"600",
		color:"#ffffff"
	},
	loginButton:{
		...notificationStyles.button
	},
	button:{
		...notificationStyles.buttonText
	},
	appleButton: {
		width:36,
		height:36,
		shadowColor:"#555555",
		shadowOpacity:0.5,
		shadowOffset:{
			width:0,
			height:3
		},
		// marginVertical:15,
	}
})
