import {Dimensions,StyleSheet} from "react-native";

export const styles = StyleSheet.create({
	flexContainer:{
		height:Dimensions.get("window").height * 0.9,
	},
	flex:{
		height:Dimensions.get("window").height * 0.84,
	},
	titleContainer:{
		flexDirection:"row",
		padding:15,
		justifyContent:"space-between",
		borderBottomWidth:1,
		borderBottomColor:"#eeeeee"
	},
	selectedButtonContainer:{
		flexDirection:"row",
	},
	arrangeButton:{
		flexDirection:"row",
		alignItems:"center",
		width:50,
		height:16,
		justifyContent:"flex-end"
	},
	arrange:{
		marginLeft:3,
		marginRight:5,
		flexDirection:"row",
	},
	arrangeNow:{
		fontWeight:"800",
		color:"#000000"
	},
	notArrange:{
		color:"#adadad"
	},
	filter:{
		marginLeft:3,
		flexDirection:"row",
	},
	filterContainer:{
		padding:10,
		flexDirection:"row",
		// height:200,
		alignItems:"center",
		justifyContent:"space-between",
		borderBottomWidth:1,
		borderBottomColor:"#eeeeee"
	},
	appointment:{
		flexDirection:"row",
		margin:5
	},
	appointmentImageContainer:{
		borderWidth:1,
		borderColor:"#adadad",
		marginRight:5
	},
	appointmentImage:{
		width:200,
		height:160,
		// marginRight:20
	},
	appointmentTextContainer:{
		alignItems:"center",
		justifyContent:"center",
		width:Dimensions.get("window").width * 0.4,
	},
	appointmentText:{
		fontSize:16,
		// flexWrap:"wrap"
	},
	pickerTitle:{
		fontWeight:"600",
		fontSize:16,
		marginLeft:5,
		marginRight:10
	},
	pickerContainer:{
		flexDirection:"row",
		alignItems:"center",
	},
	longScrollView:{
		justifyContent:"center",
		alignItems:"center",
		paddingBottom:200
	},
	scrollView:{
		justifyContent:"center",
		alignItems:"center",
		paddingBottom:100
	},
	flatList:{
		paddingBottom:100
	}
})
