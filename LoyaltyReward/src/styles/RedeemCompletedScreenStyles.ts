import {StyleSheet,Dimensions} from "react-native";

export const styles = StyleSheet.create({
    container:{},
	image:{
		width:110,
		height:110,
	},
    redeemOrderCompletedContainer:{
		height:Dimensions.get("window").height * 0.35,
		justifyContent:"flex-end",
		alignItems:"center",
		marginBottom:10
	},
	redeemOrderCompletedImageContainer:{
		marginVertical:10,
		width:112,
		// borderWidth:1,
		borderColor:"#adadad",
		borderRadius:2
	},
    redeemStatus:{
		fontSize:20,
		color:"#11aa00",
		fontWeight:"600",
		marginBottom:5
	},
    redeemNumber:{
		marginBottom:10
	},
    redeemDetailContainer:{
		paddingHorizontal:20,
	},
    redeemDetailRow:{
		flexDirection:"row",
		justifyContent:"space-between",
	},
	titleText:{
		fontSize:18,
		fontWeight:"600",
		marginBottom:10
	},
    text:{
		marginBottom:5
	},
	bottomText:{
		marginBottom:15
	},
    button:{
		alignItems:"center",
		justifyContent:"center",
		backgroundColor:"#eedd8d",
		paddingVertical:15,
		marginHorizontal:20,
		marginTop:30,
		marginBottom:15,
		borderRadius:5
	},
	buttonText:{
		color:"#ffffff",
		fontWeight:"800",
		fontSize:18
	},
	qrcodeContainer:{
		marginTop:30,
		justifyContent:"center",
		alignItems:"center",
		marginBottom:10
	},
	giftContentContainer:{
		width:Dimensions.get("window").width * 0.3,
		// alignItems:"flex-end"
	}
})