import {StyleSheet} from "react-native";
import {notificationStyles} from "./LoginScreenStyles";

export const styles = StyleSheet.create({
	main:{
		flexDirection:"column",
		flex:1,
	},
	emailInput:{
		flex:1,
		// marginTop:60,
		// justifyContent:"center",
		marginLeft:30,
		marginRight:20
	},
	title:{
		fontSize:20,
		fontWeight:"600",
		marginBottom:10
	},
	textInput:{
		marginTop:3,
		marginBottom:5,
		fontSize:20,
		borderWidth:1,
		borderColor:"#adadad",
		width:310,
		height:40,
		borderRadius:5
	},
	buttonContainer:{
		flexDirection:"row",
		justifyContent:"space-between",
	},
	sendButton:{
		borderColor:"#000000",
		paddingRight:20,
		paddingTop:10,
		paddingBottom:10,
		borderRadius:5,
		// alignSelf:"flex-end"
	},
	button:{
		color:"#888888",
		fontWeight:"600"
	},
	notificationContainer:{
		height:40,
		// backgroundColor:"#ff0000"
	},
	notification:{
		marginRight:15,
		marginBottom:4,
		...notificationStyles.successColor,
		...notificationStyles.commonNotification
	},
	dangerNotification:{
		marginRight:15,
		marginBottom:4,
		...notificationStyles.dangerColor,
		...notificationStyles.commonNotification
	},
	notificationText:{
		fontSize:20,
		color:"#adadad",
		fontWeight:"600"
	},
	dangerNotificationText:{
		fontSize:20,
		color:"#ffffff",
		fontWeight:"600"
	},
	registerContainer:{
		justifyContent:"space-between",
		alignItems:"flex-end",
		flexDirection:"row",
		marginTop:55,
		marginLeft:30,
		marginRight:30,
		marginBottom:30,
		// flex:1,
		// backgroundColor:"#ff0000"
	},
})