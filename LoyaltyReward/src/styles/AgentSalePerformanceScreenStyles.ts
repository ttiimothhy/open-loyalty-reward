import { Platform } from "react-native";
import {Dimensions,StyleSheet} from "react-native";

export const styles = StyleSheet.create({
	flexContainer:{
		height:Dimensions.get("window").height
	},
	contentContainer:{
		paddingHorizontal:15,
		paddingTop:10,
	},
    container:{
        flex:1,
        backgroundColor:"#ffffff",
        alignItems:"center",
        justifyContent:"center",
    },
    textStyle:{
        textAlign:"center",
        fontFamily:Platform.OS === "ios" ? "AvenirNext-Regular" : "Roboto",
    },
    largeText:{
        fontSize:44,
    },
    smallText:{
        fontSize:18,
    },
    textInput:{
        backgroundColor:"#666666",
        color:"white",
        height:40,
        width: 300,
        marginTop:20,
        marginHorizontal:20,
        paddingHorizontal:10,
        alignSelf:"center",
    },
    imageContainer:{
        flex:1,
    },
    image:{
        flex:1,
        width:Dimensions.get("window").width,
        height: Dimensions.get("window").height,
        resizeMode:'cover',
    },
    tabRow:{
        flexDirection:"row"
    },
    tab:{},
    subHeadRow:{
		marginBottom:10
	},
	header:{
		fontSize:20,
		fontWeight:"600",
		color:"#adadad"
	},
    detailRow:{
        flexDirection:"row",
		justifyContent:"space-between",
		alignItems:"center",
		marginBottom:5
    },
	buttonContainer:{
		flexDirection:"row"
	},
    titleContainer:{
		flexDirection:"row",
		padding:15,
		// justifyContent:"space-between",
		borderBottomWidth:1,
		borderBottomColor:"#eeeeee"
	},
	title:{
		borderBottomWidth:2,
		borderBottomColor:"#1e8449",
	},
	allTitle:{
		marginRight:20
	},
	titleFocusedText:{
        color:"#000000",
		fontSize:20,
		fontWeight:"600",
		marginBottom:3,
		// marginRight:15
	},
	titleText:{
		fontSize:20,
		color:"#adadad",
		// marginRight:15
	},
	confirmButton:{
		backgroundColor:"#81c2ff",
		padding:8,
		alignItems:"center",
		justifyContent:"center",
		borderRadius:3,
		marginRight:5
	},
	confirmText:{
		color:"#ffffff",
		fontSize:16
	},
	cancelButton:{
		backgroundColor:"#bbbbbb",
		paddingHorizontal:6,
		alignItems:"center",
		justifyContent:"center",
		borderRadius:3
	},
	scoreContainer:{
		marginBottom:20
	},
	detailSpaceBetweenRow:{
		flexDirection:"row",
		justifyContent:"space-between",
		marginBottom:5
	},

    barChartList:{},
    barChartContainer:{},
    subheadRow:{},
    subHead:{
		paddingHorizontal:10,
		paddingBottom:15,
		fontSize:18,
		fontWeight:"600"
	},
});