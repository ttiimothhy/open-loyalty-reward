import {StyleSheet} from "react-native";

export const styles = StyleSheet.create({
	imageCardContainer:{
		flexDirection:"column",
		alignItems:"center"
	},
	imageBorder:{
		borderWidth:1,
		borderColor:"#adadad",
		marginHorizontal:4,
		marginBottom:3,
		position:"relative"
	},
	image:{
		width:200,
		height:160,
		// resizeMode:"contain",
	},
	imageText:{
		fontSize:18,
		marginBottom:10,
	},
})