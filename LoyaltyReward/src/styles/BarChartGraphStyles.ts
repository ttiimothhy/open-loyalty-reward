import {StyleSheet} from "react-native";

export const styles = StyleSheet.create({
	barChart:{
		borderWidth:0.5,
		borderColor:"#efefef",
		borderRadius:2,
		marginBottom:10
	},
})