import {Dimensions,StyleSheet} from "react-native";

export const giftStyles = StyleSheet.create({
	flexContainer:{
		height:Dimensions.get("window").height * 0.9,
		// marginBottom:79
	},
	gift:{
		width:Dimensions.get("window").width * 0.45,
		// backgroundColor:"#ffff00"
	},
	giftImageContainer:{
		margin:10,
		width:Dimensions.get("window").width * 0.4 + 2,
		borderWidth:1,
		borderColor:"#adadad",
	},
	giftImage:{
		width:Dimensions.get("window").width * 0.4,
		height:140,
	},
	pointContainer:{
		// width:Dimensions.get("window").width * 0.35,
		marginHorizontal:10,
		justifyContent:"space-between",
		alignItems:"flex-start",
		flexDirection:"row",
		marginBottom:5
	},
	giftContainer:{
		width:Dimensions.get("window").width * 0.32,
	},
	pointText:{
		color:"#adadad",
		fontSize:12,
		fontWeight:"600",
		// marginLeft:10
	},
	typeContainer:{
		marginHorizontal:10,
		justifyContent:"space-between",
		alignItems:"center",
		flexDirection:"row",
	},
	typeText:{
		color:"#adadad",
		fontSize:12
	},
	cartIcon:{
		fontSize:24,
		color:"#555555"
	},
	enquiryButton:{
		borderWidth:2,
		borderRadius:2,
		borderColor:"#adadad",
		padding:3
	},
	myPointText:{
		color:"#ffffff",
		fontSize:16,
		fontWeight:"600"
	},
	empty:{
		width:Dimensions.get("window").width * 0.45,
	},
	modal:{
		paddingVertical:10,
		alignItems:"center",
		justifyContent:"space-between",
		paddingHorizontal:20,
		backgroundColor:"#ffffff",
		borderRadius:5,
		borderWidth:2,
		borderColor:"#cccccc",
		flexDirection:"row"
	},
	numberContainer:{
		flexDirection:"row",
		alignItems:"center"
	},
	numberText:{
		marginRight:10
	},
	numberButton:{
		padding:8,
		backgroundColor:"#11aa00",
		borderRadius:3,
		borderWidth:2,
		borderColor:"#aaff00"
	},
	numberButtonText:{
		color:"#ffffff",
		fontWeight:"600"
	},
	numberInput:{
		height:30,
		width:Dimensions.get("window").width * 0.2,
		paddingHorizontal:3,
		fontSize:16,
		borderWidth:1,
		borderColor:"#adadad",
		borderRadius:3
	},
	giftDescriptionContainer:{
		justifyContent:"space-between",
		height:Dimensions.get("window").width * 0.18,
	}
})

export function giftStandard(height:number){
	return StyleSheet.create({
		longScrollViewContainer:{
			// height:Dimensions.get("window").height,
			paddingBottom:height + 155,
		},
		scrollViewContainer:{
			// height:Dimensions.get("window").height,
			paddingBottom:height + 105,
		},
		myPointContainer:{
			position:"absolute",
			flexDirection:"row",
			justifyContent:"space-around",
			alignItems:"center",
			width:Dimensions.get("window").width,
			paddingVertical:6,
			backgroundColor:"rgba(17,170,0,0.431)",
			bottom:height/2 + 58
		},
		myPointLongContainer:{
			position:"absolute",
			flexDirection:"row",
			justifyContent:"space-around",
			alignItems:"center",
			width:Dimensions.get("window").width,
			paddingVertical:6,
			backgroundColor:"rgba(17,170,0,0.431)",
			bottom:height + 18
		},
		longScrollView:{
			position:"relative",
			flexDirection:"row",
			justifyContent:"space-around",
			flexWrap:"wrap"
		},
		scrollView:{
			position:"relative",
			justifyContent:"space-around",
			flexDirection:"row",
			flexWrap:"wrap"
		},
	})
}