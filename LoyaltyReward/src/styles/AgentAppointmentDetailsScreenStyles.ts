import {StyleSheet,Dimensions} from "react-native";

export const styles = StyleSheet.create({
    flexContainer:{
		// flex:1,
		marginTop:5,
		// backgroundColor:"#ffcc00"
	},
	mainTextContainer:{
		marginLeft:20
	},
    mainText:{
		fontSize:18,
		fontWeight:"800"
	},
    pageContainer:{
		height:Dimensions.get("window").height
	},
	imageContainer:{
		margin:10,
		borderWidth:1,
		borderColor:"#adadad",
	},
	image:{
		height:Dimensions.get("window").height * 0.35,
		width:Dimensions.get("window").width * 0.94,
	},
	title:{
		marginHorizontal:20,
		fontSize:22,
		fontWeight:"800"
	},
	timeContainer:{
		flexDirection:"row",
		alignItems:"center",
		justifyContent:"space-between",
		paddingHorizontal:20,
		paddingBottom:5,
		// paddingTop:10
	},
	placeContainer:{
		flexDirection:"row",
		alignItems:"center",
		justifyContent:"space-between",
		paddingHorizontal:20,
		paddingTop:5
	},
	timeText:{
		fontSize:16
	},
	confirmButton:{
		alignItems:"center",
		justifyContent:"center",
		backgroundColor:"#eedd8d",
		paddingVertical:15,
		marginHorizontal:20,
		marginTop:5,
		marginBottom:10,
		borderRadius:5
	},
	confirmText:{
		color:"#ffffff",
		fontWeight:"800",
		fontSize:18
	},
	backButton:{
		alignItems:"center",
		justifyContent:"center",
		borderColor:"#eedd8d",
		borderWidth:2,
		paddingVertical:15,
		marginHorizontal:20,
		borderRadius:5
	},
	backText:{
		color:"#eedd8d",
		fontWeight:"800",
		fontSize:18
	},
	warningContainer:{
		marginTop:5,
		alignItems:"center",
		// marginTop:12,
		height:12
	},
	warning:{
		color:"#ff0000",
		fontWeight:"800",
		fontSize:11
	}
})