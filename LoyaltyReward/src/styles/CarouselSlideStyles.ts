import {StyleSheet,Dimensions} from "react-native";

export const styles = StyleSheet.create({
	carouselCard:{
		// backgroundColor:"#a0f0a0",
		// height:250,
		padding:30,
	},
	image:{
		height:250,
		resizeMode:"cover",
		backgroundColor:"#ffffff"
	},
	carouselCardText:{
		fontSize:30,
		color:"#555555"
	},
	carouselCardSubText:{
		color:"#ff6600",
		fontWeight:"600"
	},
	carouselBarCard:{
		backgroundColor:"#ff9900",
		height:80,
		justifyContent:"center",
		paddingHorizontal:20
	},
	carouselBarText:{
		fontSize:20,
		color:"#555555",
		fontWeight:"600",
		paddingBottom:5
	},
	carouselBarSubText:{
		color:"#ffffff",
		// fontWeight:"600"
	},
	carouselBarSubAgentText:{
		color:"#555555",
		fontWeight:"600"
	},
	contactCarouselCard:{
		backgroundColor:"#ffffff",
		height:80,
		padding:5,
		// marginTop:10,
		justifyContent:"center"
	},
	mainRowContainer:{
		flexDirection:"row",
		alignItems:"center",
		justifyContent:"space-between",
	},
	rowContainer:{
		flexDirection:"row",
		alignItems:"center",
		marginBottom:5,
	},
	contactButton:{
		paddingVertical:6,
		paddingHorizontal:10,
		backgroundColor:"#8ed490",
		borderRadius:3
	},
	contactText:{
		color:"#ffffff",
		fontSize:18,
		fontWeight:"600"
	},
	contactTitle:{
		fontSize:18,
		marginRight:5,
		fontWeight:"600",
		color:"#adadad"
	},
	carouselContainer:{
		flexDirection:"row",
		alignItems:"center",
		marginTop:10,
		// marginHorizontal:20
	},
	arrow:{
		marginLeft:Dimensions.get("window").width * 0.03
	},
	// arrowRight:{
	// 	marginRight:20
	// },
	carouselImage:{
		padding:0
	},
	confirmButton:{
		paddingVertical:6,
		paddingHorizontal:10,
		backgroundColor:"#adadad",
		borderRadius:3,
		marginRight:5
	},
	finishConfirm:{
		paddingVertical:6,
		paddingHorizontal:10,
		backgroundColor:"#ffcc00",
		borderRadius:3,
		marginRight:5
	},
	finishConfirmText:{
		color:"#555555",
		fontSize:18,
		fontWeight:"600"
	},
	buttonContainer:{
		flexDirection:"row"
	}
})