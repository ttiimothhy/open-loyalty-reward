import {RouteProp,useNavigation,useRoute} from "@react-navigation/native";
import React,{useState} from "react";
import {View,Image,Text,useColorScheme,TouchableOpacity} from "react-native";
import {Colors} from "react-native/Libraries/NewAppScreen";
import {useDispatch, useSelector} from "react-redux";
import envfile from "../envfile";
import {ParamList} from "../models";
import {IRootState} from "../store";
import {LongAppointmentPurchasePicker} from "./components/Picker";
import {setNumberFail,setPurchaseNull} from "./redux/treatmentPurchases/actions";
import {confirmPurchases} from "./redux/treatmentPurchases/thunks";
import {styles} from "./styles/TreatmentPurchaseScreenStyles";

let purchase = []
for(let i = 1; i <= 11; i++){
	purchase.push(i)
}
let treatmentPurchase = purchase.map(amount=>({label:amount + "",value:amount + ""}))

function TreatmentPurchasesScreen(){
	const isDarkMode = useColorScheme() === "dark";
	const navigation = useNavigation();
	const route = useRoute<RouteProp<ParamList,"Detail">>();
	const treatments = useSelector((state:IRootState)=>state.treatmentPurchasesPage.treatment);
	const sendPurchaseSuccess = useSelector((state:IRootState)=>state.treatmentPurchasesPage.purchaseSuccess);
	const [treatmentAmount,setTreatmentAmount] = useState<string|null>(null);
	const dispatch = useDispatch();
	// console.log(treatmentAmount)

	return(
		<View style={[styles.pageContainer,{backgroundColor:Colors.white}]}>
			<View style={styles.imageContainer}>
				<Image source={{uri:`${envfile.REACT_APP_IMAGE_PATH}/treatments/${treatments.filter
				(treatment=>treatment.id === route.params.treatmentId).map(treatment=>treatment.image)}`}}
				style={styles.image}/>
			</View>
			<Text style={styles.title}>{treatments.filter(treatment=>treatment.id === route.params.treatmentId)
			.map(treatment=>treatment.title)}</Text>
			<View style={styles.timeContainer}>
				<Text style={styles.timeText}>療程次數</Text>
				<LongAppointmentPurchasePicker label={"選擇數量"} items={treatmentPurchase} criteria={setTreatmentAmount}/>
			</View>
			<View style={styles.warningContainer}>
			{sendPurchaseSuccess === false && <Text style={styles.warning}>未填寫所有資料</Text>}
			</View>
			<TouchableOpacity style={styles.confirmButton} onPress={() => {
				{!treatmentAmount && dispatch(setNumberFail())}
				{treatmentAmount && dispatch(confirmPurchases(parseInt(treatmentAmount),route.params.treatmentId))}
				{treatmentAmount && navigation.navigate("FinishTreatmentPurchaseScreen",{
					treatmentId:route.params.treatmentId,
					treatmentNumber:treatmentAmount
				})}
			}}>
				<Text style={styles.confirmText}>確認</Text>
			</TouchableOpacity>
			<TouchableOpacity style={styles.backButton} onPress={() => {
				navigation.navigate("AvailablePurchasesScreen")
				dispatch(setPurchaseNull())
			}}>
				<Text style={styles.backText}>返回</Text>
			</TouchableOpacity>
		</View>
	)
}

export default TreatmentPurchasesScreen;