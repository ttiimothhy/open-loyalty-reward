import {RouteProp,useNavigation,useRoute} from "@react-navigation/native";
import React,{useEffect, useState} from "react";
import {View,Image,Text,useColorScheme,TouchableOpacity} from "react-native";
import {Colors} from "react-native/Libraries/NewAppScreen";
import {useDispatch, useSelector} from "react-redux";
import envfile from "../envfile";
import {ParamList} from "../models";
import {IRootState} from "../store";
import {LongAppointmentPicker} from "./components/Picker";
import {sendTimeslotFail,setTimeslotNull} from "./redux/availableAppointments/actions";
import {sendTimeslot} from "./redux/availableAppointments/thunks";
import {updateSearchWord} from "./redux/searchHeader/actions";
import {styles} from "./styles/AppointmentDetailsScreenStyles";

const time = [
	{label:"2020-07-02 14:00",value:"2020-07-02 14:00"},
	{label:"2020-07-03 15:00",value:"2020-07-03 15:00"},
	{label:"2020-07-06 16:00",value:"2020-07-06 16:00"},
]
const place = [
	{label:"荃灣總部",value:"1"},
	{label:"上環港島分店",value:"2"},
]

function AppointmentDetailsScreen(){
	const isDarkMode = useColorScheme() === "dark";
	const navigation = useNavigation();
	const route = useRoute<RouteProp<ParamList,"Detail">>();
	const treatments = useSelector((state:IRootState)=>state.availableAppointmentsPage.treatment);
	const sendTimeslotSuccess = useSelector((state:IRootState)=>state.availableAppointmentsPage.timeslotSuccess);
	const [timeId,setTimeId] = useState<string|null>(null);
	const [placeId,setPlaceId] = useState<string|null>(null);
	const dispatch = useDispatch();

	useEffect(() => {
		dispatch(updateSearchWord(""))
	},[dispatch])

	return(
		<View style={[styles.pageContainer,{backgroundColor:Colors.white}]}>
			<View style={styles.imageContainer}>
				<Image source={{uri:`${envfile.REACT_APP_IMAGE_PATH}/treatments/${treatments.filter
				(treatment=>treatment.id === route.params.treatmentId).map(treatment=>treatment.image)}`}}
				style={styles.image}/>
			</View>
			<Text style={styles.title}>{treatments.filter(treatment=>treatment.id === route.params.treatmentId)
			.map(treatment=>treatment.title)}</Text>
			<View style={styles.timeContainer}>
				<Text style={styles.timeText}>日期及時間</Text>
				<LongAppointmentPicker label={"選擇時間"} items={time} criteria={setTimeId}/>
			</View>
			<View style={styles.placeContainer}>
				<Text style={styles.timeText}>分店</Text>
				<LongAppointmentPicker label={"選擇地點"} items={place} criteria={setPlaceId}/>
			</View>
			<View style={styles.warningContainer}>
			{sendTimeslotSuccess === false && <Text style={styles.warning}>未填寫所有資料</Text>}
			</View>
			<TouchableOpacity style={styles.confirmButton} onPress={() => {
				{(!placeId || !timeId) && dispatch(sendTimeslotFail())}
				{placeId && timeId && dispatch(sendTimeslot(timeId,placeId,route.params.treatmentId,
				(time.filter(criteria=>criteria.value === timeId).map(criteria=>criteria.label))[0],
				(place.filter(criteria=>criteria.value === placeId).map(criteria=>criteria.label))[0]))}
				{timeId && placeId && navigation.navigate("FinishAppointmentScreen",{
					treatmentId:route.params.treatmentId,
					time:timeId,
					place:(place.filter(timeslot=>timeslot.value === placeId).map(timeslot=>timeslot.label))[0],
				})}
			}}>
				<Text style={styles.confirmText}>確認</Text>
			</TouchableOpacity>
			<TouchableOpacity style={styles.backButton} onPress={() => {
				navigation.navigate("AvailableAppointmentsScreen")
				dispatch(setTimeslotNull())
			}}>
				<Text style={styles.backText}>返回</Text>
			</TouchableOpacity>
		</View>
	)
}

export default AppointmentDetailsScreen;