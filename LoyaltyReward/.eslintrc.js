module.exports = {
    root: true,
    extends: ["prettier"],
	parser: "babel-eslint",
	"settings": {
		"import/parser":"babel-eslint",
	}
}
