import {createBottomTabNavigator} from "@react-navigation/bottom-tabs";
import {createStackNavigator} from "@react-navigation/stack";
import React, {useState} from "react";
import {TextInput, TouchableOpacity, View} from "react-native";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import AgentAppointmentDetailsScreen from "./src/AgentAppointmentDetailsScreen";
import AgentAvailableAppointmentsScreen from "./src/AgentAvailableAppointmentsScreen";
import AgentCallListScreen from "./src/AgentCallListScreen";
import AgentFinishAppointmentScreen from "./src/AgentFinishAppointmentScreen";
import AgentHomeScreen from "./src/AgentHomeScreen";
import searchHeader from "./src/components/SearchHeader";
import SearchScreen from "./src/SearchScreen";
import {styles} from "./src/styles/AgentStyles";
import MaterialCommunityIcon from "react-native-vector-icons/MaterialCommunityIcons";
import Ionicon from "react-native-vector-icons/Ionicons";
import AgentSalePerformanceScreen from "./src/AgentSalePerformanceScreen";
import {CustomerProfilePackage} from "./App";
import {updateSearchWord} from "./src/redux/searchHeader/actions";
import {FontAwesomeIcon} from "@fortawesome/react-native-fontawesome";
import {searchKeyword} from "./src/redux/searchHeader/thunks";
import {useNavigation} from "@react-navigation/native";
import {useDispatch, useSelector} from "react-redux";
import {IRootState} from "./store";
import FontAwesome5 from "react-native-vector-icons/FontAwesome5";

const AgentMainStack = createStackNavigator();
const AgentMainTab = createBottomTabNavigator();
const AgentHomeStack = createStackNavigator();
const AgentAvailableAppointmentsStack = createStackNavigator();
const AgentCallListStack = createStackNavigator();
const AgentSalePerformanceStack = createStackNavigator();

function AgentSalePerformancePackage(){
	// const search = false;
	return(
		<AgentSalePerformanceStack.Navigator initialRouteName="AgentCallListScreen" screenOptions={({
			headerStyle:{
				height:100, // Specify the height of your custom header
			},
			headerTitleAlign:"left",
			headerTitleStyle:{
				fontSize:24
			},
			headerRight:() => searchHeader(),
			title:"銷售業積"
		})}>
			<AgentSalePerformanceStack.Screen name="AgentSalePerformanceScreen" component={AgentSalePerformanceScreen}/>
		</AgentSalePerformanceStack.Navigator>
	)
}

function AgentCallListPackage(){
	// const search = false;
	return(
		<AgentCallListStack.Navigator initialRouteName="AgentCallListScreen" screenOptions={({
			headerStyle:{
				height:100, // Specify the height of your custom header
			},
			headerTitleAlign:"left",
			headerTitleStyle:{
				fontSize:24
			},
			headerRight:() => searchHeader(),
			title:"聯絡客戶"
		})}>
			<AgentCallListStack.Screen name="AgentCallListScreen" component={AgentCallListScreen}/>
		</AgentCallListStack.Navigator>
	)
}

function AgentAvailableAppointmentsPackage(){
	const [search,setSearch] = useState(false);
	const navigation = useNavigation();
	const dispatch = useDispatch();
	const searchWord = useSelector((state:IRootState)=>state.searchHeader.searchWord);
	const isAuthenticated = useSelector((state:IRootState)=>state.auth.isAuthenticated);
	const isAuthenticatedAgent = useSelector((state:IRootState)=>state.agentAuth.isAuthenticated);
	return(
		<AgentAvailableAppointmentsStack.Navigator initialRouteName="AgentAvailableAppointmentsScreen" screenOptions={({
			headerLeft:() => null,
			gestureEnabled:false
		})}>
			<AgentAvailableAppointmentsStack.Screen name="AgentAvailableAppointmentsScreen" component={AgentAvailableAppointmentsScreen}
				options={({
					headerStyle:{
						height:100, // Specify the height of your custom header
					},
					headerTitleAlign:"left",
					headerTitleStyle:{
						fontSize:24
					},
					headerRight:() => (
						<View style={styles.iconContainer}>
							{search && <View style={styles.search}>
								<TouchableOpacity onPress={() => {
									setSearch(false)
									dispatch(updateSearchWord(""))
								}}>
									<FontAwesomeIcon size={28} color={"#cccccc"} icon="times"/>
								</TouchableOpacity>
								<View style={styles.searchInputWidth}>
									<TextInput value={searchWord} style={styles.searchInput} onChangeText={(event)=>{
										dispatch(updateSearchWord(event))
									}}/>
								</View>
								<TouchableOpacity onPress={() => {
									setSearch(false)
									dispatch(searchKeyword(searchWord,isAuthenticated,isAuthenticatedAgent))
									navigation.navigate("SearchScreen")
								}}>
									<FontAwesome5 size={26} color={"#11bb00"} name="searchengin"/>
								</TouchableOpacity>
							</View>}
							{!search && <TouchableOpacity onPress={() => {
								setSearch(true)
							}}>
								<Ionicon style={styles.headerIcon} size={26} name="search-sharp"/>
							</TouchableOpacity>}
							<MaterialIcons size={28} name="notifications-none"/>
						</View>
					),
					title:"可推銷的預約"
				})}
			/>
			<AgentAvailableAppointmentsStack.Screen name="AgentAppointmentDetailsScreen" component={AgentAppointmentDetailsScreen}
				options={({
					headerStyle:{
						height:100, // Specify the height of your custom header
					},
					headerTitleStyle:{
						fontSize:20
					},
					headerRight:() => (
						<View style={styles.iconContainer}>
							<MaterialIcons size={28} name="notifications-none"/>
						</View>
					),
					title:"療程詳情",
				})}
			/>
		</AgentAvailableAppointmentsStack.Navigator>
	)
}

function AgentHomePackage(){
	// const [search,setSearch] = useState(false);
	return(
		<AgentHomeStack.Navigator initialRouteName="HomeScreen" screenOptions={({
			headerStyle:{
				height:100, // Specify the height of your custom header
			},
			headerTitleAlign:"left",
			headerTitleStyle:{
				fontSize:24
			},
			headerRight:() => searchHeader(),
			title:"中介人首頁"
		})}>
			<AgentHomeStack.Screen name="AgentHomeScreen" component={AgentHomeScreen}/>
		</AgentHomeStack.Navigator>
	)
}

function AgentMainMinorPackage(){
	return(
		<AgentMainTab.Navigator tabBarOptions={{
			activeTintColor:"#ff0000",
			inactiveTintColor:"#a6a6a6",
			// showLabel:false,
			// labelStyle:{margin:0,padding:0},
			// iconStyle:{height:24,width:24}
		}}
		screenOptions={({route}) => ({
			tabBarIcon:({focused,color,size}) => {
				let iconName;
				let materialIconName;
				if(route.name === "AgentHome"){
					iconName = focused ? "home" : "home-outline";
				}
				else if(route.name === "AgentAvailableAppointments"){
					iconName = focused ? "compass-sharp" : "compass-outline";
				}
				else if(route.name === "AgentCallList"){
					materialIconName = focused ? "phone" : "phone-outline";
				}
				else if(route.name === "AgentSalePerformance"){
					iconName = focused ? "analytics" : "analytics-outline";
				}
				else if(route.name === "CustomerProfile"){
					iconName = focused ? "settings" : "settings-outline";
				}
				// You can return any component that you like here
				return iconName ? <Ionicon name={iconName} size={28} color={color}/> :
				materialIconName &&
				<MaterialCommunityIcon name={materialIconName} size={28} color={color}/>
			}
		})}>
			<AgentMainTab.Screen name="AgentHome" component={AgentHomePackage} options={{title:"首頁"}}/>
			<AgentMainTab.Screen name="AgentAvailableAppointments" component={AgentAvailableAppointmentsPackage}
			options={{title:"療程"}}/>
			<AgentMainTab.Screen name="AgentCallList" component={AgentCallListPackage} options={{
				title:"聯絡客戶",
				// tabBarBadge:3,
			}}/>
			<AgentMainTab.Screen name="AgentSalePerformance" component={AgentSalePerformancePackage} options={{title:"業積"}}/>
			<AgentMainTab.Screen name="CustomerProfile" component={CustomerProfilePackage} options={{title:"設定"}}/>
		</AgentMainTab.Navigator>
	)
}

export function AgentMainPackage(){
	return(
		<AgentMainStack.Navigator screenOptions={{gestureEnabled:false}}>
			<AgentMainStack.Screen name="AgentMainMinor" component={AgentMainMinorPackage} options={{
				headerShown: false
			}}/>
			<AgentMainStack.Screen name="AgentFinishAppointmentScreen" component={AgentFinishAppointmentScreen} options={{
				headerShown: false
			}}/>
			<AgentMainStack.Screen name="SearchScreen" component={SearchScreen} options={({
				headerBackTitleVisible:false,
				headerStyle:{
					height:100, // Specify the height of your custom header
				},
				headerTitleStyle:{
					fontSize:20
				},
				headerRight:() => (
					<View style={styles.iconContainer}>
						<MaterialIcons size={28} name="notifications-none"/>
					</View>
				),
				title:"搜索結果",
			})}/>
		</AgentMainStack.Navigator>
	)
}