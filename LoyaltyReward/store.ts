import {applyMiddleware,combineReducers,compose,createStore} from "redux";
// import logger from "redux-logger";
import thunk from "redux-thunk";
import adminReducer, {IAdminState} from "./src/redux/admin/reducer";
import authAgentReducer,{IAgentAuthState} from "./src/redux/agentAuth/reducer";
import agentCallListReducer,{IAgentCallListState} from "./src/redux/agentCallList/reducer";
import agentHomeReducer,{IAgentHomeState} from "./src/redux/agentHome/reducer";
import agentSalePerformanceReducer,{IAgentSalePerformanceState} from "./src/redux/agentSalePerformance/reducer";
import socialAgentAuthReducer,{ISocialAgentAuthState} from "./src/redux/agentSocialAuth/reducer";
import agentAvailableTreatmentReducer,{IAgentTreatmentState} from "./src/redux/agentTreatment/reducer";
import agentUserReducer,{IAgentUserState} from "./src/redux/agentUser/reducer";
import authReducer,{IAuthState} from "./src/redux/auth/reducer";
import availableAppointmentsReducer,{IAvailableAppointmentsState} from "./src/redux/availableAppointments/reducer";
import customerProfileReducer,{ICustomerProfileState } from "./src/redux/customerProfile/reducer";
import homeReducer,{IHomeState} from "./src/redux/home/reducer";
import myAppointmentsReducer,{IMyAppointmentsState} from "./src/redux/myAppointments/reducer";
import pointReducer,{IPointState} from "./src/redux/point/reducer";
import redeemReducer,{IRedeemState} from "./src/redux/redeem/reducer";
import roleReducer,{IRoleState} from "./src/redux/role/reducer";
import {ISearchHeaderState, searchHeaderReducer} from "./src/redux/searchHeader/reducer";
import socialAuthReducer,{ISocialAuthState} from "./src/redux/socialAuth/reducer";
import treatmentPurchasesReducer,{ITreatmentPurchasesState} from "./src/redux/treatmentPurchases/reducer";
import userReducer,{IUserState} from "./src/redux/user/reducer";

// import { ThunkDispatch } from 'redux-thunk'

// export type IRootThunkDispatch = ThunkDispatch<IRootState, null, IRootAction>



/* tslint:disable:declare */
declare global{
    /* tslint:disable:interface-name */
    interface Window{
        __REDUX_DEVTOOLS_EXTENSION_COMPOSE__:any
    }
}

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export interface IRootState{
	auth:IAuthState;
	socialAuth:ISocialAuthState;
	agentAuth:IAgentAuthState;
	socialAgentAuth:ISocialAgentAuthState;
	user:IUserState;
	agentUser:IAgentUserState;
	role:IRoleState;
	homePage:IHomeState;
	myAppointmentsPage:IMyAppointmentsState;
	availableAppointmentsPage:IAvailableAppointmentsState;
	customerProfilePage:ICustomerProfileState;
	redeemPage:IRedeemState;
	pointPage:IPointState;
	searchHeader:ISearchHeaderState;
	agentHomePage:IAgentHomeState;
	agentTreatmentPage:IAgentTreatmentState;
	agentCallListPage:IAgentCallListState;
	adminPage:IAdminState;
	treatmentPurchasesPage:ITreatmentPurchasesState;
	agentSalePerformancePage:IAgentSalePerformanceState;
}

export const reducer = combineReducers<IRootState>({
	auth:authReducer,
	socialAuth:socialAuthReducer,
	agentAuth:authAgentReducer,
	socialAgentAuth:socialAgentAuthReducer,
	user:userReducer,
	agentUser:agentUserReducer,
	role:roleReducer,
	homePage:homeReducer,
	myAppointmentsPage:myAppointmentsReducer,
	availableAppointmentsPage:availableAppointmentsReducer,
	customerProfilePage:customerProfileReducer,
	redeemPage:redeemReducer,
	pointPage:pointReducer,
	searchHeader:searchHeaderReducer,
	agentHomePage:agentHomeReducer,
	agentTreatmentPage:agentAvailableTreatmentReducer,
	agentCallListPage:agentCallListReducer,
	adminPage:adminReducer,
	treatmentPurchasesPage:treatmentPurchasesReducer,
	agentSalePerformancePage:agentSalePerformanceReducer,
})

export const store = createStore(reducer,composeEnhancers(
	applyMiddleware(thunk),
	// applyMiddleware(logger)
))