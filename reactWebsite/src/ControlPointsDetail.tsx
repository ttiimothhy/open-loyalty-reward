import {useEffect,useState} from "react";
import {useDispatch,useSelector} from "react-redux";
import {useParams} from "react-router-dom";
import {Button, Input} from "reactstrap";
import "./ControlPointsDetail.scss"
import {AppointmentCommission,ReferralCommission,UserPoint} from "./models";
import {deleteAppointmentPoint, deleteCustomerPoint, deleteReferralPoint, getAllUsersData,getUserPointsDetail,
modifyAppointmentPoint,modifyCustomerPoint,modifyReferralPoint,updateAppointmentPointValue,updateAppointmentRemark,
updateCustomerRemark,updatePointValue,updateReferralPointValue,updateReferralRemark} from "./redux/point/actions";
import {IRootState} from "./store";

function CustomerPoint(props:{point:UserPoint,params:string}){
	const [edit,setEdit] = useState(false);
	const dispatch = useDispatch();
	// console.log(props.params)
	return(
		<div className="content-point-container">
			<div className="editor">
				{/* title */}
				<div className="treatment-content-title">{props.point.treatments_title}</div>
				<div className="button-container-end">
					{/* Edit button */}
					{edit ?
						<Button color="success" onClick={() => {
							setEdit(false)
							dispatch(modifyCustomerPoint(props.params,props.point))
						}}>完成修改</Button> :
						<Button color="warning" onClick={() => {
							setEdit(true)
						}}>編輯積分</Button>
					}
					<Button color="danger" className="delete-customer-point" onClick={() => {
						dispatch(deleteCustomerPoint(props.params,props.point))
					}}>刪除積分</Button>
				</div>
			</div>
			{/* guidance text */}
			<div className="content-new-description">分數</div>
			{/* output text */}
			{edit ?
				<Input value={props.point.points} onChange={(event) => {
					dispatch(updatePointValue(props.params,event.currentTarget.value))
				}}/> :
				<div className="treatment-detail">{props.point.points}</div>
			}
			{/* guidance text */}
			<div className="content-new-description">積分尚餘可用時間</div>
			{/* output text */}
			<div>{props.point.expired_date}</div>
			{/* guidance text */}
			<div className="content-new-description">額外資訊</div>
			{/* output text */}
			{edit? <Input value={props.point.remark} onChange={(event) => {
				dispatch(updateCustomerRemark(props.params,event.currentTarget.value))
			}}/> : <div>{props.point.remark}</div>}
		</div>
	)
}

function ReferralPoint(props:{point:ReferralCommission,params:string}){
	const [edit,setEdit] = useState(false);
	const dispatch = useDispatch();
	// console.log(props.params)
	return(
		<div className="content-point-container">
			<div className="editor">
				{/* title */}
				<div className="treatment-content-title"></div>
				<div className="button-container-end">
					{/* Edit button */}
					{edit ?
						<Button color="success" onClick={() => {
							setEdit(false)
							dispatch(modifyReferralPoint(props.params,props.point))
						}}>完成修改</Button> :
						<Button color="warning" onClick={() => {
							setEdit(true)
						}}>編輯積分</Button>
					}
					<Button color="danger" className="delete-customer-point" onClick={() => {
						dispatch(deleteReferralPoint(props.params,props.point))
					}}>刪除積分</Button>
				</div>
			</div>
			{/* guidance text */}
			<div className="content-new-description">分數</div>
			{/* output text */}
			{edit ?
				<Input value={props.point.points} onChange={(event) => {
					dispatch(updateReferralPointValue(props.params,event.currentTarget.value))
				}}/> :
				<div className="treatment-detail">{props.point.points}</div>
			}
			{/* guidance text */}
			<div className="content-new-description">額外資訊</div>
			{/* output text */}
			{edit? <Input value={props.point.remark} onChange={(event) => {
				dispatch(updateReferralRemark(props.params,event.currentTarget.value))
			}}/> : <div>{props.point.remark}</div>}
		</div>
	)
}

function AppointmentPoint(props:{point:AppointmentCommission,params:string}){
	const [edit,setEdit] = useState(false);
	const dispatch = useDispatch();
	// console.log(props.params)
	return(
		<div className="content-point-container">
			<div className="editor">
				{/* title */}
				<div className="treatment-content-title">{props.point.treatments_title}</div>
				<div className="button-container-end">
					{/* Edit button */}
					{edit ?
						<Button color="success" onClick={() => {
							setEdit(false)
							dispatch(modifyAppointmentPoint(props.params,props.point))
						}}>完成修改</Button> :
						<Button color="warning" onClick={() => {
							setEdit(true)
						}}>編輯積分</Button>
					}
					<Button color="danger" className="delete-customer-point" onClick={() => {
						dispatch(deleteAppointmentPoint(props.params,props.point))
					}}>刪除積分</Button>
				</div>
			</div>
			{/* guidance text */}
			<div className="content-new-description">分數</div>
			{/* output text */}
			{edit ?
				<Input value={props.point.points} onChange={(event) => {
					dispatch(updateAppointmentPointValue(props.params,event.currentTarget.value))
				}}/> :
				<div className="treatment-detail">{props.point.points}</div>
			}
			{/* guidance text */}
			<div className="content-new-description">額外資訊</div>
			{/* output text */}
			{edit? <Input value={props.point.remark} onChange={(event) => {
				dispatch(updateAppointmentRemark(props.params,event.currentTarget.value))
			}}/> : <div>{props.point.remark}</div>}
		</div>
	)
}

function ControlPointsDetail(){
	const userPointDetail = useSelector((state:IRootState)=>state.point.userPoint);
	const userData = useSelector((state:IRootState)=>state.point.users);
	const sidebar = useSelector((state:IRootState)=>state.sidebar.sidebarToggle);
	const params = useParams<{id:string}>()
	const dispatch = useDispatch()

	useEffect(() => {
		dispatch(getAllUsersData("1000",1))
	},[dispatch])

	useEffect(() => {
		dispatch(getUserPointsDetail(parseInt(params.id)))
	},[dispatch,params.id])

	return(
		<div className={sidebar ? "treatment-container-active" : "treatment-container"}>
			<div className="flex-name-display">
				<div className="treatment-flex-title-container-new">
					<div className="treatment-content-detail-find">用戶名</div>
					<div className="point-user-name">{userData.filter(user=>user.id === params.id).map(user=>user.username)}</div>
				</div>
				<div className="treatment-flex-title-container-new">
					<div className="treatment-content-detail-find">姓名</div>
					<div className="point-user-name">
						{userData.filter(user=>user.id === params.id).map(user=>user.display_name)}
					</div>
				</div>
			</div>
			<div className="treatment-content-detail-same">顧客積分</div>
			<div className="flex-treatment-container">
				{
					userPointDetail && userPointDetail.earned_point_user && userPointDetail.earned_point_user.length > 0 &&
					userPointDetail.earned_point_user.filter(point=>point.is_deleted === "false").map((customerPoint,index)=>{
						return(
							<CustomerPoint key={index} point={customerPoint} params={customerPoint.id}/>
						)
					})
				}
			</div>
			<div className="treatment-content-detail-no-width">中介人積分</div>
			<div className="treatment-content-detail-no-width-grey">推薦顧客所獲積分</div>
			<div className="flex-treatment-container">
				{
					userPointDetail && userPointDetail.referral_commission_agent &&
					userPointDetail.referral_commission_agent.length > 0 &&
					userPointDetail.referral_commission_agent.filter(point=>point.is_deleted === "false")
					.map((referralPoint,index)=>{
						return(
							<ReferralPoint key={index} point={referralPoint} params={referralPoint.id}/>
						)
					})
				}
			</div>
			<div className="treatment-content-detail-no-width-grey">推銷療程所獲積分</div>
			<div className="flex-treatment-container">
				{
					userPointDetail && userPointDetail.appointment_commission_agent &&
					userPointDetail.appointment_commission_agent.length > 0 &&
					userPointDetail.appointment_commission_agent.filter(point=>point.is_deleted === "false")
					.map((appointmentPoint,index)=>{
						return(
							<AppointmentPoint key={index} point={appointmentPoint} params={appointmentPoint.id}/>
						)
					})
				}
			</div>
		</div>
	)
}

export default ControlPointsDetail;