import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
// import { push } from "connected-react-router";
import {useEffect, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {Button} from "reactstrap";
import "./GetAllCustomer.scss";
import {CustomerData} from "./models";
import {getAllCustomersData} from "./redux/editUsers/actions";
import {IRootState} from "./store";

export const OneCustomerDataPart:React.FC<{key:number,id:string,referral_agent_id:string,date_of_birth:string,display_name:string,
email:string,mobile:string,referral_mobile:string,username:string,gender_id:string,
role_id:string}> = ({key,id,referral_agent_id,date_of_birth,display_name,email,mobile,
referral_mobile,username,gender_id,role_id}) =>
{
	return (
		<div key={key} className="content-treatment-container">
			<div className="content-customer-new-description">客戶id:</div>
			<div>{id}</div>
			<div className="content-customer-new-description">客戶的推薦中介人id:</div>
			<div>{referral_agent_id}</div>
			<div className="content-customer-new-description">客戶的出生日期:</div>
			<div>{date_of_birth}</div>
			<div className="content-customer-new-description">客戶的顯示名稱:</div>
			<div>{display_name}</div>
			<div className="content-customer-new-description">客戶的電郵:</div>
			<div>{email}</div>
			<div className="content-customer-new-description">客戶的手提號碼:</div>
			<div>{mobile}</div>
			<div className="content-customer-new-description">客戶的推薦人手提號碼:</div>
			<div>{referral_mobile}</div>
			<div className="content-customer-new-description">客戶的用戶名稱:</div>
			<div>{username}</div>
			<div className="content-customer-new-description">客戶的性別:</div>
			{gender_id === "1"? <div>男性</div>:<div>女性</div>}
			<div className="content-customer-new-description">客戶的類別:</div>
			{role_id === "1"? <div>客戶</div>:<div>客戶及中介</div>}
		</div>
	)
}

function GetAllCustomer(){
	const sidebar = useSelector((state:IRootState)=>state.sidebar.sidebarToggle);
	const allCustomerData = useSelector((state:IRootState)=>state.editUsers.customerData);
	const [displayAmount,setDisplayAmount] = useState("10");
	const [pageNumber,setPageNumber] = useState(1);
	const dispatch = useDispatch();

	useEffect(() => {
		dispatch(getAllCustomersData(displayAmount,pageNumber));
	},[dispatch,displayAmount,pageNumber])

	return(
		// sidebar status
		<div className={sidebar ? "treatment-container-active" : "treatment-container"}>
			{/* title */}
			<div className="treatment-title-container">
				<div className="treatment-title">所有客戶</div>
				{/* AllButtons */}
				<div className="top-button-container">
					<div className="select-page-another">
					{/* previousPage button */}
					<Button className="arrow-button" onClick={() => {
						pageNumber > 1 && setPageNumber(pageNumber - 1)
					}}><FontAwesomeIcon icon="arrow-left" className="arrow"/></Button>
					{/* nextPage button */}
					<Button className="arrow-right" onClick={() => {
						setPageNumber(pageNumber + 1)
					}}>
						<FontAwesomeIcon icon="arrow-right" className="arrow"/>
					</Button>
					</div>
					<div className="select-page">
						{/* change displayAmount */}
						<div>每頁顯示</div>
						<select className="selector" value={displayAmount} onChange={(event) => {
							setDisplayAmount(event.currentTarget.value)
						}}>
							<option value="6">6</option>
							<option value="10">10</option>
							<option value="15">15</option>
						</select>
						<div>項</div>

						{/* changePageNumber */}
						<div className="page-number">第{pageNumber}頁</div>
					</div>
				</div>
			</div>
			<div className="flex-treatment-container">
				{/* show all customers */}
				{allCustomerData.length > 0 && allCustomerData.map((oneCustomerDataPart:CustomerData,index:number) => {
					return(
						<OneCustomerDataPart key={index} id={oneCustomerDataPart.id}
						referral_agent_id={oneCustomerDataPart.referral_agent_id}
						date_of_birth={oneCustomerDataPart.date_of_birth} display_name={oneCustomerDataPart.display_name}
						email={oneCustomerDataPart.email} mobile={oneCustomerDataPart.mobile}
						referral_mobile={oneCustomerDataPart.referral_mobile}
						username={oneCustomerDataPart.username} gender_id={oneCustomerDataPart.gender_id}
						role_id={oneCustomerDataPart.role_id} />
					)
				})}

			</div>
		</div>
	)
}

export default GetAllCustomer;