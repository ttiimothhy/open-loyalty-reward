export interface CustomerData{
    id:string;
    referral_agent_id:string;
    date_of_birth:string;
    email:string;
    mobile:string;
    referral_mobile:string;
    username:string;
    gender_id:string;
    role_id:string;
    is_deleted:string;
    display_name:string;
    password:string;
}
export type AllUserData = Omit<CustomerData , "is_deleted" | "password">;
export interface AgentData{
    employed_data:string;
    is_deleted:string;
    each_referral_commission:string;
    level:string;
    each_appointment_commission:string;
}
export interface UserData{
    agent:[
        {
            employed_data:string,
            is_deleted:string,
            each_referral_commission:string,
            level:string,
            id:string,
            each_appointment_commission:string
        }
    ],
    user:[
        {
            referral_mobile:string,
            role_id:string,
            date_of_birth:string,
            mobile:string,
            referral_agent_id:string,
            id:string,
            display_name:string,
            gender_id:string,
            email:string,
            username:string,
        }
    ]
}
export interface UpdateCustomerDataResult{
    success:boolean|null;
}
export interface CreateAgentResult{
    success:boolean|null;
}
export interface UpdateAgentDataResult{
    success:boolean|null;
}
//define the type of the state
export interface Treatment{
	id:string;
	image:string;
    purchase_benefit_condition:string;
    purchase_benefit_amount:string;
    default_score:string
    is_hidden:string;
	title:string;
	detail:string;
}
interface TreatmentGender{
	gender_id:string;
	gender:string;
}
interface TreatmentService{
	service_id:string;
	service:string;
}
interface TreatmentBranch{
	branch_id:string;
	name:string;
}
export interface TreatmentDetails{
	treatment:Treatment;
	gender:TreatmentGender[];
	service:TreatmentService[];
	branch:TreatmentBranch[];
}
export interface TypeCriteria{
	id:string;
	service:string;
}
export interface PlaceCriteria{
	id:string;
	name:string;
}
export interface RewardCriteria{
	id:string;
	type:string;
}
export interface AdvertisementDetail{
    image:string;
    advertisements_title:string;
    is_hidden:string;
    id:string;
    detail:string;
    treatments_title:string;
    treatment_id:string;
}
export interface AdvertisementSimple{
    image:string;
    is_hidden:string;
    title:string;
}
export interface Reward{
	id:string;
	image:string;
    is_hidden:string;
    name:string;
    detail:string;
    remain_amount:string;
    redeem_points:string;
	type?:string;
}
export interface PickUpLocation{
	pickup_location_id:string;
	name:string;
}
export interface RewardDetails{
	reward:Reward;
	pickup_location:PickUpLocation[]
}
export interface AppointmentCommission{
	id:string;
	is_deleted:string;
	remark:string;
	treatments_title:string;
	points:string;
}
export interface UserPoint{
	id:string;
	is_deleted:string;
	remark:string;
	treatments_title:string;
	points:string;
	expired_date:string;
}
export interface ReferralCommission{
	id:string;
	is_deleted:string;
	remark:string;
	points:string;
}
export interface PointDetail{
	appointment_commission_agent:AppointmentCommission[];
	earned_point_user:UserPoint[];
	referral_commission_agent:ReferralCommission[];
}