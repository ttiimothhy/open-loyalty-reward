import {push} from "connected-react-router";
import {useEffect,useState} from "react";
import {useDispatch,useSelector} from "react-redux";
import {Button} from "reactstrap";
import {getAllTreatments} from "./redux/treatment/actions";
import {IRootState} from "./store";
import "./Treatment.scss";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";

function Treatment(){
	const sidebar = useSelector((state:IRootState)=>state.sidebar.sidebarToggle);
	const treatments = useSelector((state:IRootState)=>state.treatment.treatments);
	const [selectPage,setSelectPage] = useState("30");
	const [pageNumber,setPageNumber] = useState(1)
	const dispatch = useDispatch();
	// console.log(treatments);

	useEffect(() => {
		dispatch(getAllTreatments(selectPage,pageNumber))
	},[dispatch,selectPage,pageNumber])

	return(
		// sidebar status
		<div className={sidebar ? "treatment-container-active" : "treatment-container"}>
			{/* title */}
			<div className="treatment-title-container">
				<div className="treatment-title">所有療程</div>
				{/* AllButtons */}
				<div className="top-button-container">
					<div className="select-page-another">
					{/* createTreatment button */}
					<Button color="secondary" className="add-button" onClick={() => {
						dispatch(push("/create/treatment"))
					}}>新增療程</Button>
					{/* previousPage button */}
					<Button className="arrow-button" onClick={() => {
						pageNumber > 1 && setPageNumber(pageNumber - 1)
					}}><FontAwesomeIcon icon="arrow-left" className="arrow"/></Button>
					{/* nextPage button */}
					<Button className="arrow-right" onClick={() => {
						setPageNumber(pageNumber + 1)
					}}><FontAwesomeIcon icon="arrow-right" className="arrow"/></Button>
					</div>
					<div className="select-page">
						{/* change displayAmount */}
						<div>每頁顯示</div>
						<select className="selector" value={selectPage} onChange={(event) => {
							setSelectPage(event.currentTarget.value)
						}}>
							<option value="30">30</option>
							<option value="50">50</option>
							<option value="100">100</option>
							<option value="300">300</option>
						</select>
						<div>項</div>
						{/* changePageNumber */}
						<div className="page-number">第{pageNumber}頁</div>
					</div>
				</div>
			</div>
			<div className="flex-treatment-container">
			{/* show all treatments */}
			{treatments.filter(treatment=>treatment.is_hidden === "false").map((treatment,index)=>{
				return(
					// background image and content container
					<div key={index} className="content-treatment-container"
					style={{backgroundImage:`url(${process.env.REACT_APP_IMAGE_PATH}/treatments/${treatment.image})`}}>
						<div className="editor">
							{/* title */}
							<div className="treatment-content-title">{treatment.title}</div>

							{/* Edit button */}
							<Button color="warning" onClick={() => {
								dispatch(push(`/treatment/${treatment.id}`))
							}}>編輯療程</Button>
						</div>
						{/* guidance text */}
						<div className="content-new-description">療程內容</div>
						{/* output text */}
						<div className="treatment-detail">{treatment.detail}</div>
						{/* guidance text */}
						<div className="content-new-description">療程優惠</div>
						{/* output text */}
						<div>買{treatment.purchase_benefit_condition}送{treatment.purchase_benefit_amount}</div>
						{/* guidance text */}
						<div className="content-new-description">療程提供積分</div>
						{/* output text */}
						<div>{treatment.default_score}</div>
					</div>
				)
			})}
			</div>
		</div>
	)
}

export default Treatment;