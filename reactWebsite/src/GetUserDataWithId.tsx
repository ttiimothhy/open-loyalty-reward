import {useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {Button, Input} from "reactstrap";
import "./GetUserDataWithId.scss";
import {OneCustomerDataPart} from "./GetAllCustomer";
// import { IEditUsersState } from "./models";
import {getUserDataWithId} from "./redux/editUsers/actions";
import {push} from "connected-react-router";
import {IRootState} from "./store";

export const OneAgentDataPart: React.FC<{
    employed_data: string;
    is_deleted: string;
    each_referral_commission: string;
    level: string;
    id: string;
    each_appointment_commission: string;
}> = ({employed_data, is_deleted, each_referral_commission, level, id, each_appointment_commission}) => {
    return (
        <div className="content-treatment-container">
            <div className="content-customer-new-description">中介入職時間:</div>
            <div>{employed_data}</div>
            <div className="content-customer-new-description">中介身份已鏟除:</div>
            {is_deleted === "true"?<div>是</div>:<div>否</div>}
            <div className="content-customer-ew-description">每個推薦可得佣金:</div>
            <div>{each_referral_commission}</div>
            <div className="content-customer-new-description">中介等級:</div>
            <div>{level}</div>
            <div className="content-customer-new-description">中介id:</div>
            <div>{id}</div>
            <div className="content-customer-new-description">每個療程銷售可得佣金:</div>
            <div>{each_appointment_commission}</div>
        </div>
    )
}

function GetUserDataWithId() {
    const sidebar = useSelector((state: IRootState) => state.sidebar.sidebarToggle);
    const [isOutput, setIsOutput] = useState(false);
    const [userIdEnquired, setUserIdEnquired] = useState("0");
    const userData = useSelector((state: IRootState) => state.editUsers.userData);
    const dispatch = useDispatch();

    return (
        // sidebar status
        <div className={sidebar ? "treatment-container-active" : "treatment-container"}>
            {/* title */}
            <div className="treatment-title-container">
				<div>
					<div className="treatment-title">用戶資料查詢</div>
					{/* input content */}
					{isOutput === false && (
						<div className="flex-advertisement-detail-content">
							<div className="treatment-title-long">你想查詢的用戶id:</div>
							<Input
								className="advertisement-detail-title-fix"
								value={userIdEnquired}
								onChange={(event) => {
									setUserIdEnquired(event.currentTarget.value);
								}}></Input>
						</div>
                	)}

				</div>
				{isOutput === true && (
					<div>
						<Button color="success" className="find-button" onClick={() => {
							dispatch(push("/editUser"));
							setIsOutput(false);
						}}>
							返回編輯用戶頁
						</Button>
					</div>
				)}
                {/* input buttons */}
                {isOutput === false && (
                    <div>
						<Button color="success" className="find-button" onClick={() => {
							dispatch(getUserDataWithId(userIdEnquired));
							// console.log(userData);
							setIsOutput(true);
						}}>
							確認查詢
						</Button>
						<Button color="success" className="find-button" onClick={() => {
							dispatch(push("/editUser"));
							setIsOutput(false);
						}}>
							返回編輯用戶頁
						</Button>
                    </div>
                )}
			</div>
			{/* output content */}
			{isOutput === true && (
				<div>
					<div className="treatment-title-container">
						<div className="treatment-title">中介身份資料</div>
					</div>
					{userData && userData.agent && (
						<OneAgentDataPart employed_data={userData.agent[0].employed_data}
						is_deleted={userData.agent[0].is_deleted}
						each_referral_commission={userData.agent[0].each_referral_commission} level={userData.agent[0].level}
						id={userData.agent[0].id} each_appointment_commission={userData.agent[0].each_appointment_commission}
						/>
					)}

						<div className="treatment-title-container">
							<div className="treatment-title">客戶身份資料</div>
						</div>

					<OneCustomerDataPart key={1} id={userData.user[0].id} referral_agent_id={userData.user[0].referral_agent_id}
					date_of_birth={userData.user[0].date_of_birth} display_name={userData.user[0].display_name}
					email={userData.user[0].email} mobile={userData.user[0].mobile}
					referral_mobile={userData.user[0].referral_mobile} username={userData.user[0].username}
					gender_id={userData.user[0].gender_id} role_id={userData.user[0].role_id}/>
				</div>
			)}
			{/* output buttons */}
        </div>
    )
}

export default GetUserDataWithId;