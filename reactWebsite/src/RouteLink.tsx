import React,{useEffect} from 'react';
import {useDispatch,useSelector} from 'react-redux';
import {Switch} from 'react-router';
import './RouteLink.scss';
import {IRootState} from './store';
import Navbar from './Navbar';
import {checkLogin} from './redux/auth/actions';
import {Redirect, Route} from "react-router-dom";
import Login from "./Login";
import Home from "./Home";
import Treatment from "./Treatment";
import TreatmentDetail from "./TreatmentDetail";
import EditUser from './EditUser';
import GetAllCustomer from './GetAllCustomer';
import GetUserDataWithId from './GetUserDataWithId';
import UpdateCustomerData from './UpdateCustomerData';
import CreateAgent from './CreateAgent';
import UpdateAgent from './UpdateAgent';
import {library} from "@fortawesome/fontawesome-svg-core"
import {fab} from "@fortawesome/free-brands-svg-icons"
import {faArrowLeft,faArrowRight,fas} from "@fortawesome/free-solid-svg-icons"
import Reward from "./Reward";
import Advertisement from './Advertisement';
import RewardDetail from "./RewardDetail";
import AdvertisementDetail from './AdvertisementDetail';
import ControlPoints from "./ControlPoints";
import ControlPointsDetail from "./ControlPointsDetail";
import ResetPassword from './ResetPassword';
// import PointTypes from "./PointTypes";

library.add(fab,fas,faArrowLeft,faArrowRight)

function RouteLink() {
	const isAuthenticated = useSelector((state:IRootState) => state.auth.isAuthenticated);
	const dispatch = useDispatch();
	// const menu = useSelector((state: RootState) => state.menu.menu)
	// const [Menu, setMenu] = useState<boolean>(menu)
	useEffect(() => {
		dispatch(checkLogin());
	},[dispatch]);

	// console.log(process.env.REACT_APP_BACKEND_ROUTE)

	return (
		<div className="App">
			{isAuthenticated === null &&
				<Switch>
					<Route exact path="/"><Login/></Route>
				</Switch>
			}
			{isAuthenticated === false &&
				<Switch>
					<Route exact path="/"><Login/></Route>
					<Route path="/"><Redirect to="/"></Redirect></Route>
				</Switch>
			}
			{isAuthenticated &&
			<div>
				<div className="app-header-bar">
					<Navbar/>
				</div>
				<div className="app-content" >
					<Switch>
						{/* <Route exact path="/main">
							<Main/>
							<BottomBar/>
						</Route> */}
						<Route exact path="/editUser"><EditUser/></Route>
						<Route exact path="/getAllCustomer"><GetAllCustomer/></Route>
						<Route exact path="/getUserDataWithId"><GetUserDataWithId/></Route>
						<Route exact path="/updateCustomerData"><UpdateCustomerData/></Route>
						<Route exact path="/createAgent"><CreateAgent/></Route>
						<Route exact path="/updateAgent"><UpdateAgent/></Route>
						<Route exact path="/treatment/:id"><TreatmentDetail/></Route>
						<Route exact path="/treatment"><Treatment/></Route>
						<Route exact path="/create/treatment"><TreatmentDetail/></Route>
						<Route exact path="/reward/:id"><RewardDetail/></Route>
						<Route exact path="/reward"><Reward/></Route>
						<Route exact path="/create/reward"><RewardDetail/></Route>
						<Route exact path="/advertisement/:id"><AdvertisementDetail/></Route>
						<Route exact path="/advertisement"><Advertisement/></Route>
						<Route exact path="/create/advertisement"><AdvertisementDetail/></Route>
						<Route exact path="/point/:id"><ControlPointsDetail/></Route>
						<Route exact path="/point"><ControlPoints/></Route>
						<Route exact path="/reset/password"><ResetPassword/></Route>
						<Route exact path="/users/resetPassword/:uuid"><ResetPassword/></Route>
						{/* <Route exact path="/detailBoard"><DetailBoard /></Route> */}
						{/* <Route exact path="/publishQuest"><PublishQuest /></Route> */}
						{/* <Route exact path="/questBoard"><QuestBoard /></Route> */}
						{/* <Route exact path="/questBoard/:id"><QuestBoardId /></Route> */}
						{/* <Route exact path="/chatroom"><ChatRoom /></Route> */}
						{/* <Route exact path="/chatroom/:id"><PrivateChatRoom /></Route> */}
						{/* <Route exact path="/finishedQuest/:id"><Finish /></Route> */}
						{/* <Route exact path="/completed/:id"><Completed /></Route> */}
						{/* <Route exact path="/CropPhoto"><CropPhoto /></Route> */}
						{/* <Route path="/"><FrontPage /></Route> */}
						<Route exact path="/"><Home/></Route>
					</Switch>
				</div>
			</div>
			}
		</div>
	)
}

export default RouteLink;

