import React,{useEffect} from "react";
import * as FaIcons from "react-icons/fa"
import * as AiIcons from "react-icons/ai"
import * as GiIcons from "react-icons/gi"
import * as MdIcons from "react-icons/md"
import * as BiIcons from "react-icons/bi"
// import * as FaIcons from "react-icons/fa"
import * as IoLogOutOutline from 'react-icons/io5'
import {NavLink} from "react-router-dom";
import './Navbar.scss';
import {IconContext} from 'react-icons';
import {useDispatch,useSelector} from 'react-redux';
// import {fetchUserData} from "./redux/userData/actions";
import {IRootState} from "./store";
import {logout, resetAuthenticatedNull} from "./redux/auth/actions";
import {toggleSidebar} from "./redux/sidebar/actions";

const sidebarData = [
    {title:'編輯客戶',
	path:'/editUser',
	icon: <AiIcons.AiOutlineUserSwitch/>},
    {title:'編輯療程',
	path:'/treatment',
	icon: <MdIcons.MdExposure/>},
    {title:'編輯禮物',
	path:'/reward',
	icon: <GiIcons.GiOpenTreasureChest/>},
    {title:'編輯廣告',
	path:'/advertisement',
	icon: <FaIcons.FaTripadvisor/>},
    {title:'輸入績分',
	path:'/point',
	icon: <GiIcons.GiSettingsKnobs/>},
	{title:'重設密碼',
	path:'/reset/password',
	icon: <BiIcons.BiReset/>},
	{title:'回到首頁',
	path:'/',
	icon: <FaIcons.FaHome/>},
]

function Navbar() {
	const userData = useSelector((state:IRootState) => state.userData.userData);
	// const userId = useSelector((state:IRootState) => state.auth.userId);
	// const newUserDate = userData.filter(c => c.id === userId)
	// console.log(userId)
	const sidebar = useSelector((state:IRootState)=>state.sidebar.sidebarToggle)
	// const [sidebar,setSidebar] = useState(false);
	// const showSidebar =

	const dispatch = useDispatch();
	useEffect(() => {
		// dispatch(fetchUserData());
	},[dispatch]);

	for(let c of userData){
		if (!c.display_name) {
			c.display_name = `User ${c.id}`
		}
	}

	return(
		<div className="outside">
			<IconContext.Provider value={{color:"#adadad",size:"30px"}}>
				<div className="navbar">
					<NavLink to="#" className="menu-bar">
						<FaIcons.FaBars onClick={() => {
							dispatch(toggleSidebar())
						}}/>
					</NavLink>
					<div className="new-container">
						<NavLink className="nav-link-container" to="/" onClick={() => {
							dispatch(logout())
							dispatch(resetAuthenticatedNull())
						}}>
							<IoLogOutOutline.IoLogOutOutline/>
							<span>登出</span>
						</NavLink>
					</div>
				</div>
				<nav className={sidebar ? "nav-menu active" : "nav-menu"}>
					<ul className="nav-menu-item">
						<li className="navbar-toggle">
							<NavLink to="#" className="menu-bars" onClick={() => {
								dispatch(toggleSidebar())
							}}>
								<AiIcons.AiOutlineClose/>
							</NavLink>
						</li>
						{sidebarData.map((item,index) => {
							return (
								<li key={index} className="nav-text">
									<NavLink to={item.path} onClick={() => dispatch(
										toggleSidebar()
									)}>
										{item.icon}
										<span>{item.title}</span>
									</NavLink>
								</li>
							)
						})}
					</ul>
				</nav>
			</IconContext.Provider>
		</div>
	)
}

export default Navbar;