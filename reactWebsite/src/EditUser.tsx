import {push} from "connected-react-router";
// import {useState} from "react";
import {useDispatch,useSelector} from "react-redux";
import {Button} from "reactstrap";
import "./EditUser.scss";
// import { getAllCustomersData } from "./redux/editUsers/actions";
import {IRootState} from "./store";


function EditUser(){
	const sidebar = useSelector((state:IRootState)=>state.sidebar.sidebarToggle);
	// const [pageNumber,setPageNumber] = useState(1);
	const dispatch = useDispatch();

	return(
		<div className={sidebar ? "user-container-active" : "user-container"}>
			<Button color="#ffffff" className="content-width-container" onClick={() =>{
				// dispatch(getAllCustomersData(pageNumber));
				dispatch(push("/getAllCustomer"));
			}}>
				<div className="small-content-title">查詢所有客戶資料</div>
			</Button>
			<Button  color="#ffffff" className="content-width-container" onClick={() =>{
				dispatch(push("/getUserDataWithId"));
			}}>
				<div className="small-content-title">查詢單一用戶資料</div>
			</Button>
			<Button  color="#ffffff" className="content-width-container" onClick={() =>{
				dispatch(push("/updateCustomerData"));
			}}>
				<div className="small-content-title">更新單一客戶資料</div>
			</Button>
			<Button  color="#ffffff" className="content-width-container" onClick={() =>{
				dispatch(push("/createAgent"));
			}}>
				<div className="small-content-title">創建新中介人</div>
			</Button>
			<Button  color="#ffffff" className="content-width-container" onClick={() =>{
				dispatch(push("/updateAgent"));
			}}>
				<div className="small-content-title">更新單一中介人資料</div>
			</Button>
		</div>
	)
}

export default EditUser;