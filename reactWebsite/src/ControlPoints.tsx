import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {push} from "connected-react-router";
import {useEffect,useState} from "react";
import React,{useDispatch, useSelector} from "react-redux";
import {Button,Input} from "reactstrap";
import {getAllUsersData,resetCreateToNull,updateAgentAppointmentPoint,updateAgentReferralPoint,updateCustomerPoint} from "./redux/point/actions";
import {IRootState} from "./store";
import "./ControlPoints.scss"
// import {push} from "connected-react-router";

function CustomerPoints(props:{userId:number}){
	const createCustomerPoint = useSelector((state:IRootState)=>state.point.customerCreate)
	const [customerPoint,setCustomerPoint] = useState("");
	const [remark,setRemark] = useState("");
	// const [actionType,setActionType] = useState("1");
	const dispatch = useDispatch();

	return(
		<div className="right-container">
			<div className="flex-add-point-display">
				<div className="flex-add-point-display">
					<div className="point-add-title">分數</div>
					<Input className="short-input-detail-title" value={customerPoint} onChange={(event) => {
						setCustomerPoint(event.currentTarget.value)
						dispatch(resetCreateToNull())
					}}/>
				</div>
				<div className="flex-add-point-display">
					<div className="point-add-title">注項</div>
					<Input className="short-input-detail-title" value={remark} onChange={(event) => {
						setRemark(event.currentTarget.value)
						dispatch(resetCreateToNull())
					}}/>
				</div>
			</div>
			<div className="flex-add-point-display">
				{/* <select value={actionType} className="advertisement-detail-title-top" onChange={(event) => {
					setActionType(event.currentTarget.value)
				}}>
					<option value="1">新增積分</option>
					<option value="2">修改積分</option>
				</select> */}
				<Button className="point-submit-button" onClick={() => {
					dispatch(updateCustomerPoint(props.userId,customerPoint,remark))
					setCustomerPoint("")
					setRemark("")
				}}>新增</Button>
			</div>
			{createCustomerPoint[props.userId] && <div className="text-success bold-size">新增成功</div>}
			{createCustomerPoint[props.userId] === false && <div className="text-danger bold-size">新增失敗</div>}
		</div>
	)
}

function AgentReferralPoints(props:{userId:number}){
	const agentReferralCreate = useSelector((state:IRootState)=>state.point.agentReferralCreate)
	const [agentReferralPoint,setAgentReferralPoint] = useState("");
	const [referralRemark,setReferralRemark] = useState("");
	// const [actionType,setActionType] = useState("1");
	const dispatch = useDispatch();
	return(
		<div>
			<div className="flex-add-point-display">
				<div className="flex-add-point-display">
					<div className="point-add-title">推銷顧客分數</div>
					<Input className="short-input-detail-title" value={agentReferralPoint} onChange={(event) => {
						setAgentReferralPoint(event.currentTarget.value)
						dispatch(resetCreateToNull())
					}}/>
				</div>
				<div className="flex-add-point-display">
					<div className="point-add-title">注項</div>
					<Input className="short-input-detail-title" value={referralRemark} onChange={(event) => {
						setReferralRemark(event.currentTarget.value)
						dispatch(resetCreateToNull())
					}}/>
				</div>
			</div>
			<div className="flex-add-point-display">
				{/* <select value={actionType} className="advertisement-detail-title-top" onChange={(event) => {
					setActionType(event.currentTarget.value)
				}}>
					<option value="1">新增中介人積分</option>
					<option value="2">修改中介人積分</option>
				</select> */}
				<Button className="point-submit-button" onClick={() => {
					dispatch(updateAgentReferralPoint(props.userId,agentReferralPoint,referralRemark))
					setAgentReferralPoint("")
					setReferralRemark("")
				}}>新增</Button>

			</div>
			{agentReferralCreate[props.userId] && <div className="text-success bold-size">新增成功</div>}
			{agentReferralCreate[props.userId] === false && <div className="text-danger bold-size">新增失敗</div>}
		</div>
	)
}

function AgentAppointmentPoints(props:{userId:number}){
	const agentAppointmentCreate = useSelector((state:IRootState)=>state.point.agentAppointmentCreate)
	const [agentAppointmentPoint,setAgentAppointmentPoint] = useState("");
	const [appointmentRemark,setAppointmentRemark] = useState("");
	// const [actionType,setActionType] = useState("1");
	const dispatch = useDispatch();
	return(
		<div>
			<div className="flex-add-point-display">
				<div className="flex-add-point-display">
					<div className="point-add-title">推銷療程分數</div>
					<Input className="short-input-detail-title" value={agentAppointmentPoint} onChange={(event) => {
						setAgentAppointmentPoint(event.currentTarget.value)
						dispatch(resetCreateToNull())
					}}/>
				</div>
				<div className="flex-add-point-display">
					<div className="point-add-title">注項</div>
					<Input className="short-input-detail-title" value={appointmentRemark} onChange={(event) => {
						setAppointmentRemark(event.currentTarget.value)
						dispatch(resetCreateToNull())
					}}/>
				</div>
			</div>
			<div className="flex-add-point-display">
				{/* <select value={actionType} className="advertisement-detail-title-top" onChange={(event) => {
					setActionType(event.currentTarget.value)
				}}>
					<option value="1">新增中介人積分</option>
					<option value="2">修改中介人積分</option>
				</select> */}
				<Button className="point-submit-button" onClick={() => {
					dispatch(updateAgentAppointmentPoint(props.userId,agentAppointmentPoint,appointmentRemark))
					setAgentAppointmentPoint("");
					setAppointmentRemark("")
				}}>新增</Button>
			</div>
			{agentAppointmentCreate[props.userId] && <div className="text-success bold-size">新增成功</div>}
			{agentAppointmentCreate[props.userId] === false && <div className="text-danger bold-size">新增失敗</div>}
		</div>
	)

}

function ControlPoints(){
	const sidebar = useSelector((state:IRootState)=>state.sidebar.sidebarToggle);
	const [selectPage,setSelectPage] = useState("6");
	const [pageNumber,setPageNumber] = useState(1);
	const userData = useSelector((state:IRootState)=>state.point.users);
	const dispatch = useDispatch();

	useEffect(() => {
		dispatch(getAllUsersData(selectPage,pageNumber))
	},[dispatch,selectPage,pageNumber])

	useEffect(() => {
		dispatch(resetCreateToNull())
	},[dispatch])

	return(
		<div className={sidebar ? "treatment-container-active" : "treatment-container"}>
			<div className="treatment-flex-title-container">
				<div className="blank-space"></div>
				<div className="treatment-title-container-with-standard-width">
					<div className="treatment-point-flex">
						<div className="point-title">顧客積分</div>
						<div className="point-title">中介人積分</div>
					</div>
					{/* <div className="treatment-title">所有禮物</div> */}
					<div className="top-button-container">
						<div>
							<div className="select-page">
								<div className="width-define">每頁顯示</div>
								<select className="selector" value={selectPage} onChange={(event) => {
									setSelectPage(event.currentTarget.value)
								}}>
									<option value="1">1</option>
									<option value="3">3</option>
									<option value="6">6</option>
									<option value="10">10</option>
									<option value="20">20</option>
								</select>
								<div>項</div>
								<div className="page-number-width">第{pageNumber}頁</div>
							</div>
							<div className="select-page-another-top">
								<Button className="arrow-button" onClick={() => {
									pageNumber > 1 && setPageNumber(pageNumber - 1)
								}}><FontAwesomeIcon icon="arrow-left" className="arrow"/></Button>
								<Button className="arrow-right" onClick={() => {
									setPageNumber(pageNumber + 1)
								}}><FontAwesomeIcon icon="arrow-right" className="arrow"/></Button>
							</div>
						</div>
					</div>
				</div>
			</div>
			{userData.map((user,index)=>
			(<div className="flex-user-display" key={index}>
				<div className="border-line">
					<div className="flex-name-display">
						<div className="title-name">編號</div>
						<div>{user.id}</div>
					</div>
					<div className="flex-name-display">
						<div className="title-name">用戶名</div>
						<div>{user.username}</div>
					</div>
					<div className="flex-name-display">
						<div className="title-name">姓名</div>
						<div>{user.display_name}</div>
					</div>
					<Button color="success" className="detail-button" onClick={() => {
						dispatch(push(`/point/${user.id}`))
					}}>查詢分數詳細資料</Button>
				</div>
				<CustomerPoints userId={parseInt(user.id)}/>
				<div>
					<AgentReferralPoints userId={parseInt(user.id)}/>
					<AgentAppointmentPoints userId={parseInt(user.id)}/>
				</div>
			</div>)
			)}
		</div>
	)
}

export default ControlPoints;