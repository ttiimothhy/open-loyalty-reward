import {useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {Button, Input} from "reactstrap";
import "./UpdateAgent.scss";
import {updateAgentData} from "./redux/editUsers/actions";
import {push} from "connected-react-router";
import {IRootState} from "./store";

function UpdateAgent(){
	const sidebar = useSelector((state:IRootState)=>state.sidebar.sidebarToggle);
	const [isOutput,setIsOutput] = useState(false);
	const dispatch = useDispatch();
	const [userId,setUserId] = useState("");
	const [employed_data,setEmployed_data] = useState("");
	const [each_referral_commission,setEach_referral_commission] = useState("");
	const [is_deleted,setIs_deleted] = useState("");
	const [level,setLevel]  = useState("");
	const [each_appointment_commission,setEach_appointment_commission]  = useState("");

	return(
		<div className={sidebar ? "home-active" : "home"}>
			{/* input version content */}
			{isOutput === false &&
			<div>
				<div className="treatment-title-container">
					<div className="treatment-title">更新中介人資料</div>
				</div>
				<div className="flex-advertisement-detail-content">
					<div className="advertisement-input-content-detail">用戶id:</div>
					<Input className="advertisement-input-detail-title" value={userId} onChange={(event) =>{
						setUserId(event.currentTarget.value);
					}}>
					</Input>
				</div>
				<div className="flex-advertisement-detail-content">
					<div className="advertisement-input-content-detail">中介入職時間:</div>
					<Input className="advertisement-input-detail-title" value={employed_data} onChange={(event) =>{
						setEmployed_data(event.currentTarget.value);
					}}>
					</Input>
				</div>
				<div className="flex-advertisement-detail-content">
					<div className="advertisement-input-content-detail">每個推薦可得佣金:</div>
					<Input className="advertisement-input-detail-title" value={each_referral_commission} onChange={(event) =>{
							setEach_referral_commission(event.currentTarget.value);
						}}>
					</Input>
				</div>
				<div className="flex-advertisement-detail-content">
					<div className="advertisement-input-content-detail">中介身份已刪除:</div>
					<select className="treatment-detail-title" value={is_deleted}
					onChange={(event) => {
						setIs_deleted(event.currentTarget.value)
					}}>
						<option value=""></option>
						<option value="true">是</option>
						<option value="false">否</option>
					</select>
				</div>
				<div className="flex-advertisement-detail-content">
					<div className="advertisement-input-content-detail">中介等級:</div>
					<Input className="advertisement-input-detail-title" value={level} onChange={(event) =>{
							setLevel(event.currentTarget.value);
						}}>
					</Input>
				</div>
				<div className="flex-advertisement-detail-content">
					<div className="advertisement-input-content-detail">每個療程銷售可得佣金:</div>
					<Input className="advertisement-input-detail-title" value={each_appointment_commission} onChange={(event) =>{
						setEach_appointment_commission(event.currentTarget.value);
					}}/>
				</div>
			</div>}
			{/* input version buttons */}
			{isOutput === false && (
				<div>
					<Button color="danger" className="change-button" onClick={() =>{
						dispatch(updateAgentData(userId,employed_data,is_deleted,each_referral_commission,level,
						each_appointment_commission));
						setIsOutput(true);
					}}>確認更新</Button>
					<Button color="danger" className="change-button" onClick={() =>{
						dispatch(push("/editUser"));
						setIsOutput(false);
					}}>返回編輯用戶頁</Button>
			</div>)}
			{/* output version content */}
			{isOutput === true &&
				<div className="content-treatment-container">
					<div className="treatment-title-container">
						<div className="treatment-title">更新的中介人資料</div>
					</div>
					<div className="content-new-description">中介入職時間:</div>
					<div className="treatment-detail">{employed_data}</div>
					<div className="content-new-description">推薦中介人id:</div>
					<div className="treatment-detail">{is_deleted}</div>
					<div className="content-new-description">每個推薦可得佣金:</div>
					<div className="treatment-detail">{each_referral_commission}</div>
					<div className="content-new-description">中介等級:</div>
					<div className="treatment-detail">{level}</div>
					<div className="content-new-description">每個療程銷售可得佣金:</div>
					<div className="treatment-detail">{each_appointment_commission}</div>
				</div>}
			{/* output version button */}
			{isOutput === true && <div className="AllButtonsContainer">
				<div className="EachButtonContainer">
					<Button color="info" sonClick={() =>{
						dispatch(push("/editUser"));
						setIsOutput(false);
					}}>返回編輯用戶頁</Button>
				</div>
			</div>}
		</div>
	)
}

export default UpdateAgent;