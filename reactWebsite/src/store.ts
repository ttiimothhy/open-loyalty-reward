import {CallHistoryMethodAction,connectRouter,routerMiddleware,RouterState} from "connected-react-router";
import {createBrowserHistory} from "history";
import {applyMiddleware,combineReducers,compose,createStore} from "redux";
import logger from "redux-logger";
import thunk from "redux-thunk";
import { advertisementReducer, IAdvertisementState } from "./redux/advertisement/reducer";
// import {IEditUsersState} from "./models";
import {authReducer,IAuthState} from "./redux/auth/reducer";
import {editUsersReducer, IEditUsersState} from "./redux/editUsers/reducer";
import {IPointState, pointReducer} from "./redux/point/reducer";
import resetPasswordReducer, { IResetPasswordState } from "./redux/resetPassword/reducer";
import {IRewardsState, rewardsReducer} from "./redux/rewards/reducer";
import {ISidebarState,sidebarReducer} from "./redux/sidebar/reducer";
import {ITreatmentState, treatmentReducer} from "./redux/treatment/reducer";
import {IUserDataState,userDataReducer} from "./redux/userData/reducer";

declare global {
    /* tslint:disable:interface-name */
    interface Window {
        __REDUX_DEVTOOLS_EXTENSION_COMPOSE__: any
    }
}

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export const history = createBrowserHistory();
export interface IRootState{
	router:RouterState;
	userData:IUserDataState;
	auth:IAuthState;
	sidebar:ISidebarState;
	editUsers:IEditUsersState;
	treatment:ITreatmentState;
	rewards:IRewardsState;
	advertisement:IAdvertisementState;
	point:IPointState;
	resetPassword:IResetPasswordState;
}

export const reducer = combineReducers<IRootState|CallHistoryMethodAction>({
	router:connectRouter(history),
	userData:userDataReducer,
	auth:authReducer,
	sidebar:sidebarReducer,
	editUsers:editUsersReducer,
	treatment:treatmentReducer,
	rewards:rewardsReducer,
	advertisement:advertisementReducer,
	point:pointReducer,
	resetPassword:resetPasswordReducer,
})

export const store = createStore(reducer,composeEnhancers(
	applyMiddleware(thunk),
	applyMiddleware(routerMiddleware(history)),
	applyMiddleware(logger)
))