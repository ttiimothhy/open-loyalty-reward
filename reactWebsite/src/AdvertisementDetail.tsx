import React,{useEffect,useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {useParams} from "react-router-dom";
import {Button,Input} from "reactstrap";
import {IRootState} from "./store";
import "./AdvertisementDetail.scss"
import {createAdvertisement,getAdvertisementWithId,resetCreateNull,resetUpdateNull,updateAdvertisement,
updateAdvertisementDetail,updateAdvertisementTitle,updateAdvTreatmentsId,updateHidden}
from "./redux/advertisement/actions";
import {getAllTreatments} from "./redux/treatment/actions";

function upload(){
	document.getElementById("file-upload")?.click();
}

// let purchaseAmount:number[] = []
// for(let i = 1; i <= 20; i++){
// 	purchaseAmount.push(i)
// }

function AdvertisementDetail(){
	const sidebar = useSelector((state:IRootState)=>state.sidebar.sidebarToggle);
	const params = useParams<{id:string}>();
	const advertisement = useSelector((state:IRootState)=>state.advertisement.advertisementDetail);
	const [photo,setPhoto] = useState<File|null>(null)
	const [preview,setPreview] = useState<string|undefined>();
	const [selectedFile,setSelectedFile] = useState();
	const updateSuccess = useSelector((state:IRootState)=>state.advertisement.updateAdvResult)
	// const deleteSuccess = useSelector((state:IRootState)=>state.advertisement.delete)
	const createSuccess = useSelector((state:IRootState)=>state.advertisement.createAdvResult)
	const dispatch = useDispatch();
	// console.log(advertisement)

	useEffect(() => {
		dispatch(getAdvertisementWithId(parseInt(params.id)))
		// dispatch(typeCriteria())
		// dispatch(placeCriteria())
	},[dispatch,params.id])

	useEffect(() => {
		dispatch(getAllTreatments("1000",1))
	},[dispatch])

    useEffect(() => {
        if (!selectedFile) {
            setPreview(undefined)
            return
        }
        const objectUrl = URL.createObjectURL(selectedFile)
        setPreview(objectUrl)

        // free memory when ever this component is unmounted
        return () => URL.revokeObjectURL(objectUrl)
    }, [selectedFile])

	useEffect(() => {
		dispatch(resetUpdateNull())
		// dispatch(resetDeleteNull())
		dispatch(resetCreateNull())
	},[dispatch])

	return(
		<div className={sidebar ? "advertisement-container-active" : "treatment-container"}>
			{/* change advertisement name */}
			{/* text input */}
			<div className="flex-advertisement-detail-content">
				<div className="advertisement-content-detail">廣告名稱</div>
				<Input className="advertisement-detail-title"
				value={advertisement.advertisements_title}
				onChange={(event) => {
					dispatch(updateAdvertisementTitle(event.currentTarget.value))
				}}/>
			</div>
			{/* change advertisement detail */}
			{/* text input */}
			<div className="advertisement-content-detail">廣告內容</div>
			<Input className="advertisement-detail-description" value={advertisement?.detail}
			onChange={(event) => {
				dispatch(updateAdvertisementDetail(event.currentTarget.value))
			}}/>
			{/* change advertisement image */}
			{/* image input */}
			<div className="advertisement-content-detail-no-width">廣告相關相片</div>
			<div className="image-container">
			<div className="advertisement-image-container">
				<img src={selectedFile ? preview : `${process.env.REACT_APP_IMAGE_PATH}/advertisements/${advertisement?.image}`}
				alt="" className="advertisement-image"/>
			</div>
			<Button onClick={() => {upload()}}>更改圖片</Button>
			<input id="file-upload" type="file" hidden onChange={(event:any) => {
				let files = event.currentTarget.files
				// console.log(files)
				if(files){
					setPhoto(files[0])
					setSelectedFile(files[0])
				}
				// previewImage(event)
			}}></input>
			</div>
			{/* change advertisement treatments_title */}
			{/* text input */}
			<div className="flex-advertisement-detail-content">
				<div className="advertisement-content-detail-no-width">廣告的療程號數</div>
				<div className="small-input-container">
					<Input className="advertisement-detail-title" value={advertisement?.treatment_id}
					onChange={(event) => {
						dispatch(updateAdvTreatmentsId(event.currentTarget.value))
					}}/>
				</div>
			</div>
			<div className="flex-advertisement-detail-content">
				<div className="advertisement-content-detail-no-width">隱藏廣告</div>
				<select className="advertisement-detail-title" value={advertisement?.is_hidden}
				onChange={(event) => {
					dispatch(updateHidden(event.currentTarget.value))
				}}>
					<option value=""></option>
					<option value="true">是</option>
					<option value="false">否</option>
				</select>
			</div>
			{
				params.id &&
				<div>

					{/* update button */}
					<Button color="success" className="change-button" onClick={() => {
						dispatch(updateAdvertisement(parseInt(params.id),photo,advertisement.is_hidden,
						advertisement.advertisements_title,advertisement.treatment_id))
					}}>更改廣告</Button>
					{updateSuccess ? <div className="notification">更改成功</div> : updateSuccess === false &&
					<div className="warning">更改失敗</div>}
				</div>
			}
			{!params.id &&
				<div>
					<Button color="success" className="change-button" onClick={() => {
						dispatch(createAdvertisement(photo,advertisement.is_hidden,advertisement.advertisements_title,
						advertisement.treatment_id))
					}}>新增廣告</Button>
					{createSuccess ? <div className="notification">新增成功</div> : createSuccess === false &&
					<div className="warning">未填寫所有資料/未選擇是否顯示/系統故障</div>}
				</div>
			}
		</div>
		// <div></div>
	)
}

export default AdvertisementDetail;