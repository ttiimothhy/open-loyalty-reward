import {ISidebarActions} from "./actions";

export interface ISidebarState{
	sidebarToggle:boolean
}

const initialState = {
	sidebarToggle:false
}

export const sidebarReducer = (state:ISidebarState = initialState,action:ISidebarActions):ISidebarState => {
	switch(action.type){
		case "@@sidebar/Toggle_sidebar":
			return{
				...state,
				sidebarToggle:!state.sidebarToggle
			}
		default:
			return state
	}
}