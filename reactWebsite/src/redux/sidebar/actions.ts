export function toggleSidebar(){
	return{
		type:"@@sidebar/Toggle_sidebar" as const,
	}
}

export type ISidebarActions = ReturnType<typeof toggleSidebar>