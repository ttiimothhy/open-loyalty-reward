import { IResetPasswordActions } from "./actions"

export interface IResetPasswordState{
    isRequested:boolean|null;
	isReset:boolean|null;
    message:string;
}

const initialState = {
    isRequested:null,
    isReset:null,
    message:""
}

const resetPasswordReducer = (state: IResetPasswordState = initialState,action:IResetPasswordActions):IResetPasswordState => {
    switch(action.type){
        case "@@resetPassword/Request_reset_link_success": 
            return{
                ...state,
                isRequested:true
            }
        case "@@resetPassword/Request_reset_link_failure": 
            return{
                ...state,
                isRequested:false
            }
        case "@@resetPassword/Reset_isRequest_null":
            return{
                ...state,
                isRequested:null
            }
		case "@@resetPassword/Reset_password_success":
			return{
				...state,
				isReset:true
			}
		case "@@resetPassword/Reset_password_failure":
			return{
				...state,
				isReset:false,
				message:action.message
			}
		case "@@resetPassword/Reset_null":
			return{
				...state,
				isReset:null
			}
    default:
        return state
    }
}

export default resetPasswordReducer;