// import {CallHistoryMethodAction,push} from "connected-react-router";
import {Dispatch} from "redux";
import { CreateAgentResult, CustomerData, UpdateAgentDataResult, UpdateCustomerDataResult, UserData } from "../../models"
// import { UserData } from "./reducer";

export function getAllCustomersData(displayAmount:string,pageNumber:number) {
    return async (dispatch:Dispatch<IEditUsersActions>) => {
        const token = localStorage.getItem("token")
		// const token = localStorage.getItem("token.0k  ") //keep ".0k"???
		const res = await fetch(`${process.env.REACT_APP_BACKEND_ROUTE}/admin/edit/user?display=${displayAmount}&page=${pageNumber}`,{
			headers:{
				Authorization:`Bearer ${token}`
			}
		})
		const result = await res.json();
		// console.log(result)
		if(!result.message){
			dispatch(getAllCustomersDataSuccess(result))
		}else{
			dispatch(getAllCustomersDataFail())
			dispatch(getAllCustomersDataFailure(result.message));
		}
    }
}

export function getUserDataWithId(userId:string) {
    return async (dispatch:Dispatch<IEditUsersActions>) => {
		console.log(userId)
        const token = localStorage.getItem("token")
		// const token = localStorage.getItem("token.0k  ") //keep ".0k"???
		const res = await fetch(`${process.env.REACT_APP_BACKEND_ROUTE}/admin/edit/user/${userId}`,{
			headers:{
				Authorization:`Bearer ${token}`
			}
		})
		const result = await res.json();
		// console.log(result)
		if(!result.message){
			// if(result.length > 0){
				// dynamic typing
				// dispatch(getUserDataWithIdSuccess(result[0]));
				dispatch(getUserDataWithIdSuccess(result));
				// }
		}else{
			dispatch(getUserDataWithIdFailure(result.message));
		}
    }
}

export function updateCustomerData(userId:string,password:string,referral_agent_id:string,date_of_birth:string,display_name:string,email:string,is_deleted:string,mobile:string,referral_mobile:string,username:string,gender_id:string,role_id:string){
    return async (dispatch:Dispatch<IEditUsersActions>) => {
        const token = localStorage.getItem("token")
		let birthday = date_of_birth + " 00:00:00"
		// const token = localStorage.getItem("token.0k  ") //keep ".0k"???
		const res = await fetch(`${process.env.REACT_APP_BACKEND_ROUTE}/admin/edit/user/${userId}`,{
            method:"post",
			headers:{
				Authorization:`Bearer ${token}`,
                "Content-Type":"application/json"
			},
            body:JSON.stringify({password,referral_agent_id,date_of_birth:birthday,display_name,email,
			is_deleted,mobile,referral_mobile,username,gender_id,role_id})
		})
		const result = await res.json();
		// console.log(result)
		if(!result.message){
			dispatch(updateCustomerDataSuccess(result))
		}else{
			dispatch(updateCustomerDataFailure(result.message));
		}
    }
}

export function createAgent(userId:string,employed_data:string,is_deleted:string,each_referral_commission:string,level:string,each_appointment_commission:string){
    return async (dispatch:Dispatch<IEditUsersActions>) => {
        const token = localStorage.getItem("token")
		// const token = localStorage.getItem("token.0k  ") //keep ".0k"???
		const res = await fetch(`${process.env.REACT_APP_BACKEND_ROUTE}/admin/edit/create/agent/${userId}`,{
            method:"post",
			headers:{
				Authorization:`Bearer ${token}`,
                "Content-Type":"application/json"
			},
            body:JSON.stringify({employed_data,is_deleted,each_referral_commission,level,each_appointment_commission})
		})
		const result = await res.json();
		// console.log(result)
		if(!result.message){
			dispatch(createAgentSuccess(result))
		}else{
			dispatch(createAgentFailure(result.message));
		}
	}
}

export function updateAgentData(userId:string,employed_data:string,is_deleted:string,each_referral_commission:string,level:string,each_appointment_commission:string){
    return async (dispatch:Dispatch<IEditUsersActions>) => {
        const token = localStorage.getItem("token")
		// const token = localStorage.getItem("token.0k  ") //keep ".0k"???
		const res = await fetch(`${process.env.REACT_APP_BACKEND_ROUTE}/admin/edit/update/agent/${userId}`,{
            method:"post",
			headers:{
				Authorization:`Bearer ${token}`,
                "Content-Type":"application/json"
			},
            body:JSON.stringify({employed_data,is_deleted,each_referral_commission,level,each_appointment_commission})
		})
		const result = await res.json();
		// console.log(result)
		if(!result.message){
			dispatch(updateAgentDataSuccess(result))
		}else{
			dispatch(updateAgentDataFailure(result.message));
		}
	}
}

// export function updateReferral_agent_id(referral_agent_id:string){
// 	return{
// 		type:"@@user/Update_referral_agent_id" as const,
// 		referral_agent_id
// 	}
// }

function getAllCustomersDataSuccess(customerData:CustomerData[]){
    return{
		type:"@@user/Get_all_users" as const,
        customerData
	}
}

function getAllCustomersDataFailure(message:string){
    return{
		type:"@@user/Get_all_users_failure" as const,
		message
	}
}

function createAgentSuccess(createAgentResult:CreateAgentResult){
    return{
		type:"@@user/Create_agent" as const,
        createAgentResult
	}
}

function createAgentFailure(message:string){
    return{
		type:"@@user/Create_agent_failure" as const,
		message
	}
}

function updateAgentDataSuccess(updateAgentDataResult:UpdateAgentDataResult){
    return{
		type:"@@user/Update_agent_data" as const,
        updateAgentDataResult
	}
}

function updateAgentDataFailure(message:string){
    return{
		type:"@@user/Update_agent_data_failure" as const,
		message
	}
}


function updateCustomerDataSuccess(updateCustomerDataResult:UpdateCustomerDataResult){
    return{
		type:"@@user/Update_user_data" as const, // response {"success": true"} if success
        updateCustomerDataResult
	}
}

function updateCustomerDataFailure(message:string){
    return{
		type:"@@user/Update_user_data_failure" as const,
		message
	}
}

function getUserDataWithIdSuccess(userData:UserData){
    return{
		type:"@@user/Get_all_user_data_with_id" as const,
        userData
	}
}

function getUserDataWithIdFailure(message:string){
    return{
		type:"@@user/Get_all_user_data_with_id_failure" as const,
		message
	}
}

function getAllCustomersDataFail(){
	return{
		type:"@@user/Get_all_customers_data_fail" as const,
	}
}

// Multiple failed actions can be grouped together to use one failed action which shows message globally

export type IEditUsersActions = ReturnType<typeof getAllCustomersDataSuccess|typeof getAllCustomersDataFailure|
typeof getUserDataWithIdSuccess|typeof getUserDataWithIdFailure|typeof updateCustomerDataSuccess|typeof updateCustomerDataFailure|
typeof createAgentSuccess|typeof createAgentFailure|typeof updateAgentDataSuccess|
typeof getAllCustomersDataFail|typeof updateAgentDataFailure>
