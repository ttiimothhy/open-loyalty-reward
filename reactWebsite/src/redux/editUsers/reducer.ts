
import { AgentData, CreateAgentResult, CustomerData, UpdateAgentDataResult, UpdateCustomerDataResult } from "../../models"
import {IEditUsersActions} from "./actions"

export interface IEditUsersState{
    userData:{
        agent:[
            {
                employed_data:string,
                is_deleted:string,
                each_referral_commission:string,
                level:string,
                id:string,
                each_appointment_commission:string
            }
        ],
        user:[
            {
                referral_mobile:string,
                role_id:string,
                date_of_birth:string,
                mobile:string,
                referral_agent_id:string,
                id:string,
                display_name:string,
                gender_id:string,
                email:string,
                username:string
            }
		]

    },
    customerData:CustomerData[];
    agentData:AgentData[];
    updateCustomerDataResult:UpdateCustomerDataResult;
    createAgentResult:CreateAgentResult;
    updateAgentDataResult:UpdateAgentDataResult
}
//define initial state
const initialState:IEditUsersState = {
    userData:{
        agent:[
            {
                employed_data:"",
                is_deleted:"",
                each_referral_commission:"",
                level:"",
                id:"",
                each_appointment_commission:""
            }
        ],
        user:[
            {
                referral_mobile:"",
                role_id:"",
                date_of_birth:"",
                mobile:"",
                referral_agent_id:"",
                id:"",
                display_name:"",
                gender_id:"",
                email:"",
                username:"",
            }
        ]
    },
    customerData:[],
    agentData:[],
    updateCustomerDataResult:{
        success:null,
    },
    createAgentResult:{
        success:null,
    },
    updateAgentDataResult:{
        success:null,
    }
}

//define the reducer for editUsers
export const editUsersReducer = (state:IEditUsersState = initialState, action: IEditUsersActions):IEditUsersState => {
    switch(action.type){
        case "@@user/Get_all_users":
            return{
                ...state,
                customerData:action.customerData
			}
        case "@@user/Get_all_user_data_with_id":
            return{
                ...state,
                userData:action.userData
            }
        case "@@user/Update_user_data":
            return{
                ...state,
                updateCustomerDataResult:action.updateCustomerDataResult
            }
        case "@@user/Create_agent":
            return{
                ...state,
                createAgentResult:action.createAgentResult
            }
        case "@@user/Update_agent_data":
            return{
                ...state,
                updateAgentDataResult:action.updateAgentDataResult
            }
		case "@@user/Get_all_customers_data_fail":
			return{
				...state,
				customerData:[]
			}
    default:
            return state
    }
}