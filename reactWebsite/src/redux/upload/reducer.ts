import {IUploadActions} from "./actions";

export interface IUploadState{
	uploadSuccess:boolean|null
}

const initialState = {
	uploadSuccess:null
}

export const uploadReducer = (state:IUploadState = initialState,action:IUploadActions):IUploadState => {
	switch(action.type){
		case "@@upload/Upload_photo":
			return{
				...state,
				uploadSuccess:true
			}
		default:
			return state
	}
}