import {Dispatch} from "redux";

export function uploadPhoto(file:File|null){
	return async(dispatch:Dispatch<IUploadActions>) => {
		const data = new FormData();
		if(file){
			data.append("file",file);
		}
		data.append("title","test title");
		data.append("default_score","20");
		data.append("detail","test detail");
		data.append("is_hidden","false");
		data.append("purchase_benefit_condition","1");
		data.append("purchase_benefit_amount","1");
		data.append("gender","1");
		data.append("branch","1,2")
		data.append("service","1,2");
		const res = await fetch(`${process.env.REACT_APP_BACKEND_PATH}/admin/edit/treatment/create`,{
			method:"post",
			headers:{
				Authorization:`Bearer ${process.env.REACT_APP_JWT_TOKEN}`
			},
			body:data
		})
		const result = await res.json();
		if(result.success){
			dispatch(uploadPhotoSuccess())
		}else{
			dispatch(failed("Upload_photo_failed",result.message))
		}
	}
}

function uploadPhotoSuccess(){
	return{
		type:"@@upload/Upload_photo" as const,
	}
}

type Failed = "Upload_photo_failed";
function failed(type:Failed,message:string){
	return{
		type,
		message
	}
}

export type IUploadActions = ReturnType<typeof uploadPhotoSuccess|typeof failed>