import {IUserDataActions} from "./actions"
export interface UserData{
    id:number;
    username:string;
    password:string;
    email:string;
    currency:number;
    rank:number|string;
    taking_quest:number;
    created_at:string;
    photo:string;
    achievement:string;
    display_name:string;
    self_intro:string;
}
export interface IUserDataState{
    userData:UserData[]
}
const initialState:IUserDataState = {
    userData:[]
}
export const userDataReducer = (state:IUserDataState = initialState, action: IUserDataActions):IUserDataState => {
    switch(action.type){
        case "@@userData/Load_user_data":
            return{
                ...state,
                userData:action.userData
            }
        default:
            return state
    }
}