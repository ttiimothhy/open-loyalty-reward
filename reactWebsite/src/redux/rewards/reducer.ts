import {PlaceCriteria,Reward,RewardCriteria,RewardDetails} from "../../models";
import {IRewardsActions} from "./actions";

export interface IRewardsState{
	rewards:Reward[];
	rewardDetail:RewardDetails;
	rewardType:RewardCriteria[];
	pickUpCriteria:PlaceCriteria[];
	update:boolean|null;
	delete:boolean|null;
	create:boolean|null;
}

const initialState = {
	rewards:[],
	rewardDetail:{
		reward:{
			id:"",
			image:"",
			is_hidden:"",
			name:"",
			detail:"",
			remain_amount:"",
			redeem_points:"",
			type:"",
		},
		pickup_location:[],
	},
	rewardType:[],
	pickUpCriteria:[],
	update:null,
	delete:null,
	create:null,
}

export const rewardsReducer = (state:IRewardsState = initialState,action:IRewardsActions):IRewardsState => {
	switch(action.type){
		case "@@rewards/Get_all_rewards":
			return{
				...state,
				rewards:action.reward
			}
		case "@@rewards/Get_all_rewards_fail":
			return{
				...state,
				rewards:[]
			}
		case "@@rewards/Get_reward_with_id":
			let rewardTypeId = state.rewardType.filter(type=>type.type === action.reward.reward.type).map(reward=>reward.id)
			let newRewardDetail = {...state.rewardDetail,
				reward:{
					...action.reward.reward,
					type:rewardTypeId[0]
				},
				pickup_location:action.reward.pickup_location
			}
			return{
				...state,
				rewardDetail:newRewardDetail
			}
		case "@@rewards/Update_reward_name":
			return{
				...state,
				rewardDetail:{...state.rewardDetail,
					reward:{
						...state.rewardDetail.reward,
						name:action.name
					}
				}
			}
		case "@@rewards/Update_reward_detail":
			return{
				...state,
				rewardDetail:{...state.rewardDetail,
					reward:{
						...state.rewardDetail.reward,
						detail:action.detail
					}
				}
			}
			case "@@rewards/Get_reward_type":
				return{
					...state,
					rewardType:action.rewardType
				}
			case "@@rewards/Update_reward_type":
				// console.log(action.rewardType)
				return{
					...state,
					rewardDetail:{...state.rewardDetail,
						reward:{
							...state.rewardDetail.reward,
							type:action.rewardType
						}
					}
				}
			case "@@rewards/Get_pick_up_location":
				return{
					...state,
					pickUpCriteria:action.criteria
				}
			case "@@rewards/Update_pick_up_location":
				let containsThatPickUpLocation = state.rewardDetail.pickup_location.filter(location=>location.pickup_location_id === action.id)
				let newPickUpLocation = state.rewardDetail.pickup_location.slice();
				if(containsThatPickUpLocation.length > 0){
					newPickUpLocation = state.rewardDetail.pickup_location.filter(location=>location.pickup_location_id !== action.id)
				}else{
					newPickUpLocation.push({pickup_location_id:action.id,name:action.criteria})
				}
				return{
					...state,
					rewardDetail:{...state.rewardDetail,
						pickup_location:newPickUpLocation
					}
				}
			case "@@rewards/Update_reward_redeem_point":
				return{
					...state,
					rewardDetail:{...state.rewardDetail,
						reward:{
							...state.rewardDetail.reward,
							redeem_points:action.point
						}
					}
				}
			case "@@rewards/Update_reward_remaining_amount":
				return{
					...state,
					rewardDetail:{...state.rewardDetail,
						reward:{
							...state.rewardDetail.reward,
							remain_amount:action.remaining
						}
					}
				}
			case "@@rewards/Update_reward_hidden":
				return{
					...state,
					rewardDetail:{...state.rewardDetail,
						reward:{
							...state.rewardDetail.reward,
							is_hidden:action.hidden
						}
					}
				}
			case "@@rewards/Update_reward":
				return{
					...state,
					update:true
				}
			case "@@rewards/Update_reward_fail":
				return{
					...state,
					update:false
				}
			// case "@@treatment/Change_gender":
			// 	let containsThatGender = state.treatmentDetail.gender.filter(gender=>gender.gender_id === action.id);
			// 	let newGender = state.treatmentDetail.gender.slice();
			// 	// console.log(containsThatGender)
			// 	// console.log(newGender)
			// 	// console.log(action.id)
			// 	if(containsThatGender.length > 0){
			// 		newGender = state.treatmentDetail.gender.filter(gender=>gender.gender_id !== action.id)
			// 		// console.log(newService)
			// 	}else if(action.id === "1"){
			// 		newGender.push({gender:"Male",gender_id:action.id})
			// 	}else if(action.id === "2"){
			// 		newGender.push({gender:"Female",gender_id:action.id})
			// 	}
			// 	return{
			// 		...state,
			// 		treatmentDetail:{...state.treatmentDetail,
			// 			gender:newGender
			// 		}
			// 	}
			case "@@rewards/Reset_update_null":
				return{
					...state,
					update:null
				}
			case "@@rewards/Delete_reward":
				return{
					...state,
					delete:true
				}
			case "@@rewards/Delete_reward_fail":
				return{
					...state,
					delete:false
				}
			case "@@rewards/Reset_delete_null":
				return{
					...state,
					delete:null
				}
			// case "@@treatment/Update_hidden":
			// 	return{
			// 		...state,
			// 		treatmentDetail:{...state.treatmentDetail,
			// 			treatment:{
			// 				...state.treatmentDetail.treatment,
			// 				is_hidden:action.value
			// 			}
			// 		}
			// 	}
			case "@@rewards/Create_reward":
				return{
					...state,
					create:true
				}
			case "@@rewards/Create_reward_fail":
				return{
					...state,
					create:false
				}
			case "@@rewards/Reset_create_null":
				return{
					...state,
					create:null
				}
		default:
			return state
	}
}