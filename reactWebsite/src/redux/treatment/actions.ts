import {CallHistoryMethodAction,push} from "connected-react-router";
import {Dispatch} from "redux";
import {PlaceCriteria,Treatment,TreatmentDetails,TypeCriteria} from "../../models";
import {IAdvertisementActions,updateAdvertisementTreatId} from "../advertisement/actions";

export function getAllTreatments(pageDisplay:string,pageNumber:number){
	return async(dispatch:Dispatch<ITreatmentActions|IAdvertisementActions>) => {
		const token = localStorage.getItem("token");
		// console.log(pageNumber)
		const res = await fetch(`${process.env.REACT_APP_BACKEND_ROUTE}/admin/edit/treatment?display=${pageDisplay}\
		&page=${pageNumber}`,{
			headers:{
				Authorization:`Bearer ${token}`
			}
		})
		const result = await res.json();
		if(!result.message){
			dispatch(getAllTreatmentsSuccess(result))
			dispatch(updateAdvertisementTreatId(result))
		}else{
			dispatch(getAllTreatmentsFailure())
			dispatch(failed("Get_all_treatments_failed",result.message))
		}
	}
}

export function getTreatmentWithId(id:number|undefined){
	return async(dispatch:Dispatch<ITreatmentActions>) => {
		const token = localStorage.getItem("token");
		if(id){
			const res = await fetch(`${process.env.REACT_APP_BACKEND_ROUTE}/admin/edit/treatment/${id}`,{
				headers:{
					Authorization:`Bearer ${token}`
				}
			})
			const result = await res.json();
			// console.log(result)
			if(!result.message){
				dispatch(getTreatmentWithIdSuccess(result))
			}else{
				dispatch(failed("Get_treatment_with_id_failed",result.message))
			}
		}else{
			const treatmentDetail = {
				treatment:{
					id:"",
					image:"",
					purchase_benefit_condition:"",
					purchase_benefit_amount:"",
					default_score:"",
					is_hidden:"",
					title:"",
					detail:"",
				},
				gender:[],
				service:[],
				branch:[],
			}
			dispatch(getTreatmentWithIdSuccess(treatmentDetail))
		}
	}
}

export function typeCriteria(){
	return async(dispatch:Dispatch<ITreatmentActions>) => {
		const token = localStorage.getItem("token");
		const res = await fetch(`${process.env.REACT_APP_BACKEND_ROUTE}/treatment/service`,{
			headers:{
				Authorization:`Bearer ${token}`
			}
		})
		const result = await res.json();
		// console.log(result)
		if(!result.message){
			dispatch(typeCriteriaSuccess(result))
		}else{
			dispatch(failed("Get_type_criteria_failed",result.message))
		}
	}
}

export function placeCriteria(){
	return async(dispatch:Dispatch<ITreatmentActions>) => {
		const token = localStorage.getItem("token");
		const res = await fetch(`${process.env.REACT_APP_BACKEND_ROUTE}/treatment/branch`,{
			headers:{
				Authorization:`Bearer ${token}`
			}
		})
		const result = await res.json();
		// console.log(result)
		if(!result.message){
			dispatch(placeCriteriaSuccess(result))
		}else{
			dispatch(failed("Get_place_criteria_failed",result.message))
		}
	}
}

export function updateTreatment(treatmentId:number,photo:File|null,treatment:TreatmentDetails){
	return async(dispatch:Dispatch<ITreatmentActions|CallHistoryMethodAction>) => {
		const token = localStorage.getItem("token");
		let updateFormDate = new FormData();
		if(photo){
			updateFormDate.append("file",photo);
		}
		updateFormDate.append("title",treatment.treatment.title);
		updateFormDate.append("default_score",treatment.treatment.default_score);
		updateFormDate.append("detail",treatment.treatment.detail);
		// updateFormDate.append("is_hidden",treatment.treatment.is_hidden);
		updateFormDate.append("purchase_benefit_condition",treatment.treatment.purchase_benefit_condition);
		updateFormDate.append("purchase_benefit_amount",treatment.treatment.purchase_benefit_amount);
		updateFormDate.append("gender",treatment.gender.map(gender=>parseInt(gender.gender_id)).toString());
		updateFormDate.append("branch",treatment.branch.map(place=>parseInt(place.branch_id)).toString());
		updateFormDate.append("service",treatment.branch.map(place=>parseInt(place.branch_id)).toString())

		const res = await fetch(`${process.env.REACT_APP_BACKEND_ROUTE}/admin/edit/treatment/update/${treatmentId}`,{
			method:"post",
			headers:{
				Authorization:`Bearer ${token}`
			},
			body:updateFormDate
		})
		const result = await res.json();
		// console.log(result)
		if(result.success){
			dispatch(updateTreatmentSuccess())
			setTimeout(() => {
				dispatch(push("/treatment"))
			},2000)
		}else{
			dispatch(updateTreatmentFailure())
			dispatch(failed("Update_treatment_failed",result.message))
		}
	}
}

export function deleteTreatment(treatmentId:number){
	return async(dispatch:Dispatch<ITreatmentActions|CallHistoryMethodAction>) => {
		const token = localStorage.getItem("token");
		let updateFormDate = new FormData();
		updateFormDate.append("is_hidden","true");
		const res = await fetch(`${process.env.REACT_APP_BACKEND_ROUTE}/admin/edit/treatment/update/${treatmentId}`,{
			method:"post",
			headers:{
				Authorization:`Bearer ${token}`,
				// "Content-type":"application/json"
			},
			body:updateFormDate
		})
		const result = await res.json();
		// console.log(result)
		if(result.success){
			dispatch(deleteTreatmentSuccess())
			setTimeout(() => {
				dispatch(push("/treatment"))
			},2000)
		}else{
			dispatch(deleteTreatmentFailure())
			dispatch(failed("Delete_treatment_failed",result.message))
		}
	}
}

export function createTreatment(photo:File|null,treatment:TreatmentDetails){
	return async(dispatch:Dispatch<ITreatmentActions|CallHistoryMethodAction>) => {
		const token = localStorage.getItem("token");
		let updateFormDate = new FormData();
		if(photo){
			updateFormDate.append("file",photo);
		}
		if(!treatment.treatment.title || !treatment.treatment.default_score || !treatment.treatment.detail ||
		!treatment.treatment.is_hidden || treatment.branch.map(place=>parseInt(place.branch_id)).length === 0 ||
		treatment.gender.map(gender=>parseInt(gender.gender_id)).length === 0 ||
		treatment.branch.map(place=>parseInt(place.branch_id)).length === 0){
			dispatch(createTreatmentFailure());
			return;
		}

		updateFormDate.append("title",treatment.treatment.title);
		updateFormDate.append("default_score",treatment.treatment.default_score);
		updateFormDate.append("detail",treatment.treatment.detail);
		updateFormDate.append("is_hidden",treatment.treatment.is_hidden);
		if(treatment.treatment.purchase_benefit_condition){
			updateFormDate.append("purchase_benefit_condition",treatment.treatment.purchase_benefit_condition);
		}
		if(treatment.treatment.purchase_benefit_amount){
			updateFormDate.append("purchase_benefit_amount",treatment.treatment.purchase_benefit_amount);
		}
		updateFormDate.append("gender",treatment.gender.map(gender=>parseInt(gender.gender_id)).toString());
		updateFormDate.append("branch",treatment.branch.map(place=>parseInt(place.branch_id)).toString());
		updateFormDate.append("service",treatment.branch.map(place=>parseInt(place.branch_id)).toString())

		const res = await fetch(`${process.env.REACT_APP_BACKEND_ROUTE}/admin/edit/treatment/create`,{
			method:"post",
			headers:{
				Authorization:`Bearer ${token}`
			},
			body:updateFormDate
		})
		const result = await res.json();
		// console.log(result)
		if(result.success){
			dispatch(createTreatmentSuccess())
			setTimeout(() => {
				dispatch(push("/treatment"))
			},2000)
		}else{
			dispatch(createTreatmentFailure())
			dispatch(failed("Create_treatment_failed",result.message))
		}
	}
}

function getAllTreatmentsSuccess(treatment:Treatment[]){
	return{
		type:"@@treatment/Get_all_treatments" as const,
		treatment
	}
}

function getAllTreatmentsFailure(){
	return{
		type:"@@treatment/Get_all_treatments_fail" as const,
	}
}

function getTreatmentWithIdSuccess(treatment:TreatmentDetails){
	return{
		type:"@@treatment/Get_treatment_with_id" as const,
		treatment
	}
}

export function updateTreatmentName(name:string){
	return{
		type:"@@treatment/Update_treatment_name" as const,
		name,
	}
}

export function updateTreatmentDetail(detail:string){
	return{
		type:"@@treatment/Update_treatment_detail" as const,
		detail
	}
}

export function updateTreatmentScore(score:string){
	return{
		type:"@@treatment/Update_treatment_score" as const,
		score
	}
}

export function updateTreatmentValue(value:string){
	return{
		type:"@@treatment/Update_treatment_value" as const,
		value
	}
}

export function updateTreatmentBenefit(value:string){
	return{
		type:"@@treatment/Update_treatment_benefit" as const,
		value
	}
}

export function typeCriteriaSuccess(criteria:TypeCriteria[]){
	return{
		type:"@@treatment/Get_type_criteria" as const,
		criteria
	}
}

export function placeCriteriaSuccess(criteria:PlaceCriteria[]){
	return{
		type:"@@treatment/Get_place_criteria" as const,
		criteria
	}
}

export function changeType(bodyType:string,id:string){
	return{
		type:"@@treatment/Change_type" as const,
		bodyType,
		id
	}
}

export function changePlace(place:string,id:string){
	return{
		type:"@@treatment/Change_place" as const,
		place,
		id
	}
}

export function updateTreatmentSuccess(){
	return{
		type:"@@treatment/Update_treatment" as const,
	}
}

export function updateTreatmentFailure(){
	return{
		type:"@@treatment/Update_treatment_fail" as const,
	}
}

export function resetUpdateNull(){
	return{
		type:"@@treatment/Reset_update_null" as const,
	}
}

export function changeGender(id:string){
	return{
		type:"@@treatment/Change_gender" as const,
		id
	}
}

export function deleteTreatmentSuccess(){
	return{
		type:"@@treatment/Delete_treatment" as const,
	}
}

export function deleteTreatmentFailure(){
	return{
		type:"@@treatment/Delete_treatment_fail" as const,
	}
}

export function resetDeleteNull(){
	return{
		type:"@@treatment/Reset_delete_null" as const,
	}
}

export function updateHidden(value:string){
	return{
		type:"@@treatment/Update_hidden" as const,
		value
	}
}

function createTreatmentSuccess(){
	return{
		type:"@@treatment/Create_treatment" as const,
	}
}

function createTreatmentFailure(){
	return{
		type:"@@treatment/Create_treatment_fail" as const,
	}
}

export function resetCreateNull(){
	return{
		type:"@@treatment/Reset_create_null" as const,
	}
}

type Failed = "Get_all_treatments_failed"|"Get_treatment_with_id_failed"|"Get_type_criteria_failed"|"Get_place_criteria_failed"|
"Update_treatment_failed"|"Delete_treatment_failed"|"Create_treatment_failed"
function failed(type:Failed,message:string){
	return{
		type,
		message
	}
}

export type ITreatmentActions = ReturnType<typeof getAllTreatmentsSuccess|typeof getTreatmentWithIdSuccess|
typeof updateTreatmentName|typeof updateTreatmentDetail|typeof updateTreatmentScore|typeof updateTreatmentValue|
typeof updateTreatmentBenefit|typeof typeCriteriaSuccess|typeof placeCriteriaSuccess|typeof changeType|
typeof changePlace|typeof updateTreatmentSuccess|typeof updateTreatmentFailure|typeof resetUpdateNull|
typeof changeGender|typeof deleteTreatmentSuccess|typeof deleteTreatmentFailure|typeof resetDeleteNull|
typeof updateHidden|typeof createTreatmentSuccess|typeof createTreatmentFailure|typeof resetCreateNull|
typeof getAllTreatmentsFailure|typeof failed>