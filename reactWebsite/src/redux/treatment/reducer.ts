import {PlaceCriteria,Treatment,TreatmentDetails,TypeCriteria} from "../../models";
import {ITreatmentActions} from "./actions";

export interface ITreatmentState{
	treatments:Treatment[];
	treatmentDetail:TreatmentDetails;
	typeCriteria:TypeCriteria[];
	placeCriteria:PlaceCriteria[];
	update:boolean|null;
	delete:boolean|null;
	create:boolean|null;
}

const initialState = {
	treatments:[],
	treatmentDetail:{
		treatment:{
			id:"",
			image:"",
			purchase_benefit_condition:"",
			purchase_benefit_amount:"",
			default_score:"",
			is_hidden:"",
			title:"",
			detail:"",
		},
		gender:[],
		service:[],
		branch:[],
	},
	typeCriteria:[],
	placeCriteria:[],
	update:null,
	delete:null,
	create:null,
}

export const treatmentReducer = (state:ITreatmentState = initialState,action:ITreatmentActions):ITreatmentState => {
	switch(action.type){
		case "@@treatment/Get_all_treatments":
			return{
				...state,
				treatments:action.treatment
			}
		case "@@treatment/Get_all_treatments_fail":
			return{
				...state,
				treatments:[]
			}
		case "@@treatment/Get_treatment_with_id":
			return{
				...state,
				treatmentDetail:action.treatment
			}
		case "@@treatment/Update_treatment_name":
			return{
				...state,
				treatmentDetail:{...state.treatmentDetail,
					treatment:{
						...state.treatmentDetail.treatment,
						title:action.name
					}
				}
			}
		case "@@treatment/Update_treatment_detail":
			return{
				...state,
				treatmentDetail:{...state.treatmentDetail,
					treatment:{
						...state.treatmentDetail.treatment,
						detail:action.detail
					}
				}
			}
			case "@@treatment/Update_treatment_score":
				return{
					...state,
					treatmentDetail:{...state.treatmentDetail,
						treatment:{
							...state.treatmentDetail.treatment,
							default_score:action.score
						}
					}
				}
			case "@@treatment/Update_treatment_value":
				return{
					...state,
					treatmentDetail:{...state.treatmentDetail,
						treatment:{
							...state.treatmentDetail.treatment,
							purchase_benefit_condition:action.value
						}
					}
				}
			case "@@treatment/Update_treatment_benefit":
				return{
					...state,
					treatmentDetail:{...state.treatmentDetail,
						treatment:{
							...state.treatmentDetail.treatment,
							purchase_benefit_amount:action.value
						}
					}
				}
			case "@@treatment/Get_type_criteria":
				return{
					...state,
					typeCriteria:action.criteria
				}
			case "@@treatment/Get_place_criteria":
				return{
					...state,
					placeCriteria:action.criteria
				}
			case "@@treatment/Change_type":
				let containsThatType = state.treatmentDetail.service.filter(service=>service.service_id === action.id)
				let newService = state.treatmentDetail.service.slice();
				// console.log(containsThatType)
				if(containsThatType.length > 0){
					newService = state.treatmentDetail.service.filter(service=>service.service_id !== action.id)
					// console.log(newService)
				}else{
					newService.push({service:action.bodyType,service_id:action.id})
				}
				return{
					...state,
					treatmentDetail:{...state.treatmentDetail,
						service:newService
					}
				}
			case "@@treatment/Change_place":
				let containsThatPlace = state.treatmentDetail.branch.filter(place=>place.branch_id === action.id)
				let newPlace = state.treatmentDetail.branch.slice();
				// console.log(containsThatType)
				if(containsThatPlace.length > 0){
					newPlace = state.treatmentDetail.branch.filter(place=>place.branch_id !== action.id)
					// console.log(newService)
				}else{
					newPlace.push({name:action.place,branch_id:action.id})
				}
				return{
					...state,
					treatmentDetail:{...state.treatmentDetail,
						branch:newPlace
					}
				}
			case "@@treatment/Update_treatment":
				return{
					...state,
					update:true
				}
			case "@@treatment/Update_treatment_fail":
				return{
					...state,
					update:false
				}
			case "@@treatment/Change_gender":
				let containsThatGender = state.treatmentDetail.gender.filter(gender=>gender.gender_id === action.id);
				let newGender = state.treatmentDetail.gender.slice();
				// console.log(containsThatGender)
				// console.log(newGender)
				// console.log(action.id)
				if(containsThatGender.length > 0){
					newGender = state.treatmentDetail.gender.filter(gender=>gender.gender_id !== action.id)
					// console.log(newService)
				}else if(action.id === "1"){
					newGender.push({gender:"Male",gender_id:action.id})
				}else if(action.id === "2"){
					newGender.push({gender:"Female",gender_id:action.id})
				}
				return{
					...state,
					treatmentDetail:{...state.treatmentDetail,
						gender:newGender
					}
				}
			case "@@treatment/Reset_update_null":
				return{
					...state,
					update:null
				}
			case "@@treatment/Delete_treatment":
				return{
					...state,
					delete:true
				}
			case "@@treatment/Delete_treatment_fail":
				return{
					...state,
					delete:false
				}
			case "@@treatment/Reset_delete_null":
				return{
					...state,
					delete:null
				}
			case "@@treatment/Update_hidden":
				return{
					...state,
					treatmentDetail:{...state.treatmentDetail,
						treatment:{
							...state.treatmentDetail.treatment,
							is_hidden:action.value
						}
					}
				}
			case "@@treatment/Create_treatment":
				return{
					...state,
					create:true
				}
			case "@@treatment/Create_treatment_fail":
				return{
					...state,
					create:false
				}
			case "@@treatment/Reset_create_null":
				return{
					...state,
					create:null
				}
		default:
			return state
	}
}