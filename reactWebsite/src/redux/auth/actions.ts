import {Dispatch} from "redux";

// Can be named as loginThunk
export function login(username:string,password:string){
	return async(dispatch:Dispatch<IAuthActions>) => {
		const res = await fetch(`${process.env.REACT_APP_BACKEND_ROUTE}/admin/login`,{
			method:"post",
			headers:{
				"Content-Type":"application/json"
			},
			body:JSON.stringify({username,password})
		})
		const result = await res.json();
		if(result.success){
			localStorage.setItem("token",result.data);
			dispatch(loginSuccess(result.id,result.data));
		}else{
			dispatch(loginFailure())
		}
	}
}
export function checkLogin(){
	return async (dispatch:Dispatch<IAuthActions>) => {
		const token = localStorage.getItem("token");
		if(token){
			const res = await fetch(`${process.env.REACT_APP_BACKEND_ROUTE}/users/currentUsersCheck`,{
				headers:{
					Authorization:`Bearer ${token}`
				}
			})
			const result = await res.json()
			if(result.success){
				dispatch(loginSuccess(result.id,token))
			}
		}else{
			dispatch(logoutSuccess())
		}
	}
}

export function logout(){
	return async(dispatch:Dispatch<IAuthActions>) => {
		localStorage.removeItem("token");
		dispatch(logoutSuccess())
	}
}

export function userData(){
	return{
		type:"@@auth/Get_user_data" as const,
	}
}

function loginSuccess(id:number,token:string){
	return{
		type:"@@auth/Login_success" as const,
		id,
		token
	}
}

function loginFailure(){
	return{
		type:"@@auth/Login_fail" as const,
	}
}

export function resetAuthenticatedNull(){
	return{
		type:"@@auth/Reset_authenticated_null" as const,
	}
}

export function resetLoggedInNull(){
	return{
		type:"@@auth/Reset_logged_in_null" as const,
	}
}



function logoutSuccess(){
	return{
		type:"@@auth/Logout" as const,
	}
}

type Failed = "Login_fail";
function failed(type:Failed,message:string){
	return{
		type,
		message
	}
}

export type IAuthActions = ReturnType<typeof userData|typeof loginSuccess|typeof loginFailure|typeof logoutSuccess|
typeof resetLoggedInNull|typeof resetAuthenticatedNull|typeof failed>