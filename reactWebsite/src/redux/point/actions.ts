import {Dispatch} from "redux";
import {AllUserData, AppointmentCommission, PointDetail, ReferralCommission, UserPoint} from "../../models";

export function getAllUsersData(pageDisplay:string,pageNumber:number){
	return async(dispatch:Dispatch<IPointActions>) => {
		const token = localStorage.getItem("token")
		const res = await fetch(`${process.env.REACT_APP_BACKEND_ROUTE}/admin/edit/user?display=${pageDisplay}\
		&page=${pageNumber}`,{
			headers:{
				Authorization:`Bearer ${token}`
			}
		})
		const result = await res.json()
		if(!result.message){
			dispatch(getAllUsersDataSuccess(result))
		}else{
			dispatch(failed("Get_all_users_data_failed",result.message))
		}
	}
}

export function updateCustomerPoint(id:number,points:string,remark:string){
	return async(dispatch:Dispatch<IPointActions>) => {
		const t = new Date()
		let newMonth = ""
		let newDate = ""

		if((t.getMonth() + "").length === 1){
			newMonth = 0 + ((t.getMonth() + 1) + "")
		}else{
			newMonth = (t.getMonth() + 1) + ""
		}
		if((t.getDate() + "").length === 1){
			newDate = 0 + (t.getDate() + "")
		}else{
			newDate = t.getDate() + ""
		}
		const expired = `${t.getFullYear() + 1}-${newMonth}-${newDate} ${(t.getHours())}:${t.getMinutes()}:${t.getSeconds()}`

		const token = localStorage.getItem("token");
		const res = await fetch(`${process.env.REACT_APP_BACKEND_ROUTE}/admin/edit/point/create/earned_point/${id}`,{
			method:"post",
			headers:{
				Authorization:`Bearer ${token}`,
				"Content-type":"application/json"
			},
			body:JSON.stringify({expired_date:expired,remark,points:parseInt(points),is_deleted:false})
		})
		const result = await res.json()
		if(result.success){
			dispatch(createCustomerPointSuccess(id))
		}else{
			dispatch(createCustomerPointFailure(id))
			dispatch(failed("Create_customer_point_fail",result.message))
		}
	}
}

export function updateAgentReferralPoint(id:number,points:string,remark:string){
	return async(dispatch:Dispatch<IPointActions>) => {
		const token = localStorage.getItem("token");
		const res = await fetch(`${process.env.REACT_APP_BACKEND_ROUTE}/admin/edit/point/create/referral_commission/${id}`,{
			method:"post",
			headers:{
				Authorization:`Bearer ${token}`,
				"Content-type":"application/json"
			},
			body:JSON.stringify({remark,points:parseInt(points),is_deleted:false})
		})
		const result = await res.json()
		if(result.success){
			dispatch(createAgentReferralPointSuccess(id))
		}else{
			dispatch(createAgentReferralPointFailure(id))
			dispatch(failed("Create_agent_referral_point_fail",result.message))
		}
	}
}

export function updateAgentAppointmentPoint(id:number,points:string,remark:string){
	return async(dispatch:Dispatch<IPointActions>) => {
		const token = localStorage.getItem("token");
		const res = await fetch(`${process.env.REACT_APP_BACKEND_ROUTE}/admin/edit/point/create/appointment_commission/${id}`,{
			method:"post",
			headers:{
				Authorization:`Bearer ${token}`,
				"Content-type":"application/json"
			},
			body:JSON.stringify({remark,points:parseInt(points),is_deleted:false})
		})
		const result = await res.json()
		if(result.success){
			dispatch(createAgentAppointmentPointSuccess(id))
		}else{
			dispatch(createAgentAppointmentPointFailure(id))
			dispatch(failed("Create_agent_appointment_point_fail",result.message))
		}
	}
}

export function getUserPointsDetail(userId:number){
	return async(dispatch:Dispatch<IPointActions>) => {
		const token = localStorage.getItem("token");
		const res = await fetch(`${process.env.REACT_APP_BACKEND_ROUTE}/admin/edit/point/${userId}`,{
			headers:{
				Authorization:`Bearer ${token}`,
			}
		})
		const result = await res.json()
		if(!result.message){
			dispatch(getUserPointsDetailSuccess(result))
		}else{
			// dispatch(createAgentAppointmentPointFailure(id))
			dispatch(failed("Get_user_points_detail_fail",result.message))
		}
	}
}

export function modifyCustomerPoint(pointId:string,point:UserPoint){
	return async(dispatch:Dispatch<IPointActions>) => {
		const token = localStorage.getItem("token");
		const res = await fetch(`${process.env.REACT_APP_BACKEND_ROUTE}/admin/edit/point/update/earned_point/${pointId}`,{
			method:"put",
			headers:{
				Authorization:`Bearer ${token}`,
				"Content-type":"application/json"
			},
			body:JSON.stringify({expired_date:point.expired_date,remark:point.remark,points:point.points,is_deleted:false})
		})
		const result = await res.json()
		if(result.success){
			dispatch(modifyCustomerPointSuccess())
		}else{
			dispatch(modifyCustomerPointFailure())
			dispatch(failed("Modify_customer_point_fail",result.message))
		}
	}
}

export function deleteCustomerPoint(pointId:string,point:UserPoint){
	return async(dispatch:Dispatch<IPointActions>) => {
		const token = localStorage.getItem("token");
		const res = await fetch(`${process.env.REACT_APP_BACKEND_ROUTE}/admin/edit/point/update/earned_point/${pointId}`,{
			method:"put", // method delete
			headers:{
				Authorization:`Bearer ${token}`,
				"Content-type":"application/json"
			},
			body:JSON.stringify({expired_date:point.expired_date,remark:point.remark,points:point.points,is_deleted:true})
		})
		const result = await res.json()
		if(result.success){
			dispatch(deleteCustomerPointSuccess(pointId))
		}else{
			dispatch(deleteCustomerPointFailure())
			dispatch(failed("Delete_customer_point_fail",result.message))
		}
	}
}

export function modifyReferralPoint(pointId:string,point:ReferralCommission){
	return async(dispatch:Dispatch<IPointActions>) => {
		const token = localStorage.getItem("token");
		const res = await fetch(`${process.env.REACT_APP_BACKEND_ROUTE}/admin/edit/point/update/referral_commission/${pointId}`,{
			method:"put",
			headers:{
				Authorization:`Bearer ${token}`,
				"Content-type":"application/json"
			},
			body:JSON.stringify({remark:point.remark,points:point.points,is_deleted:false})
		})
		const result = await res.json()
		if(result.success){
			// dispatch(modifyCustomerPointSuccess())
		}else{
			// dispatch(modifyCustomerPointFailure())
			dispatch(failed("Modify_Referral_point_fail",result.message))
		}
	}
}

export function deleteReferralPoint(pointId:string,point:ReferralCommission){
	return async(dispatch:Dispatch<IPointActions>) => {
		const token = localStorage.getItem("token");
		const res = await fetch(`${process.env.REACT_APP_BACKEND_ROUTE}/admin/edit/point/update/referral_commission/${pointId}`,{
			method:"put",
			headers:{
				Authorization:`Bearer ${token}`,
				"Content-type":"application/json"
			},
			body:JSON.stringify({remark:point.remark,points:point.points,is_deleted:true})
		})
		const result = await res.json()
		if(result.success){
			dispatch(deleteReferralPointSuccess(pointId))
		}else{
			// dispatch(deleteCustomerPointFailure())
			dispatch(failed("Delete_Referral_point_fail",result.message))
		}
	}
}

export function modifyAppointmentPoint(pointId:string,point:AppointmentCommission){
	return async(dispatch:Dispatch<IPointActions>) => {
		const token = localStorage.getItem("token");
		const res = await fetch(`${process.env.REACT_APP_BACKEND_ROUTE}/admin/edit/point/update/appointment_commission/${pointId}`,{
			method:"put",
			headers:{
				Authorization:`Bearer ${token}`,
				"Content-type":"application/json"
			},
			body:JSON.stringify({remark:point.remark,points:point.points,is_deleted:false})
		})
		const result = await res.json()
		if(result.success){
			// dispatch(modifyCustomerPointSuccess())
		}else{
			// dispatch(modifyCustomerPointFailure())
			dispatch(failed("Modify_Appointment_point_fail",result.message))
		}
	}
}

export function deleteAppointmentPoint(pointId:string,point:AppointmentCommission){
	return async(dispatch:Dispatch<IPointActions>) => {
		const token = localStorage.getItem("token");
		const res = await fetch(`${process.env.REACT_APP_BACKEND_ROUTE}/admin/edit/point/update/appointment_commission/${pointId}`,{
			method:"put",
			headers:{
				Authorization:`Bearer ${token}`,
				"Content-type":"application/json"
			},
			body:JSON.stringify({remark:point.remark,points:point.points,is_deleted:true})
		})
		const result = await res.json()
		if(result.success){
			dispatch(deleteAppointmentPointSuccess(pointId))
		}else{
			// dispatch(deleteCustomerPointFailure())
			dispatch(failed("Delete_Appointment_point_fail",result.message))
		}
	}
}

function getAllUsersDataSuccess(users:AllUserData[]){
	return{
		type:"@@point/Get_all_users_data" as const,
		users
	}
}

function createCustomerPointSuccess(id:number){
	return{
		type:"@@point/Create_customer_point" as const,
		id
	}
}

function createCustomerPointFailure(id:number){
	return{
		type:"@@point/Create_customer_point_fail" as const,
		id
	}
}

export function resetCreateToNull(){
	return{
		type:"@@point/Reset_create_to_null" as const,
	}
}

function createAgentReferralPointSuccess(id:number){
	return{
		type:"@@point/Create_agent_referral_point" as const,
		id
	}
}

function createAgentReferralPointFailure(id:number){
	return{
		type:"@@point/Create_agent_referral_point_fail" as const,
		id
	}
}

function createAgentAppointmentPointSuccess(id:number){
	return{
		type:"@@point/Create_agent_appointment_point" as const,
		id
	}
}

function createAgentAppointmentPointFailure(id:number){
	return{
		type:"@@point/Create_agent_appointment_point_fail" as const,
		id
	}
}

function getUserPointsDetailSuccess(detail:PointDetail){
	return{
		type:"@@point/Get_user_point_detail" as const,
		detail
	}
}

export function updatePointValue(pointId:string,value:string){
	return{
		type:"@@point/Update_point_value" as const,
		pointId,
		value
	}
}

function modifyCustomerPointSuccess(){
	return{
		type:"@@point/Modify_customer_point" as const,
	}
}

function modifyCustomerPointFailure(){
	return{
		type:"@@point/Modify_customer_point_fail" as const,
	}
}

function deleteCustomerPointSuccess(id:string){
	return{
		type:"@@point/Delete_customer_point" as const,
		id
	}
}

function deleteCustomerPointFailure(){
	return{
		type:"@@point/Delete_customer_point_fail" as const,
	}
}

export function updateReferralPointValue(referralPointId:string,value:string){
	return{
		type:"@@point/Update_referral_point_value" as const,
		referralPointId,
		value
	}
}

function deleteReferralPointSuccess(id:string){
	return{
		type:"@@point/Delete_referral_point" as const,
		id
	}
}

export function updateAppointmentPointValue(appointmentPointId:string,value:string){
	return{
		type:"@@point/Update_appointment_point_value" as const,
		appointmentPointId,
		value
	}
}

function deleteAppointmentPointSuccess(id:string){
	return{
		type:"@@point/Delete_appointment_point" as const,
		id
	}
}

export function updateCustomerRemark(pointId:string,remark:string){
	return{
		type:"@@point/Update_customer_remark" as const,
		pointId,
		remark
	}
}

export function updateReferralRemark(pointId:string,remark:string){
	return{
		type:"@@point/Update_referral_remark" as const,
		pointId,
		remark
	}
}

export function updateAppointmentRemark(pointId:string,remark:string){
	return{
		type:"@@point/Update_appointment_remark" as const,
		pointId,
		remark
	}
}


// Can replace with one generic failed
type Failed = "Get_all_users_data_failed"|"Create_customer_point_fail"|"Modify_customer_point_fail"|
"Create_agent_referral_point_fail"|"Create_agent_appointment_point_fail"|"Get_user_points_detail_fail"|
"Modify_customer_point_fail"|"Delete_customer_point_fail"|"Modify_Referral_point_fail"|"Delete_Referral_point_fail"|
"Modify_Appointment_point_fail"|"Delete_Appointment_point_fail"
function failed(type:Failed,message:string){
	return{
		type,
		message
	}
}

export type IPointActions = ReturnType<typeof getAllUsersDataSuccess|typeof createCustomerPointSuccess|
typeof createAgentReferralPointSuccess|typeof createCustomerPointFailure|typeof resetCreateToNull|
typeof createAgentReferralPointFailure|typeof createAgentAppointmentPointSuccess|typeof getUserPointsDetailSuccess|
typeof createAgentAppointmentPointFailure|typeof updatePointValue|typeof modifyCustomerPointSuccess|
typeof modifyCustomerPointFailure|typeof deleteCustomerPointSuccess|typeof deleteCustomerPointFailure|
typeof updateReferralPointValue|typeof deleteReferralPointSuccess|typeof deleteAppointmentPointSuccess|
typeof updateCustomerRemark|typeof updateReferralRemark|typeof updateAppointmentRemark|
typeof updateAppointmentPointValue|typeof failed>