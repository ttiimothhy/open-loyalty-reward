import {AllUserData,PointDetail} from "../../models";
import {IPointActions} from "./actions";

export interface IPointState{
	users:AllUserData[];
	customerCreate:Array<boolean|null>;
	agentReferralCreate:Array<boolean|null>;
	agentAppointmentCreate:Array<boolean|null>;
	userPoint:PointDetail;
}

const initialState = {
	users:[],
	customerCreate:[],
	agentReferralCreate:[],
	agentAppointmentCreate:[],
	userPoint:{
		appointment_commission_agent:[],
		earned_point_user:[],
		referral_commission_agent:[],
	}
}

export const pointReducer = (state:IPointState = initialState,action:IPointActions):IPointState => {
	switch(action.type){
		case "@@point/Get_all_users_data":
			return{
				...state,
				users:action.users
			}
		case "@@point/Create_customer_point":
			{
				let newCustomerCreate = state.customerCreate.slice()
				newCustomerCreate[action.id] = true
				return{
					...state,
					customerCreate:newCustomerCreate
				}
			}
		case "@@point/Create_customer_point_fail":
			{
				let newCustomerCreate = state.customerCreate.slice()
				newCustomerCreate[action.id] = false
				return{
					...state,
					customerCreate:newCustomerCreate
				}
			}
		case "@@point/Reset_create_to_null":
			return{
				...state,
				customerCreate:[],
				agentReferralCreate:[],
				agentAppointmentCreate:[]
			}
		case "@@point/Create_agent_referral_point":
			{
				let newAgentReferralCreate = state.agentReferralCreate.slice()
				newAgentReferralCreate[action.id] = true
				return{
					...state,
					agentReferralCreate:newAgentReferralCreate
				}
			}
		case "@@point/Create_agent_referral_point_fail":
			{
				let newAgentReferralCreate = state.agentReferralCreate.slice()
				newAgentReferralCreate[action.id] = false
				return{
					...state,
					agentReferralCreate:newAgentReferralCreate
				}
			}
		case "@@point/Create_agent_appointment_point":
			{
				let newAgentAppointmentCreate = state.agentAppointmentCreate.slice()
				newAgentAppointmentCreate[action.id] = true
				return{
					...state,
					agentAppointmentCreate:newAgentAppointmentCreate
				}
			}
		case "@@point/Create_agent_appointment_point_fail":
			{
				let newAgentAppointmentCreate = state.agentAppointmentCreate.slice()
				newAgentAppointmentCreate[action.id] = false
				return{
					...state,
					agentAppointmentCreate:newAgentAppointmentCreate
				}
			}
		case "@@point/Get_user_point_detail":
			return{
				...state,
				userPoint:action.detail
			}
		case "@@point/Update_point_value":
			{
				let newUserPoint = state.userPoint.earned_point_user.slice();
				(newUserPoint.filter(point=>point.id === action.pointId))[0].points = action.value
				// newUserPoint = newUserPoint.filter(user=>user.id !== action.userId);
				// newUserPoint.push(user[0])
				return{
					...state,
					userPoint:{
						...state.userPoint,
						earned_point_user:newUserPoint
					}
				}
			}
		case "@@point/Delete_customer_point":
			return{
				...state,
				userPoint:{
					...state.userPoint,
					earned_point_user:state.userPoint.earned_point_user.filter(point=>point.id !== action.id)
				}
			}
		case "@@point/Update_referral_point_value":
			{
				let newReferralPoint = state.userPoint.referral_commission_agent.slice();
				(newReferralPoint.filter(point=>point.id === action.referralPointId))[0].points = action.value
				return{
					...state,
					userPoint:{
						...state.userPoint,
						referral_commission_agent:newReferralPoint
					}
				}
			}
		case "@@point/Delete_referral_point":
			return{
				...state,
				userPoint:{
					...state.userPoint,
					referral_commission_agent:state.userPoint.referral_commission_agent.filter(point=>point.id !== action.id)
				}
			}
		case "@@point/Delete_appointment_point":
			return{
				...state,
				userPoint:{
					...state.userPoint,
					appointment_commission_agent:state.userPoint.appointment_commission_agent.filter(point=>point.id !== action.id)
				}
			}
		case "@@point/Update_appointment_point_value":
			{
				let newAppointmentPoint = state.userPoint.appointment_commission_agent.slice();
				(newAppointmentPoint.filter(point=>point.id === action.appointmentPointId))[0].points = action.value
				return{
					...state,
					userPoint:{
						...state.userPoint,
						appointment_commission_agent:newAppointmentPoint
					}
				}
			}
		case "@@point/Update_customer_remark":
			{
				let newUserPoint = state.userPoint.earned_point_user.slice();
				(newUserPoint.filter(point=>point.id === action.pointId))[0].remark = action.remark
				return{
					...state,
					userPoint:{
						...state.userPoint,
						earned_point_user:newUserPoint
					}
				}
			}
		case "@@point/Update_referral_remark":
			{
				let newReferralPoint = state.userPoint.referral_commission_agent.slice();
				(newReferralPoint.filter(point=>point.id === action.pointId))[0].remark = action.remark
				return{
					...state,
					userPoint:{
						...state.userPoint,
						referral_commission_agent:newReferralPoint
					}
				}
			}
		case "@@point/Update_appointment_remark":
			{
				let newAppointmentPoint = state.userPoint.appointment_commission_agent.slice();
				(newAppointmentPoint.filter(point=>point.id === action.pointId))[0].remark = action.remark
				return{
					...state,
					userPoint:{
						...state.userPoint,
						appointment_commission_agent:newAppointmentPoint
					}
				}
			}
		default:
			return state
	}
}