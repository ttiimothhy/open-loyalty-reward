## loyalty reward

- [ ] open javaBackend

- windows

```Bash
.\gradlew bootrun
```

- mac

```Bash
./gradlew bootrun
```

- [ ] open LoyaltyReward

```Bash
yarn
```

```Bash
cd ios
pod install
cd ..
```

```Bash
yarn react-native start
```

```Bash
yarn react-native run-ios
```

- [ ] open reactWebsite

```Bash
yarn
```

```Bash
yarn start
```